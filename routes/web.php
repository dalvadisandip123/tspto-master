<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'Auth\LoginController@showLoginForm')->name('site.home');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/dashboard', 'HomeController@index')->name('dashboard');


Route::prefix('ajax')->group(function () {
    Route::match(array('GET', 'POST'), '/get-vehicle-types', 'AjaxController@getVehicletypes')->name('ajax.master.mt.getVehicletypes');
    Route::match(array('GET', 'POST'), '/get-vehicle-variant', 'AjaxController@getVehicleVariant')->name('ajax.master.mt.get_vehicle_variant');
    Route::match(array('GET', 'POST'), '/get-officer', 'AjaxController@getOfficer')->name('ajax.master.mt.get_officer');
    Route::match(array('GET', 'POST'), '/get-emp', 'AjaxController@getEmp')->name('ajax.master.mt.get_emp');
    Route::match(array('GET', 'POST'), '/get-vehicle-variant-data', 'AjaxController@getVehicleVariantData')->name('ajax.master.mt.get_vehicle_variant_data');
    Route::match(array('GET', 'POST'), '/get-vehicle-batery', 'AjaxController@getVehicleBatary')->name('ajax.mt.get_vehicle_batary');
    Route::match(array('GET', 'POST'), '/get-vehicle-tyre', 'AjaxController@getVehicleTyre')->name('ajax.mt.get_vehicle_tyers');

    Route::match(array('GET', 'POST'), '/get-vehicle-by-unit', 'AjaxController@getVehicleByUnit')->name('ajax.get_vehicle_by_unit');

});
Route::prefix('master')->group(function () {
    Route::prefix('mt')->group(function () {
        Route::match(array('GET', 'POST'), '/units', 'Master\MtController@UnitsMaster')->name('site.master.mt.units');
        Route::match(array('GET', 'POST'), '/vehicle-make', 'Master\MtController@vehicleMake')->name('site.master.mt.vehicleMake');
        Route::match(array('GET', 'POST'), '/vehicle-type', 'Master\MtController@vehicleType')->name('site.master.mt.vehicleType');
        Route::match(array('GET', 'POST'), '/vehicle-variant', 'Master\MtController@vehicleVariant')->name('site.master.mt.vehicleVariant');
        Route::match(array('GET', 'POST'), '/batteries', 'Master\MtController@batteries')->name('site.master.mt.batteries');
        Route::match(array('GET', 'POST'), '/vehicle-tyres', 'Master\MtController@vehicleTyres')->name('site.master.mt.vehicleTyres');
        Route::match(array('GET', 'POST'), '/vehicle-category', 'Master\MtController@vehicleCategory')->name('site.master.mt.vehicleCategory');
        Route::match(array('GET', 'POST'), '/vehicle-group', 'Master\MtController@vehicleGroup')->name('site.master.mt.vehicleGroup');


        Route::match(array('GET', 'POST'), '/unin-head', 'Master\MtController@uninHead')->name('site.master.mt.uninHead');
        Route::match(array('GET', 'POST'), '/description-Vehicle', 'Master\MtController@DescriptionByVehicle')->name('site.master.mt.description_vehicle_type');
        Route::match(array('GET', 'POST'), '/vehicle-usage', 'Master\MtController@VehicleUsage')->name('site.master.mt.vehicle_usage');
        Route::match(array('GET', 'POST'), '/vehicle-body-type', 'Master\MtController@VehicleBodyType')->name('site.master.mt.vehicle_body_type');
        Route::match(array('GET', 'POST'), '/condemnation', 'Master\MtController@Condemnation')->name('site.master.mt.condemnation');
        Route::match(array('GET', 'POST'), '/employee-role', 'Master\MtController@EmployeeRole')->name('site.master.mt.employee_role');
        Route::match(array('GET', 'POST'), '/employee-rank', 'Master\MtController@EmployeeRank')->name('site.master.mt.employee_rank');
        Route::match(array('GET', 'POST'), '/office-dept', 'Master\MtController@OfficeDept')->name('site.master.mt.office_det');

        Route::match(array('GET', 'POST'), '/office-designation', 'Master\MtController@OfficeDesignation')->name('site.master.mt.office_designation');


        Route::match(array('GET', 'POST'), '/officer_name', 'Master\MtController@OfficerName')->name('site.master.mt.officer_name');

        Route::match(array('GET', 'POST'), '/batteries-tyres-by-variant', 'Master\MtController@BatteriesTyresByVariant')->name('site.master.mt.batteries_tyres_by_variant');

        Route::match(array('GET', 'POST'), '/vehicle-unit-historys/{action?}/{id?}', 'MtController@vehicleUnitHistory')->name('site.mt.vehicle_unit_history');
        Route::match(array('GET', 'POST'), '/vehicle-officer-historys/{action?}/{id?}', 'MtController@vehicleOfficerHistory')->name('site.mt.vehicle_officer_history');
        Route::match(array('GET', 'POST'), '/vehicle-officer-drivers/{officerid?}/{action?}/{id?}', 'MtController@vehicleOfficerDrivers')->name('site.mt.vehicle_officer_drivers');


        Route::prefix('pol')->group(function () {
            Route::match(array('GET', 'POST'), '/oil-type', 'Master\PolController@oilType')->name('site.master.pol.oil_type');
            Route::match(array('GET', 'POST'), '/filters', 'Master\PolController@filters')->name('site.master.pol.filters');
            Route::match(array('GET', 'POST'), '/oil_grade', 'Master\PolController@oilGrade')->name('site.master.pol.oil_grade');
            Route::match(array('GET', 'POST'), '/account-numbers', 'Master\PolController@accountNumbers')->name('site.master.pol.account_numbers');
            Route::match(array('GET', 'POST'), '/outlets', 'Master\PolController@outlets')->name('site.master.pol.outlets');
            Route::match(array('GET', 'POST'), '/fuel-type', 'Master\PolController@fuelType')->name('site.master.pol.fuel_type');
            Route::match(array('GET', 'POST'), '/license-certificates', 'Master\PolController@licenseCertificates')->name('site.master.pol.license_certificates');

            Route::match(array('GET', 'POST'), '/outlet-allocation', 'Master\PolController@outletAllocation')->name('site.master.pol.outlet_allocation');

            Route::match(array('GET', 'POST'), '/service-schedule/{action?}/{id?}', 'Master\PolController@serviceSchedule')->name('site.master.pol.service_schedule');


            Route::match(array('GET', 'POST'), '/fuel-pumps/{action?}/{id?}', 'Master\PolController@fuelPumps')->name('site.master.pol.fuel_pumps');
            Route::match(array('GET', 'POST'), '/fuel-ground-tank/{action?}/{id?}', 'Master\PolController@fuelGroundTank')->name('site.master.pol.fuel_ground_tank');
            Route::match(array('GET', 'POST'), '/generator-spl-machines/{action?}/{id?}', 'Master\PolController@generatorSplMachines')->name('site.master.pol.generator_spl_machines');

            Route::match(array('GET', 'POST'), '/hired-vehicles/{action?}/{id?}', 'Master\PolController@hiredVehicles')->name('site.master.pol.hired_vehicles');

        });

        Route::prefix('workshop')->group(function () {

            Route::match(array('GET', 'POST'), '/job-card-repairs', 'Master\WorkShopController@jobCardRepairs')->name('site.master.workshop.job_card_repairs');
            Route::match(array('GET', 'POST'), '/past-vehicle-expense', 'Master\WorkShopController@PastVehicleExpense')->name('site.master.workshop.past_vehicle_expense');
            Route::match(array('GET', 'POST'), '/full-it-repairs', 'Master\WorkShopController@FullITRepairs')->name('site.master.workshop.full_it_repairs');
            Route::match(array('GET', 'POST'), '/mechanics-names', 'Master\WorkShopController@MechanicsNames')->name('site.master.workshop.mechanics_names');
            Route::match(array('GET', 'POST'), '/section-names', 'Master\WorkShopController@SectionNames')->name('site.master.workshop.section_names');

            Route::match(array('GET', 'POST'), '/spares-parts-entry', 'Master\WorkShopController@sparesPartsEntry')->name('site.master.workshop.spares_parts_entry');
            Route::match(array('GET', 'POST'), '/part-category', 'Master\WorkShopController@partCategory')->name('site.master.workshop.part_category');
            Route::match(array('GET', 'POST'), '/part-name', 'Master\WorkShopController@partName')->name('site.master.workshop.part_name');
            Route::match(array('GET', 'POST'), '/firm-name', 'Master\WorkShopController@firmName')->name('site.master.workshop.firm_name');

        });
    });
});


Route::prefix('mt')->group(function () {

    Route::match(array('GET', 'POST'), '/vehicles/info/{action?}/{id?}', 'MTController@vehicle')->name('site.mt.vehicle');
//    Route::match(array('GET', 'POST'), '/e3-section', 'MTController@e3Section')->name('site.mt.e3_section');
    Route::match(array('GET', 'POST'), '/employee/info/{action?}/{id?}', 'MTController@employee')->name('site.mt.employee');
    Route::match(array('GET', 'POST'), '/employee/employee_attendance/{action?}/{id?}', 'MTController@employeeAttendance')->name('site.mt.employee_attendance');
    Route::match(array('GET', 'POST'), '/employee/deputation-ptos', 'MTController@empDeputationPtos')->name('site.mt.emp_deputation_ptos');
    Route::match(array('GET', 'POST'), '/employee/reporting', 'MTController@empReporting')->name('site.mt.emp_reporting');
    Route::match(array('GET', 'POST'), '/employee/halidy-duties', 'MTController@empHalidyDuties')->name('site.mt.emp_halidy_duties');

    Route::match(array('GET', 'POST'), '/vdra-daily-diary/{action?}/{id?}', 'MTController@vdraDailyDiary')->name('site.mt.vdra_daily_diary');

    Route::match(array('GET', 'POST'), '/issue-voucher/{action?}/{id?}', 'MTController@issueVoucher')->name('site.mt.issue_voucher');

    Route::match(array('GET', 'POST'), '/mt-update-form/{vehicles_id?}/{action?}/{id?}', 'MTController@MTUpdateForm')->name('site.mt.mt_update_form');

});


Route::prefix('pol')->group(function () {

    Route::match(array('GET', 'POST'), '/fuel-price/{action?}/{id?}', 'POLController@fuelPrice')->name('site.pol.fuel_price');
    Route::match(array('GET', 'POST'), '/lubes-price/{action?}/{id?}', 'POLController@lubesPrice')->name('site.pol.lubes_price');
    Route::match(array('GET', 'POST'), '/purchase-register/{action?}/{id?}', 'POLController@purchaseRegister')->name('site.pol.purchase_register');
    Route::match(array('GET', 'POST'), '/fuel-lubricant-balances-register', 'POLController@fuelLubricantBalancesRegister')->name('site.pol.fuel_lubricant_balances_register');

    Route::match(array('GET', 'POST'), '/out-station-filling/{action?}/{id?}', 'POLController@OutStationFilling')->name('site.pol.out_station_filling');
    Route::match(array('GET', 'POST'), '/certificate-license-schedule', 'POLController@CertificateLicenseSchedule')->name('site.pol.certificate_license_schedule');
    Route::match(array('GET', 'POST'), '/budget-register/{action?}/{id?}', 'POLController@budgetRegister')->name('site.pol.budget_register');


    Route::match(array('GET', 'POST'), '/fuel_quota_allotment/{type}/{action?}/{id?}', 'POLController@fuelQuotaAllotment')->name('site.pol.fuel_quota_allotments');

    Route::match(array('GET', 'POST'), '/fuel-issue/{type?}/{action?}/{id?}', 'POLController@fuelIssue')->name('site.pol.fuel_issue');


});

Route::match(array('GET', 'POST'), '/users', 'UserController@users')->name('site.users');

Route::group(['prefix' => 'workshop'], function () {

    Route::group(['namespace' => 'Workshop'], function () {
        Route::get('/job_card/add', 'JobcardController@create')->name('job_card.create');
        
        Route::get('/workshop/ptostockledger/deleteSpareParts', 'PTOStockLedgerController@deleteSpareParts')->name('workshop.pto_stock_ledger.deleteSpareParts');
        Route::get('/workshop/ptostockledger/loadModelMaster', 'PTOStockLedgerController@loadModelMaster')->name('workshop.pto_stock_ledger.loadModelMaster');
        
        $paths = array(
                'pto_stock_ledger'       => 'PTOStockLedgerController',
                'pto_issue_voucher'       => 'PTOIssueVoucherController',
        );    
        foreach($paths as $slug => $controller){
            Route::get('/'.$slug.'/index', $controller.'@index')->name('workshop.'.$slug.'.index');
            Route::post('/'.$slug.'/list', $controller.'@list')->name('workshop.'.$slug.'.list');
            Route::delete('/'.$slug.'/delete/{id}', $controller.'@destroy')->name('workshop.'.$slug.'.delete');
            Route::get('/'.$slug.'/form/{id}', $controller.'@form')->name('workshop.'.$slug.'.form');
            Route::post('/'.$slug.'/store/', $controller.'@store')->name('workshop.'.$slug.'.save');
        }

    });
});

