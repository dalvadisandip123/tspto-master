@extends('layout')
@section('title', 'PTO Issue voucher Unit Wise')

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Work Shop</a>
                                            <a href="#!" class="breadcrumb">
                                                PTO Issue Voucher Unit Wise
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    

                                    <div class="col s12 m12 l12 ">
                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('workshop.pto_issue_voucher.form',['id'=>0]) }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Add
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        {{ form_flash_message('flash_message') }}
                                                    </div>

                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <div class="panel-body table-responsive programslistcls">
                                                            <div id="tabarticleid_wrapper" class="dataTables_wrapper no-footer">
                                                         
                                                                <table id="example" class="display nowrap" style="width:100%" role="grid" aria-describedby="tabarticleid_info" style="width: 100%px;">
                                                                    <thead>
                                                                        <tr role="row">
                                                                            
                                                                            <th class="sorting" tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 100px;" aria-label="Id:activate to sort column ascending">Id</th>
                                                                            <th class="sorting" tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 220px;" aria-label="company_name:activate to sort column ascending">I.V. No</th>
                                                                            <th class="sorting" tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 220px;" aria-label="company_name:activate to sort column ascending">Unit Name</th>
                                                                            <th class="sorting" tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 220px;" aria-label="company_name:activate to sort column ascending">Issue Date</th>
                                                                            <th class="sorting" tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 220px;" aria-label="company_name:activate to sort column ascending">RC No</th>
                                                                            <th class="sorting" tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 220px;" aria-label="company_name:activate to sort column ascending">RC Date</th>
                                                                            <th class="sorting" tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 220px;" aria-label="company_name:activate to sort column ascending">Scan Copy of RC</th>
                                                                            <th  tabindex="0" aria-controls="tabarticleid" rowspan="1" colspan="1" style="width: 200px;" aria-label="Action:activate to sort column ascending">Action</th>
                                                                        </tr>
                                                                    </thead>                
                                                                </table> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function()
        {
            $('#example').DataTable({
                  
               "processing": true,
               "serverSide": true,
               "rowId": 'Id',
               "ajax":{
                    "url": "{{ route('workshop.pto_issue_voucher.list') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"},
                },
       
                "columns": [
                    { "data": "id"},
                    { "data": "iv_district"},
                    { "data": "unit_name"},
                    { "data": "issue_date"},
                    { "data": "rc_no"},
                    { "data": "rc_date"},
                    { "data": "image"},
                    { "data": "action","orderable":false,"bSortable": true },                
                ]  
            });
        });

    </script>
@endsection
