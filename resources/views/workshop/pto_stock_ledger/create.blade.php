
@extends('layout')
@section('title', 'Add PTO Stock Ledger')

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                PTO STOCK LEDGER 
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    
                                    <div class="col s12 m12 l12 ">

                                        <div class="row">
                                            <div class="col s6 show-btn align-right mt-1">
                                                <span class="card-title">Add {{ ucwords('Pto stock ledger') }}</span>


                                            </div>
                                            <div class="col s6 show-btn align-right mt-1">
                                                <a href="{{ route('workshop.pto_stock_ledger.index') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>

                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">


                                                <form role="form" class="form-horizontal manageForm" action="{{route('workshop.pto_stock_ledger.save')}}"
                                                      method="post" id="add_0"
                                                      enctype="multipart/form-data">


                                                    @csrf

                                                    <input type="hidden" name="id" value="{{$PTOStockLedger->id ? $PTOStockLedger->id : 0}}"/>

                                                    <div class="row">

                                                        <div class="col m12">

                                                            {{-- <div class="card"> --}}

                                                                {{-- <div class="card-content"> --}}
                                                                    <h4 class="card-title"> vehicles Info </h4>

                                                                    <div class="row">


                                                                        <div class="col m3">

                                                                            <div class="input-field col m12">
                                                                                <p>Select Category</p>
                                                                                 <p>
                                                                                     <label>
                                                                                        <input name="category" type="radio" value="1" @if($PTOStockLedger->category == '1') checked @endif/>
                                                                                        <span>General Ledger</span>
                                                                                      </label>
                                                                                 </p>
                                                                                 <p>
                                                                                      <label>
                                                                                     
                                                                                    <input name="category" type="radio" value="2" @if($PTOStockLedger->category == '2') checked @endif/>
                                                                                    <span>Reserve Ledger</span>
                                                                                  </label>
                                                                                 </p>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <p>D1. No</p>
                                                                                <input type="text" name="d1_no" placeholder="D1.No" value="{{$PTOStockLedger->d1_no ? $PTOStockLedger->d1_no : ''}}">

                                                                                {{-- <label>D1. No</label> --}}
                                                                            </div>

                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <p>Date</p>
                                                                                <input type="date" name="stock_date" value="{{$PTOStockLedger->stock_date ? $PTOStockLedger->stock_date : ''}}">

                                                                                {{-- <label>Date</label> --}}
                                                                            </div>

                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div class="input-field col m12">
                                                                                <p>Select Type</p>
                                                                                <p>
                                                                                     <label>
                                                                                        <input name="type" type="radio" value="1" @if($PTOStockLedger->type == '1') checked @endif/>
                                                                                        <span>Date</span>
                                                                                      </label>
                                                                                 </p>
                                                                                 <p>
                                                                                  <label>
                                                                                     
                                                                                    <input name="type" type="radio" value="2" @if($PTOStockLedger->type == '2') checked @endif/>
                                                                                    <span>R.V.No</span>
                                                                                  </label>
                                                                                 </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        {{-- <hr>    --}}
                                                                        <div class="col m3">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <p> Firm Name</p>
                                                                                <select class="select2" 
                                                                                    name="firm_name"
                                                                                    class="vehicle_category"
                                                                                >
                                                                                    <option value=""> Select</option>

                                                                                    @if ($firms)
                                                                                        @foreach($firms AS $firm)
                                                                                            <option value="{{ $firm->id }}" @if($firm->id == $PTOStockLedger->firm_name) selected @endif>{{  $firm->name }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                {{-- <label> Firm Name</label> --}}
                                                                            </div>


                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <p> P O No</p>
                                                                                <input type="text" name="po_name" class="form-control"  value="{{$PTOStockLedger->po_no ? $PTOStockLedger->po_no : ''}}">
                                                                                {{-- <label> P O No</label> --}}
                                                                            </div>


                                                                        </div>
                                                                        <div class="col m2">

                                                                            
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <p>PO. Date</p>
                                                                                <input type="date" name="ponamedate" class="form-control" value="{{$PTOStockLedger->po_date ? $PTOStockLedger->po_date : ''}}">
                                                                                {{-- <label> Invoice Date</label> --}}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m2">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <p>Invoice No</p>
                                                                                <input type="text" name="invoice_no" class="form-control" value="{{$PTOStockLedger->invoice_no ? $PTOStockLedger->invoice_no : ''}}">
                                                                                {{-- <label> Invoice No</label> --}}
                                                                            </div>


                                                                        </div>
                                                                        <div class="col m2">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <p>Invoice Date</p>
                                                                                <input type="date" name="invoice_date" class="form-control" value="{{$PTOStockLedger->invoice_date ? $PTOStockLedger->invoice_date : ''}}">
                                                                                {{-- <label> Invoice Date</label> --}}
                                                                            </div>


                                                                        </div>



                                                                    </div><br><br>
                                                                    {{-- <hr> --}}
                                                                    <div class="row">
                                                                        
                                                                        <h6>For Spare Sparts of Respective Vehicle:</h6>
                                                                    </div><br>
                                                                    <div class="row">
                                                                        <div class="col m1">Make</div>
                                                                        <div class="col m1">Model</div>
                                                                        <div class="col m2">Part Category</div>
                                                                        <div class="col m2">Part Name</div>
                                                                        <div class="col m1">Quantity</div>
                                                                        <div class="col m1">Cost</div>
                                                                        <div class="col m2">Rack Location</div>
                                                                        <div class="col m2">Add/Remove</div>
                                                                    </div>
                                                                    <div id="spare_all_id">
                                                                        @if(count($spareParts))
                                                                            @foreach($spareParts as $key=>$sparePart)
                                                                            
                                                                            <div class="row" id="spareId_{{$key}}">
                                                                                <input type="hidden" name="spareId[]" value="{{$sparePart->id}}" id="spareIdName{{$key}}">
                                                                                <div class="col m1">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field" id="make_spare_select">
                                                                                            <select class="select2"  name="make_spare[]" onchange='loadModel(this,"modelIdName{{$key}}")'>
                                                                                                <option value="">Select Make</option>
                                                                                                @foreach($makes as $make)
                                                                                                    <option value="{{$make->id}}" @if($make->id == $sparePart->make_id) selected @endif>{{$make->vehicle_make}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col m1">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field ">
                                                                                        
                                                                                            <select class="select2"  name="model_spare[]" id="modelIdName{{$key}}">
                                                                                            
                                                                                            <option value="">Select Type</option>
                                                                                                @if(isset($sparePartModel[$key]))
                                                                                                    <?php 
                                                                                                    $spareModelS = $sparePartModel[$key];
                                                                                                    ?>
                                                                                                        @foreach($spareModelS as $spareModel)
                                                                                                            <option value="{{$spareModel->id}}" @if($spareModel->id == $sparePart->model_id) selected @endif>{{$spareModel->vehicle_type}}</option>
                                                                                                        @endforeach
                                                                                                    @endif
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col m2">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field ">
                                                                                            <select class="select2"  name="part_category_spare[]">
                                                                                                <option value="">Select Category</option>
                                                                                                @foreach($partCategorys as $partCategory)
                                                                                                    <option value="{{$partCategory->id}}" @if($partCategory->id == $sparePart->part_category_id) selected @endif>{{$partCategory->name}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col m2">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field">
                                                                                            <select class="select2"  name="part_name_spare[]">
                                                                                                <option value="">Select Part Name</option>
                                                                                                @foreach($partNames as $partName)
                                                                                                
                                                                                                    <option value="{{$partName->id}}"  @if($partName->id == $sparePart->part_name_id) selected @endif>{{$partName->name}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col m1">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field ">
                                                                                            <input type="text" name="quantity_spare[]" placeholder="Quantity" value="{{$sparePart->quantity ? $sparePart->quantity : 0 }}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col m1">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field ">
                                                                                            <input type="text" name="cost_spare[]" placeholder="Cost" value="{{$sparePart->cost ? $sparePart->cost : 0 }}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col m2">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field ">
                                                                                            <input type="text" name="location_spare[]" placeholder="Location" value="{{$sparePart->rock_location ? $sparePart->rock_location : '' }}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col m2">
                                                                                    <div class="input-group">
                                                                                        <div class="input-field " style="display: flex;">
                                                                                            <button type="button" class="btn btn-success" onclick="addSpare()" style="margin-right:5px;">Add</button>
                                                                                            <button type="button" class="btn btn-success" onclick="removeSpare({{$key}},'yes')">Remove</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endforeach
                                                                        @else
                                                                        <div class="row">

                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field" id="make_spare_select">
                                                                                        <select class="select2"  name="make_spare[]" onchange='loadModel(this,"modelIdName")'>
                                                                                            <option value="">Select Make</option>
                                                                                            @foreach($makes as $make)
                                                                                                <option value="{{$make->id}}">{{$make->vehicle_make}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <select class="select2"  name="model_spare[]" id="modelIdName">
                                                                                            <option value="">Select Type</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <select class="select2"  name="part_category_spare[]">
                                                                                            <option value="">Select Category</option>
                                                                                            @foreach($partCategorys as $partCategory)
                                                                                                <option value="{{$partCategory->id}}">{{$partCategory->name}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field">
                                                                                        <select class="select2"  name="part_name_spare[]">
                                                                                            
                                                                                            <option value="">Select Part Name</option>
                                                                                            @foreach($partNames as $partName)
                                                                                                <option value="{{$partName->id}}">{{$partName->name}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <input type="text" name="quantity_spare[]" placeholder="Quantity">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <input type="text" name="cost_spare[]" placeholder="Cost">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <input type="text" name="location_spare[]" placeholder="Location">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <button type="button" class="btn btn-success" onclick="addSpare()">Add</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                    </div> 
                                                                    <br><br>
                                                                    <div class="row">
                                                                        
                                                                        <h6>Common Parts:</h6>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col m1">Make</div>
                                                                        <div class="col m1">Model</div>
                                                                        <div class="col m2">Part Category</div>
                                                                        <div class="col m2">Part Name</div>
                                                                        <div class="col m1">Quantity</div>
                                                                        <div class="col m1">Cost</div>
                                                                        <div class="col m2">Rack Location</div>
                                                                        <div class="col m2">Add/Remove</div>
                                                                    </div>
                                                                    <div id="common_all_id">
                                                                        @if(count($spareCommons))
                                                                            @foreach($spareCommons as $key=>$spareCommon)
                                                                            <div class="row" id="commonId_{{$key}}">
                                                                                <input type="hidden" name="commonId[]" value="{{$spareCommon->id}}" id="commonIdName{{$key}}">

                                                                                    <div class="col m1">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field ">
                                                                                                <select class="select2"  name="make_common[]" onchange='loadModel(this,"modelIdCommon{{$key}}")'>
                                                                                                    <option value="">Select Make</option>
                                                                                                    @foreach($makes as $make)
                                                                                                        <option value="{{$make->id}}" @if($spareCommon->make_id == $make->id) selected @endif>{{$make->vehicle_make}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col m1">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field ">
                                                                                                <select class="select2"  name="model_common[]" id="modelIdCommon{{$key}}">
                                                                                                    
                                                                                                    <option value="">Select Type</option>
                                                                                                    
                                                                                                    
                                                                                                    <?php $spareCommonS = $spareCommonModel[$key];
                                                                                                    ?>
                                                                                                        @foreach($spareCommonS as $spareCommonName)
                                                                                                            <option value="{{$spareCommonName->id}}" @if($spareCommonName->id == $spareCommon->model_id) selected @endif>{{$spareCommonName->vehicle_type}}</option>
                                                                                                        @endforeach
                                                                                                    

                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col m2">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field ">
                                                                                                <select class="select2"  name="part_category_common[]">
                                                                                                    
                                                                                                    <option value="">Select Category</option>
                                                                                                    @foreach($partCategorys as $partCategory)
                                                                                                        <option value="{{$partCategory->id}}" @if($spareCommon->part_category_id == $partCategory->id) selected @endif >{{$partCategory->name}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col m2">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field ">
                                                                                                <select class="select2"  name="part_name_common[]">
                                                                                                    
                                                                                                    <option value="">Select Part Name</option>
                                                                                                    @foreach($partNames as $partName)
                                                                                                        <option value="{{$partName->id}}" @if($spareCommon->part_name_id == $partName->id) selected @endif>{{$partName->name}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col m1">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field ">
                                                                                                <input type="text" name="quantity_common[]" placeholder="Quantity" value="{{$spareCommon->quantity ? $spareCommon->quantity : 0}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col m1">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field ">
                                                                                                <input type="text" name="cost_common[]" placeholder="Cost" value="{{$spareCommon->cost ? $spareCommon->cost : 0}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col m2">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field ">
                                                                                                <input type="text" name="location_common[]" placeholder="Location" value="{{$spareCommon->rock_location ? $spareCommon->rock_location : ''}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col m2">
                                                                                        <div class="input-group">
                                                                                            <div class="input-field " style="display: flex;">
                                                                                                <button type="button" class="btn btn-success" onclick="addCommon()" style="margin-right:5px;">Add</button>
                                                                                                <button type="button" class="btn btn-success" onclick="removeCommon({{$key}},'yes')">Remove</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                            </div>
                                                                            @endforeach
                                                                        @else
                                                                        <div class="row">

                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <select class="select2"  name="make_common[]" onchange='loadModel(this,"modelIdCommon")'>
                                                                                            <option value="">Select Make</option>
                                                                                            @foreach($makes as $make)
                                                                                                <option value="{{$make->id}}" >{{$make->vehicle_make}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <select class="select2"  name="model_common[]" id="modelIdCommon">
                                                                                            
                                                                                            <option value="">Select Type</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <select class="select2"  name="part_category_common[]">
                                                                                            
                                                                                            <option value="">Select Category</option>
                                                                                            @foreach($partCategorys as $partCategory)
                                                                                                <option value="{{$partCategory->id}}" >{{$partCategory->name}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <select class="select2"  name="part_name_common[]">
                                                                                            
                                                                                            <option value="">Select Part Name</option>
                                                                                            @foreach($partNames as $partName)
                                                                                                <option value="{{$partName->id}}" >{{$partName->name}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <input type="text" name="quantity_common[]" placeholder="Quantity">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m1">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <input type="text" name="cost_common[]" placeholder="Cost">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <input type="text" name="location_common[]" placeholder="Location">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col m2">
                                                                                <div class="input-group">
                                                                                    <div class="input-field ">
                                                                                        <button type="button" class="btn btn-success" onclick="addCommon()">Add</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        @endif
                                                                    </div>  
                                                                {{-- </div> --}}
                                                            {{-- </div> --}}
                                                            <div class="col s12 display-flex justify-content-center mt-3">
                                                                <button type="submit" class="btn indigo" style="margin-right: 1rem;">
                                                                  Submit</button>
                                                                <button type="button" class="btn btn-light">Cancel</button>
                                                              </div>
                                                        </div>
                                                    </div>
                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    @include('mt.vehicles.scripts')
    <script type="text/javascript">
    var spareIdCount = "{{count($spareParts)}}";
    var commonIdCount = "{{count($spareCommons)}}";

        function addSpare(){
            spareIdCount++;
            var spareId = '<div class="row" id="spareId_'+spareIdCount+'">'+

            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<select  class="select2" name="make_spare[]" onchange=loadModel(this,"modelIdName'+spareIdCount+'")>'+
                            '<option value="">Select Make</option>'+
                            '@foreach($makes as $make)'+
                                '<option value="{{$make->id}}">{{$make->vehicle_make}}</option>'+
                            '@endforeach'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<select class="select2"  name="model_spare[]" id="modelIdName'+spareIdCount+'">'+
                            '<option value="">Select Type</option>'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<select class="select2"  name="part_category_spare[]">'+
                            '<option value="">Select Category</option>'+
                            '@foreach($partCategorys as $partCategory)'+
                                '<option value="{{$partCategory->id}}">{{$partCategory->name}}</option>'+
                            '@endforeach'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field">'+
                        '<select class="select2"  name="part_name_spare[]">'+
                            '<option value="">Part Name</option>'+
                            '@foreach($partNames as $partName)'+
                                '<option value="{{$partName->id}}">{{$partName->name}}</option>'+
                            '@endforeach'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<input type="text" name="quantity_spare[]" placeholder="Quantity">'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<input type="text" name="cost_spare[]" placeholder="Cost">'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<input type="text" name="location_spare[]" placeholder="Location">'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field " style="display: flex;">'+
                        '<button type="button" class="btn btn-success" onclick="addSpare()" style="margin-right:5px;">Add</button>'+
                        '<button type="button" class="btn btn-success" onclick=removeSpare('+spareIdCount+',"no")>Remove</button>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';
        jQuery("#spare_all_id").append(spareId);
            
            $('.select2').sm_select()

        }
        
        function addCommon(){
            var commonId = '<div class="row" id="commonId_'+commonIdCount+'">'+

            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<select class="select2 name="make_common[]" onchange=loadModel(this,"modelIdCommon'+commonIdCount+'")>'+
                            '<option value="">Select Make</option>'+
                            '@foreach($makes as $make)'+
                                '<option value="{{$make->id}}">{{$make->vehicle_make}}</option>'+
                            '@endforeach'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<select class="select2"  name="model_common[]" id="modelIdCommon'+commonIdCount+'">'+
                            '<option value="">Select Type</option>'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<select class="select2"  name="part_category_common[]">'+
                            '<option value="">Select Category</option>'+
                            '@foreach($partCategorys as $partCategory)'+
                                '<option value="{{$partCategory->id}}">{{$partCategory->name}}</option>'+
                            '@endforeach'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field">'+
                        '<select class="select2"  name="part_name_common[]">'+
                            '<option value="">Select Part Name</option>'+
                            '@foreach($partNames as $partName)'+
                                '<option value="{{$partName->id}}">{{$partName->name}}</option>'+
                            '@endforeach'+
                        '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<input type="text" name="quantity_common[]" placeholder="Quantity">'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m1">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<input type="text" name="cost_common[]" placeholder="Cost">'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field ">'+
                        '<input type="text" name="location_common[]" placeholder="Location">'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col m2">'+
                '<div class="input-group">'+
                    '<div class="input-field " style="display: flex;">'+
                        '<button type="button" class="btn btn-success" onclick="addCommon()" style="margin-right:5px;">Add</button>'+
                        '<button type="button" class="btn btn-success" onclick=removeCommon('+commonIdCount+',"no")>Remove</button>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';
        jQuery("#common_all_id").append(commonId);
        $('.select2').sm_select()
        
        }
        function removeCommon(id,removeName){

            if(removeName == 'yes'){
                var idValue = jQuery('#commonIdName'+id).val();
                jQuery.ajax({
                    url: "{{route('workshop.pto_stock_ledger.deleteSpareParts')}}",  
                    type: 'GET',  
                    data: {'id':idValue },   
                    success: function (data) {
                        // jQuery("#times"+id).remove();
                    }
                });
            }
            
            jQuery("#commonId_"+id).remove(); 

        }
        function removeSpare(id,removeName){
            if(removeName == 'yes'){
                var idValue = jQuery('#spareIdName'+id).val();
                jQuery.ajax({
                    url: "{{route('workshop.pto_stock_ledger.deleteSpareParts')}}",  
                    type: 'GET',  
                    data: {'id':idValue },   
                    success: function (data) {
                        // jQuery("#times"+id).remove();
                    }
                });
            }
            jQuery('#spareId_'+id).remove();
            
        }
        function loadModel(objs, idName){
            // var idValue = jQuery('#commonIdName'+id).val();
            if(objs.value != '' || objs.value != 0){
                jQuery.ajax({
                    url: "{{route('workshop.pto_stock_ledger.loadModelMaster')}}",  
                    type: 'GET',  
                    data: {'id':objs.value },   
                    success: function (data) {  
                        jQuery("#"+idName).html(data);
                        $('.select2').sm_select()
                    }
                });
            } 
        }
        
        
    </script>

@endsection
