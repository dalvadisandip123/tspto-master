@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">
                                            <div class="col s6 show-btn align-right mt-1">
                                                <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                            </div>
                                            <div class="col s6 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.vehicle_unit_history') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>

                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post" id="add_0"
                                                      enctype="multipart/form-data">


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">

                                                        <div class="col m12">

                                                            <div class="card">

                                                                <div class="card-content">
                                                                    <span class="card-title"> vehicles Info </span>

                                                                    <div class="row">
                                                                        <div class="col m3">
                                                                            <div
                                                                                    class="input-field col m12">
                                                                                <select
                                                                                        name="vehicle_details_id"
                                                                                        class="vehicle_details_id"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(getVehicleDetails())> 0)
                                                                                        @foreach(getVehicleDetails() AS $vkey=>$vvalue)
                                                                                            <option

                                                                                                    value="{{ $vkey }}">{{  $vvalue }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label> Vehicle Details</label>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="allotment_order_number"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('allotment_order_number') }}"
                                                                                           name="allotment_order_number"
                                                                                           class="validate">
                                                                                    <label for="allotment_order_number">
                                                                                        Allotment Order Number
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'allotment_order_number') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div
                                                                                    class="input-field col m12">
                                                                                <select
                                                                                        name="unit_id"
                                                                                        class="unit_id"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(getUnits())> 0)
                                                                                        @foreach(getUnits() AS $vvalue)
                                                                                            <option

                                                                                                    value="{{ $vvalue->id }}">{{  $vvalue->unit_name }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label> Unit Info</label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="from_km"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('from_km') }}"
                                                                                           name="from_km"
                                                                                           class="validate">
                                                                                    <label for="from_km">
                                                                                        From Km
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'from_km') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="issue_date"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('issue_date') }}"
                                                                                           name="issue_date"
                                                                                           class="validate datepicker_ui">
                                                                                    <label for="issue_date">
                                                                                        Issue Date
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'issue_date') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <textarea id="remarks"
                                                                                              value="{{ imputOldValue('remarks') }}"
                                                                                              name="remarks"
                                                                                              class="validate"></textarea>
                                                                                    <label for="issue_date">
                                                                                        Remarks
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'remarks') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div
                                                                                    class="input-field col m12">
                                                                                <select
                                                                                        name="receiver_user_id"
                                                                                        class="receiver_user_id"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(getUsers())> 0)
                                                                                        @foreach(getUsers() AS $vkey=>$vvalue)
                                                                                            <option

                                                                                                    value="{{ $vkey }}">{{  $vvalue }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label> Receiver user</label>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div
                                                                                    class="input-field col m12">
                                                                                <select
                                                                                        name="usage_pupose_id"
                                                                                        class="usage_pupose_id"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(getVehicleUseages())> 0)
                                                                                        @foreach(getVehicleUseages() AS $vvalue)
                                                                                            <option

                                                                                                    value="{{ $vvalue->id }}">{{  $vvalue->name }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label> Useage Purpose</label>
                                                                            </div>
                                                                        </div>


                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>


                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>
                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    @include('mt.vehicle_unit_history.scripts')


@endsection
