<script>


    $(function () {

        $('.manageForm').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',

                errorPlacement: function (error, e) {

                    var parentInputGroup = e.parents('.input-group');

                    console.log(parentInputGroup)

                    if (parentInputGroup.length == 0) {

                        var placement = $(e).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(e);
                        }

                    } else {
                        e.parents('.input-group > div').append(error);

                    }


                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },


                rules: {
                    vehicle_details_id: {
                        required: true
                    },
                    allotment_order_number: {
                        required: true
                    },
                    unit_id: {
                        required: true
                    },
                    from_km: {
                        required: true,
                    },
                    issue_date: {
                        required: true,
                    },
                    receiver_user_id: {
                        required: true,
                    },
                    usage_pupose_id: {
                        required: true,
                    },
                    end_date: {
                        required: true,
                    },
                    to_km: {
                        required: true,

                    },
                    status: {
                        required: true,
                    },

                },
                messages: {
                    vehicle_details_id: {
                        required: 'Select vehicle',
                    },
                    allotment_order_number: {
                        required: 'Enter allotment order number',
                    },
                    unit_id: {
                        required: 'Select unit',
                    },
                    from_km: {
                        required: 'enter from km',
                    },
                    issue_date: {
                        required: 'select issue date',
                    },
                    receiver_user_id: {
                        required: 'select receiver user',
                    },
                    usage_pupose_id: {
                        required: 'Select usage purpose',
                    },
                    end_date: {
                        required: 'Select end date',
                    },
                    to_km: {
                        required: 'Enter to km',

                    },

                    status: {
                        required: 'Select Status',

                    },

                },
            })


        })


        // $(".modal").modal({
        //     dismissible: false,
        // });
        // // $("#modal3").modal("open"),
        // // $("#modal3").modal("close")
    });

</script>
