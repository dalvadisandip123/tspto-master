<script>


    function vehicleTypeLoad(formID, selected = null) {
        var parentForm = formID.closest('form').attr('id');

        console.log(parentForm);
        var vehiclemake = formID.val();

        // console.log(vehiclemake);

        var $selectDropdown = $('#' + parentForm).find('.vehicle_type');
        var selected = $('#' + parentForm).find('.vehicle_type').data('selected');
        $selectDropdown.empty();
        // $selectDropdown.html(' ');
        // $selectDropdown.children('option').remove();
        if (vehiclemake != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.master.mt.getVehicletypes') }}',
                type: 'POST',
                data: {'vehiclemake': vehiclemake},
                success: function (response) {
                    var resultdata = response;
                    var Options = "<option  value='' > </option>";

                    $.each(resultdata.vehicletypes, function (k, v) {

                        if (selected == k) {
                            Options = Options + "<option  value='" + k + "' selected='selected'>" + v + "</option>";
                        } else {
                            Options = Options + "<option value='" + k + "'>" + v + "</option>";
                        }


                    });
                    $selectDropdown.append(Options);
                    $selectDropdown.formSelect();


                    if (selected) {
                        var formid = $('#' + parentForm).find('.vehicle_type');
                        vehicleVariantLoad(formid);

                    }


                }
            });
        }


    }

    function vehicleVariantLoad(formID, selected = null) {

        var parentForm = formID.closest('form').attr('id');

        console.log(parentForm);
        var vehicleType = formID.val();

        // console.log(vehiclemake);

        var $selectDropdown = $('#' + parentForm).find('.vehicle_variant');
        var selected = $('#' + parentForm).find('.vehicle_variant').data('selected');
        $selectDropdown.empty();
        // $selectDropdown.html(' ');
        // $selectDropdown.children('option').remove();
        if (vehicleType != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.master.mt.get_vehicle_variant') }}',
                type: 'POST',
                data: {'vehicle_type': vehicleType},
                success: function (response) {
                    var resultdata = response;
                    var Options = "<option  value='' > </option>";
                    $.each(resultdata.vehicle_variant, function (k, v) {

                        if (selected == k) {
                            Options = Options + "<option  value='" + k + "' selected='selected'>" + v + "</option>";
                        } else {
                            Options = Options + "<option value='" + k + "'>" + v + "</option>";
                        }


                    });
                    $selectDropdown.append(Options);
                    $selectDropdown.formSelect();
                }
            });
        }


    }


    // function tyereHtml(Id) {
    //     var htmlData = '';
    //     htmlData += '';
    // }


    function vehicleTyresLoop(InputId, itemId = null) {

        var parentForm = InputId.closest('form').attr('id');

        console.log(parentForm);
        var tyersCount = InputId.val();

        if (tyersCount != 0) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.mt.get_vehicle_tyers') }}',
                type: 'POST',
                data: {'no_tyers': tyersCount, 'item_id': itemId},
                success: function (response) {
                    var resultdata = response;

                    $('.tyresLists_box').html(resultdata)
                    // $('.select2').formSelect('destroy')
                    $('select').formSelect();

                    $('.tyre_number').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter Number"
                            }
                        });
                    });

                    $('.tyre_issue_date').each(function () {
                        $(this).rules('add', {
                            required: true,
                            validDate: true,
                            messages: {
                                required: "Enter Date"
                            }
                        });
                    });

                    $('.tyre_size').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter Size"
                            }
                        });
                    });
                    $('.tyre_life_span').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter  life span"
                            }
                        });
                    });
                    $('.tyre_make').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter  tyre"
                            }
                        });
                    });
                    loadDatepickerUi()
                    // console.log(resultdata)


                }
            });
        }


    }

    function vehicleBaratyLoop(InputId, itemId = null) {

        var parentForm = InputId.closest('form').attr('id');

        console.log(parentForm);
        var tyersCount = InputId.val();

        if (tyersCount != 0) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.mt.get_vehicle_batary') }}',
                type: 'POST',
                data: {'no_bataries': tyersCount, 'item_id': itemId},
                success: function (response) {
                    var resultdata = response;

                    $('.bataries_box').html(resultdata)
                    // $('.select2').formSelect('destroy')
                    $('select').formSelect()


                    $('.battery_make').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter  Make"
                            }
                        });
                    });

                    $('.battery_voltage').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter voltage"
                            }
                        });
                    });
                    $('.battery_ampere').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter ampere"
                            }
                        });
                    });

                    $('.battery_size').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter Size"
                            }
                        });
                    });

                    $('.battery_life_span').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter  life span"
                            }
                        });
                    });
                    $('.battery_issue_date').each(function () {
                        $(this).rules('add', {
                            required: true,
                            messages: {
                                required: "Enter  Number"
                            }
                        });
                    });
                    $('.battery_issue_date').each(function () {
                        $(this).rules('add', {
                            required: true,
                            validDate: true,
                            messages: {
                                required: "Enter Date"
                            }
                        });
                    });

                    loadDatepickerUi()

                    // console.log(resultdata)


                }
            });
        }


    }


    $(function () {


        $('.no_of_tyres').each(function () {
            var formid = $(this);
            vehicleTyresLoop(formid, '{{ isset($item->id)? $item->id: '' }}');
        })


        $(".no_of_tyres").on('input', function () {
            // $(".tyresLists_box").html('');
            var formid = $(this);
            vehicleTyresLoop(formid, {{ isset($item->id)? $item->id: '' }});
        });

        $('.no_of_battery').each(function () {
            var formid = $(this);
            vehicleBaratyLoop(formid, '{{ isset($item->id)? $item->id: '' }}');
        })

        $(".no_of_battery").on('input', function () {
            // $(".tyresLists_box").html('');
            var formid = $(this);
            vehicleBaratyLoop(formid, {{ isset($item->id)? $item->id: '' }});
        });



        $('.vehicle_make').each(function () {
            var formid = $(this);
            console.log('Load list ' + formid);
            vehicleTypeLoad(formid);
        })

        $(".vehicle_make").change(function () {
            var formid = $(this);
            vehicleTypeLoad(formid);
        });


        $('.vehicle_type').each(function () {
            var formid = $(this);
            console.log('Load list ' + formid);
            vehicleVariantLoad(formid);
        })


        $(".vehicle_type").change(function () {
            var formid = $(this);
            vehicleVariantLoad(formid);
        });


        // $('.select2').formSelect('destroy')
        // $('.select2').sm_select()


        $('.manageForm').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',

                errorPlacement: function (error, e) {

                    var parentInputGroup = e.parents('.input-group');

                    console.log(parentInputGroup)

                    if (parentInputGroup.length == 0) {

                        var placement = $(e).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(e);
                        }

                    } else {
                        e.parents('.input-group > div').append(error);

                    }


                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },


                rules: {
                    vehicle_make_id: {
                        required: true
                    },
                    vehicle_type_id: {
                        required: true
                    },
                    vehicle_variant_id: {
                        required: true
                    },
                    vehicle_category_id: {
                        required: true,
                    },
                    vin_no: {
                        required: true,
                    },
                    vehicle_no: {
                        required: true,
                    },
                    tr_no: {
                        required: true,
                    },
                    fuel_type_id: {
                        required: true,
                    },
                    model: {
                        required: true,

                    },
                    engine_no: {
                        required: true,

                    },

                    chassis_no: {
                        required: true,
                    },
                    rc_validity_date: {
                        required: true,
                        validDate: true
                    },
                    group_id: {
                        required: true,
                    },
                    usage_id: {
                        required: true,
                    },
                    body_type_id: {
                        required: true,
                    },
                    tank_capacity: {
                        required: true,
                    },

                    kmpl: {
                        required: true,
                    },
                    reserve_fuel: {
                        required: true,
                    },
                    engine_oil_grade_id: {
                        required: true,
                    },
                    engine_oil_grade_id: {
                        required: true,
                    },
                    no_of_cylinders: {
                        required: true,
                    },
                    seating_capacity: {
                        required: true,
                    },
                    allottment_order_no: {
                        required: true,
                    },
                    allotment_date: {
                        required: true,
                        validDate: true
                    },
                    bhp_rpm: {
                        required: true,
                    },
                    opening_km: {
                        required: true,
                    },
                    piston_displacement: {
                        required: true,
                    },
                    current_meter_reading: {
                        required: true,
                    },
                    go_number: {
                        required: true,
                    },
                    go_date: {
                        required: true,
                        validDate: true
                    },
                    invoice_no: {
                        required: true,
                    },
                    vehicle_purchase_from: {
                        required: true,
                    },
                    dealer_name: {
                        required: true,
                    },
                    dealer_mobile_no: {
                        required: true,
                    },
                    po_number: {
                        required: true,
                    },
                    po_date: {
                        required: true,
                        validDate: true
                    },
                    delivery_date: {
                        required: true,
                        validDate: true
                    },
                    date_of_issue: {
                        required: true,
                        validDate: true
                    },
                    vehicle_cost: {
                        required: true,
                    },
                    tax_type: {
                        required: true,
                    },
                    tax_pecentage: {
                        required: true,
                    },
                    tax_paid_amount: {
                        required: true,
                    },
                    key_no: {
                        required: true,
                    },
                    no_of_tyres: {
                        required: true,
                    },
                    no_of_battery: {
                        required: true,
                    },
                    type_of_drive: {
                        required: true,
                    },
                    stock_ledger_number: {
                        required: true,
                    },
                    page_number: {
                        required: true,
                    },
                    sl_no: {
                        required: true,
                    },
                    vehicle_description: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },

                },
                messages: {
                    vehicle_make_id: {
                        required: 'Select make',
                    },
                    vehicle_type_id: {
                        required: 'Select type',
                    },
                    vehicle_variant_id: {
                        required: 'Select variant',
                    },
                    vehicle_category_id: {
                        required: 'Select Category',
                    },
                    vin_no: {
                        required: 'Enter vin no',
                    },
                    vehicle_no: {
                        required: 'Enter vehicle no',
                    },
                    tr_no: {
                        required: 'Enter tr no',
                    },
                    fuel_type_id: {
                        required: 'Select fuel type',
                    },
                    model: {
                        required: 'Enter model',

                    },
                    engine_no: {
                        required: 'Enter engine no',

                    },

                    chassis_no: {
                        required: 'Enter chassis no',
                    },
                    rc_validity_date: {
                        required: 'Enter rc validity date',
                    },
                    group_id: {
                        required: 'Select group',
                    },
                    usage_id: {
                        required: 'Select usage',
                    },
                    body_type_id: {
                        required: 'Select body type',
                    },
                    tank_capacity: {
                        required: 'Enter tank capacity',
                    },

                    kmpl: {
                        required: 'Enter kmpl',
                    },
                    reserve_fuel: {
                        required: 'Enter reserve fuel',
                    },
                    engine_oil_grade_id: {
                        required: 'Select engine oil grade',
                    },

                    no_of_cylinders: {
                        required: 'Enter no of cylinders',
                    },
                    seating_capacity: {
                        required: 'Enter seating capacity',
                    },
                    allottment_order_no: {
                        required: 'Enter allottment order no',
                    },
                    allotment_date: {
                        required: 'Enter allotment date',
                    },
                    bhp_rpm: {
                        required: 'Enter bhp rpm',
                    },
                    opening_km: {
                        required: 'Enter opening km',
                    },
                    piston_displacement: {
                        required: 'Enter piston displacement',
                    },
                    current_meter_reading: {
                        required: 'Enter current meter reading',
                    },
                    go_number: {
                        required: 'Enter go number',
                    },
                    go_date: {
                        required: 'Enter go date',
                    },
                    invoice_no: {
                        required: 'Enter invoice no',
                    },
                    vehicle_purchase_from: {
                        required: 'Enter vehicle purchase from',
                    },
                    dealer_name: {
                        required: 'Enter dealer name',
                    },
                    dealer_mobile_no: {
                        required: 'Enter dealer mobile no',
                    },
                    po_number: {
                        required: 'Enter po number',
                    },
                    po_date: {
                        required: 'Enter po date',
                    },
                    delivery_date: {
                        required: 'Enter delivery date',
                    },
                    date_of_issue: {
                        required: 'Enter date of issue',
                    },
                    vehicle_cost: {
                        required: 'Enter vehicle cost',
                    },
                    tax_type: {
                        required: 'Enter tax type',
                    },
                    tax_pecentage: {
                        required: 'Enter tax pecentage',
                    },
                    tax_paid_amount: {
                        required: 'Enter tax paid amount',
                    },
                    key_no: {
                        required: 'Enter key no',
                    },
                    no_of_tyres: {
                        required: 'Enter no of tyres',
                    },
                    no_of_battery: {
                        required: 'Enter no of battery',
                    },
                    type_of_drive: {
                        required: 'Enter type of drive',
                    },
                    stock_ledger_number: {
                        required: 'Enter stock ledger number',
                    },
                    page_number: {
                        required: 'Enter Page Number',
                    },
                    sl_no: {
                        required: 'Enter SL NO',
                    },
                    vehicle_description: {
                        required: 'Enter description',
                    },

                    status: {
                        required: 'Select Status',

                    },

                },
            })


        })


        // $(".modal").modal({
        //     dismissible: false,
        // });
        // // $("#modal3").modal("open"),
        // // $("#modal3").modal("close")
    });

</script>
