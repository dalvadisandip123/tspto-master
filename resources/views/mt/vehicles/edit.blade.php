@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.employee') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                </div>


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post" id="add_2"
                                                      enctype="multipart/form-data">


                                                    @csrf

                                                    <input type="hidden" name="action" value="edit"/>

                                                    <input type="hidden" name="id" value="{{ $item->id }}"/>

                                                    <div class="row">

                                                        <div class="col m12">

                                                            <div class="card">

                                                                <div class="card-content">
                                                                    <span class="card-title"> vehicles Info </span>

                                                                    <div class="row">


                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    name="vehicle_make_id"
                                                                                    class="vehicle_make"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(getVehicleMakes()))
                                                                                        @foreach(getVehicleMakes() AS $key=>$value)
                                                                                            <option
                                                                                                {{ $item->vehicle_make_id == $key ?'selected':'' }}
                                                                                                value="{{ $key }}">{{  $value }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label>Vehicle
                                                                                    Make</label>
                                                                            </div>


                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    data-selected="{{ $item->vehicle_type_id }}"
                                                                                    class="vehicle_type"
                                                                                    name="vehicle_type_id">


                                                                                </select>

                                                                                <label>Vehicle Type</label>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    data-selected="{{ $item->vehicle_variant_id }}"
                                                                                    class="vehicle_variant"
                                                                                    name="vehicle_variant_id">


                                                                                </select>

                                                                                <label>Vehicle Variant</label>
                                                                            </div>


                                                                        </div>

                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    name="vehicle_category_id"
                                                                                    class="vehicle_category"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(vehicleCategory()))
                                                                                        @foreach(vehicleCategory() AS $value)
                                                                                            <option
                                                                                                {{ $item->vehicle_category_id == $value->id ?'selected':'' }}
                                                                                                value="{{ $value->id }}">{{  $value->vehicle_category }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label> Category</label>
                                                                            </div>


                                                                        </div>


                                                                    </div>


                                                                    <div class="row">

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="vin_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('vin_no', $item->vin_no) }}"
                                                                                           name="vin_no"
                                                                                           class="validate">
                                                                                    <label for="vehicle_cost">
                                                                                        VIN NO
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'vin_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="vehicle_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('vehicle_no', $item->vehicle_no) }}"
                                                                                           name="vehicle_no"
                                                                                           class="validate">
                                                                                    <label for="vehicle_no">
                                                                                        Vehicle no
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'vehicle_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="tr_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('tr_no', $item->tr_no) }}"
                                                                                           name="tr_no"
                                                                                           class="validate">
                                                                                    <label for="tr_no">
                                                                                        TR NO
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'tr_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    name="fuel_type_id"
                                                                                    class="fuel_type_id"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count($FuelTypeMaster)> 0)
                                                                                        @foreach($FuelTypeMaster AS $value)
                                                                                            <option
                                                                                                {{ $item->fuel_type_id == $value->id ?'selected':'' }}
                                                                                                value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label> Fuel Type</label>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="model"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('model', $item->model) }}"
                                                                                           name="model"
                                                                                           class="validate">
                                                                                    <label for="model">
                                                                                        Model
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'model') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="engine_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('engine_no', $item->engine_no) }}"
                                                                                           name="engine_no"
                                                                                           class="validate">
                                                                                    <label for="engine_no">
                                                                                        Engine No
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'engine_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="chassis_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('chassis_no', $item->chassis_no) }}"
                                                                                           name="chassis_no"
                                                                                           class="validate">
                                                                                    <label for="chassis_no">
                                                                                        Chassis No
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'chassis_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="rc_validity_date"
                                                                                           type="text"
                                                                                           value="{{ $item->rc_validity_date!='' ?date('d-m-Y', strtotime($item->rc_validity_date)) :'' }}"

                                                                                           name="rc_validity_date"
                                                                                           class="validate datepicker_ui">
                                                                                    <label for="rc_validity_date">
                                                                                        RC Validity Date
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'rc_validity_date') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                    <div class="row">


                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">

                                                                                    <select
                                                                                        name="group_id"
                                                                                        class="group_id"
                                                                                    >

                                                                                        <option value=""> Select
                                                                                        </option>

                                                                                        @if (count($vehicleGroupLists)> 0)
                                                                                            @foreach($vehicleGroupLists AS $value)
                                                                                                <option
                                                                                                    {{ $item->group_id == $value->id ?'selected':'' }}
                                                                                                    value="{{ $value->id }}">{{  $value->vehicle_group_name }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>

                                                                                    <label for="group_id"> Group</label>


                                                                                    {{ form_validation_error($errors, 'group_id') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">

                                                                                    <select
                                                                                        name="usage_id"
                                                                                        class="usage_id"
                                                                                    >

                                                                                        <option value=""> Select
                                                                                        </option>

                                                                                        @if (count($vehicleUsageLists)> 0)
                                                                                            @foreach($vehicleUsageLists AS $value)
                                                                                                <option
                                                                                                    {{ $item->usage_id == $value->id ?'selected':'' }}
                                                                                                    value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>

                                                                                    <label for="usage_id"> usage /
                                                                                        Pupose</label>
                                                                                    {{ form_validation_error($errors, 'usage_id') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">


                                                                                    <select
                                                                                        name="body_type_id"
                                                                                        class="body_type_id"
                                                                                    >

                                                                                        <option value=""> Select
                                                                                        </option>

                                                                                        @if (count($vehicleBodyTypeLists)> 0)
                                                                                            @foreach($vehicleBodyTypeLists AS $value)
                                                                                                <option
                                                                                                    {{ $item->body_type_id == $value->id ?'selected':'' }}
                                                                                                    value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>

                                                                                    <label for="body_type_id"> body
                                                                                        type</label>
                                                                                    {{ form_validation_error($errors, 'body_type_id') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="tank_capacity"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('tank_capacity',  $item->tank_capacity ) }}"
                                                                                           name="tank_capacity"
                                                                                           class="validate ">
                                                                                    <label for="tank_capacity">
                                                                                        Tank Capacity
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'tank_capacity') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="kmpl"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('kmpl',  $item->kmpl ) }}"
                                                                                           name="kmpl"
                                                                                           class="validate ">
                                                                                    <label for="kmpl">
                                                                                        KMPL
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'kmpl') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="reserve_fuel"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('reserve_fuel',  $item->reserve_fuel ) }}"
                                                                                           name="reserve_fuel"
                                                                                           class="validate ">
                                                                                    <label for="reserve_fuel">
                                                                                        Reserve Fuel
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'reserve_fuel') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="card">

                                                                <div class="card-content">
                                                                    <span class="card-title"> Record  Info </span>


                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">


                                                                                    <select
                                                                                        name="engine_oil_grade_id"
                                                                                        class="engine_oil_grade_id"
                                                                                    >

                                                                                        <option value=""> Select
                                                                                        </option>

                                                                                        @if (count($oilGradeLists)> 0)
                                                                                            @foreach($oilGradeLists AS $value)
                                                                                                <option
                                                                                                    {{ $item->engine_oil_grade_id == $value->id ?'selected':'' }}
                                                                                                    value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>

                                                                                    <label for="engine_oil_grade_id">
                                                                                        Engine Oil Grade </label>


                                                                                    {{ form_validation_error($errors, 'engine_oil_grade_id') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="bore_stroke"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('bore_stroke', $item->bore_stroke ) }}"
                                                                                           name="bore_stroke"
                                                                                           class="validate ">
                                                                                    <label for="bore_stroke">
                                                                                        Bore Stroke
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'bore_stroke') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="no_of_cylinders"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('no_of_cylinders', $item->no_of_cylinders) }}"
                                                                                           name="no_of_cylinders"
                                                                                           class="validate ">
                                                                                    <label for="no_of_cylinders">
                                                                                        No of Cylinders
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'no_of_cylinders') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="seating_capacity"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('seating_capacity', $item->seating_capacity) }}"
                                                                                           name="seating_capacity"
                                                                                           class="validate ">
                                                                                    <label for="seating_capacity">
                                                                                        Seating Capacity
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'seating_capacity') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="allottment_order_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('allottment_order_no', $item->allottment_order_no) }}"
                                                                                           name="allottment_order_no"
                                                                                           class="validate">
                                                                                    <label for="allottment_order_no">
                                                                                        Allottment Order No
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'allottment_order_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="allotment_date"
                                                                                           type="text"
                                                                                           value="{{ $item->allotment_date!='' ?date('d-m-Y', strtotime($item->allotment_date)) :'' }}"
                                                                                           name="allotment_date"
                                                                                           class="validate datepicker_ui ">
                                                                                    <label for="allotment_date">
                                                                                        Allotment Date
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'allotment_date') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="bhp_rpm"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('bhp_rpm', $item->bhp_rpm) }}"
                                                                                           name="bhp_rpm"
                                                                                           class="validate ">
                                                                                    <label for="bhp_rpm">
                                                                                        BHP RPM
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'bhp_rpm') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="opening_km"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('opening_km', $item->opening_km) }}"
                                                                                           name="opening_km"
                                                                                           class="validate ">
                                                                                    <label for="opening_km">
                                                                                        Opening km
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'opening_km') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>

                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="piston_displacement"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('piston_displacement' , $item->piston_displacement) }}"
                                                                                           name="piston_displacement"
                                                                                           class="validate">
                                                                                    <label for="piston_displacement">
                                                                                        Piston Displacement
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'piston_displacement') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="current_meter_reading"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('current_meter_reading', $item->current_meter_reading) }}"
                                                                                           name="current_meter_reading"
                                                                                           class="validate">
                                                                                    <label for="current_meter_reading">
                                                                                        Current Meter Reading
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'current_meter_reading') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="go_number"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('go_number', $item->go_number) }}"
                                                                                           name="go_number"
                                                                                           class="validate">
                                                                                    <label for="go_number">
                                                                                        GO Number
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'go_number') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="go_date"
                                                                                           type="text"
                                                                                           value="{{ $item->go_date!='' ?date('d-m-Y', strtotime($item->go_date)) :'' }}"
                                                                                           name="go_date"
                                                                                           class="validate datepicker_ui">
                                                                                    <label for="go_date">
                                                                                        GO Date
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'go_date') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="card">

                                                                <div class="card-content">
                                                                    <span class="card-title"> Dealer  Info </span>


                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="invoice_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('invoice_no', $item->invoice_no) }}"
                                                                                           name="invoice_no"
                                                                                           class="validate">
                                                                                    <label for="invoice_no">
                                                                                        Invoice No
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'invoice_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="vehicle_purchase_from"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('vehicle_purchase_from', $item->vehicle_purchase_from) }}"
                                                                                           name="vehicle_purchase_from"
                                                                                           class="validate">
                                                                                    <label for="vehicle_purchase_from">
                                                                                        Vehicle Purchase From
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'vehicle_purchase_from') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="dealer_name"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('dealer_name', $item->dealer_name) }}"
                                                                                           name="dealer_name"
                                                                                           class="validate">
                                                                                    <label for="dealer_name">
                                                                                        Dealer Name
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'dealer_name') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="dealer_mobile_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('dealer_mobile_no', $item->dealer_mobile_no) }}"
                                                                                           name="dealer_mobile_no"
                                                                                           class="validate ">
                                                                                    <label for="dealer_mobile_no">
                                                                                        Dealer Mobile No
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'dealer_mobile_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="po_number"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('po_number', $item->po_number) }}"
                                                                                           name="po_number"
                                                                                           class="validate">
                                                                                    <label for="po_number">
                                                                                        PO Number
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'po_number') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="po_date"
                                                                                           type="text"
                                                                                           value="{{ $item->po_date!='' ?date('d-m-Y', strtotime($item->po_date)) :'' }}"
                                                                                           name="po_date"
                                                                                           class="validate datepicker_ui">
                                                                                    <label for="po_date">
                                                                                        PO Date
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'po_date') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="delivery_date"
                                                                                           type="text"
                                                                                           value="{{ $item->delivery_date!='' ?date('d-m-Y', strtotime($item->delivery_date)) :'' }}"
                                                                                           name="delivery_date"
                                                                                           class="validate datepicker_ui">
                                                                                    <label for="delivery_date">
                                                                                        Delivery Date
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'delivery_date') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="date_of_issue"
                                                                                           type="text"
                                                                                           value="{{ $item->date_of_issue!='' ?date('d-m-Y', strtotime($item->date_of_issue)) :'' }}"
                                                                                           name="date_of_issue"
                                                                                           class="validate datepicker_ui">
                                                                                    <label for="date_of_issue">
                                                                                        Date of Issue
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'date_of_issue') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>

                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="vehicle_cost"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('vehicle_cost', $item->vehicle_cost) }}"
                                                                                           name="vehicle_cost"
                                                                                           class="validate">
                                                                                    <label for="vehicle_cost">
                                                                                        Vehicle Cost
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'vehicle_cost') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="tax_type"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('tax_type', $item->tax_type) }}"
                                                                                           name="tax_type"
                                                                                           class="validate ">
                                                                                    <label for="tax_type">
                                                                                        Tax Type
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'tax_type') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="tax_pecentage"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('tax_pecentage', $item->tax_pecentage) }}"
                                                                                           name="tax_pecentage"
                                                                                           class="validate ">
                                                                                    <label for="tax_pecentage">
                                                                                        tax Pecentage
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'tax_pecentage') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="tax_paid_amount"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('tax_paid_amount', $item->tax_paid_amount) }}"
                                                                                           name="tax_paid_amount"
                                                                                           class="validate ">
                                                                                    <label for="tax_paid_amount">
                                                                                        Tax Paid Amount
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'tax_paid_amount') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="card">

                                                                <div class="card-content">
                                                                    <span class="card-title"> Additional  Info </span>


                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="key_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('key_no', $item->key_no) }}"
                                                                                           name="key_no"
                                                                                           class="validate">
                                                                                    <label for="key_no">
                                                                                        KEY NO
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'key_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="no_of_tyres"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('no_of_tyres', $item->no_of_tyres) }}"
                                                                                           name="no_of_tyres"
                                                                                           class="validate no_of_tyres">
                                                                                    <label for="no_of_tyres">
                                                                                        No of Tyres
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'no_of_tyres') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="no_of_battery"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('no_of_battery', $item->no_of_battery) }}"
                                                                                           name="no_of_battery"
                                                                                           class="validate no_of_battery">
                                                                                    <label for="no_of_battery">
                                                                                        No of Battery
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'no_of_battery') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="type_of_drive"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('type_of_drive', $item->type_of_drive) }}"
                                                                                           name="type_of_drive"
                                                                                           class="validate ">
                                                                                    <label for="type_of_drive">
                                                                                        Type of Drive
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'type_of_drive') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="stock_ledger_number"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('stock_ledger_number', $item->stock_ledger_number) }}"
                                                                                           name="stock_ledger_number"
                                                                                           class="validate">
                                                                                    <label for="stock_ledger_number">
                                                                                        Stock Ledger Number
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'stock_ledger_number') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="page_number"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('page_number', $item->page_number) }}"
                                                                                           name="page_number"
                                                                                           class="validate">
                                                                                    <label for="page_number">
                                                                                        Page Number
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'page_number') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="sl_no"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('sl_no', $item->sl_no) }}"
                                                                                           name="sl_no"
                                                                                           class="validate">
                                                                                    <label for="sl_no">
                                                                                        SL NO
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'sl_no') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>


                                                            @if( count($item->geteTyreHistory) <= $item->no_of_tyres)
                                                                <div class="card">

                                                                    <div class="card-content">
                                                                        <span class="card-title"> Tyers  Info </span>


                                                                        <div class="tyresLists_box">


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif


                                                            @if( count($item->getBatteryHistory) <= $item->no_of_battery)
                                                                <div class="card">

                                                                    <div class="card-content">
                                                                        <span class="card-title"> Bataries  Info </span>
                                                                        <div class="bataries_box">


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif


                                                            <div class="card">

                                                                <div class="card-content">
                                                                    <span class="card-title"> Last Info </span>

                                                                    <div class="row">
                                                                        <div class="col m10">

                                                                            <div class="input-group">
                                                                                <div class="input-field col s12">
                                                                    <textarea id="vehicle_description"
                                                                              class="materialize-textarea"
                                                                              name="vehicle_description">{{ $item->vehicle_description }}</textarea>

                                                                                    <label for="vehicle_description">
                                                                                        Vehicle Description
                                                                                    </label>
                                                                                </div>
                                                                                {{ form_validation_error($errors, 'vehicle_description') }}
                                                                            </div>


                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    name="status">

                                                                                    @if (count(status_list()))
                                                                                        @foreach(status_list() AS $staus_key=>$staus)
                                                                                            <option
                                                                                                {{ $item->status == $staus_key ?'selected':'' }}
                                                                                                value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label>Status</label>
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>


                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>

                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('mt.vehicles.scripts')

@endsection
