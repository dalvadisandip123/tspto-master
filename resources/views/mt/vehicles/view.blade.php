@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.pol.purchase_register') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                </div>


                                                <div class="row">

                                                    <div class="card">

                                                        <div class="card-content">
                                                            <span class="card-title"> Purchase Register </span>

                                                            <div class="row">

                                                                <div class="col m4">
                                                                    <div class="input-group">
                                                                        <div class="input-field ">
                                                                            <input id="financial_years"
                                                                                   type="text"
                                                                                   value="{{ $item->financial_years }}"
                                                                                   name="financial_years" disabled
                                                                                   class="validate ">
                                                                            <label for="financial_years">
                                                                                Date
                                                                            </label>
                                                                            {{ form_validation_error($errors, 'financial_years') }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col m4">
                                                                    <div class="input-group">
                                                                        <div class="input-field ">
                                                                            <input id="purchase_date"
                                                                                   type="text"
                                                                                   value="{{ date('d-m-Y', strtotime($item->purchase_date)) }}"
                                                                                   name="purchase_date" disabled
                                                                                   class="validate ">
                                                                            <label for="purchase_date">
                                                                                Date
                                                                            </label>
                                                                            {{ form_validation_error($errors, 'purchase_date') }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col m4">

                                                                    <div
                                                                        class="input-field col m12">

                                                                        <input id="account_number"
                                                                               type="text"
                                                                               value="{{ $item->getAccount->name }}"
                                                                               name="account_number" disabled
                                                                               class="validate ">
                                                                        <label for="account_number">
                                                                            Account Number
                                                                        </label>


                                                                    </div>


                                                                </div>

                                                                <div class="col m4">
                                                                    <div class="input-group">
                                                                        <div class="input-field ">
                                                                            <input id="invoice_number"
                                                                                   type="text" disabled
                                                                                   value="{{ $item->invoice_number }}"
                                                                                   name="invoice_number"
                                                                                   class="validate">
                                                                            <label for="invoice_number">
                                                                                Invoice Number
                                                                            </label>

                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="row">

                                                                <div class="col m4">
                                                                    <div class="input-group">
                                                                        <div class="input-field ">
                                                                            <input id="bill_amount"
                                                                                   type="number" disabled
                                                                                   value="{{ $item->bill_amount }}"
                                                                                   name="bill_amount"
                                                                                   class="validate">
                                                                            <label for="bill_amount">
                                                                                bill amount
                                                                            </label>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col m4">
                                                                    <div class="input-group">
                                                                        <div class="input-field ">
                                                                            <input id="credit_amount_IOCL"
                                                                                   type="number" disabled
                                                                                   value="{{ $item->credit_amount_IOCL }}"
                                                                                   name="credit_amount_IOCL"
                                                                                   class="validate">
                                                                            <label for="credit_amount_IOCL">
                                                                                Credit Amount to IOCL
                                                                            </label>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col m4">
                                                                    <div class="input-group">
                                                                        <div class="input-field ">
                                                                            <input id="remarks"
                                                                                   type="text" disabled
                                                                                   value="{{ $item->remarks }}"
                                                                                   name="remarks"
                                                                                   class="validate">
                                                                            <label for="remarks">
                                                                                remarks
                                                                            </label>

                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="card">

                                                        <div class="card-content">
                                                            <span class="card-title"> Fuel Info </span>


                                                            <div class="row">


                                                                @if(count($outletsMasters)> 0)
                                                                    @foreach($outletsMasters AS $outletsMaster)

                                                                        {{--                                                                            @php--}}
                                                                        {{--                                                                                $currentOutlets = $item->getFuels->where('outlet_id', $outletsMaster->id);--}}
                                                                        {{--                                                                            @endphp--}}

                                                                        <div class="col m4">
                                                                            <div class="card">

                                                                                <div class="card-content">
                                                                                    <span
                                                                                        class="card-title">
                                                                                   {{ $outletsMaster->name }}
                                                                                    </span>

                                                                                    <div class="row">


                                                                                        @if(count($fuelTypeMasters)> 0)
                                                                                            @foreach($fuelTypeMasters AS $fuelTypeMaster)

                                                                                                @php
                                                                                                    $currentOutletsFuels = $item->getFuels->where('outlet_id', $outletsMaster->id)->where('fuel_type_id', $fuelTypeMaster->id)->first();
                                                                                                @endphp

                                                                                                <div
                                                                                                    class="col m6">
                                                                                                    <?php
                                                                                                    //                                                                                                        dump($currentOutletsFuels)
                                                                                                    ?>

                                                                                                    <div
                                                                                                        class="input-group">
                                                                                                        <div
                                                                                                            class="input-field ">
                                                                                                            <input
                                                                                                                id="fuelId_{{$outletsMaster->id}}_ful_{{ $fuelTypeMaster->id  }}"
                                                                                                                type="number" disabled
                                                                                                                value="{{ $currentOutletsFuels->fuel_price }}"
                                                                                                                name="outlet[{{ $outletsMaster->id }}][{{ $fuelTypeMaster->id }}][price]"
                                                                                                                class="validate  ">
                                                                                                            <label
                                                                                                                for="fuelId_{{$outletsMaster->id}}_ful_{{ $fuelTypeMaster->id  }}">
                                                                                                                {{ $fuelTypeMaster->name }}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                        @endif


                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    @endforeach
                                                                @endif


                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="card">

                                                        <div class="card-content">
                                                            <span class="card-title"> Lubes  Info </span>


                                                            <div class="row">


                                                                @if(count($oilGradeLists)> 0)
                                                                    @foreach($oilGradeLists AS $oilGradeList)

                                                                        @php

                                                                            $currentLubs = $item->getLubs->where('lube_id', $oilGradeList->id)->first();

                                                                        @endphp
                                                                        <div
                                                                            class="col m4">

                                                                            <?php
                                                                            //                                                                                dump($currentLubs)
                                                                            ?>


                                                                            <div
                                                                                class="input-group">
                                                                                <div
                                                                                    class="input-field ">
                                                                                    <input
                                                                                        id="lubes_{{$oilGradeList->id}}"
                                                                                        type="number" disabled
                                                                                        value="{{ $currentLubs->lube_price }}"
                                                                                        name="lubes[{{$oilGradeList->id}}][price]"
                                                                                        class="validate  ">
                                                                                    <label
                                                                                        for="lubes_{{$oilGradeList->id}}">
                                                                                        {{ $oilGradeList->name }}
                                                                                        (liters)
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif


                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('mt.vehicles.scripts')

@endsection
