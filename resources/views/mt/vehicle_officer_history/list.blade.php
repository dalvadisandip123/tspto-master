@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>


                                    <div class="col s12 m10 l10 ">
                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.vehicle_officer_history', ['action'=>'new']) }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Add
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        {{ form_flash_message('flash_message') }}
                                                    </div>


                                                    <div class="col l12">

                                                        <form action="" method="get">
                                                            <div class="row">
                                                                <div class="col s12  l3">
                                                                    <div class="input-field col m12">
                                                                        <input id="allotment_order_number" type="text"
                                                                               name="allotment_order_number"
                                                                               value="{{ request()->attachment_order_number }}">
                                                                        <label
                                                                                for="allotment_order_number">
                                                                            Attachment Order
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col s12  l3">
                                                                    <div class="input-field col m12">
                                                                        <select name="status">
                                                                            <option value="">All</option>
                                                                            @if (count(vehicle_status_list()))
                                                                                @foreach(vehicle_status_list() AS $staus_key=>$staus)
                                                                                    <option
                                                                                            {{  request()->status==$staus_key ?'selected':'' }}
                                                                                            value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        {{--                                                                                <label>status</label>--}}
                                                                    </div>
                                                                </div>


                                                                <div class="col s12 m6 l3 display-flex  show-btn">
                                                                    <button type="submit"
                                                                            class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                        Filter Now
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>


                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="vehicle_details_id">
                                                                    Vehicle Info
                                                                </th>
                                                                <th data-field="allotment_order_number">
                                                                    Attachment order number
                                                                </th>
                                                                <th data-field="unit_id">
                                                                    Unit
                                                                </th>

                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleDetails->vehicle_no }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->attachment_order_number }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getUnitInfo->unit_name }}
                                                                        </td>

                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger btn-small"
                                                                               href="{{ route('site.mt.vehicle_officer_drivers',['officerid'=>$list->id]) }}"
                                                                               target="_blank">
                                                                                Add Drivers Info
                                                                            </a>

                                                                            <a class="waves-effect waves-light btn modal-trigger btn-small"
                                                                               href="{{ route('site.mt.vehicle_officer_history', ['action'=>'edit', 'id'=> $list->id]) }}">
                                                                                Edit
                                                                            </a>


                                                                            <form method="POST"
                                                                                  action="{{ route('site.mt.vehicle_officer_history', ['action'=>'remove','id'=> $list->id]) }}"
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary btn-small"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm('Confirm delete')">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="6">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>


                                                    <div class="pagination">
                                                        <div class="col m12 left-align">

                                                            Showing {{ $lists->firstItem() }}
                                                            to {{ $lists->lastItem() }} of {{ $lists->total() }} entries

                                                        </div>
                                                        <div class="col m12 right-align">

                                                            {{$lists->links('vendor.pagination.materializecss')}}

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

@endsection
