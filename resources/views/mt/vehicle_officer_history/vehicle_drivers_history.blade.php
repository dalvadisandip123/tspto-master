@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">

    {{--    <link rel="stylesheet" type="text/css" href="assets/css/pages/page-account-settings.min.css">--}}
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">

                        <!-- Left Aligned -->
                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ $module_name }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>
                                    <h3> Officers Info</h3>
                                    <div class="col s12 m5 12 ">
                                        <table class="striped">
                                            <tbody>
                                            <tr>
                                                <td>Vehicle No</td>
                                                <td>{{ $officerinfo->getVehicleDetails->vehicle_no }}</td>
                                            </tr>
                                            <tr>
                                                <td>Unit</td>
                                                <td>{{ $officerinfo->getUnitInfo->unit_name }}</td>
                                            </tr>


                                            </tbody>
                                        </table>

                                    </div>

                                    <div class="col s12 m5 12 ">

                                        <table class="striped">
                                            <tbody>
                                            <tr>
                                                <td>Officer</td>
                                                <td>{{ $officerinfo->getOfficerInfo->name }}</td>
                                            </tr>
                                            <tr>
                                                <td>Attachment number</td>
                                                <td>{{ $officerinfo->attachment_order_number }}</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                    <div class="col s12 m7 l7 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col s12 m12 l12 ">


                                                        <div class="row">
                                                            <form action="" method="get">

                                                                <div class="col s12  l3">
                                                                    <div class="input-field col m12">
                                                                        <select name="status">
                                                                            <option value="">All</option>
                                                                            @if (count(vehicle_status_list()))
                                                                                @foreach(vehicle_status_list() AS $staus_key=>$staus)
                                                                                    <option {{  request()->status==$staus_key ?'selected':'' }}
                                                                                            value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select> <label>status</label>
                                                                    </div>
                                                                </div>


                                                                <div class="col s12 m6 l3 display-flex  show-btn">
                                                                    <button type="submit"
                                                                            class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                        Filter Now
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>

                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="name">From Date</th>
                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->from_date }}
                                                                        </td>
                                                                        <td>
                                                                            {{ vehicle_status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Edit
                                                                            </a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">


                                                                                <div class="modal-content">
                                                                                    <h4>
                                                                                        Manage {{ $module_name }}
                                                                                    </h4>


                                                                                    <div class="card lighten-1">


                                                                                        <div class="card-content">
                                                                                            <div class="row">
                                                                                                <div
                                                                                                        class="col s12 m12 l12">
                                                                                                    <form role="form"
                                                                                                          class="form-horizontal formValidation"
                                                                                                          method="post"
                                                                                                          enctype="multipart/form-data">
                                                                                                        @csrf

                                                                                                        <input
                                                                                                                type="hidden"
                                                                                                                name="action"
                                                                                                                value="edit"/>
                                                                                                        <input
                                                                                                                type="hidden"
                                                                                                                name="id"
                                                                                                                value="{{ $list->id }}"/>


                                                                                                        <div
                                                                                                                class="row">
                                                                                                            <div
                                                                                                                    class="col m2"></div>
                                                                                                            <div
                                                                                                                    class="col m8">

                                                                                                                <div
                                                                                                                        class="row">


                                                                                                                    <div
                                                                                                                            class="input-field col m12">
                                                                                                                        <input
                                                                                                                                id="from_date_{{ $list->id }}"
                                                                                                                                type="text"
                                                                                                                                value="{{ $list->from_date!='' ?date('d-m-Y', strtotime($list->from_date)) :'' }}"
                                                                                                                                name="from_date"
                                                                                                                                class="validate datepicker_ui">
                                                                                                                        <label
                                                                                                                                for="from_date_{{ $list->id }}">From
                                                                                                                            Date</label>
                                                                                                                    </div>

                                                                                                                    <div
                                                                                                                            class="input-field col m12">
                                                                                                                        <input
                                                                                                                                id="to_date_{{ $list->id }}"
                                                                                                                                type="text"
                                                                                                                                value="{{ $list->to_date!='' ?date('d-m-Y', strtotime($list->to_date)) :'' }}"
                                                                                                                                name="to_date"
                                                                                                                                class="validate datepicker_ui">
                                                                                                                        <label
                                                                                                                                for="to_date_{{ $list->id }}">To
                                                                                                                            Date</label>
                                                                                                                    </div>

                                                                                                                    <div
                                                                                                                            class="input-field col m12">
                                                                                                                        <input
                                                                                                                                id="start_km_{{ $list->id }}"
                                                                                                                                type="number"
                                                                                                                                value="{{ $list->start_km }}"
                                                                                                                                name="start_km"
                                                                                                                                class="">
                                                                                                                        <label
                                                                                                                                for="start_km_{{ $list->id }}">Start
                                                                                                                            km</label>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                            class="input-field col m12">
                                                                                                                        <input
                                                                                                                                id="end_km_{{ $list->id }}"
                                                                                                                                type="number"
                                                                                                                                value="{{ $list->end_km }}"
                                                                                                                                name="end_km"
                                                                                                                                class="">
                                                                                                                        <label
                                                                                                                                for="end_km_{{ $list->id }}">End
                                                                                                                            km</label>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                            class="input-field col m12">
                                                                                                                        <textarea
                                                                                                                                id="remarks_{{ $list->id }}"
                                                                                                                                name="remarks"
                                                                                                                                class="">{{ $list->remarks }}</textarea>
                                                                                                                        <label
                                                                                                                                for="remarks_{{ $list->id }}">Remarks</label>
                                                                                                                    </div>

                                                                                                                    <div
                                                                                                                            class="input-field col m12">
                                                                                                                        <select
                                                                                                                                name="status">
                                                                                                                            <option
                                                                                                                                    value="">
                                                                                                                                Choose
                                                                                                                                your
                                                                                                                                option
                                                                                                                            </option>
                                                                                                                            @if (count(vehicle_status_list()))
                                                                                                                                @foreach(vehicle_status_list() AS $staus_key=>$staus)
                                                                                                                                    <option
                                                                                                                                            {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                                                            value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                                                                @endforeach
                                                                                                                            @endif

                                                                                                                        </select>

                                                                                                                        <label>status</label>
                                                                                                                    </div>


                                                                                                                    <div
                                                                                                                            class="input-field col m12">

                                                                                                                        <button
                                                                                                                                class="btn btn-primary">
                                                                                                                            Update
                                                                                                                        </button>

                                                                                                                    </div>


                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div
                                                                                                                    class="col m2"></div>
                                                                                                        </div>


                                                                                                    </form>
                                                                                                </div>


                                                                                            </div>

                                                                                        </div>
                                                                                    </div>


                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                </div>
                                                                            </div>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete Banner"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach

                                                            @else
                                                                <tr>
                                                                    <td colspan="4">No Records Found</td>
                                                                </tr>
                                                            @endif

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pagination">
                                            <div class="col m12 left-align">

                                                Showing {{ $lists->firstItem() }}
                                                to {{ $lists->lastItem() }} of {{ $lists->total() }} entries

                                            </div>
                                            <div class="col m12 right-align">

                                                {{$lists->links('vendor.pagination.materializecss')}}

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal formValidation"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ $module_name }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>
                                                    <input type="hidden" name="vehicle_details_id"
                                                           value="{{ $officerinfo->vehicle_details_id }}"/>
                                                    <input type="hidden" name="unit_id"
                                                           value="{{ $officerinfo->unit_id }}"/>
                                                    <input type="hidden" name="officer_id"
                                                           value="{{ $officerinfo->officer_id }}"/>
                                                    <input type="hidden" name="attachment_id"
                                                           value="{{ $officerinfo->id }}"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">

                                                                <div
                                                                        class="input-field col m12">
                                                                    <select
                                                                            name="diver_id">
                                                                        <option
                                                                                value="">
                                                                            Choose
                                                                            your
                                                                            option
                                                                        </option>
                                                                        @if (count(getDrivers()))
                                                                            @foreach(getDrivers() AS $key=>$value)
                                                                                <option
                                                                                        value="{{ $key }}">{{  $value }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    <label>Driver</label>
                                                                </div>


                                                                <div
                                                                        class="input-field col m12">
                                                                    <input
                                                                            id="from_date"
                                                                            type="text"
                                                                            name="from_date"
                                                                            class="validate datepicker_ui">
                                                                    <label
                                                                            for="from_date">From
                                                                        Date</label>
                                                                </div>


                                                                <div
                                                                        class="input-field col m12">
                                                                    <input
                                                                            id="start_km"
                                                                            type="number"
                                                                            name="start_km"
                                                                            class="">
                                                                    <label
                                                                            for="start_km">Start
                                                                        km</label>
                                                                </div>

                                                                <div
                                                                        class="input-field col m12">
                                                                                                                        <textarea
                                                                                                                                id="remarks"
                                                                                                                                name="remarks"
                                                                                                                                class=""></textarea>
                                                                    <label
                                                                            for="remarks">Remarks</label>
                                                                </div>


                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.formValidation').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        from_date: {
                            required: true
                        },
                        diver_id: {
                            required: true
                        },
                        to_date: {
                            required: true
                        },
                        start_km: {
                            required: true
                        },
                        end_km: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        from_date: {
                            required: 'Please enter '
                        },
                        diver_id: {
                            required: 'Please enter '
                        },
                        to_date: {
                            required: 'Please enter '
                        },
                        start_km: {
                            required: 'Please enter '
                        },
                        end_km: {
                            required: 'Please enter '
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
