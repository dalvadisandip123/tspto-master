@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ $module_name }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>


                                    <div class="col s12 m7 l7 ">


                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col s12 m12 l12 ">


                                                        <div class="row">
                                                            <form action="" method="get">
                                                                {{--                                                                <div class="col s12  l3">--}}
                                                                {{--                                                                    <div class="input-field col m12">--}}
                                                                {{--                                                                        <input id="unit_nameSeach" type="text"--}}
                                                                {{--                                                                               value="{{ request()->unit_name }}"--}}
                                                                {{--                                                                               name="unit_name">--}}
                                                                {{--                                                                        <label for="unit_nameSearch">Name</label>--}}
                                                                {{--                                                                    </div>--}}
                                                                {{--                                                                </div>--}}

                                                                <div class="col s12  l3">
                                                                    <div class="input-field col m12">
                                                                        <select name="status">
                                                                            <option value="">All</option>
                                                                            @if (count(status_list()))
                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                    <option
                                                                                        {{  request()->status==$staus_key ?'selected':'' }}
                                                                                        value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        {{--                                                                                <label>status</label>--}}
                                                                    </div>
                                                                </div>
                                                                <div class="col s12 m6 l3 display-flex  show-btn">
                                                                    <button type="submit"
                                                                            class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                        Filter Now
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="Employee Name">Employee Name</th>

                                                                <th data-field="duty date">duty date</th>
                                                                <th data-field="reason for duty">reason for duty</th>
                                                                <th data-field="place of duty">place of duty</th>
                                                                <th data-field="time">time</th>
                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getEmployee->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->duty_date!='' ? date('d-m-Y', strtotime($list->duty_date)):'' }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->reason_for_duty}}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->place_of_duty}}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->time }}
                                                                        </td>
                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Edit
                                                                            </a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">
                                                                                <div class="modal-content">
                                                                                    <h4>
                                                                                        Units
                                                                                    </h4>

                                                                                    <div class="row">
                                                                                        <div class="col s12 m12 l12">
                                                                                            <form role="form"
                                                                                                  class="form-horizontal units"
                                                                                                  method="post"
                                                                                                  enctype="multipart/form-data">
                                                                                                @csrf

                                                                                                <input type="hidden"
                                                                                                       name="action"
                                                                                                       value="edit"/>
                                                                                                <input type="hidden"
                                                                                                       name="id"
                                                                                                       value="{{ $list->id }}"/>


                                                                                                <div class="row">
                                                                                                    <div
                                                                                                        class="col m2"></div>
                                                                                                    <div class="col m8">

                                                                                                        <div
                                                                                                            class="row">

                                                                                                            <select
                                                                                                                class="select2 employees_id"
                                                                                                                name="employees_id">

                                                                                                                <option
                                                                                                                    value="">
                                                                                                                    Select
                                                                                                                    Employee
                                                                                                                </option>

                                                                                                                @if (count(getEmployees()))
                                                                                                                    @foreach(getEmployees() AS $value)
                                                                                                                        <option
                                                                                                                            {{ $list->employees_id == $value->id ?'selected':'' }}
                                                                                                                            value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                                                    @endforeach
                                                                                                                @endif

                                                                                                            </select>
                                                                                                            {{--                                                                        <label>  Employee Role </label>--}}


                                                                                                        </div>

                                                                                                        <small
                                                                                                            class="officer">
                                                                                                            Rank: <span
                                                                                                                class="rank">xxxxx</span>

                                                                                                            <br/>
                                                                                                            Number:
                                                                                                            <span
                                                                                                                class="number">xxxxx</span>
                                                                                                            <br/>
                                                                                                            Employee
                                                                                                            Role: <span
                                                                                                                class="role">xxxxx</span>
                                                                                                            <br/>
                                                                                                            Parent Unit
                                                                                                            : <span
                                                                                                                class="parent_unit">xxxxx</span>
                                                                                                            <br/>
                                                                                                            Mobile No :
                                                                                                            <span
                                                                                                                class="mobile_number">xxxxx</span>
                                                                                                            <br/>
                                                                                                            Residence
                                                                                                            Mobile :
                                                                                                            <span
                                                                                                                class="family_mobile_number">xxxxx</span>
                                                                                                        </small>

                                                                                                    </div>


                                                                                                    <div
                                                                                                        class="col m12 input-group">
                                                                                                        <div
                                                                                                            class="input-field ">
                                                                                                            <input
                                                                                                                id="duty_date_{{ $list->id }}"
                                                                                                                type="text"
                                                                                                                value="{{ $list->duty_date!='' ? date('d-m-Y', strtotime($list->duty_date)):'' }}"
                                                                                                                name="duty_date"
                                                                                                                class="validate datepicker_ui">
                                                                                                            <label
                                                                                                                for="duty_date_{{ $list->id }}">
                                                                                                                Date
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div
                                                                                                        class="col m12 input-group">
                                                                                                        <div
                                                                                                            class="input-field ">
                                                                                                            <input
                                                                                                                id="reason_for_duty_{{ $list->id }}"
                                                                                                                type="text"
                                                                                                                value="{{ $list->reason_for_duty }}"
                                                                                                                name="reason_for_duty"
                                                                                                                class="validate ">
                                                                                                            <label
                                                                                                                for="reason_for_duty_{{ $list->id }}">
                                                                                                                Reason
                                                                                                                for duty
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div
                                                                                                        class="col m12 input-group">
                                                                                                        <div
                                                                                                            class="input-field ">
                                                                                                            <input
                                                                                                                id="place_of_duty_{{ $list->id }}"
                                                                                                                type="text"
                                                                                                                value="{{ $list->place_of_duty }}"
                                                                                                                name="place_of_duty"
                                                                                                                class="validate ">
                                                                                                            <label
                                                                                                                for="place_of_duty_{{ $list->id }}">
                                                                                                                Place of
                                                                                                                duty
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div
                                                                                                        class="col m12 input-group">
                                                                                                        <div
                                                                                                            class="input-field ">
                                                                                                            <input
                                                                                                                id="time_{{ $list->id }}"
                                                                                                                type="text"
                                                                                                                name="time"
                                                                                                                value="{{ $list->time }}"
                                                                                                                class="validate ">
                                                                                                            <label
                                                                                                                for="time_{{ $list->id }}">
                                                                                                                Time
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div
                                                                                                        class="input-field col m12">
                                                                                                        <select
                                                                                                            name="status">

                                                                                                            @if (count(status_list()))
                                                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                                                    <option
                                                                                                                        {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                                        value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                                                @endforeach
                                                                                                            @endif

                                                                                                        </select>

                                                                                                        <label>status</label>
                                                                                                    </div>


                                                                                                    <div
                                                                                                        class="input-field col m12">

                                                                                                        <button
                                                                                                            class="btn btn-primary">
                                                                                                            Update
                                                                                                        </button>

                                                                                                    </div>


                                                                                                </div>

                                                                                                <div
                                                                                                    class="col m2"></div>


                                                                                            </form>
                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                </div>
                                                                            </div>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="4">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal units"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ $module_name }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">


                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">


                                                                        <select id='select2'
                                                                                class="select2 employees_id"
                                                                                name="employees_id">

                                                                            <option value="">Select Employee</option>

                                                                            @if (count(getEmployees()))
                                                                                @foreach(getEmployees() AS $value)
                                                                                    <option
                                                                                        {{--                                                                                        {{ $item->employee_role_masters_id == $value->id ?'selected':'' }}--}}
                                                                                        value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>
                                                                        {{--                                                                        <label>  Employee Role </label>--}}


                                                                    </div>

                                                                    <small class="officer">
                                                                        Rank: <span class="rank">xxxxx</span>

                                                                        <br/>
                                                                        Number: <span class="number">xxxxx</span>
                                                                        <br/>
                                                                        Employee Role: <span class="role">xxxxx</span>
                                                                        <br/>
                                                                        Parent Unit : <span
                                                                            class="parent_unit">xxxxx</span>
                                                                        <br/>
                                                                        Mobile No : <span
                                                                            class="mobile_number">xxxxx</span>
                                                                        <br/>
                                                                        Residence Mobile : <span
                                                                            class="family_mobile_number">xxxxx</span>
                                                                    </small>

                                                                </div>


                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="duty_date0"
                                                                               type="text"
                                                                               name="duty_date"
                                                                               class="validate datepicker_ui">
                                                                        <label for="duty_date0">
                                                                            Date
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="reason_for_duty0"
                                                                               type="text"
                                                                               name="reason_for_duty"
                                                                               class="validate ">
                                                                        <label for="reason_for_duty0">
                                                                            Reason for duty
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="place_of_duty0"
                                                                               type="text"
                                                                               name="place_of_duty"
                                                                               class="validate ">
                                                                        <label for="place_of_duty0">
                                                                            Place of duty
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="time0"
                                                                               type="text"
                                                                               name="time"
                                                                               class="validate ">
                                                                        <label for="time0">
                                                                            Time
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>

        function getEmployee(empID) {


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.master.mt.get_emp') }}',
                type: 'POST',
                data: {'emp_id': empID},
                success: function (response) {
                    var resultdata = response;

                    if (resultdata.emp) {
                        $('.rank').html(resultdata.emp.get_rank.name)
                        $('.role').html(resultdata.emp.get_role.name)
                        $('.number').html(resultdata.emp.number)
                        $('.parent_unit').html(resultdata.emp.parent_unit)
                        $('.mobile_number').html(resultdata.emp.mobile_number)
                        $('.family_mobile_number').html(resultdata.emp.family_mobile_number)

                    }

                }
            });

        }


        $(function () {


            $('.employees_id').on('change', function () {
                getEmployee($(this).val())
            });


            $('.select2').formSelect('destroy')
            $('.select2').sm_select()


            $('.units').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        var parentInputGroup = e.parents('.input-group');
                        console.log(parentInputGroup)
                        if (parentInputGroup.length == 0) {
                            var placement = $(e).data('error');
                            if (placement) {
                                $(placement).append(error)
                            } else {
                                error.insertAfter(e);
                            }
                        } else {
                            e.parents('.input-group > div').append(error);
                        }
                    },
                    success: function (e) {
                        // e.removeClass('error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        // e.closest('.input-group').removeClass('error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        // e.closest('.error').remove();
                        e.closest('.input-group').find('div.error').remove();
                    },
                    rules: {
                        employees_id: {
                            required: true
                        },
                        duty_date: {
                            required: true,
                            validDate: true
                        },
                        reason_for_duty: {
                            required: true,

                        },
                        place_of_duty: {
                            required: true,
                        },
                        time: {
                            required: true,
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        employees_id: {
                            required: "Select Employee"
                        },
                        duty_date: {
                            required: "Select Date"
                        },
                        reason_for_duty: {
                            required: "Enter reason",

                        },

                        place_of_duty: {
                            required: "Enter Place",
                        },
                        time: {
                            required: "Enter Time",
                        },

                        status: {
                            required: "Select Status",
                        },


                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
