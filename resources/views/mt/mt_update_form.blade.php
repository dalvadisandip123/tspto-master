@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ $module_name }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>


                                    <div class="col s12 m10 l10 ">


                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="col s12">

                                                        <div class="card ">
                                                            <form role="form" class="form-horizontal units"
                                                                  method="post"
                                                                  enctype="multipart/form-data">

                                                                <div class="card-content ">
                                                                    <span
                                                                        class="card-title">Add {{ $module_name }}</span>


                                                                    @csrf

                                                                    <input type="hidden" name="action" value="add"/>

                                                                    <div class="row">
                                                                        {{--                                                    <div class="col m2"></div>--}}
                                                                        <div class="col m12">

                                                                            <div class="row">


                                                                                <div class="col m4 input-group">
                                                                                    <div class="input-field ">


                                                                                        <select
                                                                                            class="select2 vehicle_no"
                                                                                            name="vehicle_no">

                                                                                            <option value="">
                                                                                                Select Vehicle
                                                                                            </option>

                                                                                            @if (count($vehicle_lists))
                                                                                                @foreach($vehicle_lists AS $value)
                                                                                                    <option
                                                                                                        {{  imputOldValue('vehicle_no') ==$value->id ?'selected':'' }}
                                                                                                        value="{{ $value->vehicle_no }}"
                                                                                                        data-make="{{ $value->getVehicleMake->vehicle_make }}"
                                                                                                        data-type="{{ $value->getVehicleType->vehicle_type }}"
                                                                                                    >{{  $value->vehicle_no }}</option>
                                                                                                @endforeach
                                                                                            @endif

                                                                                        </select>

                                                                                        <small class="vehicle">
                                                                                            Make: <span
                                                                                                class="vehicle_box_make">
                                                                                                {{ $vehicle_info->getVehicleMake->vehicle_make }}
                                                                                            </span>

                                                                                            <br/>
                                                                                            Type: <span
                                                                                                class="vehicle_box_type">
                                                                                                 {{ $vehicle_info->getVehicleType->vehicle_type }}
                                                                                            </span>
                                                                                            <br/>
                                                                                            variant: <span
                                                                                                class="vehicle_box_variant">
                                                                                                 {{ $vehicle_info->getVehicleVariant->vehicle_variant }}
                                                                                            </span>
                                                                                            <br/>
                                                                                            model: <span
                                                                                                class="vehicle_box_model">
                                                                                                {{ $vehicle_info->model }}
                                                                                            </span>
                                                                                            <br/>
                                                                                            category: <span
                                                                                                class="vehicle_box_category">
                                                                                                 {{ $vehicle_info->getCategory->vehicle_category }}

                                                                                            </span>
                                                                                        </small>
                                                                                    </div>

                                                                                </div>

                                                                                <div class="col m4">

                                                                                    <div
                                                                                        class="input-field ">
                                                                                        <input
                                                                                            id="vehicle_current_meter_reading"
                                                                                            type="text"

                                                                                            name="relieve_date"
                                                                                            class="validate ">
                                                                                        <label
                                                                                            for="vehicle_current_meter_reading">
                                                                                            Vehicle Current Meter
                                                                                            reading
                                                                                        </label>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col m4 input-group">
                                                                                    <div
                                                                                        class="input-field ">
                                                                                        <input
                                                                                            id="date"
                                                                                            type="text"
                                                                                            value="{{ date('d-m-Y') }}"
                                                                                            name="relieve_date"
                                                                                            class="validate datepicker_ui">
                                                                                        <label
                                                                                            for="date">
                                                                                            Date:*(Month & year)
                                                                                        </label>
                                                                                    </div>

                                                                                </div>

                                                                            </div>

                                                                            <?php
//                                                                            dump($vehicle_info->getBatteryHistory);
//                                                                            dump($vehicle_info->geteTyreHistory);
//                                                                            dump($vehicle_info)
                                                                            ?>

{{--                                                                            <div class="row">--}}

{{--                                                                                --}}
{{--                                                                                <div class="col m4"></div>--}}


{{--                                                                            </div>--}}
                                                                            <div class="row">


                                                                                <div class="col m12">
                                                                                    <div class="card">

                                                                                        <div class="card-content">
                                                                                            <span class="card-title"> Tyers  Info </span>


                                                                                            @php

                                                                                                $no_tyers = count($getExistingTyres)+4;
                                                                                            @endphp

                                                                                            @if (count($getExistingTyres) <= $no_tyers)

                                                                                                @if ($no_tyers > 0)

                                                                                                    @for ($i = 0; $i < $no_tyers; $i++)

                                                                                                        @php
                                                                                                            $itemarray = isset($getExistingTyres[$i]) ? $getExistingTyres[$i] : '';
                                                                                                            $Value_tyre_life_span = ($itemarray != "" && $itemarray->tyre_life_span != '') ? $itemarray->tyre_life_span : '';
                                                                                                            $Value_tyre_issue_date = ($itemarray != "" && $itemarray->tyre_issue_date != '') ? date('d-m-Y', strtotime($itemarray->tyre_issue_date)) : '';
                                                                                                        $getTyremake = getTyremake();
                                                                                                            $Value_tyre_tyre_end_date = ($itemarray != "" && $itemarray->tyre_end_date != '') ? date('d-m-Y', strtotime($itemarray->tyre_end_date)) : '';
                                                                                                            $Value_tyre_number = ($itemarray != "" && $itemarray->tyre_number != '') ? $itemarray->tyre_number : '';
                                                                                                        @endphp



                                                                                                        <div class="row tyresLists">
                                                                                                            <div class="col m3">
                                                                                                                <div class="input-group">
                                                                                                                    <div class="input-field ">
                                                                                                                        <select
                                                                                                                            class="tyre_make"
                                                                                                                            name="tyre[{{$i}}][tyre_make]">';

                                                                                                                            @if (count($getTyremake) > 0)

                                                                                                                                @foreach ($getTyremake AS $getTyremak)

                                                                                                                                    @php
                                                                                                                                        $selectedVal = ($itemarray != "" && $getTyremak->id == $itemarray->tyre_make) ? "selected" : "";
                                                                                                                                    @endphp
                                                                                                                                    <option
                                                                                                                                        {{ $selectedVal }}  value="{{ $getTyremak->id }}">{{ $getTyremak->tyre_make }}</option>

                                                                                                                                @endforeach
                                                                                                                            @endif


                                                                                                                        </select>
                                                                                                                        <label
                                                                                                                            for="tyre_make_{{$i}}">
                                                                                                                            Make
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>


                                                                                                            <div class="col m2">
                                                                                                                <div class="input-group">
                                                                                                                    <div class="input-field ">

                                                                                                                        <input
                                                                                                                            id="tyre_life_span_{{$i}}"
                                                                                                                            type="text"
                                                                                                                            value="{{ $Value_tyre_life_span }}"
                                                                                                                            name="tyre[{{$i}}][tyre_life_span]"
                                                                                                                            class="validate tyre_life_span">
                                                                                                                        <label
                                                                                                                            for="tyre_life_span_{{$i}}"
                                                                                                                            class="active">
                                                                                                                            Life
                                                                                                                            Span
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>


                                                                                                            <div class="col m3">
                                                                                                                <div class="input-group">
                                                                                                                    <div class="input-field ">


                                                                                                                        <input
                                                                                                                            id="tyre_issue_date_{{$i}}"
                                                                                                                            type="text"
                                                                                                                            value="{{ $Value_tyre_issue_date }}"
                                                                                                                            name="tyre[{{$i}}][tyre_issue_date]"
                                                                                                                            class="validate datepicker_ui tyre_issue_date ">
                                                                                                                        <label
                                                                                                                            for="tyre_issue_date_{{$i}}"
                                                                                                                            class="active">
                                                                                                                            issue
                                                                                                                            date
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col m3">
                                                                                                                <div class="input-group">
                                                                                                                    <div class="input-field ">


                                                                                                                        <input
                                                                                                                            id="tyre_end_date_{{$i}}"
                                                                                                                            type="text"
                                                                                                                            value="{{ $Value_tyre_tyre_end_date }}"
                                                                                                                            name="tyre[{{$i}}][tyre_end_date]"
                                                                                                                            class="validate datepicker_ui tyre_issue_date ">
                                                                                                                        <label
                                                                                                                            for="tyre_end_date_{{$i}}"
                                                                                                                            class="active">
                                                                                                                            End
                                                                                                                            date
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>


                                                                                                            <div class="col m2">
                                                                                                                <div class="input-group">
                                                                                                                    <div class="input-field ">



                                                                                                                        <input
                                                                                                                            id="tyre_number_{{$i}}"
                                                                                                                            type="text"
                                                                                                                            value="{{ $Value_tyre_number }}"
                                                                                                                            name="tyre[{{$i}}][tyre_number]"
                                                                                                                            class="validate tyre_number">
                                                                                                                        <label
                                                                                                                            for="tyre_number_{{$i}}"
                                                                                                                            class="active">
                                                                                                                            Number
                                                                                                                        </label>

                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>


                                                                                                        </div>


                                                                                                    @endfor

                                                                                                @endif
                                                                                            @endif


                                                                                        </div>
                                                                                    </div>


                                                                                    <div class="card">

                                                                                        <div class="card-content">
                                                                                            <span class="card-title"> Bataries  Info </span>
                                                                                            <div class="bataries_box">
                                                                                                <div class="row ">

                                                                                                    @php
                                                                                                        $no_bataries = count($getExistingData)+2;

                                                                                                    dump($getExistingData)
                                                                                                    @endphp



                                                                                                    @if (count($getExistingData) <= $no_bataries)


                                                                                                        @if ($no_bataries > 0)
                                                                                                            @for ($i = 0; $i < $no_bataries; $i++)

                                                                                                                @php
                                                                                                                    $itemarray = isset($getExistingData[$i]) ? $getExistingData[$i] : '';

                                                                                                                    $Value_battery_voltage = ($itemarray != "" && $itemarray->battery_voltage != '') ? $itemarray->battery_voltage : '';
                                                                                                                    $Value_battery_size = ($itemarray != "" && $itemarray->battery_size != '') ? $itemarray->battery_size : '';
                                                                                                                    $Value_battery_ampere = ($itemarray != "" && $itemarray->battery_ampere != '') ? $itemarray->battery_ampere : '';
                                                                                                                    $Value_battery_life_span = ($itemarray != "" && $itemarray->battery_life_span != '') ? $itemarray->battery_life_span : '';
                                                                                                                    $Value_battery_number = ($itemarray != "" && $itemarray->battery_number != '') ? $itemarray->battery_number : '';
                                                                                                                    $Value_battery_cost = ($itemarray != "" && $itemarray->cost != '') ? $itemarray->cost : '';
                                                                                                                    $Value_battery_issue_date = ($itemarray != "" && $itemarray->battery_issue_date != '') ?date('d-m-Y', strtotime($itemarray->battery_issue_date)) : '';
                                                                                                                    $Value_battery_end_date = ($itemarray != "" && $itemarray->battery_end_date != '') ?date('d-m-Y', strtotime($itemarray->battery_end_date)) : '';
                                                                                                                @endphp
                                                                                                                <div
                                                                                                                    class="row bataries_list">


                                                                                                                    <div
                                                                                                                        class="col m2">
                                                                                                                        <div
                                                                                                                            class="input-group">
                                                                                                                            <div
                                                                                                                                class="input-field ">


                                                                                                                                <select
                                                                                                                                    name="battery[{{$i}}][battery_make]">';


                                                                                                                                    @php
                                                                                                                                        $getBatteriesmake = getBatteriesmake();
                                                                                                                                    @endphp


                                                                                                                                    @if (count($getBatteriesmake) > 0)
                                                                                                                                        @foreach ($getBatteriesmake AS $getBatteriesmak)
                                                                                                                                            @php
                                                                                                                                                $selectedVal = ($itemarray != "" && $getBatteriesmak->id == $itemarray->battery_make) ? "selected" : "";
                                                                                                                                            @endphp
                                                                                                                                            <option
                                                                                                                                                {{ $selectedVal }} value="{{ $getBatteriesmak->id }}"> {{$getBatteriesmak->battery_make}} </option>
                                                                                                                                        @endforeach

                                                                                                                                    @endif


                                                                                                                                </select>


                                                                                                                                <label
                                                                                                                                    for="battery_make_{{$i}}">
                                                                                                                                    Make
                                                                                                                                </label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>


                                                                                                                    <div
                                                                                                                        class="col m2">
                                                                                                                        <div
                                                                                                                            class="input-group">
                                                                                                                            <div
                                                                                                                                class="input-field ">
                                                                                                                                <input
                                                                                                                                    id="battery_life_span_{{$i}}"
                                                                                                                                    type="text"
                                                                                                                                    value="{{ $Value_battery_life_span }}"
                                                                                                                                    name="battery[{{$i}}][battery_life_span]"
                                                                                                                                    class="validate battery_life_span">
                                                                                                                                <label
                                                                                                                                    for="battery_life_span_{{$i}}"
                                                                                                                                    class="active">
                                                                                                                                    Life
                                                                                                                                    Span
                                                                                                                                </label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div
                                                                                                                        class="col m2">
                                                                                                                        <div
                                                                                                                            class="input-group">
                                                                                                                            <div
                                                                                                                                class="input-field ">
                                                                                                                                <input
                                                                                                                                    id="battery_number_{{$i}}"
                                                                                                                                    type="text"
                                                                                                                                    value="{{ $Value_battery_number }}"
                                                                                                                                    name="battery[{{$i}}][battery_number]"
                                                                                                                                    class="validate battery_number">
                                                                                                                                <label
                                                                                                                                    for="battery_number_{{$i}}"
                                                                                                                                    class="active">
                                                                                                                                    Number
                                                                                                                                </label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div
                                                                                                                        class="col m2">
                                                                                                                        <div
                                                                                                                            class="input-group">
                                                                                                                            <div
                                                                                                                                class="input-field ">
                                                                                                                                <input
                                                                                                                                    id="cost_{{$i}}"
                                                                                                                                    type="text"
                                                                                                                                    value="{{ $Value_battery_cost }}"
                                                                                                                                    name="battery[{{$i}}][cost]"
                                                                                                                                    class="validate battery_number">
                                                                                                                                <label
                                                                                                                                    for="cost_{{$i}}"
                                                                                                                                    class="active">
                                                                                                                                    Cost
                                                                                                                                </label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div
                                                                                                                        class="col m2">
                                                                                                                        <div
                                                                                                                            class="input-group">
                                                                                                                            <div
                                                                                                                                class="input-field ">
                                                                                                                                <input
                                                                                                                                    id="battery_issue_date_{{$i}}"
                                                                                                                                    type="text"
                                                                                                                                    value="{{ $Value_battery_issue_date }}"
                                                                                                                                    name="battery[{{$i}}][battery_issue_date]"
                                                                                                                                    class="validate datepicker_ui battery_issue_date">
                                                                                                                                <label
                                                                                                                                    for="battery_issue_date_{{$i}}"
                                                                                                                                    class="active">
                                                                                                                                    Issue
                                                                                                                                    Date
                                                                                                                                </label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div
                                                                                                                        class="col m2">
                                                                                                                        <div
                                                                                                                            class="input-group">
                                                                                                                            <div
                                                                                                                                class="input-field ">
                                                                                                                                <input
                                                                                                                                    id="battery_end_date_{{$i}}"
                                                                                                                                    type="text"
                                                                                                                                    value="{{ $Value_battery_end_date }}"
                                                                                                                                    name="battery[{{$i}}][battery_end_date]"
                                                                                                                                    class="validate datepicker_ui battery_end_date">
                                                                                                                                <label
                                                                                                                                    for="battery_end_date_{{$i}}"
                                                                                                                                    class="active">
                                                                                                                                    End
                                                                                                                                    Date
                                                                                                                                </label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>


                                                                                                                </div>
                                                                                                            @endfor


                                                                                                        @endif
                                                                                                    @endif


                                                                                                </div>


                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <div class="card-action">

                                                                    <button class="btn btn-primary">
                                                                        Save
                                                                    </button>

                                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                                </div>

                                                            </form>

                                                        </div>


                                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>

        function getVehicle(vhicleID) {

            var type = vhicleID.find('option:selected').data('type');
            var make = vhicleID.find('option:selected').data('make');

            $('.vehicle_box_type').html(type);
            $('.vehicle_box_make').html(make);


        }

        $(function () {


            $('.vehicle_no').on('change', function () {
                getVehicle($(this))
            });


            $('.vehicle_no').on('change', function () {
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = '{{ route('site.mt.mt_update_form') }}/' + url; // redirect
                }
                return false;
            });


            $('.units').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        var parentInputGroup = e.parents('.input-group');
                        console.log(parentInputGroup)
                        if (parentInputGroup.length == 0) {
                            var placement = $(e).data('error');
                            if (placement) {
                                $(placement).append(error)
                            } else {
                                error.insertAfter(e);
                            }
                        } else {
                            e.parents('.input-group > div').append(error);
                        }
                    },
                    success: function (e) {
                        // e.removeClass('error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        // e.closest('.input-group').removeClass('error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        // e.closest('.error').remove();
                        e.closest('.input-group').find('div.error').remove();
                    },
                    rules: {
                        employees_id: {
                            required: true
                        },
                        units_masters_id: {
                            required: true
                        },
                        atached_date: {
                            required: true,
                            validDate: true
                        },
                        return_date: {
                            validDate: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        employees_id: {
                            required: "Select Employee"
                        },
                        units_masters_id: {
                            required: "Select Unit"
                        },
                        atached_date: {
                            required: "Enter Atached Date",

                        },
                        return_date: {
                            required: "Enter return Date",
                        },
                        status: {
                            required: "Select Status",
                        },


                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
