@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>


                                    <div class="col s12 m10 l10 ">
                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.employee_attendance', ['action'=>'new']) }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Add

                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        {{ form_flash_message('flash_message') }}
                                                    </div>


                                                    <div class="col l12">

                                                        <form action="" method="get">
                                                            <div class="row">
                                                                <div class="col s12  l3 input-group">
                                                                    <div
                                                                        class="input-field ">


                                                                        <select
                                                                            id='select2'
                                                                            class="select2"
                                                                            name="search_employees_id">

                                                                            <option
                                                                                value="">
                                                                                Select
                                                                                Employee
                                                                            </option>

                                                                            @if (count(getEmployees()))
                                                                                @foreach(getEmployees() AS $value)
                                                                                    <option
                                                                                        {{ request()->search_employees_id == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>
                                                                        {{--                                                                        <label>  Employee Role </label>--}}


                                                                    </div>

                                                                </div>

                                                                <div class="col s12  l3">
                                                                    <div class="input-field col m12">
                                                                        <select name="search_role"
                                                                        >
                                                                            <option value="">All</option>
                                                                            @if (count(getEmpRoles()))
                                                                                @foreach(getEmpRoles() AS $role)
                                                                                    <option
                                                                                        {{  request()->search_role== $role->id ?'selected':'' }}
                                                                                        value="{{ $role->id }}">{{  $role->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        {{--                                                                                <label>status</label>--}}
                                                                    </div>
                                                                </div>

                                                                <div class="col s12  l2">
                                                                    <div class="input-field col m12">
                                                                        <input id="from_date_search" type="text"
                                                                               name="from_date_search"
                                                                               class="datepicker_ui"
                                                                               value="{{ request()->from_date_search }}">
                                                                        <label
                                                                            for="from_date_search">
                                                                            From Date
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col s12  l2">
                                                                    <div class="input-field col m12">
                                                                        <input id="to_date_search" type="text"
                                                                               name="to_date_search"
                                                                               class="datepicker_ui"
                                                                               value="{{ request()->to_date_search }}">
                                                                        <label
                                                                            for="to_date_search">
                                                                            To Date
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                                <div class="col s12 m6 l2 display-flex  show-btn">
                                                                    <button type="submit"
                                                                            class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                        Filter Now
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>


                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="Years">Name</th>
                                                                <th data-field="Employee Id">Role</th>
                                                                <th data-field="attendance">attendance</th>
                                                                <th data-field="Attendance Date">Attendance Date</th>
                                                                <th data-field="Status">Status</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getEmployee->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getRole->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ attendanceTypes($list->attendance) }}

                                                                        </td>
                                                                        <td>
                                                                            {{ $list->attendance_date!='' ? date('d-m-Y', strtotime($list->attendance_date)):'' }}


                                                                        </td>
                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>

                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="7">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>


                                                        <div class="pagination">
                                                            <div class="col m12 left-align">

                                                                Showing {{ $lists->firstItem() }}
                                                                to {{ $lists->lastItem() }} of {{ $lists->total() }}
                                                                entries

                                                            </div>
                                                            <div class="col m12 right-align">

                                                                {{$lists->links('vendor.pagination.materializecss')}}

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.select2').formSelect('destroy')
            $('.select2').sm_select()




        });

    </script>
@endsection
