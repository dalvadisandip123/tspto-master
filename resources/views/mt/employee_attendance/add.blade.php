@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">
                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.employee') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>


                                            <div class="col l12">

                                                <?php
//                                                dump($old_attnd)
                                                ?>

                                                <form action="" method="get">
                                                    <div class="row">
                                                        <div class="col s12  l3">


                                                            <div
                                                                class="col m12 input-group">
                                                                <div
                                                                    class="input-field ">
                                                                    <input
                                                                        id="attnd_date0"
                                                                        type="text"
                                                                        value="{{ request()->attnd_date!='' ? date('d-m-Y', strtotime(request()->attnd_date)):'' }}"
                                                                        name="attnd_date"
                                                                        class="validate datepicker_ui">
                                                                    <label
                                                                        for="attnd_date0">
                                                                        Date
                                                                    </label>
                                                                </div>
                                                            </div>


                                                        </div>

                                                        <div class="col s12  l3">
                                                            <div class="input-field col m12">
                                                                <select name="role" onchange="this.form.submit()">
                                                                    <option value="">All</option>
                                                                    @if (count(getEmpRoles()))
                                                                        @foreach(getEmpRoles() AS $role)
                                                                            <option
                                                                                {{  request()->role== $role->id ?'selected':'' }}
                                                                                value="{{ $role->id }}">{{  $role->name }}</option>
                                                                        @endforeach
                                                                    @endif

                                                                </select>

                                                                {{--                                                                                <label>status</label>--}}
                                                            </div>
                                                        </div>


                                                        {{--                                                        <div class="col s12 m6 l3 display-flex  show-btn">--}}
                                                        {{--                                                            <button type="submit"--}}
                                                        {{--                                                                    class="btn btn-small align-items-right btn-block indigo waves-effect waves-light">--}}
                                                        {{--                                                               Get Employees--}}
                                                        {{--                                                            </button>--}}
                                                        {{--                                                        </div>--}}
                                                    </div>
                                                </form>


                                            </div>


                                        </div>


                                        @if(count($old_attnd)> 0 )

                                            <div class="col m12">
                                                <div class="card-alert card red">
                                                    <div class="card-content white-text">
                                                        <p>DANGER : this role and Employees lready submited
                                                            Attendence </p>
                                                    </div>
                                                    <button type="button" class="close white-text" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                            </div>

                                        @else

                                            <div id="bordered-table" class="card card card-default scrollspy">
                                                <div class="card-content">


                                                    <form role="form" class="form-horizontal manageForm"
                                                          method="post"
                                                          enctype="multipart/form-data">


                                                        <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                                        @csrf

                                                        <input type="hidden" name="action" value="add"/>
                                                        <input type="hidden" name="role" value="{{ request()->role }}"/>
                                                        <input type="hidden" name="attendance_date"
                                                               value="{{ request()->attnd_date!='' ? date('d-m-Y', strtotime(request()->attnd_date)):'' }}"/>

                                                        <div class="row">

                                                            <div class="col- m12">

                                                                <table class="bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <th data-field="id">#</th>
                                                                        <th data-field="Employee Name">Employee Name</th>
                                                                        <th data-field="Rank & No">Rank & No</th>
                                                                        <th data-field="Mobile">Attendance</th>
                                                                        <th data-field="Driving Licence">Cell Number</th>

                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @if($emp_lists!='' && $emp_lists->count() > 0)
                                                                        @foreach($emp_lists AS $emp_list)
                                                                            <input type="hidden"
                                                                                   name="emp[{{$loop->iteration}}][employees_id]"
                                                                                   value="{{ request()->role }}"/>
                                                                            <tr>
                                                                                <td>
                                                                                    {{ $loop->iteration  }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $emp_list->name }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $emp_list->getRank->name }}
                                                                                </td>
                                                                                <td>

                                                                                    <select
                                                                                        name="emp[{{$loop->iteration}}][attendance]">

                                                                                        @if (count(attendanceTypes()))
                                                                                            @foreach(attendanceTypes() AS $key=>$value)
                                                                                                <option
                                                                                                    {{  imputOldValue('attendance') ==$key ?'selected':'' }}

                                                                                                    value="{{ $key }}">{{  $value }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>


                                                                                </td>
                                                                                <td>
                                                                                    {{ $emp_list->mobile_number }}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>


                                                        </div>
                                                        <div class="card-action">

                                                            <button type="submit" class="btn btn-primary">
                                                                Save
                                                            </button>

                                                            {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                            {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                        </div>


                                                    </form>


                                                </div>
                                            </div>

                                        @endif




                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    <script>



        $(function () {




        });

    </script>
@endsection
