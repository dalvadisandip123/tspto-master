@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>


                                    <div class="col s12 m10 l10 ">
                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.issue_voucher', ['action'=>'new']) }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Add

                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        {{ form_flash_message('flash_message') }}
                                                    </div>


                                                    <div class="col l12">

                                                        <form action="" method="get">
                                                            <div class="row">
                                                                <div class="col s12  l3 input-group">
                                                                    <div
                                                                        class="input-field ">

                                                                        <input id="search_vehicle_no" type="text"
                                                                               name="search_vehicle_no"
                                                                               class=""
                                                                               value="{{ request()->vehicle_no }}">

                                                                        <label for="search_vehicle_no"> vehicle
                                                                            no </label>


                                                                    </div>

                                                                </div>


                                                                <div class="col s12  l2">
                                                                    <div class="input-field col m12">
                                                                        <input id="search_out_from_date" type="text"
                                                                               name="search_out_from_date"
                                                                               class="datepicker_ui"
                                                                               value="{{ request()->search_out_from_date }}">
                                                                        <label
                                                                            for="search_out_from_date">
                                                                            From Out from Date
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col s12  l2">
                                                                    <div class="input-field col m12">
                                                                        <input id="search_out_to_date" type="text"
                                                                               name="search_out_to_date"
                                                                               class="datepicker_ui"
                                                                               value="{{ request()->search_out_to_date }}">
                                                                        <label
                                                                            for="search_out_to_date">
                                                                            To out to Date
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                                <div class="col s12 m6 l2 display-flex  show-btn">
                                                                    <button type="submit"
                                                                            class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                        Filter Now
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>


                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="Years">Unit</th>
                                                                <th data-field="Years">vehicle_no</th>
                                                                <th data-field="Employee Id">Issue Voucher No</th>
                                                                <th data-field="Attendance Date">CO Allotment No</th>
                                                                <th data-field="attendance">Recevier Name</th>
                                                                <th data-field="attendance">issue_voucher_date</th>
                                                                <th data-field="Status">Status</th>
                                                                <th data-field="Action">Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getUnit->unit_name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->vehicle_no }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->issue_voucher_no }} /
                                                                            {{ $list->issue_voucher_type }} /
                                                                            {{ $list->issue_voucher_year }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $list->co_allotment_no }} /
                                                                            {{ $list->co_allotment_no2 }} /
                                                                            {{ $list->co_allotment_year }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $list->recevier_name }}
                                                                        </td>


                                                                        <td>
                                                                            {{ $list->issue_voucher_date!='' ? date('d-m-Y', strtotime($list->issue_voucher_date)):'' }}


                                                                        </td>

                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>

                                                                        <td>

                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="{{ route('site.mt.issue_voucher', ['action'=>'view', 'id'=> $list->id]) }}">
                                                                                View
                                                                            </a>
                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="{{ route('site.mt.issue_voucher', ['action'=>'edit', 'id'=> $list->id]) }}">
                                                                                Edit
                                                                            </a>


                                                                            <form method="POST"
                                                                                  action="{{ route('site.mt.issue_voucher', ['action'=>'remove', 'id'=> $list->id]) }}"
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>

                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="7">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>


                                                        <div class="pagination">
                                                            <div class="col m12 left-align">

                                                                Showing {{ $lists->firstItem() }}
                                                                to {{ $lists->lastItem() }} of {{ $lists->total() }}
                                                                entries

                                                            </div>
                                                            <div class="col m12 right-align">

                                                                {{$lists->links('vendor.pagination.materializecss')}}

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.select2').formSelect('destroy')
            $('.select2').sm_select()


        });

    </script>
@endsection
