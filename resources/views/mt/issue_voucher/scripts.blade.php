<script>


    function getUnit(vhicleID) {

        var selectUnitHead = vhicleID.find('option:selected').data('unithead');
        var unitCode = vhicleID.find('option:selected').data('unitcode');

        $('.issued_to').html(selectUnitHead);
        $('.vehicle_issued_to').val(selectUnitHead);
        $('.issue_voucher_no').val(unitCode);


    }

    function getVehicle(e) {


        console.log(e.target.options);


        var opts = e.target.options;


        var content = ""


        for (var i = 0; i < opts.length; i++) {

            if (opts[i].selected) {

                var selectItem = opts[i];

                console.log('sssss' + selectItem.dataset.make)


                var vehicle_no = opts[i].value;
                var make = selectItem.dataset.make;
                var type = selectItem.dataset.type;
                var engine_no = selectItem.dataset.engine_no;
                var chassis_no = selectItem.dataset.chassis_no;
                var vehicle_cost = selectItem.dataset.vehicle_cost;
                var po_date = selectItem.dataset.po_date;
                var condition = selectItem.dataset.condition;

                content += '<tr>' +
                    '<td>' + vehicle_no + '</td>' +
                    '<td>' + po_date + '</td>' +
                    '<td>' + vehicle_cost + '</td>' +
                    '<td>' + condition + '</td>' +
                    '<td>' + make + '</td>' +
                    '<td>' + type + '</td>' +
                    '<td>' + engine_no + '</td>' +
                    '<td>' + chassis_no + '</td>' +
                    '</tr>';

                console.log(content)

            }
        }

        content += ""

        $('#vehicle_table_data').html(content);
        // $('.vehicle_table_data').html(content)


        // $.each(opts[0], function () {
        //
        // console.log(make);
        // });


        //
        // console.log(engine_no);

        //
        // $('.driver_box_' + no).find('.mobile').html(mobile);
    }


    $(function () {

        $('.unit_select').each(function () {
            getUnit($(this))
        })


        $('.unit_select').on('change', function () {
            getUnit($(this))
        });

        $('.vehicle_select').on('change', function (e) {
            getVehicle(e)
        });


        $('.manageForm').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',

                errorPlacement: function (error, e) {

                    var parentInputGroup = e.parents('.input-group');

                    console.log(parentInputGroup)

                    if (parentInputGroup.length == 0) {

                        var placement = $(e).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(e);
                        }

                    } else {
                        e.parents('.input-group > div').append(error);

                    }


                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },


                rules: {
                    vehicle_detail_id: {
                        required: true
                    },
                    vehicle_issued_to: {
                        required: true
                    },
                    issue_voucher_no: {
                        required: true,

                    },
                    issue_voucher_type: {
                        required: true,

                    },
                    issue_voucher_year: {
                        required: true,
                    },
                    issue_voucher_date: {
                        required: true,
                        validDate: true
                    },
                    co_allotment_no: {
                        required: true,
                    },
                    co_allotment_no2: {
                        required: true,

                    },
                    co_allotment_year: {
                        required: true,

                    },

                    co_allotment_date: {
                        required: true,
                        validDate: true
                    },
                    vehicle_allotment_purpose: {
                        required: true,
                    },
                    vehicle_charged_off_today: {
                        required: true,
                        validDate: true
                    },
                    recevier_name: {
                        required: true,
                    },
                    recevier_designation: {
                        required: true,
                    },
                    unit_head: {
                        required: true,
                    },
                    purpose_text: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                },
                messages: {

                    status: {
                        required: "Select Status",
                    },

                },
            });

        })


    });

</script>
