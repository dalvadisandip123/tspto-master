@extends('layout')
@section('title', $title)

@section('headerStyles')


    <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css">

    <style>
        @media print {
            @page {
                size: A4;
                margin-left: -8cm;
                margin-right: -0cm;
                margin-top: -2cm;
                margin-bottom: -5cm;
            }
            body { margin: 1.6cm; }
        }

    </style>

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.issue_voucher') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <h3
                                            class="card-title">Edit {{ ucwords($module_name) }}
                                        </h3>
                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row printview" id="printview">

                                                    <?php
                                                    //                                                    dump($item)
                                                    ?>

                                                    <div class="col m12">

                                                        <div class="row">
                                                            <div class="col m6">
                                                                <b>
                                                                    ISSUE VOUCHER OF MOTOR VEHICLE <br>
                                                                </b>

                                                                <table>
                                                                    <tr>
                                                                        <td> Vehicle Issued To :</td>
                                                                        <td> {{ $item->vehicle_issued_to }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Issue Voucher No :
                                                                        </td>
                                                                        <td>
                                                                            {{ $item->issue_voucher_no}}/{{
                                                                            $item->issue_voucher_type}}/{{ $item->issue_voucher_year}}
                                                                            &nbsp;&nbsp;&nbsp;&nbsp; Date
                                                                            : {{ date('d-m-Y', strtotime($item->issue_voucher_date)) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            CO Allotment No :
                                                                        </td>
                                                                        <td>
                                                                            {{ $item->co_allotment_no}}/{{
                                                                            $item->co_allotment_no2}}/{{ $item->co_allotment_year}}

                                                                            &nbsp;&nbsp;&nbsp;&nbsp; Date
                                                                            : {{ date('d-m-Y', strtotime($item->co_allotment_date)) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Vehicle Allotment Purpose :</td>
                                                                        <td>
                                                                            {{ $item->vehicle_allotment_purpose}}
                                                                        </td>
                                                                    </tr>

                                                                </table>


                                                                <b>
                                                                    VEHICLE DETAILS
                                                                </b>

                                                                <table>
                                                                    <tr>
                                                                        <td>

                                                                            <table>
                                                                                <tr>
                                                                                    <td> Vehicle No :</td>
                                                                                    <td>
                                                                                        {{ $item->vehicle_no }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td> Pur Date :</td>
                                                                                    <td>
                                                                                        {{ date('d-m-Y', strtotime($item->getVehicle->po_date)) }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Vehicle Cost :
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->vehicle_cost }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Vehicle Cond :
                                                                                    </td>
                                                                                    <td>
                                                                                        New
                                                                                    </td>
                                                                                </tr>
                                                                            </table>


                                                                        </td>
                                                                        <td>

                                                                            <table>
                                                                                <tr>
                                                                                    <td> Vehicle Make :</td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->getVehicleMake->vehicle_make }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td> Vehicle Type :</td>
                                                                                    <td>
                                                                                        {{  $item->getVehicle->getVehicleType->vehicle_type }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Engine No :
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->engine_no }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Cha. No :
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->chassis_no }}
                                                                                    </td>
                                                                                </tr>
                                                                            </table>


                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                                <div class="row">

                                                                    <div class="col m12">
                                                                        <ul style="margin: 2px 0px; padding: 4px 0px">
                                                                            <li>*
                                                                                <label>Log Book With Vehicle</label>
                                                                            </li>
                                                                            <li>*
                                                                                <label>KMPL &amp; Battery No of the
                                                                                    Vehicle Furnished at Log Book
                                                                                    Page No 1</label>
                                                                            </li>
                                                                            <li>*
                                                                                <label>Tyre No Are furnished at Log
                                                                                    Book Page No. 33</label>
                                                                            </li>
                                                                            <li>*
                                                                                <label>Tools / Accessories furnished
                                                                                    at Log Book Page No. 41</label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <hr style="border: 1px dashed black;"/>


                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="col m12 pt-2">
                                                                        Ceritified that the above Vehicle has
                                                                        been
                                                                        charged off today
                                                                        Date :
                                                                        {{ date('d-m-Y', strtotime($item->vehicle_charged_off_today)) }}
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Signature :
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Designation :
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Unit :
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="col m12 pt-2">
                                                                        Ceritified that the above Vehicle has
                                                                        been Received & taken on Charge by me
                                                                        today <br>
                                                                        | Date : {{ date('d-m-Y') }}

                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Signature :

                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Recevier Name :
                                                                        {{$item->recevier_name}}
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Designation :
                                                                        {{$item->recevier_designation}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>


                                                        <div style=" text-align: center; padding: 5px  0px">
                                                            <hr style="border: 1px dotted black; width: 60%"/>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col m6">
                                                                <b>
                                                                    ISSUE VOUCHER OF MOTOR VEHICLE <br>
                                                                </b>

                                                                <table>
                                                                    <tr>
                                                                        <td> Vehicle Issued To :</td>
                                                                        <td> {{ $item->vehicle_issued_to }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Issue Voucher No :
                                                                        </td>
                                                                        <td>
                                                                            {{ $item->issue_voucher_no}}/{{
                                                                            $item->issue_voucher_type}}/{{ $item->issue_voucher_year}}
                                                                            &nbsp;&nbsp;&nbsp;&nbsp; Date
                                                                            : {{ date('d-m-Y', strtotime($item->issue_voucher_date)) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            CO Allotment No :
                                                                        </td>
                                                                        <td>
                                                                            {{ $item->co_allotment_no}}/{{
                                                                            $item->co_allotment_no2}}/{{ $item->co_allotment_year}}

                                                                            &nbsp;&nbsp;&nbsp;&nbsp; Date
                                                                            : {{ date('d-m-Y', strtotime($item->co_allotment_date)) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Vehicle Allotment Purpose :</td>
                                                                        <td>
                                                                            {{ $item->vehicle_allotment_purpose}}
                                                                        </td>
                                                                    </tr>

                                                                </table>


                                                                <b>
                                                                    VEHICLE DETAILS
                                                                </b>

                                                                <table>
                                                                    <tr>
                                                                        <td>

                                                                            <table>
                                                                                <tr>
                                                                                    <td> Vehicle No :</td>
                                                                                    <td>
                                                                                        {{ $item->vehicle_no }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td> Pur Date :</td>
                                                                                    <td>
                                                                                        {{ date('d-m-Y', strtotime($item->getVehicle->po_date)) }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Vehicle Cost :
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->vehicle_cost }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Vehicle Cond :
                                                                                    </td>
                                                                                    <td>
                                                                                        New
                                                                                    </td>
                                                                                </tr>
                                                                            </table>


                                                                        </td>
                                                                        <td>

                                                                            <table>
                                                                                <tr>
                                                                                    <td> Vehicle Make :</td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->getVehicleMake->vehicle_make }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td> Vehicle Type :</td>
                                                                                    <td>
                                                                                        {{  $item->getVehicle->getVehicleType->vehicle_type }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Engine No :
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->engine_no }}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Cha. No :
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $item->getVehicle->chassis_no }}
                                                                                    </td>
                                                                                </tr>
                                                                            </table>


                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                                <div class="row">

                                                                    <div class="col m12">
                                                                        <ul style="margin: 2px 0px; padding: 4px 0px">
                                                                            <li>*
                                                                                <label>Log Book With Vehicle</label>
                                                                            </li>
                                                                            <li>*
                                                                                <label>KMPL &amp; Battery No of the
                                                                                    Vehicle Furnished at Log Book
                                                                                    Page No 1</label>
                                                                            </li>
                                                                            <li>*
                                                                                <label>Tyre No Are furnished at Log
                                                                                    Book Page No. 33</label>
                                                                            </li>
                                                                            <li>*
                                                                                <label>Tools / Accessories furnished
                                                                                    at Log Book Page No. 41</label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <hr style="border: 1px dashed black;"/>


                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="col m12 pt-2">
                                                                        Ceritified that the above Vehicle has
                                                                        been
                                                                        charged off today
                                                                        Date :
                                                                        {{ date('d-m-Y', strtotime($item->vehicle_charged_off_today)) }}
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Signature :
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Designation :
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Unit :
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="col m12 pt-2">
                                                                        Ceritified that the above Vehicle has
                                                                        been Received & taken on Charge by me
                                                                        today <br>
                                                                        | Date : {{ date('d-m-Y') }}

                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Signature :

                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Recevier Name :
                                                                        {{$item->recevier_name}}
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Designation :
                                                                        {{$item->recevier_designation}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>


                                                    </div>
                                                </div>


                                                <div class="row ">

                                                    <div class="col m12 m-5 align-items-center">
                                                        <button class="bnt btn-small" onclick="printJS(
                                                            {
                                                            printable: 'printview',
                                                            type: 'html',
                                                            header: null,
                                                            base64: true
                                                            }
                                                        )">Print
                                                        </button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('mt.issue_voucher.scripts')
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

    <script>

        function PrintDiv() {
            var divToPrint = document.getElementById('divToPrint');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
            popupWin.document.close();
        }

        $(function () {
            // $(".getprint").click(function () {
            //     //Hide all other elements other than printarea.
            //     $("#printview").show();
            //     window.print();
            // });
        })
    </script>
@endsection
