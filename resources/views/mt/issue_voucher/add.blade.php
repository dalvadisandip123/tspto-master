@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--        <link rel="stylesheet" type="text/css" href="https://pixinvent.com/materialize-material-design-admin-template/app-assets/css/pages/form-select2.min.css">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">
                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.issue_voucher') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>


                                            <div class="col l12">

                                                <h5
                                                    class="card-title">Add {{ ucwords($module_name) }}</h5>


                                                <div id="bordered-table" class="card card card-default scrollspy">
                                                    <div class="card-content">


                                                        <form role="form" class="form-horizontal manageForm"
                                                              method="post"
                                                              enctype="multipart/form-data">


                                                            @csrf

                                                            <input type="hidden" name="action" value="add"/>

                                                            <div class="row">

                                                                <div class="col m12">
                                                                    <select multiple
                                                                            class="select2 vehicle_select"
                                                                            name="vehicle_ids[]">


                                                                        @if (count($vehicle_lists))
                                                                            @foreach($vehicle_lists AS $value)
                                                                                <option
                                                                                    {{  imputOldValue('vehicle_no') ==$value->id ?'selected':'' }}
                                                                                    value="{{ $value->vehicle_no }}"
                                                                                    data-make="{{ $value->getVehicleMake->vehicle_make }}"
                                                                                    data-type="{{ $value->getVehicleType->vehicle_type }}"
                                                                                    data-engine_no="{{ $value->engine_no }}"
                                                                                    data-chassis_no="{{ $value->chassis_no }}"
                                                                                    data-vehicle_cost="{{ $value->vehicle_cost }}"
                                                                                    data-po_date="{{ $value->po_date }}"
                                                                                    data-condition="new"
                                                                                >{{  $value->vehicle_no }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>
                                                                    <label>
                                                                        Select vehicle
                                                                    </label>
                                                                </div>
                                                                <div class="col m12">
                                                                    <h3 style="font-size:18px;  font-weight: bold;">
                                                                        ISSUE VOUCHER OF MOTOR VEHICLE <br>
                                                                    </h3>




                                                                    <table>
                                                                        <tr>
                                                                            <td width="40%">
                                                                                Unit
                                                                            </td>
                                                                            <td>
                                                                                :
                                                                            </td>
                                                                            <td width="50%">

                                                                                <select
                                                                                    class="select2 unit_select"
                                                                                    name="unit_id">

                                                                                    <option value="">
                                                                                        Select Unit
                                                                                    </option>

                                                                                    @if (count($units_lists))
                                                                                        @foreach($units_lists AS $value)
                                                                                            <option
                                                                                                {{  imputOldValue('vehicle_no') ==$value->id ?'selected':'' }}
                                                                                                value="{{ $value->id }}"
                                                                                                data-unithead="{{ isset($value->getUnitHead)? $value->getUnitHead->name:'' }}"
                                                                                                data-unitcode="{{  $value->unit_code }}"
                                                                                                {{--                                                                                                data-type="{{ $value->getVehicleType->vehicle_type }}"--}}
                                                                                            >{{ $value->unit_name }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Issue Voucher No
                                                                            </td>
                                                                            <td>
                                                                                :
                                                                            </td>
                                                                            <td>

                                                                                <div class="row">
                                                                                    <div class=" col md4">
                                                                                        <input type="text"
                                                                                               name="issue_voucher_no"
                                                                                               class="validate issue_voucher_no"/>

                                                                                    </div>
                                                                                    <div class=" col md4">
                                                                                        <br/>
                                                                                        <input type="hidden"
                                                                                               name="issue_voucher_type"
                                                                                               value="E2"/>
                                                                                        <input type="hidden"
                                                                                               name="issue_voucher_year"
                                                                                               value="{{date('Y')}}"/>
                                                                                        / E2 / {{date('Y')}} Date :
                                                                                    </div>
                                                                                    <div class=" col md4">

                                                                                        <input type="text"
                                                                                               name="issue_voucher_date"
                                                                                               value="{{ date('d-m-Y') }}"
                                                                                               class="datepicker_ui"/>
                                                                                    </div>
                                                                                </div>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                CO Allotment No
                                                                            </td>
                                                                            <td>
                                                                                :
                                                                            </td>
                                                                            <td>

                                                                                <div class="row">
                                                                                    <div class=" col m3">
                                                                                        <input type="text"
                                                                                               name="co_allotment_no"
                                                                                               class="validate"/>
                                                                                        <label></label>

                                                                                    </div>
                                                                                    <div class=" col m3">

                                                                                        /
                                                                                        <input id="email_inline"
                                                                                               style="width: 80%"
                                                                                               type="text"
                                                                                               name="co_allotment_no2"
                                                                                               class="validate">
                                                                                    </div>


                                                                                    <div class=" col m3">
                                                                                        <br/>


                                                                                        <input type="hidden"
                                                                                               name="co_allotment_year"
                                                                                               value="{{date('Y')}}"/>

                                                                                        / E2 /{{date('Y')}}  Date :
                                                                                    </div>
                                                                                    <div class=" col m3">

                                                                                        <input type="text"
                                                                                               name="co_allotment_date"
                                                                                               value="{{ date('d-m-Y') }}"
                                                                                               class="validate datepicker_ui"/>
                                                                                    </div>


                                                                                </div>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Vehicle Allotment Purpose:
                                                                            </td>
                                                                            <td>
                                                                                :
                                                                            </td>
                                                                            <td>

                                                                                <div class="row">

                                                                                    <div class=" col m12">

                                                                                        <input type="text"
                                                                                               name="vehicle_allotment_purpose"
                                                                                               class="validate "/>
                                                                                    </div>


                                                                                </div>

                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                    <h3>
                                                                        VEHICLE DETAILS
                                                                    </h3>


                                                                    <table>
                                                                        <thead>
                                                                        <tr>
                                                                            <th>
                                                                                Vehicle No
                                                                            </th>
                                                                            <th>
                                                                                Pur Date
                                                                            </th>
                                                                            <th>Vehicle Cost</th>
                                                                            <th>Vehicle Cond</th>
                                                                            <th>Vehicle Make</th>
                                                                            <th>Vehicle Type</th>
                                                                            <th>Engine No</th>
                                                                            <th>Cha. No</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="vehicle_table_data"
                                                                               id="vehicle_table_data">

                                                                        </tbody>
                                                                    </table>

                                                                    <div class="row">
                                                                        <div class="col m12">
                                                                            <ul>
                                                                                <li>*
                                                                                    <label>Log Book With Vehicle</label>
                                                                                </li>
                                                                                <li>*
                                                                                    <label>KMPL &amp; Battery No of the
                                                                                        Vehicle Furnished at Log Book
                                                                                        Page No 1</label>
                                                                                </li>
                                                                                <li>*
                                                                                    <label>Tyre No Are furnished at Log
                                                                                        Book Page No. 33</label>
                                                                                </li>
                                                                                <li>*
                                                                                    <label>Tools / Accessories furnished
                                                                                        at Log Book Page No. 41</label>
                                                                                </li>
                                                                            </ul>
                                                                        </div>


                                                                    </div>


                                                                    <div class="row">

                                                                        <div class="col m5 ">

                                                                            <div class="col m12 pt-2">
                                                                                Ceritified that the above Vehicle has
                                                                                been
                                                                                charged off today
                                                                                Date :
                                                                                <input type="text"
                                                                                       name="vehicle_charged_off_today"
                                                                                       class="validate datepicker_ui"
                                                                                       value="{{ date('d-m-Y') }}"
                                                                                       style="width: 50%"/>
                                                                            </div>
                                                                            <div class="col m12 pt-2">
                                                                                Signature :
                                                                            </div>
                                                                            <div class="col m12 pt-2">
                                                                                Designation :
                                                                            </div>
                                                                            <div class="col m12 pt-2">
                                                                                Unit :
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m1 "> |</div>
                                                                        <div class="col m6 ">

                                                                            <div class="col m12 pt-2">
                                                                                Ceritified that the above Vehicle has
                                                                                been Received & taken on Charge by me
                                                                                today
                                                                                | Date : 19/04/2020

                                                                            </div>
                                                                            <div class="col m12 pt-2">
                                                                                Signature :

                                                                            </div>
                                                                            <div class="col m12 pt-2">
                                                                                Recevier Name :
                                                                                <input type="text" name="recevier_name"
                                                                                       class="validate"
                                                                                       style="width: 50%"/>
                                                                            </div>
                                                                            <div class="col m12 pt-2">
                                                                                Designation :
                                                                                <input type="text"
                                                                                       name="recevier_designation"
                                                                                       class="validate"
                                                                                       style="width: 50%"/>
                                                                            </div>
                                                                            <div class="col m12 pt-2">
                                                                                Vehicle Issued To :
                                                                                <b class="issued_to"> asdasd </b>
                                                                                <input type="hidden"
                                                                                       name="vehicle_issued_to"
                                                                                       class="vehicle_issued_to"/>
                                                                            </div>

                                                                        </div>


                                                                    </div>


                                                                </div>
                                                            </div>
                                                            <div class="card-action">

                                                                <button type="submit" class="btn btn-primary">
                                                                    Save
                                                                </button>

                                                            </div>


                                                        </form>


                                                    </div>
                                                </div>
                                            </div>


                                        </div>


                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    {{--    <script src="https://pixinvent.com/materialize-material-design-admin-template/app-assets/js/scripts/form-select2.min.js"></script>--}}

    @include('mt.issue_voucher.scripts')
@endsection
