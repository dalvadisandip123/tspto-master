@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.issue_voucher') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <h3
                                            class="card-title">Edit {{ ucwords($module_name) }}
                                        </h3>
                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                </div>


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post"
                                                      enctype="multipart/form-data">


                                                    @csrf

                                                    <input type="hidden" name="action" value="edit"/>
                                                    <input type="hidden" name="id" value="{{ $item->id }}"/>

                                                    <div class="row">

                                                        <?php
//                                                        dump($item)
                                                        ?>

                                                        <div class="col m12">
                                                            <h3 style="font-size:18px;  font-weight: bold;">
                                                                ISSUE VOUCHER OF MOTOR VEHICLE <br>
                                                            </h3>


                                                            <table>
                                                                <tr>
                                                                    <td width="40%">
                                                                        vehicle_no
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td width="50%">

                                                                        {{ $item->vehicle_no }}

                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="40%">
                                                                        Unit
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td width="50%">

                                                                        <select
                                                                            class="select2 unit_select"
                                                                            name="unit_id">

                                                                            <option value="">
                                                                                Select Unit
                                                                            </option>

                                                                            @if (count($units_lists))
                                                                                @foreach($units_lists AS $value)
                                                                                    <option
                                                                                        {{  $item->unit_id ==$value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}"
                                                                                        data-unithead="{{ isset($value->getUnitHead)? $value->getUnitHead->name:'' }}"
                                                                                        data-unitcode="{{  $value->unit_code }}"
                                                                                        {{--                                                                                                data-type="{{ $value->getVehicleType->vehicle_type }}"--}}
                                                                                    >{{ $value->unit_name }}
                                                                                    </option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Issue Voucher No
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>

                                                                        <div class="row">
                                                                            <div class=" col md4">
                                                                                <input type="text"
                                                                                       value="{{ $item->issue_voucher_no}}"
                                                                                       name="issue_voucher_no"
                                                                                       class="validate issue_voucher_no"/>

                                                                            </div>
                                                                            <div class=" col md4">
                                                                                <br/>
                                                                                <input type="hidden"
                                                                                       value="{{ $item->issue_voucher_type}}"
                                                                                       name="issue_voucher_type"
                                                                                />
                                                                                <input type="hidden"
                                                                                       name="issue_voucher_year"
                                                                                       value="{{date('Y')}}"/>
                                                                                / E2 / {{date('Y')}} Date :
                                                                            </div>
                                                                            <div class=" col md4">

                                                                                <input type="text"
                                                                                       value="{{ date('d-m-Y', strtotime($item->issue_voucher_date)) }}"
                                                                                       name="issue_voucher_date"
                                                                                       value="{{ date('d-m-Y') }}"
                                                                                       class="datepicker_ui"/>
                                                                            </div>
                                                                        </div>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        CO Allotment No
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>

                                                                        <div class="row">
                                                                            <div class=" col m3">
                                                                                <input type="text"
                                                                                       value="{{ $item->co_allotment_no}}"
                                                                                       name="co_allotment_no"
                                                                                       class="validate"/>
                                                                                <label></label>

                                                                            </div>
                                                                            <div class=" col m3">

                                                                                /
                                                                                <input id="email_inline"
                                                                                       style="width: 80%"
                                                                                       type="text"
                                                                                       value="{{ $item->co_allotment_no2}}"
                                                                                       name="co_allotment_no2"
                                                                                       class="validate">
                                                                            </div>


                                                                            <div class=" col m3">
                                                                                <br/>


                                                                                <input type="hidden"
                                                                                       value="{{ $item->co_allotment_year}}"
                                                                                       name="co_allotment_year"
                                                                                />

                                                                                / E2 /{{date('Y')}} Date :
                                                                            </div>
                                                                            <div class=" col m3">

                                                                                <input type="text"
                                                                                       name="co_allotment_date"
                                                                                       value="{{ date('d-m-Y', strtotime($item->co_allotment_date)) }}"

                                                                                       class="validate datepicker_ui"/>
                                                                            </div>


                                                                        </div>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Vehicle Allotment Purpose:
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>

                                                                        <div class="row">

                                                                            <div class=" col m12">

                                                                                <input type="text"
                                                                                       value="{{ $item->vehicle_allotment_purpose}}"
                                                                                       name="vehicle_allotment_purpose"
                                                                                       class="validate "/>
                                                                            </div>


                                                                        </div>

                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <h3>
                                                                VEHICLE DETAILS
                                                            </h3>


                                                            <table>
                                                                <thead>
                                                                <tr>
                                                                    <th>
                                                                        Vehicle No
                                                                    </th>
                                                                    <th>
                                                                        Pur Date
                                                                    </th>
                                                                    <th>Vehicle Cost</th>
                                                                    <th>Vehicle Cond</th>
                                                                    <th>Vehicle Make</th>
                                                                    <th>Vehicle Type</th>
                                                                    <th>Engine No</th>
                                                                    <th>Cha. No</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody class="">
                                                                @if(isset($item->getVehicle))
                                                                    <tr>
                                                                        <td>{{ $item->getVehicle->vehicle_no }}</td>
                                                                        <td>{{ $item->getVehicle->po_date }}</td>
                                                                        <td>{{ $item->getVehicle->vehicle_cost }}</td>
                                                                        <td>New</td>
                                                                        <td>{{ $item->getVehicle->getVehicleMake->vehicle_make }}</td>
                                                                        <td>{{ $item->getVehicle->getVehicleType->vehicle_type }}</td>
                                                                        <td>{{ $item->getVehicle->engine_no }}</td>
                                                                        <td>{{ $item->getVehicle->chassis_no }}</td>
                                                                    </tr>
                                                                @endif
                                                                </tbody>
                                                            </table>

                                                            <div class="row">
                                                                <div class="col m12">
                                                                    <ul>
                                                                        <li>*
                                                                            <label>Log Book With Vehicle</label>
                                                                        </li>
                                                                        <li>*
                                                                            <label>KMPL &amp; Battery No of the
                                                                                Vehicle Furnished at Log Book
                                                                                Page No 1</label>
                                                                        </li>
                                                                        <li>*
                                                                            <label>Tyre No Are furnished at Log
                                                                                Book Page No. 33</label>
                                                                        </li>
                                                                        <li>*
                                                                            <label>Tools / Accessories furnished
                                                                                at Log Book Page No. 41</label>
                                                                        </li>
                                                                    </ul>
                                                                </div>


                                                            </div>


                                                            <div class="row">

                                                                <div class="col m5 ">

                                                                    <div class="col m12 pt-2">
                                                                        Ceritified that the above Vehicle has
                                                                        been
                                                                        charged off today
                                                                        Date :
                                                                        <input type="text"
                                                                               name="vehicle_charged_off_today"
                                                                               class="validate datepicker_ui"
                                                                               value="{{ date('d-m-Y', strtotime($item->vehicle_charged_off_today)) }}"
                                                                               style="width: 50%"/>
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Signature :
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Designation :
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Unit :
                                                                    </div>
                                                                </div>
                                                                <div class="col m1 "> |</div>
                                                                <div class="col m6 ">

                                                                    <div class="col m12 pt-2">
                                                                        Ceritified that the above Vehicle has
                                                                        been Received & taken on Charge by me
                                                                        today
                                                                        | Date : 19/04/2020

                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Signature :

                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Recevier Name :
                                                                        <input type="text" name="recevier_name"
                                                                               class="validate"
                                                                               value="{{$item->recevier_name}}"
                                                                               style="width: 50%"/>
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Designation :
                                                                        <input type="text"
                                                                               name="recevier_designation"
                                                                               class="validate"
                                                                               value="{{$item->recevier_designation}}"
                                                                               style="width: 50%"/>
                                                                    </div>
                                                                    <div class="col m12 pt-2">
                                                                        Vehicle Issued To :
                                                                        <b class="issued_to"> {{ $item->vehicle_issued_to }} </b>
                                                                        <input type="hidden"
                                                                               value="{{$item->vehicle_issued_to}}"
                                                                               name="vehicle_issued_to"
                                                                               class="vehicle_issued_to"/>
                                                                    </div>

                                                                </div>


                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>

                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('mt.issue_voucher.scripts')
@endsection
