@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.employee') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                </div>


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post"
                                                      enctype="multipart/form-data">


                                                                    <span
                                                                        class="card-title">Update {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="what" value="update"/>
                                                    <input type="hidden" name="id" value="{{ $item->id }}"/>


                                                    <?php
//                                                    dump($item);
                                                    ?>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m10">

                                                            <div class="row">


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">


                                                                        <select id='select2' class="select2"
                                                                                name="employee_role_masters_id">

                                                                            <option value="">Select Role</option>

                                                                            @if (count(getEmpRoles()))
                                                                                @foreach(getEmpRoles() AS $value)
                                                                                    <option
                                                                                        {{ $item->employee_role_masters_id == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>
                                                                        {{--                                                                        <label>  Employee Role </label>--}}


                                                                    </div>

                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div
                                                                        class="input-field ">
                                                                        <select name="employee_rank_masters_id">
                                                                            <option value="">Select Rank</option>
                                                                            @if (count(getEmpRank()))
                                                                                @foreach(getEmpRank() AS $value)
                                                                                    <option
                                                                                        {{ $item->employee_rank_masters_id == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        {{--                                                                    <label> Rank </label>--}}
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="employee_id"
                                                                               type="text"
                                                                               value="{{ $item->employee_id }}"
                                                                               name="employee_id"
                                                                               class="validate">
                                                                        <label for="employee_id">
                                                                            Employee ID
                                                                        </label>
                                                                        {{ form_validation_error($errors, 'employee_id') }}
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="name" type="text"
                                                                               name="name" value="{{ $item->name }}"
                                                                               class="validate">
                                                                        <label for="name"> Employee Name</label>
                                                                        {{ form_validation_error($errors, 'name') }}
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field">
                                                                        <input id="number" type="number"
                                                                               name="number" value="{{ $item->number }}"
                                                                               class="validate">
                                                                        <label for="number">Number </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="driving_licence_number" type="text"
                                                                               name="driving_licence_number"
                                                                               value="{{ $item->driving_licence_number }}"
                                                                               class="validate">
                                                                        <label for="driving_licence_number">
                                                                            Driving Licence Number
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="licence_type" type="text"
                                                                               name="licence_type"
                                                                               value="{{ $item->licence_type }}"
                                                                               class="validate">
                                                                        <label for="licence_type">
                                                                            Licence Type
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="licence_validity" type="text"
                                                                               name="licence_validity"
                                                                               value="{{ $item->licence_validity!='' ?date('d-m-Y', strtotime($item->licence_validity)) :'' }}"
                                                                               class="validate datepicker_ui">
                                                                        <label for="licence_validity">
                                                                            Licence Validity
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="parent_unit" type="text"
                                                                               name="parent_unit"
                                                                               value="{{ $item->parent_unit }}"
                                                                               class="validate">
                                                                        <label for="parent_unit">
                                                                            Parent Unit
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="working_section" type="text"
                                                                               name="working_section"
                                                                               value="{{ $item->working_section }}"
                                                                               class="validate">
                                                                        <label for="working_section">
                                                                            Working at Section
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="date_appointment" type="text"
                                                                               name="date_appointment"
                                                                               value="{{ $item->date_appointment!='' ? date('d-m-Y', strtotime($item->date_appointment)):'' }}"
                                                                               class="validate datepicker_ui">

                                                                        <label for="date_appointment">
                                                                            Date of Appointment
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="duty_type" type="text"
                                                                               name="duty_type"
                                                                               value="{{ $item->duty_type }}"
                                                                               class="validate">

                                                                        <label for="duty_type">
                                                                            Duty Type
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row">

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="retirement_date" type="text"
                                                                               name="retirement_date"
                                                                               value="{{ $item->retirement_date!='' ? date('d-m-Y', strtotime($item->retirement_date)):'' }}"
                                                                               class="validate datepicker_ui">

                                                                        <label for="retirement_date">
                                                                            Date of Retirement
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="languages_known" type="text"
                                                                               name="languages_known"
                                                                               value="{{ $item->languages_known }}"
                                                                               class="validate">

                                                                        <label for="languages_known">
                                                                            Languages Known
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="mother_tounge" type="text"
                                                                               name="mother_tounge"
                                                                               value="{{ $item->mother_tounge }}"
                                                                               class="validate">

                                                                        <label for="mother_tounge">
                                                                            Mother Tounge
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="native_place" type="text"
                                                                               name="native_place"
                                                                               value="{{ $item->native_place }}"
                                                                               class="validate">

                                                                        <label for="native_place">
                                                                            Native Place
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row">

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <select
                                                                            name="blood_group">

                                                                            @if (count(bloodGroups()))
                                                                                @foreach(bloodGroups() AS $vales)
                                                                                    <option
                                                                                        {{ $item->blood_group == $vales ?'selected':'' }}
                                                                                        value="{{ $vales }}">{{  $vales }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <label>
                                                                            Blood Group
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="mobile_number" type="text"
                                                                               name="mobile_number"
                                                                               value="{{ $item->getUser->mobile_no }}"
                                                                               class="validate" readonly>

                                                                        <label for="mobile_number">
                                                                            Mobile Number
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div
                                                                            class="input-field ">
                                                                        <select name="login_role">
                                                                            <option value="">Select Login Role</option>
                                                                            @if (count(getLoginRoles()))
                                                                                @foreach(getLoginRoles() AS $key=>$value)
                                                                                    <option
                                                                                            {{  $item->getUser->login_role == $key ?'selected':'' }}

                                                                                            value="{{ $key }}">{{  $value }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        {{--                                                                    <label> Rank </label>--}}
                                                                    </div>
                                                                </div>


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="family_mobile_number" type="text"
                                                                               name="family_mobile_number"
                                                                               value="{{ $item->family_mobile_number }}"
                                                                               class="validate">

                                                                        <label for="family_mobile_number">
                                                                            Family Mobile Number
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="arogya_bhadratha_number" type="text"
                                                                               name="arogya_bhadratha_number"
                                                                               value="{{ $item->arogya_bhadratha_number }}"
                                                                               class="validate">

                                                                        <label for="arogya_bhadratha_number">
                                                                            Arogya Bhadratha Number
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row">


                                                                <div class="col m12 input-group">
                                                                    <div class="input-field">
                                                                            <textarea id="rewards_or_pathakams"
                                                                                      class="materialize-textarea"
                                                                                      name="rewards_or_pathakams">{{ $item->rewards_or_pathakams }}</textarea>
                                                                        <label for="rewards_or_pathakams">
                                                                            Rewards/Pathakam’s
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                            <textarea id="punishments"
                                                                                      class="materialize-textarea"
                                                                                      name="punishments">{{ $item->punishments }}</textarea>
                                                                        <label for="punishments">
                                                                            Punishments
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                            <textarea id="accident_history"
                                                                                      class="materialize-textarea"
                                                                                      name="accident_history">{{ $item->accident_history }}</textarea>
                                                                        <label for="accident_history">
                                                                            Accident History
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                        <div class="col m2">
                                                            <div class="row">
                                                                <div class="col m12 input-group">


                                                                    <div class="file-field input-field">

                                                                        <img class="upload_input_box responsive-img" src="{{ $item->photo_url != '' ?$item->photo_url:'http://placehold.it/100x100' }}" />

                                                                        <div class="btn">
                                                                            <span class="material-icons">cloud_upload</span>
                                                                            <input type="file" name="photo" class="upload_input"/>
                                                                        </div>


{{--                                                                        <div class="file-path-wrapper">--}}
{{--                                                                            <input class="file-path validate"--}}
{{--                                                                                   type="text">--}}
{{--                                                                        </div>--}}
                                                                    </div>


{{--                                                                    <div class="input-field ">--}}
{{--                                                                        <input type="file" class="upload_input"/>--}}

{{--                                                                    </div>--}}
                                                                </div>


                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                        <select
                                                                            name="units_masters_id">
                                                                            <option value="">Select Unit</option>
                                                                            @if (count(getUnits()))
                                                                                @foreach(getUnits() AS $value)
                                                                                    <option
                                                                                        {{ $item->units_masters_id == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->unit_name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <label>
                                                                            Attached Unit
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">


                                                                        <select class="select2 officermasters_id"
                                                                                name="officer_name_masters_id">

                                                                            <option value="">Select Officer</option>

                                                                            @if (count(getOfficers()))
                                                                                @foreach(getOfficers() AS $value)
                                                                                    <option
                                                                                        {{ $item->officer_name_masters_id == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->officer_name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        {{--                                                                    <input id="officer_name_masters_id" type="text"--}}
                                                                        {{--                                                                           name="officer_name_masters_id"--}}
                                                                        {{--                                                                           class="validate">--}}
                                                                        <small class="officer">
                                                                            Officer Designation: <span
                                                                                class="designation">xxxxx</span>

                                                                            <br/>
                                                                            Officer Department: <span
                                                                                class="department">xxxxx</span>
                                                                        </small>
                                                                        {{--                                                                    <label for="years">--}}
                                                                        {{--                                                                        Officer--}}
                                                                        {{--                                                                    </label>--}}
                                                                    </div>
                                                                </div>


                                                                <div class="col m12 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="residence" type="text"
                                                                               name="residence"
                                                                               class="validate">

                                                                        <label for="residence">
                                                                            Residence
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m12 input-group">
                                                                    <div
                                                                        class="input-field ">
                                                                        <select
                                                                            name="status">

                                                                            @if (count(status_list()))
                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                    <option
                                                                                        {{ $item->status == $staus_key ?'selected':'' }}
                                                                                        value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <label>status</label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>
                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>

                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    <script>

        function getOfficer(officerID) {


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.master.mt.get_officer') }}',
                type: 'POST',
                data: {'officer_id': officerID},
                success: function (response) {
                    var resultdata = response;

                    if (resultdata.officer) {
                        $('.designation').html(resultdata.officer.get_office_designation.name)
                        $('.department').html(resultdata.officer.get_office.name)

                    }

                    console.log(resultdata.officer.get_office_designation.name)


                }
            });

        }


        $(function () {


            $(".upload_input").change(function () {
                fileUploadView(this);
                // fileUploadView('.upload_input')
            });


            $('.officermasters_id').on('change', function () {
                getOfficer($(this).val())
            });


            $('.select2').formSelect('destroy')
            $('.select2').sm_select()




            $('.manageForm').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',

                    errorPlacement: function (error, e) {

                        var parentInputGroup = e.parents('.input-group');

                        console.log(parentInputGroup)

                        if (parentInputGroup.length == 0) {

                            var placement = $(e).data('error');
                            if (placement) {
                                $(placement).append(error)
                            } else {
                                error.insertAfter(e);
                            }

                        } else {
                            e.parents('.input-group > div').append(error);

                        }


                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },


                    rules: {
                        employee_id: {
                            required: true
                        },
                        name: {
                            required: true
                        },
                        employee_role_masters_id: {
                            required: true
                        },
                        employee_rank_masters_id: {
                            required: true,
                        },
                        number: {
                            required: true,
                        },
                        driving_licence_number: {
                            required: true,
                        },
                        licence_type: {
                            required: true,
                        },
                        licence_validity: {
                            required: true,
                            validDate: true
                        },
                        retirement_date: {
                            required: true,
                            validDate: true
                        },

                        languages_known: {
                            required: true,
                        },
                        mother_tounge: {
                            required: true,
                        },
                        native_place: {
                            required: true,
                        },
                        blood_group: {
                            required: true,
                        },
                        mobile_number: {
                            required: true,
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        employee_id: {
                            required: 'Eneter  employee id',
                        },
                        name: {
                            required: 'Eneter Name',
                        },
                        employee_role_masters_id: {
                            required: 'Select Role',
                        },
                        employee_rank_masters_id: {
                            required: 'Select Rank',
                        },
                        number: {
                            required: 'Enter Number',
                        },
                        driving_licence_number: {
                            required: 'Enter Driving Licence Number',
                        },
                        licence_type: {
                            required: 'Enter Licence Type',
                        },
                        licence_validity: {
                            required: 'Enter Licence validity',
                        },
                        retirement_date: {
                            required: 'Enter retirement date',
                        },
                        date_appointment: {
                            validDate: true
                        },
                        languages_known: {
                            required: 'Enter languages',
                        },
                        mother_tounge: {
                            required: 'Enter mother tounge',
                        },
                        native_place: {
                            required: 'Enter native place',
                        },
                        blood_group: {
                            required: 'Select blood group',
                        },
                        mobile_number: {
                            required: 'Enter Mobile Number',
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
