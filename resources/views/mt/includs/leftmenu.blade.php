<div id="view-links">
    <div class="row">
        <div class="col s12">

            <div class="collection">

                <a href="{{ route('site.mt.vehicle') }}"
                   class="collection-item {{ $sub_active=='vehicle'?'active':'' }}">
                    Vehicle
                </a>

                <a href="{{ route('site.mt.vehicle_unit_history') }}"
                   class="collection-item {{ $sub_active=='vehicle_unit_history'?'active':'' }}">
                    E2-Vehicle Allotment/Transfer
                </a>

                <a href="{{ route('site.mt.vehicle_officer_history') }}"
                   class="collection-item {{ $sub_active=='vehicle_officer_history'?'active':'' }}">
                    Vehicle Attachments
                </a>

                {{--                <a href="{{ route('site.mt.vehicle') }}"--}}
                {{--                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">--}}
                {{--                    E2 Section--}}
                {{--                </a>--}}

                {{--                <a href="{{ route('site.mt.mt_update_form') }}"--}}
                {{--                   class="collection-item {{ $sub_active=='mt_update_form'?'active':'' }}">--}}
                {{--                    MT Update Form--}}
                {{--                </a>--}}

                <a href="{{ route('site.mt.issue_voucher') }}"
                   class="collection-item {{ $sub_active=='issue_voucher'?'active':'' }}">
                    Issue Voucher
                </a>

                {{--                <a href="{{ route('site.mt.vehicle') }}"--}}
                {{--                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">--}}
                {{--                    E2-Vehicle Allotment/Transfer--}}
                {{--                </a>--}}

                <a href="{{ route('site.mt.vehicle') }}"
                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">
                    Vehicle Attachments
                </a>

                <a href="{{ route('site.mt.vdra_daily_diary') }}"
                   class="collection-item {{ $sub_active=='vdra_daily_diary'?'active':'' }}">
                    VDRA / Daily Diary
                </a>

                <a href="{{ route('site.mt.vehicle') }}"
                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">
                    Condemnation
                </a>
                <a href="{{ route('site.mt.vehicle') }}"
                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">
                    Lot Auction
                </a>


                <a href="{{ route('site.mt.vehicle') }}"
                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">
                    Board of Survey
                </a>

                <a href="{{ route('site.mt.emp_deputation_ptos') }}"
                   class="collection-item {{ $sub_active=='emp_deputation_ptos'?'active':'' }}">
                    Deputation of PTO Employees
                </a>


                <a href="{{ route('site.mt.employee') }}"
                   class="collection-item {{ $sub_active=='employee'?'active':'' }}">
                    Employee Details
                </a>

                <a href="{{ route('site.mt.employee_attendance') }}"
                   class="collection-item {{ $sub_active=='employee_attendance'?'active':'' }}">
                    Employees Attendance
                </a>
                <a href="{{ route('site.mt.emp_reporting') }}"
                   class="collection-item {{ $sub_active=='emp_reporting'?'active':'' }}">
                    Reporting Employees
                </a>
                <a href="{{ route('site.mt.emp_halidy_duties') }}"
                   class="collection-item {{ $sub_active=='emp_halidy_duties'?'active':'' }}">
                    Holiday Duties
                </a>


            </div>

            {{--            <div class="collection">--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item active">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--            </div>--}}
        </div>
    </div>
</div>
