<script>

    function getOfficer(officerID) {


        var get_office_designation = officerID.find('option:selected').data('designation');
        var get_office = officerID.find('option:selected').data('office');

        $('.designation').html(get_office_designation)
        $('.department').html(get_office)


    }

    function getVehicle(vhicleID) {

        var type = vhicleID.find('option:selected').data('type');
        var make = vhicleID.find('option:selected').data('make');

        $('.vehicle_box_type').html(type);
        $('.vehicle_box_make').html(make);


    }

    function getDriverInfo(opts) {

        var no = opts.find('option:selected').data('no');
        var mobile = opts.find('option:selected').data('mobile');
        $('.driver_box_' + no).find('.mobile').html(mobile);
    }

    function calcVdra() {

        var fuel_opening_bal = $('#fuel_opening_bal').val();
        $('#fuel_total').val(fuel_opening_bal)
        $('#fuel_bal').val(fuel_opening_bal)

        if (fuel_opening_bal) {

            $('#fuel_total').next('label').addClass("active");
            $('#fuel_bal').next('label').addClass("active");
        }


        var journey_opening_km_reading = $('#journey_opening_km_reading').val();
        var journey_km_present = $('#journey_km_present').val();

        if (journey_opening_km_reading && journey_km_present) {
            $('#journey_close_km_reading').val((parseInt(journey_opening_km_reading) + parseInt(journey_km_present)));

        }


    }


    $(function () {

        calcVdra();

        $('#fuel_opening_bal').on('input', function () {
            calcVdra();
        })

        $('#journey_opening_km_reading').on('input', function () {
            calcVdra();
        })
        $('#journey_km_present').on('input', function () {
            calcVdra();
        })

        $('.officer_id').on('change', function () {
            getOfficer($(this))
        });

        $('.vehicle_no').on('change', function () {
            getVehicle($(this))
        });

        $('.driver').on('change', function () {
            getDriverInfo($(this))
        });


        $('.manageForm').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',

                errorPlacement: function (error, e) {

                    var parentInputGroup = e.parents('.input-group');

                    console.log(parentInputGroup)

                    if (parentInputGroup.length == 0) {

                        var placement = $(e).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(e);
                        }

                    } else {
                        e.parents('.input-group > div').append(error);

                    }


                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },


                rules: {
                    officer_id: {
                        required: true
                    },
                    vehicle_no: {
                        required: true
                    },
                    out_date: {
                        required: true,
                        validDate: true
                    },
                    date_return: {
                        required: true,
                        validDate: true
                    },
                    driver_id_1: {
                        required: true,
                    },
                    driver_id_2: {
                        required: true,
                    },
                    fuel_opening_bal: {
                        required: true,
                    },
                    fuel_total: {
                        required: true,

                    },
                    fuel_consumed: {
                        required: true,

                    },

                    fuel_bal: {
                        required: true,
                    },
                    journey_opening_km_reading: {
                        required: true,
                    },
                    journey_close_km_reading: {
                        required: true,
                    },
                    journey_km_present: {
                        required: true,
                    },
                    journey_km_present_month: {
                        required: true,
                    },
                    purpose_type: {
                        required: true,
                    },
                    purpose_text: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                },
                messages: {
                    officer_id: {
                        required: "Select Officer"
                    },
                    vehicle_no: {
                        required: "Select Vehicle"
                    },
                    out_date: {
                        required: "Select Date",
                        validDate: true
                    },
                    date_return: {
                        required: "Select Date",
                        validDate: true
                    },
                    driver_id_1: {
                        required: "Select Driver",
                    },
                    driver_id_2: {
                        required: "Select Driver",
                    },
                    fuel_opening_bal: {
                        required: "Enter Value",
                    },
                    fuel_total: {
                        required: "Enter Value",

                    },
                    fuel_consumed: {
                        required: "Enter Value",

                    },

                    fuel_bal: {
                        required: "Enter Value",
                    },
                    journey_opening_km_reading: {
                        required: "Enter Value",
                    },
                    journey_close_km_reading: {
                        required: "Enter Value",
                    },
                    journey_km_present: {
                        required: "Enter Value",
                    },
                    journey_km_present_month: {
                        required: "Enter Value",
                    },
                    purpose_type: {
                        required: "Select Option",
                    },
                    purpose_text: {
                        required: "Enter Text",
                    },
                    status: {
                        required: "Select Status",
                    },

                },
            });

        })


    });

</script>
