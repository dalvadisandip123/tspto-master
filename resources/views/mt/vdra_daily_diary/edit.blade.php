@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.employee') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                </div>


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post"
                                                      enctype="multipart/form-data">


                                                            <span
                                                                class="card-title">Add {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">

                                                        <div class="col m12">

                                                            <div class="row">


                                                                <div class="col m3 input-group">
                                                                    <div
                                                                        class="input-field ">

                                                                        <select
                                                                            class="select2 officer_id"
                                                                            name="officer_id">

                                                                            <option value="">Select Officer
                                                                            </option>

                                                                            @if (count(getOfficers()))
                                                                                @foreach(getOfficers() AS $value)
                                                                                    <option
                                                                                        data-office="{{ $value->getOffice->name }}"
                                                                                        data-designation="{{ $value->getOfficeDesignation->name }}"
                                                                                        {{ $item->officer_id == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->officer_name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <small class="officer">
                                                                            Officer Designation: <span
                                                                                class="designation">xxxxx</span>

                                                                            <br/>
                                                                            Officer Department: <span
                                                                                class="department">xxxxx</span>
                                                                        </small>
                                                                    </div>
                                                                </div>


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">

                                                                        <select
                                                                            class="select2 vehicle_no"
                                                                            name="vehicle_no">

                                                                            <option value="">
                                                                                Select Vehicle
                                                                            </option>

                                                                            @if (count($vehicle_lists))
                                                                                @foreach($vehicle_lists AS $value)
                                                                                    <option

                                                                                        value="{{ $value->vehicle_no }}"
                                                                                        {{ $item->vehicle_no == $value->vehicle_no ?'selected':'' }}
                                                                                        data-make="{{ $value->getVehicleMake->vehicle_make }}"
                                                                                        data-type="{{ $value->getVehicleType->vehicle_type }}"
                                                                                    >{{  $value->vehicle_no }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <small class="vehicle">
                                                                            Make: <span
                                                                                class="vehicle_box_make">xxxxx</span>

                                                                            <br/>
                                                                            Type: <span
                                                                                class="vehicle_box_type">xxxxx</span>
                                                                        </small>

                                                                    </div>

                                                                </div>


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="out_date" type="text"
                                                                               name="out_date"
                                                                               value="{{ $item->out_date!='' ?date('d-m-Y', strtotime($item->out_date)) :'' }}"
                                                                               class="validate datepicker_ui">
                                                                        <label for="out_date">
                                                                            Out Date
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="date_return" type="text"
                                                                               name="date_return"
                                                                               value="{{ $item->date_return!='' ?date('d-m-Y', strtotime($item->date_return)) :'' }}"
                                                                               class="validate datepicker_ui">
                                                                        <label for="date_return">
                                                                            date of return
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                            <div class="row">

                                                                <div class="col m3 input-group">
                                                                    <div
                                                                        class="input-field ">

                                                                        <select
                                                                            class="select2 driver "
                                                                            name="driver_id_1">

                                                                            <option value="">
                                                                                Select Diver 1
                                                                            </option>

                                                                            @if (count($drivers_lists))
                                                                                @foreach($drivers_lists AS $value)
                                                                                    <option

                                                                                        data-no="1"
                                                                                        data-mobile="{{ $value->getUser->mobile_no }}"

                                                                                        {{ $item->driver_id_1 == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <small class="driver_box_1">
                                                                            Mobile: <span
                                                                                class="mobile">xxxxx</span>

                                                                        </small>
                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div
                                                                        class="input-field ">

                                                                        <select
                                                                            class="select2 driver "
                                                                            name="driver_id_2">

                                                                            <option value="">
                                                                                Select Diver 2
                                                                            </option>

                                                                            @if (count($drivers_lists))
                                                                                @foreach($drivers_lists AS $value)
                                                                                    <option

                                                                                        data-no="2"
                                                                                        data-mobile="{{ $value->getUser->mobile_no }}"

                                                                                        {{ $item->driver_id_2 == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <small class="driver_box_2">
                                                                            Mobile: <span
                                                                                class="mobile">xxxxx</span>

                                                                        </small>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div
                                                                        class="input-field ">

                                                                        <select
                                                                            class="select2 driver "
                                                                            name="driver_id_3">

                                                                            <option value="">
                                                                                Select Diver 3
                                                                            </option>

                                                                            @if (count($drivers_lists))
                                                                                @foreach($drivers_lists AS $value)
                                                                                    <option

                                                                                        data-no="3"
                                                                                        data-mobile="{{ $value->getUser->mobile_no }}"

                                                                                        {{ $item->driver_id_3 == $value->id ?'selected':'' }}
                                                                                        value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <small class="driver_box_3">
                                                                            Mobile: <span
                                                                                class="mobile">xxxxx</span>

                                                                        </small>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <div class="row">


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="fuel_opening_bal"
                                                                               type="number"
                                                                               name="fuel_opening_bal"
                                                                               value="{{ $item->fuel_opening_bal }}"
                                                                               class="validate">

                                                                        <label for="fuel_opening_bal">
                                                                            Opening Balance
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                                <div class="col m2 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="drawn"
                                                                               type="text" readonly
                                                                               value="0"
                                                                               class="validate disabled">

                                                                        <label for="Drawn">
                                                                            Drawn
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m2 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="fuel_total"
                                                                               type="number"
                                                                               name="fuel_total"
                                                                               value="{{ $item->fuel_total }}"
                                                                               class="validate">

                                                                        <label for="fuel_total">
                                                                            Total
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col m2 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="fuel_consumed"
                                                                               type="number"
                                                                               name="fuel_consumed"
                                                                               value="{{ $item->fuel_consumed }}"
                                                                               class="validate">

                                                                        <label for="fuel_consumed">
                                                                            Consumed
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="fuel_bal"
                                                                               type="number"
                                                                               name="fuel_bal"
                                                                               value="{{ $item->fuel_bal }}"
                                                                               class="validate">

                                                                        <label for="fuel_bal">
                                                                            Balance
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row">


                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="journey_opening_km_reading"
                                                                               type="number"
                                                                               name="journey_opening_km_reading"
                                                                               value="{{ $item->journey_opening_km_reading }}"
                                                                               class="validate">

                                                                        <label for="journey_opening_km_reading">
                                                                            Opening KM Reading
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="journey_close_km_reading"
                                                                               type="number" readonly
                                                                               name="journey_close_km_reading"
                                                                               value="{{ $item->journey_close_km_reading }}"
                                                                               class="validate">

                                                                        <label for="journey_close_km_reading">
                                                                            Close KM Reading
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="journey_km_present"
                                                                               type="number"
                                                                               name="journey_km_present"
                                                                               value="{{ $item->journey_km_present }}"
                                                                               class="validate">

                                                                        <label for="journey_km_present">
                                                                            KM Covered (present)
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col m3 input-group">
                                                                    <div class="input-field ">
                                                                        <input id="journey_km_present_month"
                                                                               type="number"
                                                                               name="journey_km_present_month"
                                                                               value="{{ $item->journey_km_present_month }}"
                                                                               class="validate">

                                                                        <label for="journey_km_present_month">
                                                                            Total KM Covered this Month
                                                                        </label>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="row">
                                                                <div class="col m3 input-group">
                                                                    <div
                                                                        class="input-field ">

                                                                        <select class=" purpose_type"
                                                                                name="purpose_type">

                                                                            <option value=""     {{ $item->purpose_type == 'govt' ?'selected':'' }}>Select Purpose </option>
                                                                            <option value="govt">Govt Purpose </option>
                                                                            <option value="private"   {{ $item->purpose_type == 'private' ?'selected':'' }}>Private Purpose</option>

                                                                        </select>


                                                                        <label>
                                                                            Purpose
                                                                        </label>

                                                                    </div>
                                                                </div>



                                                                <div class="col m6 input-group">
                                                                    <div
                                                                        class="input-field ">

                                                                        <div class="input-field">
                                                                            <textarea id="purpose_text"
                                                                                      class="materialize-textarea purpose_text"
                                                                                      name="purpose_text">{{ $item->purpose_text }}</textarea>
                                                                            <label for="purpose_text">
                                                                                Purpose & Area Informations
                                                                            </label>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="col m3 input-group">
                                                                    <div
                                                                        class="input-field ">
                                                                        <select
                                                                            name="status">

                                                                            @if (count(status_list()))
                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                    <option
                                                                                        {{ $item->status == $staus_key ?'selected':'' }}
                                                                                        value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <label>status</label>
                                                                    </div>
                                                                </div>



                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>

                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('mt.vdra_daily_diary.scripts')
@endsection
