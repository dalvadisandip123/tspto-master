<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('_partials.headstyles')

    @yield('headerStyles')

</head>
<!-- END: Head-->
<body class="horizontal-layout page-header-light horizontal-menu preload-transitions 1-column login-bg   blank-page blank-page"
      data-open="click" data-menu="horizontal-menu" data-col="1-column">


@include('_partials.header')

@yield('content')

@include('_partials.footer')
@include('_partials.footerscripts')

@yield('footerScripts')

</body>

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/horizontal-menu-template/user-login.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Mar 2020 18:28:55 GMT -->
</html>



