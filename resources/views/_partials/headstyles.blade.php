<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
<meta name="author" content="ThemeSelect">
<title>@yield('title')</title>
<link rel="apple-touch-icon" href="{{ URL::asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/images/favicon/favicon-32x32.png') }}">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">



<!-- BEGIN: VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/vendors.min.css') }}">
<!-- END: VENDOR CSS-->
<!-- BEGIN: Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/themes/horizontal-menu-template/materialize.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/themes/horizontal-menu-template/style.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/layouts/style-horizontal.min.css') }}">

<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('assets/modules/materialize-select2/select2-materialize.css') }}"/>

<!-- END: Page Level CSS-->
<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/custom/custom.css') }}">
<!-- END: Custom CSS-->

{{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous">--}}
