<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
    <div class="navbar navbar-fixed">
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-light-blue-cyan">
            <div class="nav-wrapper">
                <ul class="left">
                    <li>
                        <h1 class="logo-wrapper">
                            <a class="brand-logo darken-1" href="{{ route('site.home') }}">
                                {{--                                <img src="{{ URL::asset('assets/images/logo/materialize-logo.png') }}" alt="materialize logo">--}}
                                <span class="logo-text hide-on-med-and-down">TS PTO</span>
                            </a>
                        </h1>
                    </li>
                </ul>
{{--                <div class="header-search-wrapper hide-on-med-and-down">--}}
{{--                    <i class="material-icons">search</i>--}}
{{--                    <input class="header-search-input z-depth-2" type="text" name="Search"--}}
{{--                           placeholder="Explore Materialize" data-search="template-list">--}}
{{--                    <ul class="search-list collection display-none"></ul>--}}
{{--                </div>--}}

                <ul class="navbar-list right">

                    <li class="hide-on-med-and-down">
                        <a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);">
                            <i class="material-icons">settings_overscan</i>
                        </a>
                    </li>
                    <li class="hide-on-large-only">
                        <a class="waves-effect waves-block waves-light search-button"
                           href="javascript:void(0);">
                            <i
                                class="material-icons">search </i>
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);"
                           data-target="notifications-dropdown">
                            <i class="material-icons">notifications_none
                                <small
                                    class="notification-badge orange accent-3">5
                                </small>
                            </i>
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);"
                           data-target="profile-dropdown">
                            <span class="avatar-status avatar-online">
                                <img
                                    src="/assets/images/avatar/avatar-7.png" alt="avatar">
                                <i></i>
                            </span>
                        </a>
                    </li>


                </ul>


                <ul class="dropdown-content" id="notifications-dropdown">
                    <li>
                        <h6>NOTIFICATIONS<span class="new badge">5</span></h6>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="black-text" href="#!">
                            <span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span>
                            A new order has been placed!
                        </a>
                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">
                            2 hours ago
                        </time>
                    </li>
                    <li>
                        <a class="black-text" href="#!">
                            <span
                                class="material-icons icon-bg-circle red small">stars</span> Completed the task</a>
                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">
                            3 days ago
                        </time>
                    </li>
                    <li>
                        <a class="black-text" href="#!"><span
                                class="material-icons icon-bg-circle teal small">settings</span> Settings
                            updated</a>
                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">4 days ago
                        </time>
                    </li>
                    <li>
                        <a class="black-text" href="#!"><span class="material-icons icon-bg-circle deep-orange small">today</span>
                            Director meeting started</a>
                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">6 days ago
                        </time>
                    </li>
                    <li>
                        <a class="black-text" href="#!"><span class="material-icons icon-bg-circle amber small">trending_up</span>
                            Generate monthly report</a>
                        <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">1 week ago
                        </time>
                    </li>
                </ul>
                <!-- profile-dropdown-->
                <ul class="dropdown-content" id="profile-dropdown">
                    <li>
                        <a class="grey-text text-darken-1" href="user-profile-page.php">
                            <i class="material-icons">person_outline</i>
                            Profile</a>
                    </li>
                    <li>
                        <a class="grey-text text-darken-1" href="app-chat.php">
                            <i class="material-icons">chat_bubble_outline</i>
                            Chat</a>
                    </li>
                    <li><a class="grey-text text-darken-1" href="page-faq.php"><i
                                class="material-icons">help_outline</i> Help</a></li>
                    <li class="divider"></li>
                    <li>
                        <a class="grey-text text-darken-1" href="user-lock-screen.php">
                            <i class="material-icons">lock_outline</i>
                            Lock</a>
                    </li>
                    <li>
                        <a class="grey-text text-darken-1"
                           href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i
                                class="material-icons">keyboard_tab</i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>


                    </li>
                </ul>
            </div>
            <nav class="display-none search-sm">
                <div class="nav-wrapper">
                    <form id="navbarForm">
                        <div class="input-field search-input-sm">
                            <input class="search-box-sm" type="search" required="" id="search"
                                   placeholder="Explore Materialize" data-search="template-list">
                            <label class="label-icon" for="search"><i
                                    class="material-icons search-sm-icon">search</i></label><i
                                class="material-icons search-sm-close">close</i>
                            <ul class="search-list collection search-list-sm display-none"></ul>
                        </div>
                    </form>
                </div>
            </nav>
        </nav>
        <!-- BEGIN: Horizontal nav start-->
        <nav class="white hide-on-med-and-down" id="horizontal-nav">
            <div class="nav-wrapper">
                <ul class="left hide-on-med-and-down" id="ul-horizontal-nav" data-menu="menu-navigation">
                    <li>
                        <a class="" href="{{ route('dashboard') }}">
                            <i class="material-icons">dashboard</i>
                            <span>
                                <span class="dropdown-title" data-i18n="Dashboard">Dashboard</span>

                            </span>
                        </a>
                    </li>

                    <li class="{{ isset($active) && $active == 'master'?'active':'' }}">
                        <a class="dropdown-menu" href="Javascript:void(0)" data-target="AppsDropdown">
                            <i class="material-icons">settings</i>
                            <span>
                                <span class="dropdown-title"
                                      data-i18n="Apps">Masters </span>
                                <i
                                    class="material-icons right">keyboard_arrow_down</i>
                            </span>
                        </a>
                        <ul class="dropdown-content dropdown-horizontal-list" id="AppsDropdown">

                            <li class="{{ isset($sub_active) && $sub_active == 'mt'?'active':'' }}" data-menu="">
                                <a href="{{ route('site.master.mt.units') }}"><span data-i18n="Mail">MT</span>
                                </a>
                            </li>


                            <li class="{{ isset($sub_active) && $sub_active == 'pol'?'active':'' }}" data-menu="">
                                <a href="{{ route('site.master.pol.oil_type') }}">
                                    <span data-i18n="Mail">POL</span>
                                </a>
                            </li>
                            <li class="{{ isset($sub_active) && $sub_active == 'workshop'?'active':'' }}" data-menu="">
                                <a href="{{ route('site.master.workshop.job_card_repairs') }}">
                                    <span data-i18n="Mail">WORKSHOP</span>
                                </a>
                            </li>


                        </ul>
                    </li>


                    <li>
                        <a class="dropdown-menu" href="Javascript:void(0)" data-target="UsersDropdown">
                            <i class="material-icons">face</i>
                            <span>
                                <span class="dropdown-title" data-i18n="Apps">Users </span>
                                <i class="material-icons right">keyboard_arrow_down</i>
                            </span>
                        </a>
                        <ul class="dropdown-content dropdown-horizontal-list" id="UsersDropdown">

                            <li data-menu="">
                                <a href="{{ route('site.users') }}"><span data-i18n="Mail">Users</span>
                                </a>
                            </li>


                            <li data-menu="">
                                <a href="">
                                    <span data-i18n="Mail">Users Level Permissions </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                                        User Logs
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a class="{{ isset($active) && $active == 'MT'?'active':'' }}"
                           href="{{ route('site.mt.vehicle') }}">
                            <i class="material-icons">folder</i>
                            <span>
                                <span class="dropdown-title" data-i18n="Apps">MT </span>

                            </span>
                        </a>

                    </li>


                    <li>
                        <a class="{{ isset($active) && $active == 'pol'?'active':'' }}"
                           href="{{ route('site.pol.fuel_price') }}">
                            <i class="material-icons">folder</i>
                            <span>
                                <span class="dropdown-title" data-i18n="Apps">POL </span>

                            </span>
                        </a>
                    </li>

                    <li>
                        <a class="dropdown-menu" href="Javascript:void(0)" data-target="WORKSHOPDropdown">
                            <i class="material-icons">folder</i>
                            <span>
                                <span class="dropdown-title" data-i18n="Apps">WORK SHOP </span>

                            <i class="material-icons right">keyboard_arrow_down</i>
                            </span>
                        </a>

                        <ul class="dropdown-content dropdown-horizontal-list" id="WORKSHOPDropdown">

                            <li data-menu="">
                                <a href="{{route('workshop.pto_stock_ledger.index')}}"><span data-i18n="Mail">
                                        PTO Stock Ledger
                                    </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="{{route('workshop.pto_issue_voucher.index')}}"><span data-i18n="Mail">
                                        PTO Issue Voucher Unit wise
                                    </span>
                                </a>
                            </li>
                            
                            <li data-menu="">
                                <a href="{{route('job_card.create')}}"><span data-i18n="Mail">
                                        Job Card
                                    </span>
                                </a>
                            </li>


                            <li data-menu="">
                                <a href="">
                                    <span data-i18n="Mail">
                                     Under Repair Status
                                    </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                                      Repair Particular
                                    </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                                     Full IT
                                    </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                                     Log Book
                                    </span>
                                </a>
                            </li>

                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                                   MTR
                                    </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                                  IT Ledger
                                    </span>
                                </a>
                            </li>

                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                               Road Test Pass
                                    </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                              Gate Pass
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a class="dropdown-menu" href="Javascript:void(0)" data-target="ReportsDropdown">
                            <i class="material-icons">folder</i>
                            <span>
                                <span class="dropdown-title" data-i18n="Apps">Reports </span>
                                <i class="material-icons right">keyboard_arrow_down</i>
                            </span>
                        </a>

                        <ul class="dropdown-content dropdown-horizontal-list" id="ReportsDropdown">

                            <li data-menu="">
                                <a href=""><span data-i18n="Mail">MT</span>
                                </a>
                            </li>


                            <li data-menu="">
                                <a href="">
                                    <span data-i18n="Mail">
                                       POL
                                    </span>
                                </a>
                            </li>
                            <li data-menu="">
                                <a href="app-email.php">
                                    <span data-i18n="Mail">
                                      WORKSHOP
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a class="dropdown-menu" href="Javascript:void(0)" data-target="UnitDropdown">
                            <i class="material-icons">folder</i>
                            <span>
                                <span class="dropdown-title" data-i18n="Apps">Unit </span>
 <i class="material-icons right">keyboard_arrow_down</i>
                            </span>
                        </a>


                        <ul class="dropdown-content dropdown-horizontal-list" id="UnitDropdown">

                            <li>
                                <a href="">
                                    Unit Masters
                                </a>
                            </li>


                            <li>
                                <a href="">
                                    Unit Repairs
                                </a>
                            </li>
                            <li>
                                <a href="app-email.php">
                                    Unit Reports
                                </a>
                            </li>

                        </ul>
                    </li>


                </ul>
            </div>
            <!-- END: Horizontal nav start-->
        </nav>
    </div>
</header>
<!-- END: Header-->
<ul class="display-none" id="default-search-main">
    <li class="auto-suggestion-title"><a class="collection-item" href="#">
            <h6 class="search-title">FILES</h6></a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar"><img src="/assets/images/icon/pdf-image.png" width="24" height="30"
                                             alt="sample image"></div>
                    <div class="member-info display-flex flex-column"><span
                            class="black-text">Two new item submitted</span>
                        <small class="grey-text">Marketing
                            Manager
                        </small>
                    </div>
                </div>
                <div class="status">
                    <small class="grey-text">17kb</small>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar"><img src="/assets/images/icon/doc-image.png" width="24" height="30"
                                             alt="sample image"></div>
                    <div class="member-info display-flex flex-column"><span
                            class="black-text">52 Doc file Generator</span>
                        <small class="grey-text">FontEnd
                            Developer
                        </small>
                    </div>
                </div>
                <div class="status">
                    <small class="grey-text">550kb</small>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar">
                        <img src="/assets/images/icon/xls-image.png" width="24" height="30"
                             alt="sample image">
                    </div>
                    <div class="member-info display-flex flex-column"><span
                            class="black-text">25 Xls File Uploaded</span>
                        <small class="grey-text">Digital Marketing
                            Manager
                        </small>
                    </div>
                </div>
                <div class="status">
                    <small class="grey-text">20kb</small>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar"><img src="/assets/images/icon/jpg-image.png" width="24" height="30"
                                             alt="sample image"></div>
                    <div class="member-info display-flex flex-column"><span class="black-text">Anna Strong</span>
                        <small
                            class="grey-text">Web Designer
                        </small>
                    </div>
                </div>
                <div class="status">
                    <small class="grey-text">37kb</small>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion-title"><a class="collection-item" href="#">
            <h6 class="search-title">MEMBERS</h6></a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar"><img class="circle" src="/assets/images/avatar/avatar-7.png" width="30"
                                             alt="sample image"></div>
                    <div class="member-info display-flex flex-column"><span class="black-text">John Doe</span>
                        <small
                            class="grey-text">UI designer
                        </small>
                    </div>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar"><img class="circle" src="/assets/images/avatar/avatar-8.png" width="30"
                                             alt="sample image"></div>
                    <div class="member-info display-flex flex-column"><span class="black-text">Michal Clark</span>
                        <small
                            class="grey-text">FontEnd Developer
                        </small>
                    </div>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar"><img class="circle" src="/assets/images/avatar/avatar-10.png" width="30"
                                             alt="sample image"></div>
                    <div class="member-info display-flex flex-column"><span
                            class="black-text">Milena Gibson</span>
                        <small class="grey-text">Digital
                            Marketing
                        </small>
                    </div>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion"><a class="collection-item" href="#">
            <div class="display-flex">
                <div class="display-flex align-item-center flex-grow-1">
                    <div class="avatar"><img class="circle" src="/assets/images/avatar/avatar-12.png" width="30"
                                             alt="sample image"></div>
                    <div class="member-info display-flex flex-column"><span class="black-text">Anna Strong</span>
                        <small
                            class="grey-text">Web Designer
                        </small>
                    </div>
                </div>
            </div>
        </a></li>
</ul>
<ul class="display-none" id="page-search-title">
    <li class="auto-suggestion-title"><a class="collection-item" href="#">
            <h6 class="search-title">PAGES</h6></a></li>
</ul>
<ul class="display-none" id="search-not-found">
    <li class="auto-suggestion"><a class="collection-item display-flex align-items-center" href="#"><span
                class="material-icons">error_outline</span><span class="member-info">No results found.</span></a>
    </li>
</ul>


