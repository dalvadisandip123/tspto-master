<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->

<script src="{{ URL::asset('assets/js/vendors.min.js') }}"></script>


{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"--}}
{{--        crossorigin="anonymous"></script>--}}
{{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"--}}
{{--        crossorigin="anonymous"></script>--}}


{{--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>--}}


<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.min.js"></script>


<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ URL::asset('assets/vendors/chartjs/chart.min.js') }}"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="{{ URL::asset('assets/js/plugins.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/search.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/custom/custom-script.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/scripts/customizer.min.js') }}"></script>
<!-- END THEME  JS-->
<script
    src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
<script src="{{ URL::asset('assets/modules/materialize-select2/select2-materialize.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/10.10.0/js/jquery.fileupload.min.js"></script>

<script>

    function fileUploadView(input) {


        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var inputbox = $(input).attr('class');
                console.log('input calss ' + inputbox)

                $('.' + inputbox + '_box').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }


    }

    function loadDatepickerUi() {
        $('.datepicker_ui').datepicker({dateFormat: 'dd-mm-yy'});
    }


    $(function () {

        $('.select2').formSelect('destroy')
        $('.select2').sm_select()

        loadDatepickerUi()

        $.validator.addMethod("validDate", function (value, element) {
            return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);

        }, "Please enter a valid date in the format DD-MM-YYYY");

    })
</script>
