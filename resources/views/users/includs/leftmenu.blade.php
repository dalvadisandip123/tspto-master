<div id="view-links">
    <div class="row">
        <div class="col s12">

            <div class="collection">

                <a href="{{ route('site.users') }}"
                   class="collection-item {{ $sub_active=='users'?'active':'' }}">
                    Users
                </a>

                <a href=""
                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">
                    User Level Permissions
                </a>

                <a href=""
                   class="collection-item {{ $sub_active=='UnitsMaster'?'active':'' }}">
                    User Logs
                </a>


            </div>


        </div>
    </div>
</div>
