@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('users.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m7 l7 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">
                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">
                                                                    <input id="name" type="text" name="q"
                                                                           value="{{ request()->q }}">
                                                                    <label
                                                                        for="unit_nameSearch">
                                                                        Name
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">
                                                                    <select name="status">
                                                                        <option value="">All</option>
                                                                        @if (count(status_list()))
                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                <option
                                                                                    {{  request()->status==$staus_key ?'selected':'' }}
                                                                                    value="{{ $staus_key }}">{{  $staus }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    {{--                                                                                <label>status</label>--}}
                                                                </div>
                                                            </div>


                                                            <div class="col s12 m6 l3 display-flex  show-btn">
                                                                <button type="submit"
                                                                        class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="name">Name</th>
                                                                <th data-field="Office">Email</th>
                                                                <th data-field="Office Designation">Mobile Number
                                                                </th>
                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->email }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->mobile_no }}
                                                                        </td>
                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Edit
                                                                            </a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">
                                                                                <div class="modal-content">
                                                                                    <h4>
                                                                                        {{ ucwords($module_name) }}
                                                                                    </h4>

                                                                                    <div class="row">
                                                                                        <div class="col s12 m12 l12">
                                                                                            <form role="form"
                                                                                                  class="form-horizontal userForm"
                                                                                                  method="post"
                                                                                                  enctype="multipart/form-data">
                                                                                                @csrf

                                                                                                <input type="hidden"
                                                                                                       name="action"
                                                                                                       value="edit"/>
                                                                                                <input type="hidden"
                                                                                                       name="id"
                                                                                                       value="{{ $list->id }}"/>


                                                                                                <div class="row">
                                                                                                    <div
                                                                                                        class="col m2"></div>
                                                                                                    <div class="col m8">

                                                                                                        <div
                                                                                                            class="row">


                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <input
                                                                                                                    id="name_{{ $list->id }}"
                                                                                                                    type="text"
                                                                                                                    value="{{ $list->name }}"
                                                                                                                    name="name"
                                                                                                                    class="">
                                                                                                                <label
                                                                                                                    for="name_{{ $list->id }}">Name</label>
                                                                                                            </div>

                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <input
                                                                                                                    id="email_{{ $list->id }}"
                                                                                                                    type="text"
                                                                                                                    value="{{ $list->email }}"
                                                                                                                    name="email"
                                                                                                                    class="">
                                                                                                                <label
                                                                                                                    for="email_{{ $list->email }}">Email</label>
                                                                                                            </div>
 <div
                                                                                                                class="input-field col m12">
                                                                                                                <input
                                                                                                                    id="mobile_no_{{ $list->id }}"
                                                                                                                    type="text"
                                                                                                                    value="{{ $list->mobile_no }}"
                                                                                                                    name="mobile_no"
                                                                                                                    class="">
                                                                                                                <label
                                                                                                                    for="mobile_no_{{ $list->mobile_no }}">Mobile no</label>
                                                                                                            </div>

                                                                                                            {{--<div--}}
                                                                                                                {{--class="input-field col m12">--}}
                                                                                                                {{--<input--}}
                                                                                                                    {{--id="password_{{ $list->id }}"--}}
                                                                                                                    {{--type="text"--}}
                                                                                                                    {{--value="{{ $list->password }}"--}}
                                                                                                                    {{--name="password"--}}
                                                                                                                    {{--class="">--}}
                                                                                                                {{--<label--}}
                                                                                                                    {{--for="password_{{ $list->id }}">Password</label>--}}
                                                                                                            {{--</div>--}}


                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <select
                                                                                                                    name="user_type">

                                                                                                                    @if (count(usertypes()))
                                                                                                                        @foreach(usertypes() AS $user_key=>$user_value)
                                                                                                                            <option
                                                                                                                                {{ $list->user_type == $user_key ?'selected':'' }}
                                                                                                                                value="{{ $user_key }}">{{  $user_value }}</option>
                                                                                                                        @endforeach
                                                                                                                    @endif

                                                                                                                </select>

                                                                                                                <label>User Type</label>
                                                                                                            </div>

                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <select
                                                                                                                    name="login_role">

                                                                                                                    @if (count(getLoginRoles()))
                                                                                                                        @foreach(getLoginRoles() AS $login_key=>$login_value)
                                                                                                                            <option
                                                                                                                                {{ $list->login_role == $login_key ?'selected':'' }}
                                                                                                                                value="{{ $login_key }}">{{  $login_value }}</option>
                                                                                                                        @endforeach
                                                                                                                    @endif

                                                                                                                </select>

                                                                                                                <label>Login role</label>
                                                                                                            </div>


                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <select multiple class="select2"
                                                                                                                    name="access_units[]">

                                                                                                                    @if (count(getUnits()))
                                                                                                                        @foreach(getUnits() AS $unit_value)
                                                                                                                            <option
                                                                                                                                {{ in_array($unit_value->id, explode(',', $list->access_units)) ?'selected':'' }}
                                                                                                                                value="{{ $unit_value->id }}">{{  $unit_value->unit_name }}</option>
                                                                                                                        @endforeach
                                                                                                                    @endif

                                                                                                                </select>

                                                                                                                <label class="active">Access Untis</label>
                                                                                                            </div>


                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <select
                                                                                                                    name="status">

                                                                                                                    @if (count(status_list()))
                                                                                                                        @foreach(status_list() AS $staus_key=>$staus)
                                                                                                                            <option
                                                                                                                                {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                                                value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                                                        @endforeach
                                                                                                                    @endif

                                                                                                                </select>

                                                                                                                <label>status</label>
                                                                                                            </div>


                                                                                                            <div
                                                                                                                class="input-field col m12">

                                                                                                                <button
                                                                                                                    class="btn btn-primary">
                                                                                                                    Update
                                                                                                                </button>

                                                                                                            </div>


                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div
                                                                                                        class="col m2"></div>
                                                                                                </div>


                                                                                            </form>
                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                </div>
                                                                            </div>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="4">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal userForm"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">

                                                                <div
                                                                        class="input-field col m12">
                                                                    <input
                                                                            id="name_{{ $list->id }}"
                                                                            type="text"
                                                                            value=""
                                                                            name="name"
                                                                            class="">
                                                                    <label
                                                                            for="name_{{ $list->id }}">Name</label>
                                                                </div>

                                                                <div
                                                                        class="input-field col m12">
                                                                    <input
                                                                            id="email_{{ $list->id }}"
                                                                            type="text"
                                                                            value=""
                                                                            name="email"
                                                                            class="">
                                                                    <label
                                                                            for="email_{{ $list->email }}">Email</label>
                                                                </div>
                                                                <div
                                                                        class="input-field col m12">
                                                                    <input
                                                                            id="mobile_no_{{ $list->id }}"
                                                                            type="text"
                                                                            value=""
                                                                            name="mobile_no"
                                                                            class="">
                                                                    <label
                                                                            for="mobile_no_{{ $list->mobile_no }}">Mobile no</label>
                                                                </div>

                                                                <div
                                                                        class="input-field col m12">
                                                                    <input
                                                                            id="password_{{ $list->id }}"
                                                                            type="text"
                                                                            value=""
                                                                            name="password"
                                                                            class="">
                                                                    <label
                                                                            for="password_{{ $list->id }}">Password</label>
                                                                </div>


                                                                <div
                                                                        class="input-field col m12">
                                                                    <select
                                                                            name="user_type">

                                                                        @if (count(usertypes()))
                                                                            @foreach(usertypes() AS $user_key=>$user_value)
                                                                                <option

                                                                                        value="{{ $user_key }}">{{  $user_value }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    <label>User Type</label>
                                                                </div>

                                                                <div
                                                                        class="input-field col m12">
                                                                    <select
                                                                            name="login_role">

                                                                        @if (count(getLoginRoles()))
                                                                            @foreach(getLoginRoles() AS $login_key=>$login_value)
                                                                                <option

                                                                                        value="{{ $login_key }}">{{  $login_value }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    <label>Login role</label>
                                                                </div>

                                                                <div
                                                                    class="input-field col m12">
                                                                    <select multiple class="select2"
                                                                            name="access_units[]">

                                                                        @if (count(getUnits()))
                                                                            @foreach(getUnits() AS $unit_value)
                                                                                <option

                                                                                    value="{{ $unit_value->id }}">{{  $unit_value->unit_name }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    <label class="active">Access Untis</label>
                                                                </div>


                                                                <div
                                                                        class="input-field col m12">
                                                                    <select
                                                                            name="status">

                                                                        @if (count(status_list()))
                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                <option

                                                                                        value="{{ $staus_key }}">{{  $staus }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    <label>status</label>
                                                                </div>


                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.userForm').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        name: {
                            required: true
                        },
                      mobile_no: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        name: {
                            required: 'Please enter  name'
                        },
                        mobile_no: {
                            required: 'Please enter  mobile no'
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
