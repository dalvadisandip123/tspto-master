@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ $module_name }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('master.mt.includs.leftmenu')



                                    </div>


                                    <div class="col s12 m7 l7 ">






                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col s12 m12 l12 ">



                                                                <div class="row">
                                                                    <form action="" method="get">
                                                                        <div class="col s12  l3">
                                                                            <div class="input-field col m12">
                                                                                <input id="unit_nameSeach" type="text" value="{{ request()->unit_name }}"
                                                                                       name="unit_name" >
                                                                                <label for="unit_nameSearch">Name</label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col s12  l3">
                                                                            <div class="input-field col m12">
                                                                                <select name="status">
                                                                                    <option value="">All</option>
                                                                                    @if (count(status_list()))
                                                                                        @foreach(status_list() AS $staus_key=>$staus)
                                                                                            <option {{  request()->status==$staus_key ?'selected':'' }}
                                                                                                value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

{{--                                                                                <label>status</label>--}}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col s12 m6 l3 display-flex  show-btn">
                                                                            <button type="submit" class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                                Filter Now
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                    </div>



                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="name">Name</th>
                                                                <th data-field="name">Code</th>
                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->unit_name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->unit_code }}
                                                                        </td>
                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Edit
                                                                            </a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">
                                                                                <div class="modal-content">
                                                                                    <h4>
                                                                                        Units
                                                                                    </h4>

                                                                                    <div class="row">
                                                                                        <div class="col s12 m12 l12">
                                                                                            <form role="form"
                                                                                                  class="form-horizontal units"
                                                                                                  method="post"
                                                                                                  enctype="multipart/form-data">
                                                                                                @csrf

                                                                                                <input type="hidden"
                                                                                                       name="action"
                                                                                                       value="edit"/>
                                                                                                <input type="hidden"
                                                                                                       name="id"
                                                                                                       value="{{ $list->id }}"/>


                                                                                                <div class="row">
                                                                                                    <div
                                                                                                        class="col m2"></div>
                                                                                                    <div class="col m8">

                                                                                                        <div
                                                                                                            class="row">

                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <input
                                                                                                                    id="unit_name_{{ $list->id }}"
                                                                                                                    type="text"
                                                                                                                    value="{{ $list->unit_name }}"
                                                                                                                    name="unit_name"
                                                                                                                    class="">
                                                                                                                <label
                                                                                                                    for="unit_name_{{ $list->id }}">Unit name</label>
                                                                                                            </div>
                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <input
                                                                                                                    id="unit_code_{{ $list->id }}"
                                                                                                                    type="text"
                                                                                                                    value="{{ $list->unit_code }}"
                                                                                                                    name="unit_code"
                                                                                                                    class="">
                                                                                                                <label
                                                                                                                    for="unit_name_{{ $list->id }}">Unit Code</label>
                                                                                                            </div>

                                                                                                            <div
                                                                                                                class="input-field col m12">
                                                                                                                <select
                                                                                                                    name="status">
                                                                                                                    <option
                                                                                                                        value="">
                                                                                                                        Choose
                                                                                                                        your
                                                                                                                        option
                                                                                                                    </option>
                                                                                                                    @if (count(status_list()))
                                                                                                                        @foreach(status_list() AS $staus_key=>$staus)
                                                                                                                            <option
                                                                                                                                {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                                                value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                                                        @endforeach
                                                                                                                    @endif

                                                                                                                </select>

                                                                                                                <label>status</label>
                                                                                                            </div>


                                                                                                            <div
                                                                                                                class="input-field col m12">

                                                                                                                <button
                                                                                                                    class="btn btn-primary">
                                                                                                                    Update
                                                                                                                </button>

                                                                                                            </div>


                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div
                                                                                                        class="col m2"></div>
                                                                                                </div>


                                                                                            </form>
                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                </div>
                                                                            </div>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="4">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal units"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ $module_name }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <input id="unit_name" type="text"
                                                                           name="unit_name"
                                                                           class="validate">
                                                                    <label for="unit_name">Unit name</label>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="unit_code" type="text"
                                                                           name="unit_code"
                                                                           class="validate">
                                                                    <label for="unit_code">Unit Code</label>
                                                                </div>




                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.units').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        unit_name: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        unit_name: {
                            required: 'Please enter Unit name'
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
