@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('master.mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">
                                            <div class="col md12 float-right mt-1">

                                                <!-- Modal Trigger -->
                                                <a class="waves-effect  btn modal-trigger "
                                                   href="#add_0">
                                                    Add
                                                </a>

                                                <!-- Modal Structure -->
                                                <div id="add_0"
                                                     class="modal modal-fixed-footer">
                                                    <div class="modal-content">
                                                        <h4 class="mb-6">
                                                            {{ ucwords($module_name) }}
                                                        </h4>

                                                        <div class="row">
                                                            <div class="col s12 m12 l12">
                                                                <form role="form"
                                                                      class="form-horizontal manageform"
                                                                      method="post"
                                                                      id="edit_0"
                                                                      enctype="multipart/form-data">
                                                                    @csrf

                                                                    <input type="hidden"
                                                                           name="action"
                                                                           value="add"/>


                                                                    <div class="row">
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    name="vehicle_make_id"
                                                                                    class="vehicle_make"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(getVehicleMakes()))
                                                                                        @foreach(getVehicleMakes() AS $key=>$value)
                                                                                            <option

                                                                                                value="{{ $key }}">{{  $value }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label>Vehicle
                                                                                    Make</label>
                                                                            </div>


                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    data-selected="0"
                                                                                    class="vehicle_type"
                                                                                    name="vehicle_type_id">


                                                                                </select>

                                                                                <label>Vehicle Type</label>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    data-selected="0"
                                                                                    class="vehicle_variant"
                                                                                    name="vehicle_variant_id">


                                                                                </select>

                                                                                <label>Vehicle Variant</label>
                                                                            </div>


                                                                        </div>
                                                                        <div class="col m3">


                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    name="vehicle_category_id"
                                                                                    class="vehicle_category"
                                                                                >

                                                                                    <option value=""> Select</option>

                                                                                    @if (count(vehicleCategory()))
                                                                                        @foreach(vehicleCategory() AS $value)
                                                                                            <option

                                                                                                value="{{ $value->id }}">{{  $value->vehicle_category }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label> Category </label>
                                                                            </div>


                                                                        </div>


                                                                        <div class="col m4">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <input
                                                                                    id="life_span_0"
                                                                                    type="text"
                                                                                    name="life_span"
                                                                                    class="">
                                                                                <label
                                                                                    for="life_span_0">
                                                                                    Life Span (In Months)
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m4">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <input
                                                                                    id="battery_size_0"
                                                                                    type="text"
                                                                                    name="battery_size"
                                                                                    class="">
                                                                                <label
                                                                                    for="battery_size_0">
                                                                                    Battery Size
                                                                                </label>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m4">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <input
                                                                                    id="battery_amp_0"
                                                                                    type="number"
                                                                                    name="battery_amp"
                                                                                    class="">
                                                                                <label
                                                                                    for="battery_amp_0">
                                                                                    Battery Ampere (A)
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m4">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <input
                                                                                    id="battery_volt_0"
                                                                                    type="number"
                                                                                    name="battery_volt"
                                                                                    class="">
                                                                                <label
                                                                                    for="battery_volt_0">
                                                                                    Battery Voltage (V)
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m4">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <input
                                                                                    id="tyre_life_span_0"
                                                                                    type="number"
                                                                                    name="tyre_life_span"
                                                                                    class="">
                                                                                <label
                                                                                    for="tyre_life_span_0">
                                                                                    Tyre Life Span (Km's)
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m4">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <input
                                                                                    id="tyre_front_size_0"
                                                                                    type="text"
                                                                                    name="tyre_front_size"
                                                                                    class="">
                                                                                <label
                                                                                    for="tyre_front_size_0">
                                                                                    Tyre Front Size
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <input
                                                                                    id="tyre_rear_size_0"
                                                                                    type="text"
                                                                                    name="tyre_rear_size"
                                                                                    class="">
                                                                                <label
                                                                                    for="tyre_rear_size_0">
                                                                                    Tyre Rear Size
                                                                                </label>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div
                                                                                class="input-field col m12">
                                                                                <select
                                                                                    name="status">

                                                                                    @if (count(status_list()))
                                                                                        @foreach(status_list() AS $staus_key=>$staus)
                                                                                            <option

                                                                                                value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label>status</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col m3">


                                                                        </div>
                                                                        <div class="col m3"></div>
                                                                    </div>

                                                                    <div
                                                                        class="input-field col m12">

                                                                        <button
                                                                            class="btn btn-primary">
                                                                            Add
                                                                        </button>

                                                                    </div>


                                                                </form>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="#!"
                                                           class="modal-close waves-effect waves-green btn-flat">close</a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">
{{--                                                            <div class="col s12  l3">--}}
{{--                                                                <div class="input-field col m12">--}}
{{--                                                                    <input id="vehicle_make" type="text" name="q"--}}
{{--                                                                           value="{{ request()->q }}">--}}
{{--                                                                    <label--}}
{{--                                                                        for="unit_nameSearch">--}}
{{--                                                                        Name--}}
{{--                                                                    </label>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}


                                                            <div class="col s12  l3 right-align">
                                                                <div class="input-field col m12 ">
                                                                    <select name="status" class="">
                                                                        <option value="">All</option>
                                                                        @if (count(status_list()))
                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                <option
                                                                                    {{  request()->status==$staus_key ?'selected':'' }}
                                                                                    value="{{ $staus_key }}">{{  $staus }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    {{--                                                                                <label>status</label>--}}
                                                                </div>
                                                            </div>

                                                            <div class="col s12  l3 "></div>
                                                            <div class="col s12  l3 "></div>
                                                            <div class="col s12 m6 l3  show-btn col-align-r">
                                                                <button type="submit"
                                                                        class="btn btn-small  indigo waves-effect waves-light float-right">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="make">make</th>
                                                                <th data-field="type">type</th>
                                                                <th data-field="variant">variant</th>
                                                                <th data-field="category">category</th>

                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleMake->vehicle_make }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleType->vehicle_type }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleVariant->vehicle_variant }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleCategory->vehicle_category }}
                                                                        </td>

                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger btn-small"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Edit
                                                                            </a>

                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">
                                                                                <div class="modal-content">
                                                                                    <h4>
                                                                                        {{ ucwords($module_name) }}
                                                                                    </h4>

                                                                                    <div class="row">
                                                                                        <div class="col s12 m12 l12">
                                                                                            <form role="form"
                                                                                                  class="form-horizontal manageform"
                                                                                                  method="post"
                                                                                                  id="edit_{{ $list->id }}"
                                                                                                  enctype="multipart/form-data">
                                                                                                @csrf

                                                                                                <input type="hidden"
                                                                                                       name="action"
                                                                                                       value="edit"/>

                                                                                                <input type="hidden"
                                                                                                       name="id"
                                                                                                       value="{{ $list->id }}"/>


                                                                                                <div class="row">
                                                                                                    <div class="col m3">

                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <select
                                                                                                                name="vehicle_make_id"
                                                                                                                class="vehicle_make"
                                                                                                            >

                                                                                                                <option
                                                                                                                    value="">
                                                                                                                    Select
                                                                                                                </option>

                                                                                                                @if (count(getVehicleMakes()))
                                                                                                                    @foreach(getVehicleMakes() AS $key=>$value)
                                                                                                                        <option
                                                                                                                            {{ $list->vehicle_make_id == $key ?'selected':'' }}
                                                                                                                            value="{{ $key }}">{{  $value }}</option>
                                                                                                                    @endforeach
                                                                                                                @endif

                                                                                                            </select>

                                                                                                            <label>Vehicle
                                                                                                                Make</label>
                                                                                                        </div>


                                                                                                    </div>
                                                                                                    <div class="col m3">

                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <select
                                                                                                                data-selected="{{ $list->vehicle_type_id }}"
                                                                                                                class="vehicle_type"
                                                                                                                name="vehicle_type_id">


                                                                                                            </select>

                                                                                                            <label>Vehicle
                                                                                                                Type</label>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                    <div class="col m3">

                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <select
                                                                                                                data-selected="{{ $list->vehicle_variant_id }}"
                                                                                                                class="vehicle_variant"
                                                                                                                name="vehicle_variant_id">


                                                                                                            </select>

                                                                                                            <label>Vehicle
                                                                                                                Variant</label>
                                                                                                        </div>


                                                                                                    </div>
                                                                                                    <div class="col m3">


                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <select
                                                                                                                name="vehicle_category_id"
                                                                                                                class="vehicle_category"
                                                                                                            >

                                                                                                                <option  value="">
                                                                                                                    Select
                                                                                                                </option>

                                                                                                                @if (count(vehicleCategory()))
                                                                                                                    @foreach(vehicleCategory() AS $value)
                                                                                                                        <option
                                                                                                                            {{ $list->vehicle_category_id == $value->id ?'selected':'' }}
                                                                                                                            value="{{ $value->id }}">{{  $value->vehicle_category }}</option>
                                                                                                                    @endforeach
                                                                                                                @endif

                                                                                                            </select>

                                                                                                            <label>
                                                                                                                Category </label>
                                                                                                        </div>


                                                                                                    </div>


                                                                                                    <div class="col m4">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <input
                                                                                                                id="life_span_0"
                                                                                                                type="text"
                                                                                                                value="{{ $list->life_span }}"
                                                                                                                name="life_span"
                                                                                                                class="">
                                                                                                            <label
                                                                                                                for="life_span_0">
                                                                                                                Life
                                                                                                                Span (In
                                                                                                                Months)
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m4">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <input
                                                                                                                id="battery_size_0"
                                                                                                                type="text"
                                                                                                                value="{{ $list->battery_size }}"
                                                                                                                name="battery_size"
                                                                                                                class="">
                                                                                                            <label
                                                                                                                for="battery_size_0">
                                                                                                                Battery
                                                                                                                Size
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="col m4">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <input
                                                                                                                id="battery_amp_0"
                                                                                                                type="number"
                                                                                                                value="{{ $list->battery_amp }}"
                                                                                                                name="battery_amp"
                                                                                                                class="">
                                                                                                            <label
                                                                                                                for="battery_amp_0">
                                                                                                                Battery
                                                                                                                Ampere
                                                                                                                (A)
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m4">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <input
                                                                                                                id="battery_volt_0"
                                                                                                                type="number"
                                                                                                                value="{{ $list->battery_volt }}"
                                                                                                                name="battery_volt"
                                                                                                                class="">
                                                                                                            <label
                                                                                                                for="battery_volt_0">
                                                                                                                Battery
                                                                                                                Voltage
                                                                                                                (V)
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m4">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <input
                                                                                                                id="tyre_life_span_0"
                                                                                                                type="number"
                                                                                                                value="{{ $list->tyre_life_span }}"
                                                                                                                name="tyre_life_span"
                                                                                                                class="">
                                                                                                            <label
                                                                                                                for="tyre_life_span_0">
                                                                                                                Tyre
                                                                                                                Life
                                                                                                                Span
                                                                                                                (Km's)
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m4">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <input
                                                                                                                id="tyre_front_size_0"
                                                                                                                type="text"
                                                                                                                value="{{ $list->tyre_front_size }}"
                                                                                                                name="tyre_front_size"
                                                                                                                class="">
                                                                                                            <label
                                                                                                                for="tyre_front_size_0">
                                                                                                                Tyre
                                                                                                                Front
                                                                                                                Size
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m3">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <input
                                                                                                                id="tyre_rear_size_0"
                                                                                                                type="text"
                                                                                                                value="{{ $list->tyre_rear_size }}"
                                                                                                                name="tyre_rear_size"
                                                                                                                class="">
                                                                                                            <label
                                                                                                                for="tyre_rear_size_0">
                                                                                                                Tyre
                                                                                                                Rear
                                                                                                                Size
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="col m3">
                                                                                                        <div
                                                                                                            class="input-field col m12">
                                                                                                            <select
                                                                                                                name="status">

                                                                                                                @if (count(status_list()))
                                                                                                                    @foreach(status_list() AS $staus_key=>$staus)
                                                                                                                        <option
                                                                                                                            {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                                            value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                                                    @endforeach
                                                                                                                @endif

                                                                                                            </select>

                                                                                                            <label>status</label>
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div
                                                                                                        class="input-field col m12">

                                                                                                        <button
                                                                                                            class="btn btn-primary">
                                                                                                            Update
                                                                                                        </button>

                                                                                                    </div>

                                                                                                </div>


                                                                                            </form>
                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                </div>
                                                                            </div>


                                                                            <a class="waves-effect waves-light btn modal-trigger btn-small"
                                                                               href="#view_{{ $list->id }}">
                                                                                View
                                                                            </a>

                                                                            <div id="view_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">
                                                                                <div class="modal-content">
                                                                                    <h4>
                                                                                        {{ ucwords($module_name) }}
                                                                                    </h4>

                                                                                    <div class="row">
                                                                                        <div class="col s12 m12 l12">
                                                                                            <form role="form"
                                                                                                  class="form-horizontal manageform"
                                                                                                  method="post"
                                                                                                  id="edit_{{ $list->id }}"
                                                                                                  enctype="multipart/form-data">
                                                                                                @csrf

                                                                                                <input type="hidden"
                                                                                                       name="action"
                                                                                                       value="edit"/>

                                                                                                <input type="hidden"
                                                                                                       name="id"
                                                                                                       value="{{ $list->id }}"/>


                                                                                                <div class="row">
                                                                                                    <div class="col m12">

                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Vehicle Make
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->getVehicleMake->vehicle_make }}
                                                                                                                </td>

                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Vehicle Type
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->getVehicleType->vehicle_type }}
                                                                                                                </td>


                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Vehicle Variant
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->getVehicleVariant->vehicle_variant }}
                                                                                                                </td>


                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Vehicle Category
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->getVehicleCategory->vehicle_category }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Life Span
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->life_span }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Battery Size
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->battery_size }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Battery
                                                                                                                    Ampere
                                                                                                                    (A)
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->battery_amp }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Battery
                                                                                                                    Voltage
                                                                                                                    (V)
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->battery_volt }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Tyre
                                                                                                                    Life
                                                                                                                    Span
                                                                                                                    (Km's)
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->tyre_life_span }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Tyre Front Size
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->tyre_front_size }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Tyre Rear Size
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ $list->tyre_rear_size }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    Status
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    {{ status_list($list->status) }}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>


                                                                                                    </div>





















                                                                                                </div>


                                                                                            </form>
                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                </div>
                                                                            </div>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal manageform"
                                                                                  id="edit_{{ $list->id }}"
                                                                                  style="display:inline">

                                                                                @csrf

                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary red btn-small"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="4">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')
    <script>

        function vehicleTypeLoad(formID, selected = null) {
            var parentForm = formID.closest('form').attr('id');

            console.log(parentForm);
            var vehiclemake = formID.val();

            // console.log(vehiclemake);

            var $selectDropdown = $('#' + parentForm).find('.vehicle_type');
            var selected = $('#' + parentForm).find('.vehicle_type').data('selected');
            $selectDropdown.empty();
            // $selectDropdown.html(' ');
            // $selectDropdown.children('option').remove();
            if (vehiclemake != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('ajax.master.mt.getVehicletypes') }}',
                    type: 'POST',
                    data: {'vehiclemake': vehiclemake},
                    success: function (response) {
                        var resultdata = response;
                        var Options = "<option  value='' > </option>";

                        $.each(resultdata.vehicletypes, function (k, v) {

                            if (selected == k) {
                                Options = Options + "<option  value='" + k + "' selected='selected'>" + v + "</option>";
                            } else {
                                Options = Options + "<option value='" + k + "'>" + v + "</option>";
                            }


                        });
                        $selectDropdown.append(Options);
                        $selectDropdown.formSelect();


                        if (selected) {
                            var formid = $('#' + parentForm).find('.vehicle_type');
                            vehicleVariantLoad(formid);

                        }


                    }
                });
            }


        }

        function vehicleVariantLoad(formID, selected = null) {

            var parentForm = formID.closest('form').attr('id');

            console.log(parentForm);
            var vehicleType = formID.val();

            // console.log(vehiclemake);

            var $selectDropdown = $('#' + parentForm).find('.vehicle_variant');
            var selected = $('#' + parentForm).find('.vehicle_variant').data('selected');
            $selectDropdown.empty();
            // $selectDropdown.html(' ');
            // $selectDropdown.children('option').remove();
            if (vehicleType != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('ajax.master.mt.get_vehicle_variant') }}',
                    type: 'POST',
                    data: {'vehicle_type': vehicleType},
                    success: function (response) {
                        var resultdata = response;
                        var Options = "<option  value='' > </option>";
                        $.each(resultdata.vehicle_variant, function (k, v) {

                            if (selected == k) {
                                Options = Options + "<option  value='" + k + "' selected='selected'>" + v + "</option>";
                            } else {
                                Options = Options + "<option value='" + k + "'>" + v + "</option>";
                            }


                        });
                        $selectDropdown.append(Options);
                        $selectDropdown.formSelect();
                    }
                });
            }


        }


        $(function () {

            $('.vehicle_make').each(function () {
                var formid = $(this);
                console.log('Load list ' + formid);
                vehicleTypeLoad(formid);
            })

            $(".vehicle_make").change(function () {
                // var parentForm = $(this).closest('form').attr('id');

                var formid = $(this);
                console.log(formid);
                vehicleTypeLoad(formid);
            });


            $(".vehicle_type").change(function () {
                // var parentForm = $(this).closest('form').attr('id');

                var formid = $(this);
                console.log(formid);
                vehicleVariantLoad(formid);
            });


            $('.vehicle_type').each(function () {
                var formid = $(this);
                vehicleVariantLoad(formid);
            })


            $('.manageform').each(function () {

                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        vehicle_make_id: {
                            required: true
                        },
                        vehicle_type_id: {
                            required: true
                        },
                        vehicle_variant_id: {
                            required: true
                        },
                        vehicle_category_id: {
                            required: true
                        },
                        life_span: {
                            required: true
                        },
                        battery_size: {
                            required: true
                        },
                        battery_amp: {
                            required: true
                        },
                        battery_volt: {
                            required: true
                        },
                        tyre_life_span: {
                            required: true
                        },
                        tyre_front_size: {
                            required: true
                        },
                        tyre_rear_size: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        vehicle_make_id: {
                            required: "Select vehicle make"
                        },
                        vehicle_type_id: {
                            required: "Select vehicle type"
                        },
                        vehicle_variant_id: {
                            required: "Select vehicle variant"
                        },
                        vehicle_category_id: {
                            required: "Select vehicle category"
                        },
                        life_span: {
                            required: "Enter life span"
                        },
                        battery_size: {
                            required: "Enter battery size"
                        },
                        battery_amp: {
                            required: "Enter battery AMP"
                        },
                        battery_volt: {
                            required: "Enter battery VOLT"
                        },
                        tyre_life_span: {
                            required: "Enter tyre life span"
                        },
                        tyre_front_size: {
                            required: "Enter tyre front size"
                        },
                        tyre_rear_size: {
                            required: "Enter tyre reare size"
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
        });

    </script>
@endsection
