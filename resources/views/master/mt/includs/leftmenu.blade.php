<div id="view-links">
    <div class="row">
        <div class="col s12">

            <div class="collection">

                <a href="{{ route('site.master.mt.units') }}"
                   class="collection-item {{ $sub1_active=='UnitsMaster'?'active':'' }}">
                    Units
                </a>


                <a href="{{ route('site.master.mt.vehicleMake') }}"
                   class="collection-item {{ $sub1_active=='vehicleMake'?'active':'' }} ">
                    Vehicle Make
                </a>

                <a href="{{ route('site.master.mt.vehicleType') }}"
                   class="collection-item {{ $sub1_active=='vehicleType'?'active':'' }}">
                    Vehicle Type
                </a>

                <a href="{{ route('site.master.mt.vehicleVariant') }}"
                   class="collection-item {{ $sub1_active=='vehicleVariant'?'active':'' }}">
                    Vehicle
                    Variant
                </a>

                <a href="{{ route('site.master.mt.batteries') }}"
                   class="collection-item {{ $sub1_active=='BatteriesMaster'?'active':'' }}">
                    Batteries
                </a>

                <a href="{{ route('site.master.mt.vehicleTyres') }}"
                   class="collection-item {{ $sub1_active=='vehicleTyres'?'active':'' }}">
                    Vehicle Tyres
                </a>

                <a href="{{ route('site.master.mt.batteries_tyres_by_variant') }}"
                   class="collection-item {{ $sub1_active=='batteries_tyres_by_variant'?'active':'' }} ">
                    Batteries  &amp; Tyres by Variant
                </a>

                <a href="{{ route('site.master.mt.vehicleCategory') }}"
                   class="collection-item {{ $sub1_active=='vehicleCategory'?'active':'' }}">
                    Vehicle Category
                </a>

                <a href="{{ route('site.master.mt.vehicleGroup') }}"
                   class="collection-item {{ $sub1_active=='vehicleGroup'?'active':'' }}">
                    Vehicle
                    Group
                </a>

                <a href="{{ route('site.master.mt.description_vehicle_type') }}"
                   class="collection-item  {{ $sub1_active=='description_vehicle_type'?'active':'' }}">
                    Description
                    by Vehicle Type


                </a>

                <a href="{{ route('site.master.mt.vehicle_usage') }}"
                   class="collection-item {{ $sub1_active=='vehicle_usage'?'active':'' }} ">
                    Vehicle
                    Usage
                </a>

                <a href="{{ route('site.master.mt.vehicle_body_type') }}"
                   class="collection-item {{ $sub1_active=='vehicle_body_type'?'active':'' }} ">
                    Vehicle
                    Body Type
                </a>

                <a href="{{ route('site.master.mt.condemnation') }}"
                   class="collection-item {{ $sub1_active=='condemnation'?'active':'' }}">
                    Condemnation
                </a>

                <a href="{{ route('site.master.mt.employee_role') }}"
                   class="collection-item {{ $sub1_active=='employee_role'?'active':'' }}">
                    Employee
                    Roles
                </a>

                <a href="{{ route('site.master.mt.office_det') }}"
                   class="collection-item {{ $sub1_active=='office_dept'?'active':'' }}">
                    Office of
                </a>

                <a href="{{ route('site.master.mt.employee_rank') }}"
                   class="collection-item {{ $sub1_active=='employee_rank'?'active':'' }}">
                    Employee
                    Rank
                </a>

                <a href="{{ route('site.master.mt.office_designation') }}"
                   class="collection-item {{ $sub1_active=='office_designation'?'active':'' }} ">
                    Officer Designation
                </a>

                <a href="{{ route('site.master.mt.officer_name') }}"
                   class="collection-item {{ $sub1_active=='officer_name'?'active':'' }}">
                            <span>
                                Officer Names
                            </span>
                </a>

                <a href="{{ route('site.master.mt.uninHead') }}"
                   class="collection-item {{ $sub1_active=='unit_head'?'active':'' }}">
                    {{--                    <i class="material-icons">brightness_low</i>--}}
                    <span>
                                Unit Head
                            </span>
                </a>

            </div>

            {{--            <div class="collection">--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item active">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--            </div>--}}
        </div>
    </div>
</div>
