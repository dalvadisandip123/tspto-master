<div id="view-links">
    <div class="row">
        <div class="col s12">

            <div class="collection">

                <a href="{{ route('site.master.pol.oil_type') }}"
                   class="collection-item {{ $sub1_active=='oil_type'?'active':'' }}">
                    Oil Type
                </a>


                <a href="{{ route('site.master.pol.filters') }}"
                   class="collection-item {{ $sub1_active=='filters'?'active':'' }} ">
                    Filters
                </a>

                <a href="{{ route('site.master.pol.oil_grade') }}"
                   class="collection-item {{ $sub1_active=='oil_grade'?'active':'' }}">
                    Oil Grade
                </a>

                <a href="{{ route('site.master.pol.account_numbers') }}"
                   class="collection-item {{ $sub1_active=='accountNumbers'?'active':'' }}">
                    Account Numbers
                </a>

                <a href="{{ route('site.master.pol.outlets') }}"
                   class="collection-item {{ $sub1_active=='outlets'?'active':'' }}">
                    Fuel Outlets
                </a>

                <a href="{{ route('site.master.pol.fuel_type') }}"
                   class="collection-item {{ $sub1_active=='fuel_type'?'active':'' }}">
                    Fuel Type
                </a>

                <a href="{{ route('site.master.pol.fuel_pumps') }}" class="collection-item {{ $sub1_active=='fuel_pumps'?'active':'' }}">
                    Fuel Pumps
                </a>

                <a href="{{ route('site.master.pol.fuel_ground_tank') }}"
                   class="collection-item {{ $sub1_active=='fuel_ground_tank'?'active':'' }}">
                    Fuel Ground Tank
                </a>

                <a href="{{ route('site.master.pol.license_certificates') }}"
                   class="collection-item {{ $sub1_active=='license_certificates'?'active':'' }}">
                    License & Certificates
                </a>

                <a href="{{ route('site.master.pol.generator_spl_machines') }}"
                   class="collection-item  {{ $sub1_active=='generator_spl_machines'?'active':'' }}">
                    Generator / Spl. Machines


                </a>

                <a href="{{ route('site.master.pol.service_schedule') }}"
                   class="collection-item {{ $sub1_active=='service_schedule'?'active':'' }} ">
                    Service Schedule
                </a>

                <a href="{{ route('site.master.pol.oil_type') }}"
                   class="collection-item {{ $sub1_active=='vehicle_body_type'?'active':'' }} ">
                    Fuel Quota Allotment
                </a>

                <a href="{{ route('site.master.pol.hired_vehicles') }}"
                   class="collection-item {{ $sub1_active=='hired_vehicles'?'active':'' }}">
                    Hired Vehicles
                </a>

                <a href="{{ route('site.master.pol.outlet_allocation') }}"
                   class="collection-item {{ $sub1_active=='outlet_allocation'?'active':'' }}">
                    Outlet Allocation
                </a>

            </div>

        </div>
    </div>
</div>
