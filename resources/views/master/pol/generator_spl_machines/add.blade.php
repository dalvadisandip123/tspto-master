@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('master.pol.includes.leftmenu')


                                    </div>


                                    <div class="col s12 m10 17 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal formValidation"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">

                                                        <div class="col m3">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <input id="description" type="text"
                                                                           name="description">
                                                                    <label for="name">Description</label>
                                                                </div>


                                                            </div>

                                                        </div>

                                                        <div class="col m3">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <select name="fuel_type"
                                                                            class="fuel_type">
                                                                        <option value="">Choose your option
                                                                        </option>
                                                                        @if (count(getFuelTypes()))
                                                                            @foreach(getFuelTypes() AS $key=>$value)
                                                                                <option
                                                                                    value="{{ $key }}">{{  $value }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    <label>Fuel Type</label>
                                                                </div>


                                                            </div>

                                                        </div>


                                                        <div class="col m3">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <select name="unit_name"
                                                                            class="unit_name">
                                                                        <option value="">Choose your option
                                                                        </option>
                                                                        @if (count(getUnits()))
                                                                            @foreach(getUnits() AS $value)
                                                                                <option
                                                                                    value="{{ $value->id }}">{{  $value->unit_name }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    <label>Attached Unit</label>
                                                                </div>


                                                            </div>

                                                        </div>


                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m3">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <input id="quota_allotted" type="text"
                                                                           name="quota_allotted">
                                                                    <label for="quota_allotted">Quota Allotted</label>
                                                                </div>


                                                            </div>

                                                        </div>


                                                        <div class="col m3">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <select
                                                                        name="status">

                                                                        @if (count(status_list()))
                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                <option

                                                                                    value="{{ $staus_key }}">{{  $staus }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>
                                                                    <label for="name">Status</label>
                                                                </div>


                                                            </div>

                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    @include('master.pol.generator_spl_machines.scripts')

@endsection
