<script>
    $(function () {


        $('.formValidation').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',
                errorPlacement: function (error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    description: {
                        required: true
                    },
                    fuel_type: {
                        required: true
                    },
                    unit_name: {
                        required: true
                    },
                    quota_allotted: {
                        required: true
                    },

                    status: {
                        required: true,
                    },

                },
                messages: {
                    description: {
                        required: 'Please enter  description'
                    },
                    fuel_type: {
                        required: 'Please enter  fuel type'
                    },
                    unit_name: {
                        required: 'Please enter  unit name'
                    },
                    quota_allotted: {
                        required: 'Please enter  quota allotted'
                    },

                    status: {
                        required: 'Select Status',

                    },

                },
            });

        })
    });

</script>