@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('master.pol.includes.leftmenu')


                                    </div>

                                    <div class="col s12 m10 20 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">
                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">
                                                                    <input type="text" name="q"
                                                                           value="{{ request()->q }}">
                                                                    <label
                                                                            for="SearchName">
                                                                        Quota allotted
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">
                                                                    <select name="status">
                                                                        <option value="">All</option>
                                                                        @if (count(status_list()))
                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                <option
                                                                                        {{  request()->status==$staus_key ?'selected':'' }}
                                                                                        value="{{ $staus_key }}">{{  $staus }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    {{--                                                                                <label>status</label>--}}
                                                                </div>
                                                            </div>


                                                            <div class="col s12 m6 l3 display-flex  show-btn">
                                                                <button type="submit"
                                                                        class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                        <div class="col s12 m6 l3 display-flex  show-btn">
                                                            <a href="{{ route('site.master.pol.generator_spl_machines',['action'=>'add']) }}"
                                                               class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                Add
                                                            </a>
                                                        </div>
                                                    </div>


                                                    <div class="col s12">

                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="outlet_name">Description</th>
                                                                <th data-field="fuel_type">Fuel type</th>
                                                                <th data-field="pump_number">Attached Unit</th>
                                                                <th data-field="pump_make">Quota allotted</th>
                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->description }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getFuelType->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getUnitInfo->unit_name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->quota_allotted }}
                                                                        </td>

                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="{{ route('site.master.pol.generator_spl_machines',['action'=>'edit','id'=>$list->id]) }}">
                                                                                Edit
                                                                            </a>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="6">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pagination">
                                            <div class="col m12 left-align">

                                                Showing {{ $lists->firstItem() }}
                                                to {{ $lists->lastItem() }} of {{ $lists->total() }} entries

                                            </div>
                                            <div class="col m12 right-align">

                                                {{$lists->links('vendor.pagination.materializecss')}}

                                            </div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {

        });

    </script>
@endsection
