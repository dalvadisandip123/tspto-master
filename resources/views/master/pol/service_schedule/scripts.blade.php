<script>

    function vehicleTypeLoad(formID, selected = null) {
        var parentForm = formID.closest('form').attr('id');

        console.log(parentForm);
        var vehiclemake = formID.val();

        var $selectDropdown = $('#' + parentForm).find('.vehicle_type');
        var selected = $('#' + parentForm).find('.vehicle_type').data('selected');
        $selectDropdown.empty();
        if (vehiclemake != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.master.mt.getVehicletypes') }}',
                type: 'POST',
                data: {'vehiclemake': vehiclemake},
                success: function (response) {
                    var resultdata = response;
                    var Options = "<option  value='' > </option>";

                    $.each(resultdata.vehicletypes, function (k, v) {

                        if (selected == k) {
                            Options = Options + "<option  value='" + k + "' selected='selected'>" + v + "</option>";
                        } else {
                            Options = Options + "<option value='" + k + "'>" + v + "</option>";
                        }


                    });
                    $selectDropdown.append(Options);
                    $selectDropdown.formSelect();

                    if (selected) {
                        var formid = $('#' + parentForm).find('.vehicle_type');
                        vehicleVariantLoad(formid);

                    }


                }
            });
        }


    }

    function vehicleVariantLoad(formID, selected = null) {

        var parentForm = formID.closest('form').attr('id');

        console.log(parentForm);
        var vehicleType = formID.val();

        // console.log(vehiclemake);

        var $selectDropdown = $('#' + parentForm).find('.vehicle_variant');
        var selected = $('#' + parentForm).find('.vehicle_variant').data('selected');
        $selectDropdown.empty();
        // $selectDropdown.html(' ');
        // $selectDropdown.children('option').remove();
        if (vehicleType != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '{{ route('ajax.master.mt.get_vehicle_variant') }}',
                type: 'POST',
                data: {'vehicle_type': vehicleType},
                success: function (response) {
                    var resultdata = response;
                    var Options = "<option  value='' > </option>";
                    $.each(resultdata.vehicle_variant, function (k, v) {

                        if (selected == k) {
                            Options = Options + "<option  value='" + k + "' selected='selected'>" + v + "</option>";
                        } else {
                            Options = Options + "<option value='" + k + "'>" + v + "</option>";
                        }


                    });
                    $selectDropdown.append(Options);
                    $selectDropdown.formSelect();
                }
            });
        }


    }


    $(function () {
// for ajax data
        $(".vehicle_make").change(function () {
            var formid = $(this);
            if($('.vehicle_make').val()){
            vehicleTypeLoad(formid,$('.vehicle_make').val());
            }else{
            vehicleTypeLoad(formid);
            }
        }).change();
        $(".vehicle_type").change(function () {
            var formid = $(this);
            vehicleVariantLoad(formid);
        }).change();


        // onload
        // $('.vehicle_make').each(function () {
        //     var formid = $(this);
        //     console.log('Load list ' + formid);
        //     vehicleTypeLoad(formid);
        // })
        // $('.vehicle_type').each(function () {
        //     var formid = $(this);
        //     vehicleVariantLoad(formid);
        // })

// validations
        $('.formValidation').each(function () {

            $($(this)).validate({
                ignore: [],
                errorElement: 'div',
                errorPlacement: function (error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    vehicle_make_masters_id: {
                        required: true
                    },
                    vehicle_type_masters_id: {
                        required: true
                    },
                    vehicle_variant_masters_id: {
                        required: true
                    },
                    status: {
                        required: true,
                    },

                },
                messages: {
                    vehicle_make_masters_id: {
                        required: "Select vehicle make"
                    },
                    vehicle_type_masters_id: {
                        required: "Select vehicle type"
                    },
                    vehicle_variant_masters_id: {
                        required: "Select vehicle variant"
                    },
                    status: {
                        required: 'Select Status',

                    },

                },
            });

        })


    });

</script>
