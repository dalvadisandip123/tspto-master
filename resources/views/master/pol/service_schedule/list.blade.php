@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('master.pol.includes.leftmenu')


                                    </div>

                                    <div class="col s12 m10 20 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">


                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">

                                                                    <select
                                                                            name="vehicle_make"
                                                                            class="vehicle_make"
                                                                    >

                                                                        <option value=""> Select</option>

                                                                        @if (count(getVehicleMakes()))
                                                                            @foreach(getVehicleMakes() AS $key=>$value)
                                                                                <option {{  request()->vehicle_make==$key ?'selected':'' }}

                                                                                        value="{{ $key }}">{{  $value }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>


                                                                 <label>Vehicle make</label>
                                                                </div>
                                                            </div>


                                                            <div class="col s12 m6 l3 display-flex  show-btn">
                                                                <button type="submit"
                                                                        class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                        <div class="col s12 m6 l3 display-flex  show-btn">
                                                            <a href="{{ route('site.master.pol.service_schedule',['action'=>'add']) }}"
                                                               class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                Add
                                                            </a>
                                                        </div>
                                                    </div>


                                                    <div class="col s12">

                                                    </div>
                                                    <div class="col s12">


                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="outlet_name">Vehicle Make</th>
                                                                <th data-field="fuel_type">Vehicle Type</th>
                                                                <th data-field="pump_number">Vehicle variant</th>

                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleMake->vehicle_make }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleType->vehicle_type }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getVehicleVariant->vehicle_variant }}
                                                                        </td>


                                                                        <td>
                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#oils_{{ $list->id }}">Existing
                                                                                Oils</a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="oils_{{ $list->id }}"
                                                                                 class="modal">
                                                                                <div class="modal-content">
                                                                                    <table>
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>Oil</th>
                                                                                            <th>First Service</th>
                                                                                            <th>Quantity</th>
                                                                                            <th>Grade</th>
                                                                                            <th>Interval(KM)</th>
                                                                                        </tr>
                                                                                        </thead>

                                                                                        <tbody>
                                                                                        @if(getServiceScheduleOilsBasedOnID($list->id))
                                                                                            @foreach(getServiceScheduleOilsBasedOnID($list->id) as $oil)
                                                                                                <tr>
                                                                                                    <td>{{ $oil['get_oil_type']['name'] }}</td>
                                                                                                    <td>{{ $oil['first_servicing'] }}</td>
                                                                                                    <td>{{ $oil['quantity'] }}</td>
                                                                                                    <td>{{ $oil['get_grade']['name'] }}</td>
                                                                                                    <td>{{ $oil['interval'] }}</td>
                                                                                                </tr>
                                                                                            @endforeach
                                                                                        @else
                                                                                            <tr>
                                                                                                <td colspan="5"></td>
                                                                                            </tr>
                                                                                        @endif

                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">Close</a>
                                                                                </div>
                                                                            </div>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#filters_{{ $list->id }}">Existing
                                                                                Filters</a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="filters_{{ $list->id }}"
                                                                                 class="modal">
                                                                                <div class="modal-content">
                                                                                    <table>
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>Filter Type</th>
                                                                                            <th>Interval</th>
                                                                                        </tr>
                                                                                        </thead>

                                                                                        <tbody>
                                                                                        @if(getServiceScheduleFiltersBasedOnID($list->id))
                                                                                            @foreach(getServiceScheduleFiltersBasedOnID($list->id) as $filter)
                                                                                        <tr>
                                                                                            <td>{{ $filter['get_filter']['name'] }}</td>
                                                                                            <td>{{ $filter['interval'] }}</td>
                                                                                        </tr>
                                                                                            @endforeach
                                                                                        @else
                                                                                            <tr>
                                                                                                <td colspan="2"></td>
                                                                                            </tr>
                                                                                        @endif
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">Close</a>
                                                                                </div>
                                                                            </div>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="{{ route('site.master.pol.service_schedule',['action'=>'edit','id'=>$list->id]) }}">
                                                                                Edit
                                                                            </a>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="8">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pagination">
                                            <div class="col m12 left-align">

                                                Showing {{ $lists->firstItem() }}
                                                to {{ $lists->lastItem() }} of {{ $lists->total() }} entries

                                            </div>
                                            <div class="col m12 right-align">

                                                {{$lists->links('vendor.pagination.materializecss')}}

                                            </div>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {
            $(".modal").modal({
                dismissible: false,
            });
        });

    </script>
@endsection
