@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('master.pol.includes.leftmenu')


                                    </div>


                                    <div class="col s12 m10 17 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal formValidation"
                                                  method="post" id="add0"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Update {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="edit"/>

                                                    <div class="row">


                                                        <div class="col m3">

                                                            <div
                                                                    class="input-field col m12">
                                                                <select
                                                                        name="vehicle_make_masters_id"
                                                                        class="vehicle_make">

                                                                    <option value=""> Select</option>

                                                                    @if (count(getVehicleMakes()))
                                                                        @foreach(getVehicleMakes() AS $key=>$value)
                                                                            <option {{ $list->vehicle_make_masters_id == $key ?'selected':'' }}

                                                                                    value="{{ $key }}">{{  $value }}</option>
                                                                        @endforeach
                                                                    @endif

                                                                </select>

                                                                <label>Vehicle
                                                                    Make</label>
                                                            </div>


                                                        </div>
                                                        <div class="col m3">

                                                            <div
                                                                    class="input-field col m12">
                                                                <select
                                                                        data-selected="{{ $list->vehicle_type_masters_id }}"
                                                                        class="vehicle_type"
                                                                        name="vehicle_type_masters_id">


                                                                </select>

                                                                <label>Vehicle Type</label>
                                                            </div>

                                                        </div>
                                                        <div class="col m3">

                                                            <div
                                                                    class="input-field col m12">
                                                                <select
                                                                        data-selected="{{ $list->vehicle_variant_masters_id }}"
                                                                        class="vehicle_variant"
                                                                        name="vehicle_variant_masters_id">


                                                                </select>

                                                                <label>Vehicle Variant</label>
                                                            </div>


                                                        </div>


                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                    <div class="row">

                                                        <table class="col col12">
                                                            <thead class="blue text-white">
                                                            <tr>
                                                                <th>Oil</th>
                                                                <th>First Service (KM)</th>
                                                                <th>Quantity (L)</th>
                                                                <th>Grade</th>
                                                                <th>Interval (KM)</th>
                                                            </tr>
                                                            </thead>
                                                            @if(count($oils)>0)
                                                                @foreach($oils as $oil)
                                                                    @if (array_search($oil->id, array_column($existedOils, 'oil_type_masters_id')) !== false)
                                                                        @php
                                                                            $resultarray = getArrayFromKeys($oil->id, $existedOils,'oil_type_masters_id');
                                                                        @endphp
                                                                    @else
                                                                        @php
                                                                            $resultarray=array();
                                                                        @endphp
                                                                    @endif

                                                                    <tr>
                                                                        <td>
                                                                            {{ $oil->name }}
                                                                        </td>
                                                                        <td>

                                                                            <div class="input-field col m12">
                                                                                <input id="first_service" type="text"
                                                                                       name="oils[{{ $oil->id }}][first_service]"
                                                                                       class="validate"
                                                                                       value="{{ (!empty($resultarray)) ? $resultarray['first_servicing'] : '' }}">
                                                                                <label for="first_service">First
                                                                                    service</label>
                                                                            </div>

                                                                        </td>

                                                                        <td>

                                                                            <div class="input-field col m12">
                                                                                <input id="quantity" type="text"
                                                                                       name="oils[{{ $oil->id }}][quantity]"
                                                                                       class="validate"
                                                                                       value="{{ (!empty($resultarray)) ? $resultarray['quantity'] : '' }}">
                                                                                <label for="quantity">Quantity</label>
                                                                            </div>

                                                                        </td>
                                                                        <td>
                                                                            <div
                                                                                    class="input-field col m12">

                                                                                @php
                                                                                    if(!empty($resultarray)){
                                                                                    $selectedgrade=$resultarray['oil_grade_masters_id'];
                                                                                    }else{
                                                                                    $selectedgrade='';
                                                                                    }

                                                                                @endphp


                                                                                <select
                                                                                        name="oils[{{ $oil->id }}][grade]">
                                                                                    <option value="">Select Grade
                                                                                    </option>

                                                                                    @if (count($grades))

                                                                                        @foreach($grades AS $value)
                                                                                            <option {{ $selectedgrade == $value->id  ?'selected':'' }}

                                                                                                    value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                        @endforeach
                                                                                    @endif

                                                                                </select>

                                                                                <label>Grade</label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div
                                                                                    class="input-field col m12">
                                                                                <input
                                                                                        id="interval"
                                                                                        type="text"

                                                                                        name="oils[{{ $oil->id }}][interval]"
                                                                                        class=""
                                                                                        value="{{ (!empty($resultarray)) ? $resultarray['interval'] : '' }}">
                                                                                <label
                                                                                        for="interval">Interval</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="5">No records found</td>
                                                                </tr>

                                                            @endif
                                                        </table>

                                                    </div>


                                                    <div class="row">

                                                        <table class="col col12">
                                                            <thead class="blue text-white">
                                                            <tr>
                                                                <th>filter Type</th>

                                                                <th>Interval (KM)</th>
                                                            </tr>
                                                            </thead>
                                                            @if(count($filters)>0)
                                                                @foreach($filters as $filter)

                                                                    @if (array_search($filter->id, array_column($existedFilters, 'filters_masters_id')) !== false)
                                                                        @php
                                                                            $resultarray = getArrayFromKeys($filter->id, $existedFilters,'filters_masters_id');
                                                                        @endphp
                                                                    @else
                                                                        @php
                                                                            $resultarray=array();
                                                                        @endphp
                                                                    @endif

                                                                    <tbody class="filters_box">
                                                                    <tr>

                                                                        <td>
                                                                            {{ $filter->name }}
                                                                        </td>
                                                                        <td>
                                                                            <div
                                                                                    class="input-field col m12">
                                                                                <input
                                                                                        id="intervel"
                                                                                        type="text"

                                                                                        name="filters[{{ $filter->id }}][interval]"
                                                                                        class="" value="{{ (!empty($resultarray)) ? $resultarray['interval'] : '' }}">
                                                                                <label
                                                                                        for="intervel_">Intervel</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="2">No records found</td>
                                                                </tr>

                                                            @endif

                                                        </table>

                                                    </div>

                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    @include('master.pol.service_schedule.scripts')

@endsection
