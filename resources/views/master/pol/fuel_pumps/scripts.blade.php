<script>
    $(function () {


        $('.formValidation').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',
                errorPlacement: function (error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    outlet_name: {
                        required: true
                    },
                    fuel_type: {
                        required: true
                    },
                    pump_number: {
                        required: true
                    },
                    pump_make: {
                        required: true
                    },
                    machine_number: {
                        required: true
                    },
                    status: {
                        required: true,
                    },

                },
                messages: {
                    outlet_name: {
                        required: 'Please enter  name'
                    },
                    fuel_type: {
                        required: 'Please enter  fuel type'
                    },
                    pump_number: {
                        required: 'Please enter  pump number'
                    },
                    pump_make: {
                        required: 'Please enter  pump make'
                    },
                    machine_number: {
                        required: 'Please enter  machine number'
                    },
                    status: {
                        required: 'Select Status',

                    },

                },
            });

        })
    });

</script>