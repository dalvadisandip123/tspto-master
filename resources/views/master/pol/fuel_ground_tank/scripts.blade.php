<script>
    $(function () {


        $('.formValidation').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',
                errorPlacement: function (error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    outlet_name: {
                        required: true
                    },
                    fuel_type: {
                        required: true
                    },
                    tank_capacity: {
                        required: true
                    },
                    evaporation: {
                        required: true
                    },

                    status: {
                        required: true,
                    },

                },
                messages: {
                    outlet_name: {
                        required: 'Please enter  name'
                    },
                    fuel_type: {
                        required: 'Please enter  fuel type'
                    },
                    tank_capacity: {
                        required: 'Please enter  tank capacity'
                    },
                    evaporation: {
                        required: 'Please enter  evaporation'
                    },

                    status: {
                        required: 'Select Status',

                    },

                },
            });

        })
    });

</script>