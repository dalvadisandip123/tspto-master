@extends('layout')
@section('title', $title)

@section('headerStyles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('master.pol.includes.leftmenu')


                                    </div>

                                    <div class="col s12 m7 l7 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">
                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">

                                                                    <div
                                                                        class="input-field">
                                                                        <input
                                                                            id="search_vehicle_number"
                                                                            type="text" value="{{request()->search_vehicle_number}}"
                                                                            name="search_vehicle_number"
                                                                            class="validate">
                                                                        <label
                                                                            for="search_vehicle_number">Vehicle
                                                                            Number</label>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">
                                                                    <select name="status">
                                                                        <option value="">All</option>
                                                                        @if (count(status_list()))
                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                <option
                                                                                    {{  request()->status==$staus_key ?'selected':'' }}
                                                                                    value="{{ $staus_key }}">{{  $staus }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>

                                                                    {{--                                                                                <label>status</label>--}}
                                                                </div>
                                                            </div>


                                                            <div class="col s12 m6 l3 display-flex  show-btn">
                                                                <button type="submit"
                                                                        class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>

                                                                <th data-field="name">vehicle number</th>
                                                                <th data-field="name">vehicle make and type</th>
                                                                <th data-field="name">category</th>
                                                                <th data-field="name">unit</th>
                                                                <th data-field="name">fuel type</th>
                                                                <th data-field="name">quota allotment</th>
                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->vehicle_number }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->vehicle_make_type }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getCategory->vehicle_category }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getUnit->unit_name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getFuelType->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->quota_allotment }}
                                                                        </td>

                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Edit
                                                                            </a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">
                                                                                <form role="form"
                                                                                      class="form-horizontal formValidation"
                                                                                      method="post"
                                                                                      enctype="multipart/form-data">
                                                                                    @csrf

                                                                                    <div class="modal-content">
                                                                                        <h4>
                                                                                            {{ ucwords($module_name) }}
                                                                                        </h4>

                                                                                        <div class="row">
                                                                                            <div
                                                                                                class="col s12 m12 l12">


                                                                                                <input type="hidden"
                                                                                                       name="action"
                                                                                                       value="edit"/>
                                                                                                <input type="hidden"
                                                                                                       name="id"
                                                                                                       value="{{ $list->id }}"/>


                                                                                                <div class="row">
                                                                                                    {{--                                                    <div class="col m2"></div>--}}
                                                                                                    <div
                                                                                                        class="col m12">

                                                                                                        <div
                                                                                                            class="row">

                                                                                                            <div
                                                                                                                class="col m6">
                                                                                                                <div
                                                                                                                    class="input-field">
                                                                                                                    <input
                                                                                                                        id="VehicleNumber"
                                                                                                                        type="text" value="{{$list->vehicle_number}}"
                                                                                                                        name="vehicle_number"
                                                                                                                        class="validate">
                                                                                                                    <label
                                                                                                                        for="VehicleNumber">Vehicle
                                                                                                                        Number</label>
                                                                                                                </div>

                                                                                                            </div>
                                                                                                            <div
                                                                                                                class="col m6">
                                                                                                                <div
                                                                                                                    class="input-field ">
                                                                                                                    <input
                                                                                                                        id="VehicleMakeType"
                                                                                                                        type="text" value="{{$list->vehicle_make_type}}"
                                                                                                                        name="vehicle_make_type"
                                                                                                                        class="validate">
                                                                                                                    <label
                                                                                                                        for="VehicleMakeType">Vehicle
                                                                                                                        Make
                                                                                                                        &
                                                                                                                        Type</label>
                                                                                                                </div>

                                                                                                            </div>

                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="row">
                                                                                                            <div
                                                                                                                class="col m6">
                                                                                                                <div
                                                                                                                    class="input-field ">
                                                                                                                    <select
                                                                                                                        id="category"
                                                                                                                        name="category_id">
                                                                                                                        <option
                                                                                                                            value="">
                                                                                                                            Choose
                                                                                                                            OPtion
                                                                                                                        </option>
                                                                                                                        @if (count($vehicleCategories))
                                                                                                                            @foreach($vehicleCategories AS $value)
                                                                                                                                <option
                                                                                                                                    {{ $list->category_id==$value->id ?'selected':'' }}
                                                                                                                                    value="{{ $value->id }}">{{  $value->vehicle_category }}</option>
                                                                                                                            @endforeach
                                                                                                                        @endif

                                                                                                                    </select>
                                                                                                                    <label
                                                                                                                        for="category">Category</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div
                                                                                                                class="col m6">
                                                                                                                <div
                                                                                                                    class="input-field ">
                                                                                                                    <select
                                                                                                                        id="vehicleUnits"
                                                                                                                        name="unit_id">
                                                                                                                        <option
                                                                                                                            value="">
                                                                                                                            Choose
                                                                                                                            OPtion
                                                                                                                        </option>
                                                                                                                        @if (count($vehicleUnits))
                                                                                                                            @foreach($vehicleUnits AS $value)
                                                                                                                                <option
                                                                                                                                    {{ $list->unit_id==$value->id ?'selected':'' }}
                                                                                                                                    value="{{ $value->id }}">{{  $value->unit_name }}</option>
                                                                                                                            @endforeach
                                                                                                                        @endif

                                                                                                                    </select>
                                                                                                                    <label
                                                                                                                        for="vehicleUnits">Unit</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="row">
                                                                                                            <div
                                                                                                                class="col m6">
                                                                                                                <div
                                                                                                                    class="input-field ">
                                                                                                                    <select
                                                                                                                        id="fuel_type"
                                                                                                                        name="fuel_type_id">
                                                                                                                        <option
                                                                                                                            value="">
                                                                                                                            Choose
                                                                                                                            OPtion
                                                                                                                        </option>
                                                                                                                        @if (count($fuelTypes))
                                                                                                                            @foreach($fuelTypes AS $value)
                                                                                                                                <option
                                                                                                                                    {{ $list->fuel_type_id==$value->id ?'selected':'' }}
                                                                                                                                    value="{{ $value->id }}">{{  $value->name }}</option>
                                                                                                                            @endforeach
                                                                                                                        @endif

                                                                                                                    </select>
                                                                                                                    <label
                                                                                                                        for="fuel_type">fuel
                                                                                                                        Types</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div
                                                                                                                class="col m6">
                                                                                                                <div
                                                                                                                    class="input-field ">
                                                                                                                    <input
                                                                                                                        id="Purpose"
                                                                                                                        type="text" value="{{$list->purpose}}"
                                                                                                                        name="purpose"
                                                                                                                        class="validate">
                                                                                                                    <label
                                                                                                                        for="Purpose">Purpose</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="row">

                                                                                                            <div
                                                                                                                class="col m6">

                                                                                                                <div
                                                                                                                    class="input-field ">
                                                                                                                    <input
                                                                                                                        id="QuotaAllotment"
                                                                                                                        type="number" value="{{$list->quota_allotment}}"
                                                                                                                        name="quota_allotment"
                                                                                                                        class="validate">
                                                                                                                    <label
                                                                                                                        for="QuotaAllotment">Quota
                                                                                                                        Allotment
                                                                                                                        (Liters)</label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div
                                                                                                                class="col m6">

                                                                                                                <div
                                                                                                                    class="input-field col m12">
                                                                                                                    <select
                                                                                                                        name="status">

                                                                                                                        @if (count(status_list()))
                                                                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                                                                <option
                                                                                                                                    {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                                                    value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                                                            @endforeach
                                                                                                                        @endif

                                                                                                                    </select>

                                                                                                                    <label>status</label>
                                                                                                                </div>

                                                                                                            </div>


                                                                                                        </div>
                                                                                                    </div>
                                                                                                    {{--                                                    <div class="col m2"></div>--}}
                                                                                                </div>


                                                                                            </div>


                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">

                                                                                        <button class="btn btn-primary">
                                                                                            Save
                                                                                        </button>

                                                                                        <a href="#!"
                                                                                           class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                    </div>
                                                                                </form>
                                                                            </div>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="5">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pagination">
                                            <div class="col m12 left-align">

                                                Showing {{ $lists->firstItem() }}
                                                to {{ $lists->lastItem() }} of {{ $lists->total() }} entries

                                            </div>
                                            <div class="col m12 right-align">

                                                {{$lists->links('vendor.pagination.materializecss')}}

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal formValidation"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <input id="VehicleNumber" type="text"
                                                                           name="vehicle_number"
                                                                           class="validate">
                                                                    <label for="VehicleNumber">Vehicle Number</label>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="VehicleMakeType" type="text"
                                                                           name="vehicle_make_type"
                                                                           class="validate">
                                                                    <label for="VehicleMakeType">Vehicle Make &
                                                                        Type</label>
                                                                </div>


                                                                <div class="input-field col m12">
                                                                    <select id="category"
                                                                            name="category_id">
                                                                        <option value="">
                                                                            Choose
                                                                            OPtion
                                                                        </option>
                                                                        @if (count($vehicleCategories))
                                                                            @foreach($vehicleCategories AS $value)
                                                                                <option
                                                                                    value="{{ $value->id }}">{{  $value->vehicle_category }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>
                                                                    <label
                                                                        for="category">Category</label>
                                                                </div>


                                                                <div class="input-field col m12">
                                                                    <select id="vehicleUnits"
                                                                            name="unit_id">
                                                                        <option value="">
                                                                            Choose
                                                                            OPtion
                                                                        </option>
                                                                        @if (count($vehicleUnits))
                                                                            @foreach($vehicleUnits AS $value)
                                                                                <option

                                                                                    value="{{ $value->id }}">{{  $value->unit_name }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>
                                                                    <label
                                                                        for="vehicleUnits">Unit</label>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <select id="fuel_type"
                                                                            name="fuel_type_id">
                                                                        <option value="">
                                                                            Choose
                                                                            OPtion
                                                                        </option>
                                                                        @if (count($fuelTypes))
                                                                            @foreach($fuelTypes AS $value)
                                                                                <option

                                                                                    value="{{ $value->id }}">{{  $value->name }}</option>
                                                                            @endforeach
                                                                        @endif

                                                                    </select>
                                                                    <label
                                                                        for="fuel_type">fuel Types</label>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="Purpose" type="text"
                                                                           name="purpose"
                                                                           class="validate">
                                                                    <label for="Purpose">Purpose</label>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="QuotaAllotment" type="number"
                                                                           name="quota_allotment"
                                                                           class="validate">
                                                                    <label for="QuotaAllotment">Quota Allotment
                                                                        (Liters)</label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.formValidation').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        vehicle_number: {
                            required: true
                        },
                        vehicle_make_type: {
                            required: true
                        },
                        category_id: {
                            required: true
                        },
                        unit_id: {
                            required: true
                        },
                        fuel_type_id: {
                            required: true
                        },
                        purpose: {
                            required: true
                        },
                        quota_allotment: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        vehicle_number: {
                            required: "Enter Number"
                        },
                        vehicle_make_type: {
                            required: "Enter Make and Type"
                        },
                        category_id: {
                            required: "select category"
                        },
                        unit_id: {
                            required: "select unit"
                        },
                        fuel_type_id: {
                            required: "select fuel type"
                        },
                        purpose: {
                            required: "Enter purpose"
                        },
                        quota_allotment: {
                            required: "Enter quota allotment"
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });

        });

    </script>
@endsection
