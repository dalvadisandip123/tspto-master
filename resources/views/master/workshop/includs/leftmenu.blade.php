<div id="view-links">
    <div class="row">
        <div class="col s12">

            <div class="collection">

                <a href="{{ route('site.master.workshop.job_card_repairs') }}"
                   class="collection-item {{ $sub1_active=='job_card_repairs'?'active':'' }}">
                   job card repairs
                </a>



                <a href="{{ route('site.master.workshop.past_vehicle_expense') }}"
                   class="collection-item {{ $sub1_active=='past_vehicle_expense'?'active':'' }}">
                    Past Expenses of the Vehicle
                </a>
                <a href="{{ route('site.master.workshop.full_it_repairs') }}"
                   class="collection-item {{ $sub1_active=='full_it_repairs'?'active':'' }}">
                    Full IT Repairs
                </a>
                <a href="{{ route('site.master.workshop.mechanics_names') }}"
                   class="collection-item {{ $sub1_active=='mechanics_names'?'active':'' }}">
                    Mechanics Names
                </a>
                <a href="{{ route('site.master.workshop.section_names') }}"
                   class="collection-item {{ $sub1_active=='section_names'?'active':'' }}">
                    Section Names
                </a>

                <a href="{{ route('site.master.workshop.spares_parts_entry') }}"
                   class="collection-item {{ $sub1_active=='spares_parts_entry'?'active':'' }}">
                    Spare Parts Entry
                </a>
                <a href="{{ route('site.master.workshop.part_category') }}"
                   class="collection-item {{ $sub1_active=='part_category'?'active':'' }}">
                    Part Category
                </a>
                <a href="{{ route('site.master.workshop.part_name') }}"
                   class="collection-item {{ $sub1_active=='part_name'?'active':'' }}">
                    Part Name
                </a>
                <a href="{{ route('site.master.workshop.firm_name') }}"
                   class="collection-item {{ $sub1_active=='firm_name'?'active':'' }}">
                    Firm Name
                </a>
                


            </div>

            {{--            <div class="collection">--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item active">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--            </div>--}}
        </div>
    </div>
</div>
