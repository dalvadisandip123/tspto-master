<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<head>
    @include('_partials.headstyles')

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/login.css') }}">

</head>
<!-- END: Head-->
<body class="horizontal-layout page-header-light horizontal-menu preload-transitions 1-column login-bg   blank-page blank-page"
      data-open="click" data-menu="horizontal-menu" data-col="1-column">

<div class="row">
    <div class="col s12">
        <div class="container">
            <div id="login-page" class="row">
                <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">

                        <form class="login-form" method="POST" action="{{ route('login') }}">
                            @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                <h5 class="ml-4">Sign in (TS PTO)</h5>
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="input-field col s12">
                                <i class="material-icons prefix pt-2">person_outline</i>
                                <input id="mobile_no" type="text" placeholder="Mobile Number" class="@error('mobile_no') is-invalid @enderror" name="mobile_no" value="{{ old('mobile_no') }}" required autocomplete="mobile_no" autofocus>

                                <label for="username" class="center-align">Mobile Number</label>
                                @error('mobile_no')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="input-field col s12">
                                <i class="material-icons prefix pt-2">lock_outline</i>
                                <input id="password" type="password" class="@error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">
                                <label for="password">Password</label>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 l12 ml-2 mt-1">
                                <p>
                                    <label>
                                        <input type="checkbox"/>
                                        <span>Remember Me</span>
                                    </label>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button type="submit"
                                   class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6 l6">
                                {{--<p class="margin medium-small"><a href="user-register.php">Register Now!</a></p>--}}
                            </div>
                            <div class="input-field col s6 m6 l6">
                                {{--<p class="margin right-align medium-small"><a href="user-forgot-password.php">Forgot--}}
                                        {{--password ?</a></p>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="content-overlay"></div>
    </div>
</div>


@include('_partials.footer')
@include('_partials.footerscripts')

</body>

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/horizontal-menu-template/user-login.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Mar 2020 18:28:55 GMT -->
</html>

