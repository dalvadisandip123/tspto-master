@extends('layout')
@section('title', $title)

@section('headerStyles')

    <style>
        tr.active {
            background: #0a6aa1;
            color: #FFF;
        }
    </style>
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m7 l7 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">
                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">

                                                                    <div
                                                                        class="input-field">
                                                                        <input
                                                                            id="search_date"
                                                                            type="text"
                                                                            value="{{request()->search_date}}"
                                                                            name="search_date"
                                                                            class="validate datepicker_ui">
                                                                        <label
                                                                            for="search_date">
                                                                            Date
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col s12 m6 l3 display-flex  show-btn">
                                                                <button type="submit"
                                                                        class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>

                                                                <th data-field="name">
                                                                    filling_date
                                                                </th>
                                                                <th data-field="name">
                                                                    getLicenseCertificates
                                                                </th>


                                                                <th data-field="name">
                                                                    getFuelType
                                                                </th>
                                                                <th data-field="name">
                                                                    getOutlet
                                                                </th>
                                                                <th data-field="name">
                                                                    for_licence
                                                                </th>


                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <?php
                                                            //                                                            dump($listsItems);



                                                            ?>


                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $loop->iteration }}
                                                                        </td>
                                                                        <td>
                                                                            {{ date('d-m-Y', strtotime($list->renewal_date)) }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $list->getLicenseCertificates->name }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $list->getFuelType->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->getOutlet->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->for_licence }}
                                                                        </td>

                                                                        <td>


                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Edit
                                                                            </a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">

                                                                                <form role="form"
                                                                                      class="form-horizontal units"
                                                                                      method="post"
                                                                                      enctype="multipart/form-data">

                                                                                    <div class="modal-content">
                                                                                        <h4>
                                                                                            {{--                                                                                        Units--}}
                                                                                        </h4>

                                                                                        <div class="row">
                                                                                            <div
                                                                                                class="col s12 m12 l12">

                                                                                                @csrf

                                                                                                <input type="hidden"
                                                                                                       name="action"
                                                                                                       value="edit"/>
                                                                                                <input type="hidden"
                                                                                                       name="id"
                                                                                                       value="{{ $list->id }}"/>


                                                                                                <div class="row">


                                                                                                    <div class="col m6">
                                                                                                        <div
                                                                                                            class="input-field ">

                                                                                                            <select
                                                                                                                class="select2 "
                                                                                                                name="license_certificates_id">

                                                                                                                <option
                                                                                                                    value="">
                                                                                                                    Select
                                                                                                                    license
                                                                                                                    /
                                                                                                                    Certificates
                                                                                                                </option>

                                                                                                                @if (count($licenseCertificates))
                                                                                                                    @foreach($licenseCertificates AS $value)
                                                                                                                        <option
                                                                                                                            {{ $list->license_certificates_id == $value->id  ?'selected':''}}
                                                                                                                            value="{{ $value->id }}"

                                                                                                                        >{{  $value->name }}</option>
                                                                                                                    @endforeach
                                                                                                                @endif

                                                                                                            </select>


                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m6">
                                                                                                        <div
                                                                                                            class="input-field ">

                                                                                                            <select
                                                                                                                class="select2 "
                                                                                                                name="fuel_type_id">

                                                                                                                <option value="">
                                                                                                                    Select fuel Type
                                                                                                                </option>

                                                                                                                @if (count($fuelTypeLists))
                                                                                                                    @foreach($fuelTypeLists AS $value)
                                                                                                                        <option
                                                                                                                            {{ $list->fuel_type_id == $value->id  ?'selected':''}}
                                                                                                                            value="{{ $value->id }}"

                                                                                                                        >{{  $value->name }}</option>
                                                                                                                    @endforeach
                                                                                                                @endif

                                                                                                            </select>


                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m6">
                                                                                                        <div
                                                                                                            class="input-field ">


                                                                                                            <select
                                                                                                                class="select2 "
                                                                                                                name="fuel_outlet_id">

                                                                                                                <option
                                                                                                                    value="">
                                                                                                                    Select Outlets
                                                                                                                </option>

                                                                                                                @if (count($OutletsLists))
                                                                                                                    @foreach($OutletsLists AS $value)
                                                                                                                        <option
                                                                                                                            {{ $list->fuel_outlet_id == $value->id  ?'selected':''}}
                                                                                                                            value="{{ $value->id }}"

                                                                                                                        >{{  $value->name }}</option>
                                                                                                                    @endforeach
                                                                                                                @endif

                                                                                                            </select>


                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col m6">
                                                                                                        <div
                                                                                                            class="input-field ">

                                                                                                            <select
                                                                                                                class="select2 "
                                                                                                                name="for_licence">

                                                                                                                <option
                                                                                                                    value="">
                                                                                                                    Select
                                                                                                                    For
                                                                                                                </option>

                                                                                                                <option
                                                                                                                    {{ $list->for_licence == 'Fuel Pump'  ?'selected':''}}
                                                                                                                    value="Fuel Pump">
                                                                                                                    Fuel
                                                                                                                    Pump
                                                                                                                </option>
                                                                                                                <option
                                                                                                                    {{ $list->for_licence == 'Ground Tank'  ?'selected':''}}
                                                                                                                    value="Ground Tank">
                                                                                                                    Ground
                                                                                                                    Tank
                                                                                                                </option>

                                                                                                            </select>


                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div
                                                                                                        class="input-field col m6">
                                                                                                        <input
                                                                                                            id="renewal_date"
                                                                                                            type="text"
                                                                                                            value="{{ date('d-m-Y', strtotime($list->renewal_date)) }}"
                                                                                                            name="renewal_date"
                                                                                                            class="validate datepicker_ui">
                                                                                                        <label
                                                                                                            for="renewal_date">
                                                                                                            renewal date
                                                                                                        </label>
                                                                                                    </div>


                                                                                                </div>


                                                                                            </div>


                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button class="btn btn-small">
                                                                                            Update
                                                                                        </button>
                                                                                        <a href="#!"
                                                                                           class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                    </div>
                                                                                </form>

                                                                            </div>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="6">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pagination">
                                            <div class="col m12 left-align">

                                                {{--                                                Showing {{ $listsItems->firstItem() }}--}}
                                                {{--                                                to {{ $listsItems->lastItem() }} of {{ $listsItems->total() }} entries--}}

                                            </div>
                                            <div class="col m12 right-align">

                                                {{$lists->links('vendor.pagination.materializecss')}}

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal formValidation"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">


                                                                <div class="col m12">
                                                                    <div class="input-field ">

                                                                        <select
                                                                            class="select2 "
                                                                            name="license_certificates_id">

                                                                            <option value="">
                                                                                Select license / Certificates
                                                                            </option>

                                                                            @if (count($licenseCertificates))
                                                                                @foreach($licenseCertificates AS $value)
                                                                                    <option

                                                                                        value="{{ $value->id }}"

                                                                                    >{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>


                                                                    </div>
                                                                </div>

                                                                <div class="col m12">
                                                                    <div class="input-field ">

                                                                        <select
                                                                            class="select2 "
                                                                            name="fuel_type_id">

                                                                            <option value="">
                                                                                Select fuel Type
                                                                            </option>

                                                                            @if (count($fuelTypeLists))
                                                                                @foreach($fuelTypeLists AS $value)
                                                                                    <option

                                                                                        value="{{ $value->id }}"

                                                                                    >{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>


                                                                    </div>
                                                                </div>

                                                                <div class="col m12">
                                                                    <div class="input-field ">

                                                                        <select
                                                                            class="select2 "
                                                                            name="fuel_outlet_id">

                                                                            <option value="">
                                                                                Select Outlets
                                                                            </option>

                                                                            @if (count($OutletsLists))
                                                                                @foreach($OutletsLists AS $value)
                                                                                    <option

                                                                                        value="{{ $value->id }}"

                                                                                    >{{  $value->name }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>


                                                                    </div>
                                                                </div>

                                                                <div class="col m12">
                                                                    <div class="input-field ">

                                                                        <select class="select2 "
                                                                                name="for_licence">

                                                                            <option value="">
                                                                                Select For
                                                                            </option>

                                                                            <option value="Fuel Pump">Fuel Pump</option>
                                                                            <option value="Ground Tank">Ground Tank
                                                                            </option>

                                                                        </select>


                                                                    </div>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="renewal_date"
                                                                           type="text"

                                                                           name="renewal_date"
                                                                           class="validate datepicker_ui">
                                                                    <label for="renewal_date">
                                                                        renewal date
                                                                    </label>
                                                                </div>


                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>


        $(function () {


            $('.formValidation').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        license_certificates_id: {
                            required: true
                        },
                        fuel_outlet_id: {
                            required: true
                        },
                        fuel_type_id: {
                            required: true
                        },
                        for_licence: {
                            required: true
                        },
                        renewal_date: {
                            required: true,
                            validDate: true
                        },

                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        license_certificates_id: {
                            required: 'Select license certificates',
                        },
                        fuel_outlet_id: {
                            required: 'Select fuel outlet',
                        },
                        fuel_type_id: {
                            required: 'Select fuel type',
                        },
                        for_licence: {
                            required: 'Select For',
                        },
                        renewal_date: {
                            required: 'Select Date',

                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });

        });

    </script>
@endsection
