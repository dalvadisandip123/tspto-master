<div id="view-links">
    <div class="row">
        <div class="col s12">

            <div class="collection">

                <a href="{{ route('site.pol.fuel_price') }}"
                   class="collection-item {{ $sub_active=='fuel_price'?'active':'' }}">
                    Fuel Price
                </a>

                <a href="{{ route('site.pol.lubes_price') }}"
                   class="collection-item {{ $sub_active=='lubes_price'?'active':'' }}">
                    Lubes Price
                </a>


                <a href="{{ route('site.pol.purchase_register') }}"
                   class="collection-item {{ $sub_active=='purchase_register'?'active':'' }}">
                    Purchase Register
                </a>


                <a href="{{ route('site.pol.fuel_lubricant_balances_register') }}"
                   class="collection-item {{ $sub_active=='fuel_lubricant_balances_register'?'active':'' }}">
                    Fuel &amp; Lubricant Balances
                </a>


                <a href="{{ route('site.pol.budget_register') }}"
                   class="collection-item {{ $sub_active=='budget_register'?'active':'' }}">
                    Budget Register
                </a>

                <a href="{{ route('site.pol.fuel_price') }}"
                   class="collection-item {{ $sub_active=='other_dept_fuel_issue_register'?'active':'' }}">
                    Other Dept. Fuel Issue Register
                </a>

                <a href="{{ route('site.pol.fuel_quota_allotments', ['type'=>'regular']) }}"
                   class="collection-item {{ $sub_active=='fuel_quota_allotments_regular'?'active':'' }}">
                     Fuel Quota Allotments
                </a>
                <a href="{{ route('site.pol.fuel_quota_allotments', ['type'=>'additional']) }}"
                   class="collection-item {{ $sub_active=='fuel_quota_allotments_additional'?'active':'' }}">
                    Additional Fuel Quota
                </a>

                <a href="{{ route('site.pol.fuel_quota_allotments', ['type'=>'enhanced']) }}"
                   class="collection-item {{ $sub_active=='fuel_quota_allotments_enhanced'?'active':'' }}">
                    Enhanced Fuel Quota Allotment
                </a>


                <a href="{{ route('site.pol.fuel_issue') }}"
                   class="collection-item {{ $sub_active=='fuel_issue'?'active':'' }}">
                    Fuel Issue
                </a>

                <a href="{{ route('site.pol.out_station_filling') }}"
                   class="collection-item {{ $sub_active=='out_station_filling'?'active':'' }}">
                    Out Station Filling
                </a>



                <a href="{{ route('site.pol.fuel_price') }}"
                   class="collection-item {{ $sub_active=='vehicle_servicing'?'active':'' }}">
                    Vehicle Servicing
                </a>

                <a href="{{ route('site.pol.certificate_license_schedule') }}"
                   class="collection-item {{ $sub_active=='certificate_license_schedule'?'active':'' }}">
                    License &amp; Certificates Schedule
                </a>


            </div>

            {{--            <div class="collection">--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item active">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--                <a href="#!" class="collection-item">Alvin</a>--}}
            {{--            </div>--}}
        </div>
    </div>
</div>
