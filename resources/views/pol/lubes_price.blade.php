@extends('layout')
@section('title', $title)

@section('headerStyles')

    <style>
        tr.active{
            background: #0a6aa1;
            color: #FFF;
        }
    </style>
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m7 l7 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">
                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">

                                                                    <div
                                                                        class="input-field">
                                                                        <input
                                                                            id="search_date"
                                                                            type="text"
                                                                            value="{{request()->search_date}}"
                                                                            name="search_date"
                                                                            class="validate datepicker_ui">
                                                                        <label
                                                                            for="search_date">
                                                                            Date
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col s12 m6 l3 display-flex  show-btn">
                                                                <button type="submit"
                                                                        class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr >
                                                                <th data-field="id">#</th>

                                                                <th data-field="name">Date</th>

                                                                @if (count($oliGrades))
                                                                    @foreach($oliGrades AS $fuelType)

                                                                        <th data-field="name">
                                                                            {{ $fuelType->name }} Price
                                                                        </th>




                                                                    @endforeach
                                                                @endif


                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <?php
//                                                            dump($listsItems);



                                                            ?>


                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $date=>$listPrices)


                                                                    <tr class="{{ $loop->iteration==1?'active':'' }}">
                                                                        <td>
                                                                            {{ $loop->iteration }}
                                                                        </td>
                                                                        <td>
                                                                            {{ date('d-m-Y', strtotime($date)) }}
                                                                        </td>

                                                                        @if(count($listPrices)> 0)
                                                                            @foreach($listPrices AS $listPrice)
                                                                                <td>
                                                                                    {{ $listPrice->lubes_price }}
                                                                                </td>
                                                                            @endforeach
                                                                        @endif


                                                                        <td>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="date"
                                                                                       value="{{ $date }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="5">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pagination">
                                            <div class="col m12 left-align">

                                                {{--                                                Showing {{ $listsItems->firstItem() }}--}}
                                                {{--                                                to {{ $listsItems->lastItem() }} of {{ $listsItems->total() }} entries--}}

                                            </div>
                                            <div class="col m12 right-align">

                                                {{$listsItems->links('vendor.pagination.materializecss')}}

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal formValidation"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">

                                                                <div class="input-field col m12">
                                                                    <input id="feulDate" type="text"
                                                                           name="lubes_price_date"
                                                                           value="{{ date('d-m-Y') }}"
                                                                           class="validate datepicker_ui">
                                                                    <label for="feulDate">
                                                                        lubes Price Date
                                                                    </label>
                                                                </div>


                                                                @if (count($oliGrades))
                                                                    @foreach($oliGrades AS $fuelType)

                                                                        <input type="hidden"
                                                                               name="lubes_type[{{$fuelType->id}}][lubes_type_id]"
                                                                               value="{{ $fuelType->id }}"/>


                                                                    @php
                                                                        $fuelTypeLastPrice = $listsItems->where('lubes_type_id', $fuelType->id)->where('lubes_price', '!=',null)->first();

                                                                    @endphp

                                                                        <div class="input-field col m12">
                                                                            <input id="fuel_{{ $fuelType->id }}"
                                                                                   type="number"
                                                                                   value="{{ isset($fuelTypeLastPrice) ? $fuelTypeLastPrice->lubes_price:'' }}"
                                                                                   name="lubes_type[{{$fuelType->id}}][lubes_price]"
                                                                                   class="validate">
                                                                            <label for="fuel_{{ $fuelType->id }}">
                                                                                lubes  {{ $fuelType->name }} Price
                                                                            </label>
                                                                        </div>


                                                                    @endforeach



                                                                @endif


                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.formValidation').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        vehicle_number: {
                            required: true
                        },
                        vehicle_make_type: {
                            required: true
                        },
                        category_id: {
                            required: true
                        },
                        unit_id: {
                            required: true
                        },
                        fuel_type_id: {
                            required: true
                        },
                        purpose: {
                            required: true
                        },
                        quota_allotment: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        vehicle_number: {
                            required: "Enter Number"
                        },
                        vehicle_make_type: {
                            required: "Enter Make and Type"
                        },
                        category_id: {
                            required: "select category"
                        },
                        unit_id: {
                            required: "select unit"
                        },
                        fuel_type_id: {
                            required: "select fuel type"
                        },
                        purpose: {
                            required: "Enter purpose"
                        },
                        quota_allotment: {
                            required: "Enter quota allotment"
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });

        });

    </script>
@endsection
