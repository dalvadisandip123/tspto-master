@extends('layout')
@section('title', $title)

@section('headerStyles')

    <style>
        tr.active{
            background: #0a6aa1;
            color: #FFF;
        }
    </style>
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">




                                                    <div class="card">

                                                        <div class="card-content">
                                                            <span class="card-title"> Fuel Info </span>


                                                            <div class="row">


                                                                @if(count($outletsMasters)> 0)
                                                                    @foreach($outletsMasters AS $outletsMaster)

                                                                        <div class="col m4">
                                                                            <div class="card">

                                                                                <div class="card-content">
                                                                                    <span
                                                                                        class="card-title">
                                                                                   {{ $outletsMaster->name }}
                                                                                    </span>

                                                                                    <div class="row">


                                                                                        @if(count($fuelTypeMasters)> 0)
                                                                                            @foreach($fuelTypeMasters AS $fuelTypeMaster)
                                                                                                <div
                                                                                                    class="col m6">
                                                                                                    <div
                                                                                                        class="input-group">
                                                                                                        <div
                                                                                                            class="input-field ">
                                                                                                            <input
                                                                                                                id="fuelId_{{$outletsMaster->id}}_ful_{{ $fuelTypeMaster->id  }}"
                                                                                                                type="number"
                                                                                                                value="{{ imputOldValue('outlet['.$outletsMaster->id.']['.$fuelTypeMaster->id.'][price]') }}"
                                                                                                                name="outlet[{{ $outletsMaster->id }}][{{ $fuelTypeMaster->id }}][price]"
                                                                                                                class="validate  ">
                                                                                                            <label
                                                                                                                for="fuelId_{{$outletsMaster->id}}_ful_{{ $fuelTypeMaster->id  }}">
                                                                                                                {{ $fuelTypeMaster->name }}
                                                                                                            </label>
                                                                                                            {{ form_validation_error($errors, 'outlet['.$outletsMaster->id.']['.$fuelTypeMaster->id.'][price]') }}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                        @endif


                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    @endforeach
                                                                @endif


                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="card">

                                                        <div class="card-content">
                                                            <span class="card-title"> Lubes  Info </span>


                                                            <div class="row">


                                                                @if(count($oilGradeLists)> 0)
                                                                    @foreach($oilGradeLists AS $oilGradeList)
                                                                        <div
                                                                            class="col m4">
                                                                            <div
                                                                                class="input-group">
                                                                                <div
                                                                                    class="input-field ">
                                                                                    <input
                                                                                        id="lubes_{{$oilGradeList->id}}"
                                                                                        type="number"
                                                                                        value="{{ imputOldValue('lubes['.$oilGradeList->id.'][price]') }}"
                                                                                        name="lubes[{{$oilGradeList->id}}][price]"
                                                                                        class="validate  ">
                                                                                    <label
                                                                                        for="lubes_{{$oilGradeList->id}}">
                                                                                        {{ $oilGradeList->name }}
                                                                                        (liters)
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'lubes['.$oilGradeList->id.'][price]') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif


                                                            </div>


                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.formValidation').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        vehicle_number: {
                            required: true
                        },
                        vehicle_make_type: {
                            required: true
                        },
                        category_id: {
                            required: true
                        },
                        unit_id: {
                            required: true
                        },
                        fuel_type_id: {
                            required: true
                        },
                        purpose: {
                            required: true
                        },
                        quota_allotment: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        vehicle_number: {
                            required: "Enter Number"
                        },
                        vehicle_make_type: {
                            required: "Enter Make and Type"
                        },
                        category_id: {
                            required: "select category"
                        },
                        unit_id: {
                            required: "select unit"
                        },
                        fuel_type_id: {
                            required: "select fuel type"
                        },
                        purpose: {
                            required: "Enter purpose"
                        },
                        quota_allotment: {
                            required: "Enter quota allotment"
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });

        });

    </script>
@endsection
