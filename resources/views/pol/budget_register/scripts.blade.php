<script>



    $(function () {



        $('.manageForm').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',

                errorPlacement: function (error, e) {

                    var parentInputGroup = e.parents('.input-group');

                    console.log(parentInputGroup)

                    if (parentInputGroup.length == 0) {

                        var placement = $(e).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(e);
                        }

                    } else {
                        e.parents('.input-group > div').append(error);

                    }


                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },


                rules: {
                    purchase_date: {
                        required: true,
                        validDate: true
                    },
                    accout_number_id: {
                        required: true
                    },
                    invoice_number: {
                        required: true
                    },
                    bill_amount: {
                        required: true,
                    },
                    credit_amount_IOCL: {
                        required: true,
                    },
                    remarks: {
                        required: true,
                    },

                    status: {
                        required: true,
                    },

                },
                messages: {
                    purchase_date: {
                        required: "Select Date",
                    },
                    accout_number_id: {
                        required: "Select Account",
                    },
                    invoice_number: {
                        required: "Enter  Invoice Number",
                    },
                    bill_amount: {
                        required: "Enter Amount",
                    },
                    credit_amount_IOCL: {
                        required: "Enter Credit Amount",
                    },
                    remarks: {
                        required: "Enter Remark",
                    },

                    status: {
                        required: 'Select Status',

                    },

                },
            })


        })


        // $(".modal").modal({
        //     dismissible: false,
        // });
        // // $("#modal3").modal("open"),
        // // $("#modal3").modal("close")
    });

</script>
