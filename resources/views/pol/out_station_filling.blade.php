@extends('layout')
@section('title', $title)

@section('headerStyles')

    <style>
        tr.active {
            background: #0a6aa1;
            color: #FFF;
        }
    </style>
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m7 l7 ">

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                    <div class="row">
                                                        <form action="" method="get">
                                                            <div class="col s12  l3">
                                                                <div class="input-field col m12">

                                                                    <div
                                                                        class="input-field">
                                                                        <input
                                                                            id="search_date"
                                                                            type="text"
                                                                            value="{{request()->search_date}}"
                                                                            name="search_date"
                                                                            class="validate datepicker_ui">
                                                                        <label
                                                                            for="search_date">
                                                                            Date
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col s12 m6 l3 display-flex  show-btn">
                                                                <button type="submit"
                                                                        class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                    Filter Now
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>

                                                                <th data-field="name">
                                                                    filling_date
                                                                </th>
                                                                <th data-field="name">vehicle_number</th>


                                                                <th data-field="name">
                                                                    quantity
                                                                </th>
                                                                <th data-field="name">
                                                                    office_incharge
                                                                </th>


                                                                <th data-field="action">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <?php
                                                            //                                                            dump($listsItems);



                                                            ?>


                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr >
                                                                        <td>
                                                                            {{ $loop->iteration }}
                                                                        </td>
                                                                        <td>
                                                                            {{ date('d-m-Y', strtotime($list->filling_date)) }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $list->vehicle_number }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $list->quantity }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->office_incharge }}
                                                                        </td>

                                                                        <td>


                                                                            <form method="POST" action=""
                                                                                  accept-charset="UTF-8"
                                                                                  class="form-horizontal"
                                                                                  style="display:inline">
                                                                                {{ csrf_field() }}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $list->id }}"/>
                                                                                <input type="hidden" name="action"
                                                                                       value="remove"/>
                                                                                <button type="submit"
                                                                                        class="btn btn-sm btn-primary"
                                                                                        title="Delete {{ $module_name }}"
                                                                                        onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                                    Remove
                                                                                </button>

                                                                            </form>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="6">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pagination">
                                            <div class="col m12 left-align">

                                                {{--                                                Showing {{ $listsItems->firstItem() }}--}}
                                                {{--                                                to {{ $listsItems->lastItem() }} of {{ $listsItems->total() }} entries--}}

                                            </div>
                                            <div class="col m12 right-align">

                                                {{$lists->links('vendor.pagination.materializecss')}}

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col s12 m3 l3 ">

                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ form_flash_message('flash_message') }}
                                            </div>
                                        </div>

                                        <div class="card ">
                                            <form role="form" class="form-horizontal formValidation"
                                                  method="post"
                                                  enctype="multipart/form-data">

                                                <div class="card-content ">
                                                    <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                        <div class="col m12">

                                                            <div class="row">


                                                                <div class="col m12">
                                                                    <div class="input-field ">

                                                                        <select
                                                                            class="select2 vehicle_no"
                                                                            name="vehicle_no">

                                                                            <option value="">
                                                                                Select Vehicle
                                                                            </option>

                                                                            @if (count($vehicle_lists))
                                                                                @foreach($vehicle_lists AS $value)
                                                                                    <option
                                                                                        {{  imputOldValue('vehicle_no') ==$value->id ?'selected':'' }}
                                                                                        value="{{ $value->vehicle_no }}"
                                                                                        data-make="{{ $value->getVehicleMake->vehicle_make }}"
                                                                                        data-type="{{ $value->getVehicleType->vehicle_type }}"
                                                                                    >{{  $value->vehicle_no }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <small class="vehicle">
                                                                            Make: <span
                                                                                class="vehicle_box_make">xxxxx</span>

                                                                            <br/>
                                                                            Type: <span
                                                                                class="vehicle_box_type">xxxxx</span>
                                                                            <br/>
                                                                            Attached Unit: <span
                                                                                class="vehicle_box_AttachedUnit">---</span>
                                                                            <br/>
                                                                            Present KM: <span
                                                                                class="vehicle_box_PresentKM">---</span>
                                                                        </small>

                                                                    </div>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="filling_date"
                                                                           type="text"

                                                                           name="filling_date"
                                                                           class="validate datepicker_ui">
                                                                    <label for="filling_date">
                                                                        filling date
                                                                    </label>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="quantity"
                                                                           type="number"

                                                                           name="quantity"
                                                                           class="validate">
                                                                    <label for="quantity">
                                                                        quantity
                                                                    </label>
                                                                </div>

                                                                <div class="input-field col m12">
                                                                    <input id="office_incharge"
                                                                           type="text"

                                                                           name="office_incharge"
                                                                           class="validate">
                                                                    <label for="office_incharge">
                                                                        office_incharge
                                                                    </label>
                                                                </div>


                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="col m2"></div>--}}
                                                    </div>


                                                </div>


                                                <div class="card-action">

                                                    <button class="btn btn-primary">
                                                        Save
                                                    </button>

                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                </div>

                                            </form>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>

        function getVehicle(vhicleID) {

            var type = vhicleID.find('option:selected').data('type');
            var make = vhicleID.find('option:selected').data('make');

            $('.vehicle_box_type').html(type);
            $('.vehicle_box_make').html(make);


        }

        $(function () {

            $('.vehicle_no').on('change', function () {
                getVehicle($(this))
            });


            $('.formValidation').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        vehicle_number: {
                            required: true
                        },
                        vehicle_make_type: {
                            required: true
                        },
                        category_id: {
                            required: true
                        },
                        unit_id: {
                            required: true
                        },
                        fuel_type_id: {
                            required: true
                        },
                        purpose: {
                            required: true
                        },
                        quota_allotment: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        vehicle_number: {
                            required: "Enter Number"
                        },
                        vehicle_make_type: {
                            required: "Enter Make and Type"
                        },
                        category_id: {
                            required: "select category"
                        },
                        unit_id: {
                            required: "select unit"
                        },
                        fuel_type_id: {
                            required: "select fuel type"
                        },
                        purpose: {
                            required: "Enter purpose"
                        },
                        quota_allotment: {
                            required: "Enter quota allotment"
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });

        });

    </script>
@endsection
