@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">{{ ucwords($active) }}</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>


                                    <div class="col s12 m10 l10 ">


                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        {{ form_flash_message('flash_message') }}
                                                    </div>



                                                    <a href="{{ route('site.pol.fuel_issue', ['type'=>'vehicles']) }}" class="btn btn-large">
                                                        vehicles
                                                    </a>
                                                    <a href="{{ route('site.pol.fuel_issue', ['type'=>'hired-vehicles']) }}" class="btn btn-large">
                                                        Hired Vehicles
                                                    </a>

                                                    <a href="{{ route('site.pol.fuel_issue', ['type'=>'generator-spl']) }}" class="btn btn-large">
                                                        Generator / Spl. Purpose
                                                    </a>


                                                    <div class="col s12">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.vehicleGroup').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        years: {
                            required: true
                        },
                        mcondition: {
                            required: true
                        },
                        kms_reading: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        years: {
                            required: 'Please enter years'
                        },
                        mcondition: {
                            required: 'Please Select condition'
                        },
                        kms_reading: {
                            required: 'Please enter kms reading'
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
