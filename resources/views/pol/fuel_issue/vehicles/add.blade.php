@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">{{ ucwords($active) }}</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">
                                            <div class="col s6 show-btn align-right mt-1">
                                                <h4 class="card-title">Add {{ ucwords($module_name) }}</h4>


                                            </div>
                                            <div class="col s6 show-btn align-right mt-1">
                                                <a href="{{ route('site.pol.budget_register') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>

                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post" id="add_0"
                                                      enctype="multipart/form-data">


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>

                                                    <div class="row">

                                                        <div class="col m12">

                                                            <h3 class="tx_no">
                                                                TX No: <span class="number">
                                                                    {{ $tx_info['outlet'] }}{{ date('dmY', strtotime($tx_info['date'] ))  }}-{{ $tx_info['no'] }}
                                                                </span>
                                                            </h3>

                                                            <input type="hidden" name="tx_no_outlet_code"
                                                                   value="{{$tx_info['outlet']}}"/>
                                                            <input type="hidden" name="tx_no_date"
                                                                   value="{{$tx_info['date']}}"/>
                                                            <input type="hidden" name="tx_no_day_sno"
                                                                   value="{{$tx_info['no']}}"/>
                                                            <input type="hidden" name="fuel_issues_for"
                                                                   value="{{ request()->type }}"/>

                                                            <div class="card">

                                                                <?php
//                                                                dump($vehicle_lists[0]->getFuelQuotaAllotments('additional')->get())
                                                                ?>

                                                                <div class="card-content">
                                                                    {{--                                                                    <span class="card-title"> budget Register </span>--}}

                                                                    <div class="row">


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="issue_date"
                                                                                           type="text" readonly
                                                                                           value="{{ date('d-m-Y') }}"
                                                                                           name="issue_date"
                                                                                           class="validate ">
                                                                                    <label for="issue_date">
                                                                                        Date
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'issue_date') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <select
                                                                                class="select2 vehicle_no"
                                                                                name="vehicle_number">

                                                                                <option value="">
                                                                                    Select Vehicle Number
                                                                                </option>

                                                                                @if (count($vehicle_lists))
                                                                                    @foreach($vehicle_lists AS $value)
                                                                                        @php

                                                                                            $additionalData = $value->getFuelQuotaAllotments('additional')->get();
                                                                                        $additionalActive = $additionalData->where('status', 1)->first();

                                                                                            $regularData = $value->getFuelQuotaAllotments('regular')->get();
                                                                                        $regularActive = $regularData->where('status', 1)->first();

                                                                                            $enhancedData = $value->getFuelQuotaAllotments('enhanced')->get();
                                                                                        $enhancedActive = $enhancedData->where('status', 1)->first();

                                                                                            $drownData = $value->getFuelIssuesWithinMonth->sum('issue_liters');


                                                                                            $indentNo = $value->getFuelIssuesWithinDay->count();



                                                                                        @endphp
                                                                                        <option
                                                                                            {{  imputOldValue('vehicle_no') ==$value->id ?'selected':'' }}
                                                                                            value="{{ $value->vehicle_no }}"
                                                                                            data-make="{{ $value->getVehicleMake->vehicle_make }}"
                                                                                            data-type="{{ $value->getVehicleType->vehicle_type }}"
                                                                                            data-ident_no="{{ (isset($indentNo)) ? $indentNo+1 :1 }}"
                                                                                            data-additional-quota="{{ (isset($additionalActive->quota_liters)) ? $additionalActive->quota_liters:0 }}"
                                                                                            data-drown="{{ (isset($drownData)) ? $drownData:0 }}"
                                                                                            data-enhanced-quota="{{ (isset($enhancedActive->quota_liters)) ? $enhancedActive->quota_liters:0 }}"
                                                                                            data-regular-quota="{{ (isset($regularActive->quota_liters)) ? $regularActive->quota_liters:0}}"
                                                                                        >{{  $value->vehicle_no }}</option>
                                                                                    @endforeach
                                                                                @endif

                                                                            </select>

                                                                            <small class="vehicle">
                                                                                Make: <span
                                                                                    class="vehicle_box_make">
                                                                                               --
                                                                                            </span>

                                                                                <br/>
                                                                                Type: <span
                                                                                    class="vehicle_box_type">
                                                                                               --
                                                                                            </span>
                                                                                <br/>
                                                                                variant: <span
                                                                                    class="vehicle_box_variant">
                                                                                                --
                                                                                            </span>


                                                                            </small>
                                                                        </div>
                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="indent_number"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('indent_number ') }}"
                                                                                           name="indent_number_by_item"
                                                                                           class="validate">
                                                                                    <label for="indent_number">
                                                                                        Indent Number
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'indent_number') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="issue_liters"
                                                                                           type="number"
                                                                                           value="{{ imputOldValue('issue_liters') }}"
                                                                                           name="issue_liters"
                                                                                           class="validate">
                                                                                    <label for="issue_liters">
                                                                                        Required (L)
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'issue_liters') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <div class="row">

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="drown" readonly
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('drown ') }}"
                                                                                           name="drown"
                                                                                           class="validate">
                                                                                    <label for="drawn">
                                                                                        drown
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'drown') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="present_km"
                                                                                           type="number" readonly
                                                                                           value="0"
                                                                                           name="present_km"
                                                                                           class="validate">
                                                                                    <label for="present_km">
                                                                                        Present KM
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'present_km') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="quota_alloted"
                                                                                           type="number" readonly
                                                                                           value="{{ imputOldValue('quota_alloted') }}"
                                                                                           name="rec_quota_regular"
                                                                                           class="validate">
                                                                                    <label for="quota_alloted">
                                                                                        Quota Alloted
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'quota_alloted') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="additional_quota"
                                                                                           type="number" readonly
                                                                                           value="{{ imputOldValue('additional_quota') }}"
                                                                                           name="rec_quota_addit"
                                                                                           class="validate">
                                                                                    <label for="additional_quota">
                                                                                        Additional Quota
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'additional_quota') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="enhanced_quota"
                                                                                           type="number" readonly
                                                                                           value="{{ imputOldValue('enhanced_quota') }}"
                                                                                           name="rec_quota_enhan"
                                                                                           class="validate">
                                                                                    <label for="enhanced_quota">
                                                                                        Enhanced Quota
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'enhanced_quota') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m2">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="rec_quota_balance"
                                                                                           type="number" readonly
                                                                                           value="{{ imputOldValue('total_quota') }}"
                                                                                           name="rec_quota_balance" data-original="0"
                                                                                           class="validate">
                                                                                    <label for="rec_quota_balance">
                                                                                        Total Quota
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'total_quota') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>


                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>

                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')




    @include('pol.fuel_issue.vehicles.scripts')


@endsection
