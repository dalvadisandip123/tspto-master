<script>


    function totalQuotaInfo() {
        var quota_alloted = $('#quota_alloted').val();
        var additional_quota = $('#additional_quota').val();
        var enhanced_quota = $('#enhanced_quota').val();
        var issue_liters = $('#issue_liters').val();
        var drown = $('#drown').val();

        var total_quota = parseInt(quota_alloted) + parseInt(additional_quota) + parseInt(enhanced_quota);


        if (isNaN(parseInt(issue_liters))) {
            issue_liters = 0
        }


        // console.log( issue_liters)

        var ussageLiters =  parseInt(issue_liters) +  parseInt(drown);


        $('#rec_quota_balance').val(total_quota - parseInt(ussageLiters));
        $('#rec_quota_balance').attr('original', total_quota);
        $('#rec_quota_balance').next('label').addClass('active');
    }

    function getVehicle(vhicleID) {

        var type = vhicleID.find('option:selected').data('type');
        var make = vhicleID.find('option:selected').data('make');
        var additional_quota_data = vhicleID.find('option:selected').data('additional-quota');
        var regular_quota_data = vhicleID.find('option:selected').data('regular-quota');
        var enhanced_quota_data = vhicleID.find('option:selected').data('enhanced-quota');
        var ident_no = vhicleID.find('option:selected').data('ident_no');
        var drown = vhicleID.find('option:selected').data('drown');

        console.log(additional_quota_data)

        $('.vehicle_box_type').html(type);
        $('.vehicle_box_make').html(make);

        $('#additional_quota').val(additional_quota_data);
        $('#additional_quota').next('label').addClass('active');

        $('#quota_alloted').val(regular_quota_data);
        $('#quota_alloted').next('label').addClass('active');

        $('#enhanced_quota').val(enhanced_quota_data);
        $('#enhanced_quota').next('label').addClass('active');

        $('#indent_number').val(ident_no);
        $('#indent_number').next('label').addClass('active');

        $('#drown').val(drown);
        $('#drown').next('label').addClass('active');


        totalQuotaInfo();


    }

    $(function () {

        $('#issue_liters').on('input', function () {
            totalQuotaInfo();
        });

        $('.vehicle_no').on('change', function () {
            getVehicle($(this))
        });


        $.validator.addMethod('drownLimit', function (value, element, param) {

                return parseInt(value) <= parseInt($('#rec_quota_balance').attr('original'));
            },
            'Please enter a below total quota Limit');

        $('.manageForm').each(function () {
            $($(this)).validate({
                ignore: '',
                errorElement: 'div',
                errorPlacement: function (error, e) {

                    var parentInputGroup = e.parents('.input-group');

                    console.log(parentInputGroup)

                    if (parentInputGroup.length == 0) {

                        var placement = $(e).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(e);
                        }

                    } else {
                        e.parents('.input-group > div').append(error);

                    }


                },
                rules: {
                    issue_date: {
                        required: true,
                    },
                    vehicle_nos: {
                        required: true,
                    },
                    indent_number: {
                        required: true,
                    },
                    issue_liters: {
                        required: true,
                        drownLimit: true,
                        min: 1
                    },
                    drawn: {
                        required: true,
                    },
                    present_km: {
                        required: true,
                    },
                    quota_alloted: {
                        required: true,
                    },
                    additional_quota: {
                        required: true,
                    },
                    enhanced_quota: {
                        required: true,
                    },
                    total_quota: {
                        required: true,
                        min: 0
                    },
                },
                messages: {
                    issue_date: {
                        required: "select date",
                    },
                    issue_liters: {
                        required: "Enter ",
                        min: 'Enter minimum {0}'
                    },
                    vehicle_nos: {
                        required: "select vehicle",
                    },
                    indent_number: {
                        required: "enter ..",
                    },
                    quota_alloted: {
                        required: "enter Liters",
                    },
                    additional_quota: {
                        required: "enter Liters",
                    },
                    enhanced_quota: {
                        required: "enter Liters",
                    },
                    total_quota: {
                        required: "enter Liters",
                        min: 'Minimum 0 Liters'
                    },
                },
            })
        });


    })
</script>
