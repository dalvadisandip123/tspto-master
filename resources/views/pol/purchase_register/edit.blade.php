@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.employee') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                </div>


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post" id="add_2"
                                                      enctype="multipart/form-data">


                                                    @csrf

                                                    <input type="hidden" name="action" value="edit"/>

                                                    <input type="hidden" name="id" value="{{ $item->id }}"/>

                                                    <div class="row">

                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Purchase Register </span>

                                                                <div class="row">

                                                                    <div
                                                                        class="input-field col m3">
                                                                        <select id="financial_years"
                                                                                name="financial_years"
                                                                                class=""
                                                                        >

                                                                            <option value=""> Select</option>

                                                                            @if (count(financialYears()))
                                                                                @foreach(financialYears() AS $loop_item)
                                                                                    <option
                                                                                        {{$item->financial_years == $loop_item ?'selected':'' }}
                                                                                        value="{{ $loop_item }}">{{ $loop_item }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        <label for="financial_years">
                                                                            Finance Year </label>
                                                                    </div>

                                                                    <div class="col m3">
                                                                        <div class="input-group">
                                                                            <div class="input-field ">
                                                                                <input id="purchase_date"
                                                                                       type="text"
                                                                                       value="{{ date('d-m-Y', strtotime($item->purchase_date)) }}"
                                                                                       name="purchase_date"
                                                                                       class="validate datepicker_ui">
                                                                                <label for="purchase_date">
                                                                                    Date
                                                                                </label>
                                                                                {{ form_validation_error($errors, 'purchase_date') }}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col m3">

                                                                        <div
                                                                            class="input-field col m12">
                                                                            <select id="accout_number_id"
                                                                                    name="accout_number_id"
                                                                                    class=""
                                                                            >

                                                                                <option value=""> Select</option>

                                                                                @if (count($accountNumbersMasters))
                                                                                    @foreach($accountNumbersMasters AS $loop_item)
                                                                                        <option
                                                                                            {{$item->accout_number_id == $loop_item->id ?'selected':'' }}
                                                                                            value="{{ $loop_item->id }}">{{  $loop_item->name }}</option>
                                                                                    @endforeach
                                                                                @endif

                                                                            </select>

                                                                            <label for="accout_number_id">
                                                                                Account Numbers </label>
                                                                        </div>


                                                                    </div>

                                                                    <div class="col m3">
                                                                        <div class="input-group">
                                                                            <div class="input-field ">
                                                                                <input id="invoice_number"
                                                                                       type="text"
                                                                                       value="{{ $item->invoice_number }}"
                                                                                       name="invoice_number"
                                                                                       class="validate">
                                                                                <label for="invoice_number">
                                                                                    Invoice Number
                                                                                </label>
                                                                                {{ form_validation_error($errors, 'invoice_number') }}
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="row">

                                                                    <div class="col m4">
                                                                        <div class="input-group">
                                                                            <div class="input-field ">
                                                                                <input id="bill_amount"
                                                                                       type="number"
                                                                                       value="{{ $item->bill_amount }}"
                                                                                       name="bill_amount"
                                                                                       class="validate">
                                                                                <label for="bill_amount">
                                                                                    bill amount
                                                                                </label>
                                                                                {{ form_validation_error($errors, 'bill_amount') }}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col m4">
                                                                        <div class="input-group">
                                                                            <div class="input-field ">
                                                                                <input id="credit_amount_IOCL"
                                                                                       type="number"
                                                                                       value="{{ $item->credit_amount_IOCL }}"
                                                                                       name="credit_amount_IOCL"
                                                                                       class="validate">
                                                                                <label for="credit_amount_IOCL">
                                                                                    Credit Amount to IOCL
                                                                                </label>
                                                                                {{ form_validation_error($errors, 'credit_amount_IOCL') }}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col m4">
                                                                        <div class="input-group">
                                                                            <div class="input-field ">
                                                                                <input id="remarks"
                                                                                       type="text"
                                                                                       value="{{ $item->remarks }}"
                                                                                       name="remarks"
                                                                                       class="validate">
                                                                                <label for="remarks">
                                                                                    remarks
                                                                                </label>
                                                                                {{ form_validation_error($errors, 'remarks') }}
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                            </div>
                                                        </div>


                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Fuel Info </span>


                                                                <div class="row">


                                                                    @if(count($outletsMasters)> 0)
                                                                        @foreach($outletsMasters AS $outletsMaster)

                                                                            {{--                                                                            @php--}}
                                                                            {{--                                                                                $currentOutlets = $item->getFuels->where('outlet_id', $outletsMaster->id);--}}
                                                                            {{--                                                                            @endphp--}}

                                                                            <div class="col m4">
                                                                                <div class="card">

                                                                                    <div class="card-content">
                                                                                    <span
                                                                                        class="card-title">
                                                                                   {{ $outletsMaster->name }}
                                                                                    </span>

                                                                                        <div class="row">


                                                                                            @if(count($fuelTypeMasters)> 0)
                                                                                                @foreach($fuelTypeMasters AS $fuelTypeMaster)

                                                                                                    @php
                                                                                                        $currentOutletsFuels = $item->getFuels->where('outlet_id', $outletsMaster->id)->where('fuel_type_id', $fuelTypeMaster->id)->first();
                                                                                                    @endphp

                                                                                                    <div
                                                                                                        class="col m6">
                                                                                                        <?php
//                                                                                                        dump($currentOutletsFuels)
                                                                                                        ?>

                                                                                                        <div
                                                                                                            class="input-group">
                                                                                                            <div
                                                                                                                class="input-field ">
                                                                                                                <input
                                                                                                                    id="fuelId_{{$outletsMaster->id}}_ful_{{ $fuelTypeMaster->id  }}"
                                                                                                                    type="number"
                                                                                                                    value="{{ $currentOutletsFuels->fuel_price }}"
                                                                                                                    name="outlet[{{ $outletsMaster->id }}][{{ $fuelTypeMaster->id }}][price]"
                                                                                                                    class="validate  ">
                                                                                                                <label
                                                                                                                    for="fuelId_{{$outletsMaster->id}}_ful_{{ $fuelTypeMaster->id  }}">
                                                                                                                    {{ $fuelTypeMaster->name }}
                                                                                                                </label>
                                                                                                                {{ form_validation_error($errors, 'outlet['.$outletsMaster->id.']['.$fuelTypeMaster->id.'][price]') }}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endforeach
                                                                                            @endif


                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        @endforeach
                                                                    @endif


                                                                </div>


                                                            </div>
                                                        </div>


                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Lubes  Info </span>


                                                                <div class="row">


                                                                    @if(count($oilGradeLists)> 0)
                                                                        @foreach($oilGradeLists AS $oilGradeList)

                                                                            @php

                                                                                $currentLubs = $item->getLubs->where('lube_id', $oilGradeList->id)->first();

                                                                            @endphp
                                                                            <div
                                                                                class="col m4">

                                                                                <?php
//                                                                                dump($currentLubs)
                                                                                ?>


                                                                                <div
                                                                                    class="input-group">
                                                                                    <div
                                                                                        class="input-field ">
                                                                                        <input
                                                                                            id="lubes_{{$oilGradeList->id}}"
                                                                                            type="number"
                                                                                            value="{{ $currentLubs->lube_price }}"
                                                                                            name="lubes[{{$oilGradeList->id}}][price]"
                                                                                            class="validate  ">
                                                                                        <label
                                                                                            for="lubes_{{$oilGradeList->id}}">
                                                                                            {{ $oilGradeList->name }}
                                                                                            (liters)
                                                                                        </label>
                                                                                        {{ form_validation_error($errors, 'lubes['.$oilGradeList->id.'][price]') }}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    @endif


                                                                </div>


                                                            </div>
                                                        </div>

                                                    </div>


                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>

                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('mt.vehicles.scripts')

@endsection
