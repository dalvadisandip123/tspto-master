@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">Master</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('mt.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.mt.employee') }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">


                                                </div>


                                                <div class="row">

                                                    <div class="col m12">

                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> vehicles Info </span>


                                                                <table class="table">
                                                                    <tr>
                                                                        <td>
                                                                            Vehicle
                                                                            Make: {{ $item->getVehicleMake->vehicle_make }}
                                                                        </td>
                                                                        <td>
                                                                            Vehicle
                                                                            Type: {{ $item->getVehicleType->vehicle_type }}
                                                                        </td>
                                                                        <td>
                                                                            Vehicle
                                                                            Type: {{ $item->getVehicleVariant->vehicle_variant }}
                                                                        </td>
                                                                        <td>
                                                                            Category: {{ $item->getCategory->vehicle_category }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>
                                                                            VIN NO: {{ $item->vin_no }}
                                                                        </td>
                                                                        <td>
                                                                            Vehicle no {{ $item->vehicle_no }}
                                                                        </td>
                                                                        <td>
                                                                            TR NO: {{ $item->tr_no }}
                                                                        </td>
                                                                        <td>
                                                                            Fuel Type: {{ $item->getFuelType->name }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>
                                                                            Model : {{ $item->model }}
                                                                        </td>
                                                                        <td>
                                                                            engine_no {{ $item->engine_no }}
                                                                        </td>
                                                                        <td>
                                                                            chassis_no: {{ $item->chassis_no }}
                                                                        </td>
                                                                        <td>
                                                                            RC Validity
                                                                            Date: {{ date('d-m-Y', strtotime($item->rc_validity_date))  }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>
                                                                            Group : {{ $item->getGroup->name }}
                                                                        </td>
                                                                        <td>
                                                                            usage / Pupose: {{ $item->getUsage->name }}

                                                                        </td>
                                                                        <td>
                                                                            Body Type: {{ $item->getBodyType->name }}
                                                                        </td>
                                                                        <td>
                                                                            tank_capacity: {{ $item->tank_capacity }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>
                                                                            kmpl : {{ $item->kmpl }}
                                                                        </td>
                                                                        <td>
                                                                            reserve_fuel: {{ $item->reserve_fuel }}

                                                                        </td>
                                                                        <td>

                                                                        </td>
                                                                        <td>

                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                            </div>
                                                        </div>


                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Record  Info </span>


                                                                <table class="table">

                                                                    <tr>

                                                                        <td>
                                                                            oil Grade: {{ $item->getOilGrade->name }}
                                                                        </td>
                                                                        <td>
                                                                            bore stroke: {{ $item->bore_stroke }}
                                                                        </td>
                                                                        <td>
                                                                            no of cylinders: {{ $item->no_of_cylinders }}
                                                                        </td>
                                                                        <td>
                                                                            seating capacity: {{ $item->seating_capacity }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>
                                                                            allottment order no
                                                                            : {{ $item->allottment_order_no }}
                                                                        </td>
                                                                        <td>
                                                                            allotment date
                                                                            : {{ date('d-m-Y', strtotime($item->allotment_date)) }}
                                                                        </td>
                                                                        <td>
                                                                            bhp rpm: {{ $item->bhp_rpm }}
                                                                        </td>
                                                                        <td>
                                                                            opening km : {{ $item->opening_km }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>
                                                                            piston displacement
                                                                            : {{ $item->piston_displacement }}
                                                                        </td>
                                                                        <td>
                                                                            current meter reading
                                                                            : {{ $item->current_meter_reading }}
                                                                        </td>
                                                                        <td>
                                                                            go number: {{ $item->go_number }}
                                                                        </td>
                                                                        <td>
                                                                            go_date
                                                                            : {{ date('d-m-Y', strtotime($item->go_date)) }}
                                                                        </td>
                                                                    </tr>

                                                                </table>


                                                            </div>
                                                        </div>


                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Dealer  Info </span>


                                                                <table class="table">

                                                                    <tr>

                                                                        <td>
                                                                            invoice no: {{ $item->invoice_no }}
                                                                        </td>
                                                                        <td>
                                                                            vehicle purchase from: {{ $item->vehicle_purchase_from }}
                                                                        </td>
                                                                        <td>
                                                                            dealer name: {{ $item->dealer_name }}
                                                                        </td>
                                                                        <td>
                                                                            dealer mobile no: {{ $item->dealer_mobile_no }}
                                                                        </td>
                                                                    </tr>


                                                                    <tr>

                                                                        <td>
                                                                            po number: {{ $item->po_number }}
                                                                        </td>
                                                                        <td>
                                                                            po date: {{ date('d-m-Y', strtotime($item->po_date)) }}
                                                                        </td>
                                                                        <td>
                                                                            delivery date: {{ date('d-m-Y', strtotime($item->delivery_date)) }}
                                                                        </td>
                                                                        <td>
                                                                            date of issue: {{ date('d-m-Y', strtotime($item->date_of_issue)) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>
                                                                            vehicle cost: {{ $item->vehicle_cost }}
                                                                        </td>
                                                                        <td>
                                                                            tax type: {{ $item->tax_type }}
                                                                        </td>
                                                                        <td>
                                                                            tax pecentage: {{ $item->tax_pecentage }}
                                                                        </td>
                                                                        <td>
                                                                            tax paid amount: {{$item->tax_paid_amount }}
                                                                        </td>
                                                                    </tr>


                                                                </table>


                                                            </div>
                                                        </div>


                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Additional  Info </span>

                                                                <table class="table">

                                                                    <tr>

                                                                        <td>
                                                                            key no: {{ $item->key_no }}
                                                                        </td>
                                                                        <td>
                                                                            no of tyres: {{ $item->no_of_tyres }}
                                                                        </td>
                                                                        <td>
                                                                            no of battery: {{ $item->no_of_battery }}
                                                                        </td>
                                                                        <td>
                                                                            type of drive: {{ $item->type_of_drive }}
                                                                        </td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>
                                                                            stock ledger number: {{ $item->stock_ledger_number }}
                                                                        </td>
                                                                        <td>
                                                                            page number: {{ $item->page_number }}
                                                                        </td>
                                                                        <td>
                                                                            sl no: {{ $item->sl_no }}
                                                                        </td>
                                                                        <td>


                                                                        </td>
                                                                    </tr>


                                                                </table>


                                                            </div>


                                                        </div>


                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Last Info </span>

                                                                <div class="row">
                                                                    <div class="col m10">

                                                                        Vehicle Description:

                                                                        {{ $item->vehicle_description }}


                                                                    </div>

                                                                    <div class="col m2">

                                                                        Status: {{ status_list($item->status) }}


                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>


                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Tyre  Lists </span>

                                                                <?php
                                                                $getTyreHistory = $item->geteTyreHistory->take(10);

//                                                                dump($getTyreHistory)
                                                                ?>

                                                                <table class="table">

                                                                    <tr>

                                                                        <th>
                                                                            Make:
                                                                        </th>
                                                                        <th>
                                                                            life span:
                                                                        </th>
                                                                        <th>
                                                                           Size:
                                                                        </th>
                                                                        <th>
                                                                           number:
                                                                        </th>
                                                                        <th>
                                                                            issue date:
                                                                        </th>

                                                                    </tr>

                                                                    @if(count($getTyreHistory)> 0)
                                                                        @foreach($getTyreHistory AS $getItem)
                                                                            <?php
//                                                                            dump($getItem->getTyreMake);
                                                                            ?>
                                                                            <tr>

                                                                                <td>
                                                                                    {{ (isset($getItem->getTyreMake)) ? $getItem->getTyreMake->tyre_make: '' }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getItem->tyre_life_span }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getItem->tyre_size }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getItem->tyre_number }}
                                                                                </td>

                                                                                <td>

                                                                                    {{ date('d-m-Y', strtotime($getItem->tyre_issue_date)) }}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif


                                                                </table>

                                                            </div>
                                                        </div>

                                                        <div class="card">

                                                            <div class="card-content">
                                                                <span class="card-title"> Bataries  Lists </span>

                                                                <?php
                                                                $getBatteryHistory = $item->getBatteryHistory->take(10);

                                                                //                                                                dump($getBatteryHistory)
                                                                ?>

                                                                <table class="table">

                                                                    <tr>

                                                                        <th>
                                                                            Make:
                                                                        </th>
                                                                        <th>
                                                                            Voltage:
                                                                        </th>
                                                                        <th>
                                                                            Ampere:
                                                                        </th>
                                                                        <th>
                                                                            Size:
                                                                        </th>
                                                                        <th>
                                                                            Life
                                                                            Span:
                                                                        </th>
                                                                        <th>
                                                                            Number:
                                                                        </th>
                                                                        <th>
                                                                            Issue
                                                                            Date:
                                                                        </th>
                                                                    </tr>

                                                                    @if(count($getBatteryHistory)> 0)
                                                                        @foreach($getBatteryHistory AS $getBatteryHistor)
                                                                            <tr>

                                                                                <td>
                                                                                    {{ (isset($getBatteryHistor->getBatteryMake))? $getBatteryHistor->getBatteryMake->battery_make:'' }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getBatteryHistor->battery_voltage }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getBatteryHistor->battery_ampere }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getBatteryHistor->battery_size }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getBatteryHistor->battery_life_span }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $getBatteryHistor->battery_number }}
                                                                                </td>
                                                                                <td>

                                                                                    {{ date('d-m-Y', strtotime($getBatteryHistor->battery_issue_date)) }}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif


                                                                </table>

                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('mt.vehicles.scripts')

@endsection
