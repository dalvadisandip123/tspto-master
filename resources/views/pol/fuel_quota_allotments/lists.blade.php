@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}
@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">{{ ucwords($active) }}</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>


                                    <div class="col s12 m10 l10 ">
                                        <div class="row">

                                            <div class="col s12 show-btn align-right mt-1">
                                                <a href="{{ route('site.pol.fuel_quota_allotments', ['type'=>request()->type,'action'=>'add']) }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Add
                                                </a>


                                            </div>
                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        {{ form_flash_message('flash_message') }}
                                                    </div>


                                                    <div class="col l12">

                                                        <form action="" method="get">
                                                            <div class="row">
                                                                <div class="col s12  l3">
                                                                    <div class="input-field col m12">
                                                                        <input id="mobile_number_search" type="text"
                                                                               name="vehicle_no"
                                                                               value="{{ request()->vehicle_no }}">
                                                                        <label
                                                                            for="mobile_number_search">
                                                                            vehicle no
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div class="col s12  l3">
                                                                    <div class="input-field col m12">
                                                                        <select name="status">
                                                                            <option value="">All</option>
                                                                            @if (count(status_list()))
                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                    <option
                                                                                        {{  request()->status==$staus_key ?'selected':'' }}
                                                                                        value="{{ $staus_key }}">{{  $staus }}</option>
                                                                                @endforeach
                                                                            @endif

                                                                        </select>

                                                                        {{--                                                                                <label>status</label>--}}
                                                                    </div>
                                                                </div>


                                                                <div class="col s12 m6 l3 display-flex  show-btn">
                                                                    <button type="submit"
                                                                            class="btn align-items-right btn-block indigo waves-effect waves-light">
                                                                        Filter Now
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>


                                                    </div>


                                                    <div class="col s12">
                                                    </div>
                                                    <div class="col s12">
                                                        <table class="bordered">
                                                            <thead>
                                                            <tr>
                                                                <th data-field="id">#</th>
                                                                <th data-field="vehicle_make">
                                                                    Vehicle Number
                                                                </th>
                                                                <th data-field="vehicle_make">
                                                                    Quota Liters
                                                                </th>
                                                                <th data-field="vehicle_type">
                                                                    Allotment User
                                                                </th>
                                                                <th data-field="vehicle_type">
                                                                    Allotment Date
                                                                </th>

                                                                <th data-field="Status">Status</th>
                                                                <th data-field="action">
                                                                    More..
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if (count($lists)> 0)
                                                                @foreach($lists AS $list)


                                                                    <tr>
                                                                        <td>
                                                                            {{ $list->id }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->vehicle_number }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $list->quota_liters }}
                                                                        </td>
                                                                        <td>
                                                                            {{ gerUserById($list->allotment_user_id)->name }}
                                                                        </td>
                                                                        <td>
                                                                            {{ date('d-m-Y', strtotime($list->created_at)) }}
                                                                        </td>


                                                                        <td>
                                                                            {{ status_list($list->status) }}
                                                                        </td>
                                                                        <td>

                                                                            <!-- Modal Trigger -->
                                                                            <a class="waves-effect waves-light btn modal-trigger"
                                                                               href="#edit_{{ $list->id }}">
                                                                                Logs
                                                                            </a>

                                                                            <!-- Modal Structure -->
                                                                            <div id="edit_{{ $list->id }}"
                                                                                 class="modal modal-fixed-footer">


                                                                                <div class="modal-content">
                                                                                    <h4>
                                                                                        {{--                                                                                        Units--}}
                                                                                    </h4>

                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col s12 m12 l12">

                                                                                            @if($list->logs)
                                                                                                <table class="table">
                                                                                                    <tr>
                                                                                                        <td>date</td>
                                                                                                        <td>User</td>
                                                                                                        <td>quota</td>
                                                                                                    </tr>
                                                                                                    @foreach($list->logs AS $log)
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                {{ date('d-m-Y', strtotime($log->created_at)) }}
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                {{ gerUserById($list->allotment_user_id)->name }}
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                {{ $log->quota_liters }}
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    @endforeach
                                                                                                </table>
                                                                                            @endif

                                                                                        </div>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button class="btn btn-small">
                                                                                        Update
                                                                                    </button>
                                                                                    <a href="#!"
                                                                                       class="modal-close waves-effect waves-green btn-flat">close</a>
                                                                                </div>


                                                                            </div>

                                                                        </td>
                                                                    </tr>


                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="4">No Records Found</td>
                                                                </tr>
                                                            @endif


                                                            </tbody>
                                                        </table>
                                                    </div>


                                                    <div class="pagination">
                                                        <div class="col m12 left-align">

                                                            Showing {{ $lists->firstItem() }}
                                                            to {{ $lists->lastItem() }} of {{ $lists->total() }} entries

                                                        </div>
                                                        <div class="col m12 right-align">

                                                            {{$lists->links('vendor.pagination.materializecss')}}

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')

    <script>
        $(function () {


            $('.vehicleGroup').each(function () {
                $($(this)).validate({
                    ignore: [],
                    errorElement: 'div',
                    errorPlacement: function (error, element) {
                        var placement = $(element).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        years: {
                            required: true
                        },
                        mcondition: {
                            required: true
                        },
                        kms_reading: {
                            required: true
                        },
                        status: {
                            required: true,
                        },

                    },
                    messages: {
                        years: {
                            required: 'Please enter years'
                        },
                        mcondition: {
                            required: 'Please Select condition'
                        },
                        kms_reading: {
                            required: 'Please enter kms reading'
                        },
                        status: {
                            required: 'Select Status',

                        },

                    },
                });

            })


            $(".modal").modal({
                dismissible: false,
            });
            // $("#modal3").modal("open"),
            // $("#modal3").modal("close")
        });

    </script>
@endsection
