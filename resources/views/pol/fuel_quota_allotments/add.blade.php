@extends('layout')
@section('title', $title)

@section('headerStyles')
    {{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/pages/page-account-settings.min.css') }}">--}}

@endsection


@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">


                        <div class="row">

                            <div class="col s12">
                                <nav>
                                    <div class="nav-wrapper">
                                        <div class="col s12">
                                            <a href="#!" class="breadcrumb">Home</a>
                                            <a href="#!" class="breadcrumb">{{ ucwords($active) }}</a>
                                            <a href="#!" class="breadcrumb">
                                                {{ ucwords($module_name) }}
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col s12 m12 l12">


                                <div class="row">

                                    <div class="col s12 m2 l2 ">

                                        @include('pol.includs.leftmenu')


                                    </div>

                                    <div class="col s12 m10 l10 ">

                                        <div class="row">
                                            <div class="col s6 show-btn align-right mt-1">
                                                <span class="card-title">Add {{ ucwords($module_name) }}</span>


                                            </div>
                                            <div class="col s6 show-btn align-right mt-1">
                                                <a href="{{ route('site.pol.fuel_quota_allotments', ['type'=>request()->type]) }}"
                                                   class="btn btn-small  indigo waves-effect waves-light float-right">
                                                    Back
                                                </a>


                                            </div>

                                        </div>

                                        <div id="bordered-table" class="card card card-default scrollspy">
                                            <div class="card-content">


                                                <form role="form" class="form-horizontal manageForm"
                                                      method="post" id="add_0"
                                                      enctype="multipart/form-data">


                                                    @csrf

                                                    <input type="hidden" name="action" value="add"/>
                                                    <input type="hidden" name="allotments_type"
                                                           value="{{ request()->type }}"/>

                                                    <div class="row">

                                                        <div class="col m12">

                                                            <div class="card">

                                                                <div class="card-content">


                                                                    <div class="row">

                                                                        <div class="col m3">

                                                                            <div
                                                                                class="input-field ">
                                                                                <select id="type_select" name="type"
                                                                                        class="">

                                                                                    <option value=""> Select</option>

                                                                                    <option value="vehicle">Vehicle
                                                                                    </option>
                                                                                    <option value="unit">Unit</option>

                                                                                </select>

                                                                                <label for="financial_years">
                                                                                    Type *
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="unit_box" style="display: none">
                                                                            <div class="col m3">

                                                                                <div
                                                                                    class="input-field ">
                                                                                    <select class="units_select"
                                                                                            name="units_masters_id">
                                                                                        <option value="">Select Unit
                                                                                        </option>
                                                                                        @if (count(getUnits()))
                                                                                            @foreach(getUnits() AS $value)
                                                                                                <option
                                                                                                    {{  imputOldValue('units_masters_id') ==$value->id ?'selected':'' }}
                                                                                                    value="{{ $value->id }}">{{  $value->unit_name }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>

                                                                                    <label for="financial_years">
                                                                                        Unit *
                                                                                    </label>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col m3">

                                                                                <div
                                                                                    class="input-field ">
                                                                                    <select
                                                                                        name="vehicle_category_id"
                                                                                        class="vehicle_category"
                                                                                    >

                                                                                        <option value=""> Select
                                                                                        </option>

                                                                                        @if (count(vehicleCategory()))
                                                                                            @foreach(vehicleCategory() AS $value)
                                                                                                <option

                                                                                                    value="{{ $value->id }}">{{  $value->vehicle_category }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>

                                                                                    <label for="financial_years">
                                                                                        Category *
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col m3 vehicle_box"
                                                                             style="display: none">
                                                                            <div class="input-group">

                                                                                <div class="input-field ">


                                                                                    <select
                                                                                        class="select2 vehicle_no"
                                                                                        name="vehicle_nos[]">

                                                                                        <option value="">
                                                                                            Select Vehicle
                                                                                        </option>

                                                                                        @if (count($vehicle_lists))
                                                                                            @foreach($vehicle_lists AS $value)
                                                                                                <option
                                                                                                    {{  imputOldValue('vehicle_no') ==$value->id ?'selected':'' }}
                                                                                                    value="{{ $value->vehicle_no }}"
                                                                                                    data-make="{{ $value->getVehicleMake->vehicle_make }}"
                                                                                                    data-type="{{ $value->getVehicleType->vehicle_type }}"
                                                                                                >{{  $value->vehicle_no }}</option>
                                                                                            @endforeach
                                                                                        @endif

                                                                                    </select>

                                                                                    <small class="vehicle">
                                                                                        Make: <span
                                                                                            class="vehicle_box_make">
                                                                                               --
                                                                                            </span>

                                                                                        <br/>
                                                                                        Type: <span
                                                                                            class="vehicle_box_type">
                                                                                               --
                                                                                            </span>
                                                                                        <br/>
                                                                                        variant: <span
                                                                                            class="vehicle_box_variant">
                                                                                                --
                                                                                            </span>
                                                                                        <br/>
                                                                                        model: <span
                                                                                            class="vehicle_box_model">
                                                                                             --
                                                                                            </span>
                                                                                        <br/>
                                                                                        category: <span
                                                                                            class="vehicle_box_category">
                                                                                              --

                                                                                            </span>
                                                                                    </small>
                                                                                </div>


                                                                            </div>
                                                                        </div>


                                                                        <div class="col m3 quota_liters_box"
                                                                             style="display: none">
                                                                            <div class="input-group">
                                                                                <div class="input-field ">
                                                                                    <input id="quota_liters"
                                                                                           type="text"
                                                                                           value="{{ imputOldValue('quota_liters') }}"
                                                                                           name="quota_liters"
                                                                                           class="validate">
                                                                                    <label for="quota_liters">
                                                                                        Quota Alloted ( Liters )
                                                                                    </label>
                                                                                    {{ form_validation_error($errors, 'quota_liters') }}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col m12 listData"></div>
                                                                    </div>


                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>


                                                    <div class="card-action">

                                                        <button type="submit" class="btn btn-primary">
                                                            Save
                                                        </button>

                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                        {{--                                                <a href="#!" class=" text-accent-1">This is a link</a>--}}
                                                    </div>


                                                </form>


                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('footerScripts')


    @include('pol.fuel_quota_allotments.scripts')

    {{--    @include('pol.budget_register.scripts')--}}


@endsection
