<script>

    function getVehicles() {
        var units_id = $('.units_select').val();
        var vehicle_category = $('.vehicle_category').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: '{{ route('ajax.get_vehicle_by_unit') }}',
            type: 'POST',
            data: {units_id: units_id, vehicle_category: vehicle_category},
            success: function (response) {
                var resultdata = response.list;
                //
                // if (resultdata.officer) {
                //     $('.designation').html(resultdata.officer.get_office_designation.name)
                //     $('.department').html(resultdata.officer.get_office.name)
                //
                // }

                var creteHtml = '';


                creteHtml += '<ul class="collection">';

                creteHtml += '<li class="collection-item">'
                    + '<label>'
                    + '<input type="checkbox"  class="selectall" />'
                    + '<span> Select All </span>'
                    + '</label>'
                    + '</li>';


                $.each(resultdata, function (e, v) {


                    creteHtml += '<li class="collection-item">'
                        + '<label>'
                        + '<input type="checkbox" class="vehicle_nos" value="' + v.vehicle_no + '"  name="vehicle_nos[]" />'
                        + '<span>' + v.vehicle_no + '</span>'
                        + '</label>'
                        + '</li>';

                    console.log(creteHtml)
                });
                creteHtml += '</ul>'

                $('.listData').html(creteHtml);


                $(".selectall").click(function () {
                    $('.collection input:checkbox').not(this).prop('checked', this.checked);
                });


                // $(".vehicle_nos").rules("add", {
                //     required: true,
                //     minlength: 1,
                //     messages: {
                //         required: "select vehicle",
                //     }
                // });

                console.log(resultdata)


            }
        });
    }

    function getVehicle(vhicleID) {

        var type = vhicleID.find('option:selected').data('type');
        var make = vhicleID.find('option:selected').data('make');

        $('.vehicle_box_type').html(type);
        $('.vehicle_box_make').html(make);


    }


    $(function () {


        $('.vehicle_no').on('change', function () {
            getVehicle($(this))
        });


        $('.manageForm').each(function () {
            $($(this)).validate({
                ignore: [],
                errorElement: 'div',
                errorPlacement: function (error, e) {

                    var parentInputGroup = e.parents('.input-group');

                    console.log(parentInputGroup)

                    if (parentInputGroup.length == 0) {

                        var placement = $(e).data('error');
                        if (placement) {
                            $(placement).append(error)
                        } else {
                            error.insertAfter(e);
                        }

                    } else {
                        e.parents('.input-group > div').append(error);

                    }


                },
                rules: {
                    type: {
                        required: true,
                    },
                    quota_liters: {
                        required: true,
                    },
                },
                messages: {
                    type: {
                        required: 'Select Type',
                    },
                    quota_liters: {
                        required: "enter Liters",
                    },
                },
            })
        });


        $('.units_select').on('change', function () {
            getVehicles();

        })

        $('.vehicle_category').on('change', function () {
            getVehicles();
        })


        $('#type_select').on('change', function () {
            var TypeVale = $(this).val();


            if (TypeVale == 'unit') {
                $('.vehicle_box').hide();
                $('.unit_box').show();
                $('.quota_liters_box').show();


                $(".vehicle_category").rules("add", {
                    required: true,
                    messages: {
                        required: "select category",
                    }
                });
                $(".units_select").rules("add", {
                    required: true,
                    messages: {
                        required: "select unit",
                    }
                });

                $(".vehicle_no").rules("remove");





            } else if (TypeVale == 'vehicle') {
                $('.unit_box').hide();
                $('.vehicle_box').show();
                $('.quota_liters_box').show();


                $(".vehicle_no").rules("add", {
                    required: true,
                    messages: {
                        required: "select vehicle",
                    }
                });


                $(".vehicle_category").rules("remove");
                $(".units_select").rules("remove");
                $(".vehicle_nos").rules("remove");



            }


            $('.vehicle_no').prop('selectedIndex', '0');
            $('.vehicle_category').prop('selectedIndex', '0');
            $('.units_select').prop('selectedIndex', '0');

            $('.vehicle_category').formSelect();
            $('.units_select').formSelect();
            $('.vehicle_no').sm_select();

            $('.listData').html('');
            console.log(TypeVale)
        })
    })
</script>


