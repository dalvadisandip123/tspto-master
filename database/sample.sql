
INSERT INTO `batteries_and_tyres_variant_masters` (`id`, `vehicle_make_id`, `vehicle_type_id`, `vehicle_variant_id`, `vehicle_category_id`, `life_span`, `battery_size`, `battery_amp`, `battery_volt`, `tyre_life_span`, `tyre_front_size`, `tyre_rear_size`, `battery_name`, `tyre_make`, `tyre_name`, `no_tyres`, `yom`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 3, 1, '10', '1aaaaaaaaaaaaaaa', 100, 100, 100, '10dddddddd', '10sssssssssss', NULL, NULL, NULL, NULL, NULL, '1', '2020-04-03 07:54:49', '2020-04-03 21:52:29'),
(2, 4, 6, 2, 2, '3', 'Adipisci labore accu', 39, 65, 95, 'Totam voluptates exc', 'Elit est minus sit', NULL, NULL, NULL, NULL, NULL, '1', '2020-04-03 09:30:40', '2020-04-03 09:30:40'),
(3, 1, 4, 1, 1, '3', 'Ut reprehenderit om', 59, 53, 29, 'Eaque qui dolorum ab', 'Facilis qui facilis', NULL, NULL, NULL, NULL, NULL, '1', '2020-04-03 21:52:20', '2020-04-03 21:52:20');


INSERT INTO `batteries_masters` (`id`, `battery_make`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Amaron', 1, '2020-04-02 06:16:17', '2020-04-02 06:25:34'),
(2, 'Exide', 1, '2020-04-02 06:16:21', '2020-04-02 06:25:39'),
(3, 'Luminous', 1, '2020-04-02 06:16:25', '2020-04-02 06:25:46'),
(4, 'Livguard', 1, '2020-04-02 06:25:53', '2020-04-02 06:25:53');


INSERT INTO `condemnation_masters` (`id`, `years`, `kms_reading`, `mcondition`, `status`, `created_at`, `updated_at`) VALUES
(1, 15, 25000, 'AND', 1, '2020-04-02 21:13:58', '2020-04-02 21:16:50');

INSERT INTO `description_by_vehicle_type_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'PS', 1, '2020-04-02 20:48:48', '2020-04-02 20:48:48'),
(2, 'POLCE STATION', 1, '2020-04-02 20:48:54', '2020-04-02 20:48:54'),
(3, 'MT POOL', 1, '2020-04-02 20:49:30', '2020-04-02 20:49:30');


INSERT INTO `employees` (`id`, `employee_id`, `user_id`, `name`, `photo`, `employee_role_masters_id`, `employee_rank_masters_id`, `number`, `driving_licence_number`, `licence_type`, `licence_validity`, `units_masters_id`, `parent_unit`, `working_section`, `officer_name_masters_id`, `date_appointment`, `duty_type`, `retirement_date`, `languages_known`, `mother_tounge`, `native_place`, `blood_group`, `family_mobile_number`, `arogya_bhadratha_number`, `residence`, `rewards_or_pathakams`, `punishments`, `accident_history`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dicta porro recusand', 2, 'Ryan Horne', NULL, 2, 3, 183, '473', 'Consectetur rerum ma', '2020-04-23', NULL, 'Eos pariatur In nu', 'Id dolore amet ea m', NULL, '2020-04-09', 'Obcaecati velit do e', '2020-04-01', 'Rerum aut eum possim', 'Laboris rem sed labo', 'Numquam id sunt do', 'A+', '236', '806', 'Qui suscipit fugiat', 'Id dolores fugiat q', 'Amet officiis ea ea', 'Fugiat assumenda mo', 1, '2020-04-13 09:05:55', '2020-04-13 09:05:55');



INSERT INTO `employee_rank_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'SI', 1, '2020-04-02 21:35:14', '2020-04-02 21:35:14'),
(2, 'PC', 1, '2020-04-02 21:35:20', '2020-04-02 21:35:20'),
(3, 'HC', 1, '2020-04-02 21:35:26', '2020-04-02 21:35:26'),
(4, 'CI', 1, '2020-04-02 21:35:37', '2020-04-02 21:35:37');


INSERT INTO `employee_role_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Wrighter', 1, '2020-04-02 21:24:19', '2020-04-02 21:24:19'),
(2, 'Officer Driver', 1, '2020-04-02 21:24:32', '2020-04-02 21:24:32'),
(3, 'MT Pool Driver', 1, '2020-04-02 21:24:37', '2020-04-02 21:24:37'),
(4, 'Ministerial Staff', 1, '2020-04-02 21:24:42', '2020-04-02 21:24:42'),
(5, 'Mechanic', 1, '2020-04-02 21:24:47', '2020-04-02 21:24:47'),
(6, 'Driver', 1, '2020-04-02 21:24:53', '2020-04-02 21:24:53'),
(7, 'Child Welfare Officer', 1, '2020-04-02 21:25:10', '2020-04-02 21:25:10'),
(8, 'Seniour Officers (DSP & Above)', 1, '2020-04-02 21:25:19', '2020-04-02 21:25:19'),
(9, 'Juniour Officers (Inpsector & Below)', 1, '2020-04-02 21:25:25', '2020-04-02 21:25:25'),
(10, 'ARSI', 1, '2020-04-02 21:25:32', '2020-04-02 21:25:32'),
(11, 'Officer', 1, '2020-04-02 21:25:36', '2020-04-02 21:25:36');

INSERT INTO `emp_attendances` (`id`, `employees_id`, `employee_role_masters_id`, `attendance`, `attendance_date`, `duty_type`, `other_remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'P', '2020-04-23', NULL, NULL, 1, '2020-04-05 08:19:17', '2020-04-05 08:19:17'),
(2, 2, 2, 'P', '2020-04-22', NULL, NULL, 1, '2020-04-05 08:19:17', '2020-04-05 08:19:17'),
(3, 1, 1, 'A', '2020-04-06', NULL, NULL, 1, '2020-04-05 08:19:55', '2020-04-05 08:19:55'),
(4, 2, 2, 'P', '2020-04-06', NULL, NULL, 1, '2020-04-05 08:20:08', '2020-04-05 08:20:08'),
(5, 2, 2, 'D', '2020-04-06', NULL, NULL, 1, '2020-04-05 08:20:08', '2020-04-05 08:20:08');


INSERT INTO `emp_deputation_ptos` (`id`, `employees_id`, `units_masters_id`, `atached_date`, `return_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2020-04-28', '2020-04-22', 1, '2020-04-05 06:36:37', '2020-04-05 06:36:37');


INSERT INTO `emp_holiday_duties` (`id`, `employees_id`, `duty_date`, `reason_for_duty`, `place_of_duty`, `time`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-04-15', 'bandabasth', 'vikarabad', '10 am', 1, '2020-04-05 06:56:11', '2020-04-05 06:58:52');


INSERT INTO `emp_reportings` (`id`, `employees_id`, `report_date`, `relieve_date`, `purpose`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-04-08', '2020-04-08', 'aaaaa', 1, '2020-04-05 06:46:05', '2020-04-05 06:46:52');



INSERT INTO `filters_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Filters 1', 1, '2020-04-05 09:53:55', '2020-04-05 09:54:00'),
(7, 'Filters 2', 1, '2020-04-05 09:54:03', '2020-04-05 09:54:03'),
(8, 'Filters 3', 1, '2020-04-05 09:54:05', '2020-04-05 09:54:05');


INSERT INTO `fuel_ground_tank_masters` (`id`, `outlet_name`, `fuel_type`, `tank_capacity`, `evaporation`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '12000', 'Ground Tank 1', 1, '2020-04-05 09:48:02', '2020-04-05 09:48:02'),
(2, 3, 2, '15000', 'Ground Tank 2', 1, '2020-04-05 09:48:13', '2020-04-05 09:48:13');


INSERT INTO `fuel_pumps_masters` (`id`, `outlet_name`, `fuel_type`, `pump_number`, `pump_make`, `machine_number`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '298', 'Fuel Pump 1', '978', 1, '2020-04-05 09:47:12', '2020-04-05 09:47:32'),
(2, 2, 2, '947', 'Fuel Pump 2', '990', 1, '2020-04-05 09:47:45', '2020-04-05 09:47:45');


INSERT INTO `fuel_type_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Fuel Type 1', 1, '2020-04-05 09:46:35', '2020-04-05 09:46:35'),
(2, 'Fuel Type 2', 1, '2020-04-05 09:46:37', '2020-04-05 09:46:37'),
(3, 'Fuel Type 3', 1, '2020-04-05 09:46:40', '2020-04-05 09:46:40');


INSERT INTO `full_it_repairs_masters` (`id`, `problems`, `full_it_repairs`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Engine Bolt Engine voucher', 1, '2020-04-03 19:20:54', '2020-04-03 20:21:07');


INSERT INTO `generator_spl_machines_masters` (`id`, `description`, `fuel_type`, `unit_name`, `quota_allotted`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Autem accusamus mini', 2, 3, 'Cumque sequi nostrud', 1, '2020-04-05 09:52:12', '2020-04-05 09:52:12');

INSERT INTO `job_card_repairs_masters` (`id`, `problems`, `job_card_repairs`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Clutch Plates', 1, '2020-04-03 20:10:56', '2020-04-03 20:20:13'),
(2, '2', 'Wiring', 1, '2020-04-03 20:13:39', '2020-04-03 20:20:23'),
(3, '3', 'Alignment', 1, '2020-04-03 20:15:09', '2020-04-03 20:20:34');


INSERT INTO `license_certificates_masters` (`id`, `name`, `intervel`, `status`, `created_at`, `updated_at`) VALUES
(1, 'License Certificates 1', 'Proident dolorum te', 1, '2020-04-05 09:48:24', '2020-04-05 09:48:24'),
(2, 'License Certificates 2', 'Voluptatem eum cupid', 1, '2020-04-05 09:48:33', '2020-04-05 09:48:33');


INSERT INTO `mechanics_names_masters` (`id`, `name`, `rank`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mechanic one', 'PC', 1, '2020-04-03 11:33:52', '2020-04-03 20:24:32'),
(2, 'Mechanic tow', 'HC', 1, '2020-04-03 11:34:00', '2020-04-03 20:24:45');


INSERT INTO `officer_designation_masters` (`id`, `name`, `office_of_master_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'MTO', 4, 1, '2020-04-02 21:45:57', '2020-04-02 21:54:43'),
(2, 'ACP', 4, 1, '2020-04-02 21:54:51', '2020-04-02 21:54:51'),
(3, 'Addl SP', 1, 1, '2020-04-02 21:55:04', '2020-04-02 21:55:04');


INSERT INTO `office_of_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'SP Officer', 1, '2020-04-02 21:30:28', '2020-04-02 21:30:28'),
(2, 'IG', 1, '2020-04-02 21:30:34', '2020-04-02 21:30:34'),
(3, 'DGP Office', 1, '2020-04-02 21:30:40', '2020-04-02 21:30:40'),
(4, 'Cyb Commissioner', 1, '2020-04-02 21:30:46', '2020-04-02 21:30:46');


INSERT INTO `oil_grade_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Oil Grade 1', 1, '2020-04-05 09:46:17', '2020-04-05 09:46:17'),
(2, 'Oil Grade 2', 1, '2020-04-05 09:46:20', '2020-04-05 09:46:20'),
(3, 'Oil Grade 3', 1, '2020-04-05 09:46:23', '2020-04-05 09:46:23'),
(4, 'Oil Grade 4', 1, '2020-04-05 09:46:26', '2020-04-05 09:46:26');

INSERT INTO `oil_type_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Oil Type 1', 1, '2020-04-05 09:52:37', '2020-04-05 09:52:37'),
(2, 'Oil Type 2', 1, '2020-04-05 09:52:39', '2020-04-05 09:52:39'),
(3, 'Oil Type 3', 1, '2020-04-05 09:52:42', '2020-04-05 09:52:42'),
(4, 'Oil Type 4', 1, '2020-04-05 09:52:45', '2020-04-05 09:52:45');


INSERT INTO `outlets_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Outlets 1', 1, '2020-04-05 09:46:49', '2020-04-05 09:46:49'),
(2, 'Outlets 2', 1, '2020-04-05 09:46:52', '2020-04-05 09:46:52'),
(3, 'Outlets 3', 1, '2020-04-05 09:46:54', '2020-04-05 09:46:54'),
(4, 'Outlets 4', 1, '2020-04-05 09:46:57', '2020-04-05 09:46:57');


INSERT INTO `past_expenses_vehicle_masters` (`id`, `vehicle_number`, `amount`, `financial_year`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ap1212', 2000, '2018-2019', 1, '2020-04-03 19:56:09', '2020-04-03 19:56:09'),
(2, 'araman rajaaa', 2121212, '2016-2017', 2, '2020-04-03 19:59:26', '2020-04-03 20:02:49');

INSERT INTO `section_names_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'section_names 1', 1, '2020-04-03 11:26:07', '2020-04-03 11:26:07'),
(2, 'section_names 2', 1, '2020-04-03 11:26:11', '2020-04-03 11:26:11');

INSERT INTO `service_schedule_filters` (`id`, `service_schedule_masters_id`, `filters_masters_id`, `interval`, `created_at`, `updated_at`) VALUES
(1, 10, 8, '5000', '2020-04-13 08:59:38', '2020-04-13 08:59:38'),
(2, 11, 8, '5000', '2020-04-13 09:00:21', '2020-04-13 09:00:21');

INSERT INTO `service_schedule_masters` (`id`, `vehicle_make_masters_id`, `vehicle_type_masters_id`, `vehicle_variant_masters_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 3, '2020-04-13 05:46:09', '2020-04-13 05:46:09'),
(2, 2, 2, 3, '2020-04-13 05:46:30', '2020-04-13 05:46:30'),
(3, 1, 4, 1, '2020-04-13 07:58:55', '2020-04-13 07:58:55'),
(4, 1, 4, 1, '2020-04-13 07:59:19', '2020-04-13 07:59:19'),
(5, 2, 2, 3, '2020-04-13 08:51:25', '2020-04-13 08:51:25'),
(6, 2, 2, 3, '2020-04-13 08:54:12', '2020-04-13 08:54:12'),
(7, 2, 2, 3, '2020-04-13 08:54:25', '2020-04-13 08:54:25'),
(8, 2, 2, 3, '2020-04-13 08:57:59', '2020-04-13 08:57:59'),
(9, 2, 2, 3, '2020-04-13 08:58:05', '2020-04-13 08:58:05'),
(10, 2, 2, 3, '2020-04-13 08:59:38', '2020-04-13 08:59:38'),
(11, 2, 2, 3, '2020-04-13 09:00:20', '2020-04-13 09:00:20');


INSERT INTO `service_schedule_oils` (`id`, `service_schedule_masters_id`, `oil_type_masters_id`, `first_servicing`, `quantity`, `oil_grade_masters_id`, `interval`, `created_at`, `updated_at`) VALUES
(1, 10, 4, '500', '20', '4', '100', '2020-04-13 08:59:38', '2020-04-13 08:59:38'),
(2, 11, 4, '500', '20', '4', '100', '2020-04-13 09:00:21', '2020-04-13 09:00:21');


INSERT INTO `spares_parts_entry_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Spares Parts Entry 1', 1, '2020-04-03 11:24:03', '2020-04-03 20:25:10'),
(3, 'Spares Parts Entry 2', 1, '2020-04-03 20:25:13', '2020-04-03 20:25:13');


INSERT INTO `units_masters` (`id`, `unit_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'PTO', 2, '2020-04-02 01:31:16', '2020-04-02 06:37:14'),
(2, 'CP Rachakonda', 1, '2020-04-02 06:19:05', '2020-04-02 06:19:05'),
(3, 'CP Hyderabad', 1, '2020-04-02 06:19:10', '2020-04-02 06:19:10'),
(4, 'KHAMMAM', 1, '2020-04-02 06:19:15', '2020-04-02 06:19:15'),
(5, 'KOTHAGUDEM', 1, '2020-04-02 06:19:24', '2020-04-02 06:19:24');



INSERT INTO `unit_head_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Inspector', 1, '2020-04-02 20:33:06', '2020-04-02 20:33:25'),
(2, 'SHO', 1, '2020-04-02 20:33:42', '2020-04-02 20:33:42'),
(3, 'DSP', 1, '2020-04-02 20:33:49', '2020-04-02 20:33:49');


INSERT INTO `users` (`id`, `name`, `email`, `mobile_no`, `user_type`, `login_role`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Suhasini', 'rajagonda@gmail.com', 9848090537, 1, 1, NULL, '$2y$10$sM0SSdg0n0NM2XZjHa1tT.zZyyBOwg5cOv8OlL6B9f6UWzOGsXFuq', 1, NULL, NULL, NULL),
(2, 'Ryan Horne', NULL, 8977111142, 2, NULL, NULL, '$2y$10$l8pTR6E60ovwQjOb7dSQZex4nLcuervXbu0PjSO46ansQ9ecHWYxa', 1, NULL, '2020-04-13 09:05:55', '2020-04-13 09:05:55');

INSERT INTO `vehicle_battery_histories` (`id`, `vehicle_detail_id`, `battery_make`, `battery_voltage`, `battery_ampere`, `battery_size`, `battery_life_span`, `battery_number`, `battery_issue_date`, `battery_end_date`, `cost`, `start_km`, `end_km`, `status`, `created_at`, `updated_at`) VALUES
(4, 10, 1, '52', '522', '212', '1', '2121', '2020-04-26', NULL, NULL, NULL, NULL, 1, '2020-04-13 01:50:44', '2020-04-13 01:50:44'),
(5, 10, 3, '252', '2522', '5222', '5', '222', '2020-04-30', NULL, NULL, NULL, NULL, 1, '2020-04-13 01:50:44', '2020-04-13 01:50:44');


INSERT INTO `vehicle_body_type_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Solo', 1, '2020-04-02 21:03:59', '2020-04-02 21:03:59'),
(2, 'HARD-TOP', 1, '2020-04-02 21:04:30', '2020-04-02 21:04:30'),
(3, 'Soft Top', 1, '2020-04-02 21:04:50', '2020-04-02 21:04:50');

INSERT INTO `vehicle_category_masters` (`id`, `vehicle_category`, `status`, `created_at`, `updated_at`) VALUES
(1, 'LMV', 1, '2020-04-02 06:17:45', '2020-04-02 06:27:21'),
(2, 'HMV', 1, '2020-04-02 06:17:49', '2020-04-02 06:27:27'),
(3, 'GSB', 1, '2020-04-02 06:17:57', '2020-04-02 06:27:33');


INSERT INTO `vehicle_details` (`id`, `vehicle_make_id`, `vehicle_type_id`, `vehicle_variant_id`, `vehicle_category_id`, `vin_no`, `vehicle_no`, `tr_no`, `fuel_type_id`, `model`, `engine_no`, `chassis_no`, `rc_validity_date`, `group_id`, `usage_id`, `body_type_id`, `tank_capacity`, `kmpl`, `reserve_fuel`, `piston_displacement`, `current_meter_reading`, `go_number`, `go_date`, `invoice_no`, `vehicle_purchase_from`, `dealer_name`, `dealer_mobile_no`, `po_number`, `po_date`, `delivery_date`, `date_of_issue`, `vehicle_cost`, `tax_type`, `tax_pecentage`, `tax_paid_amount`, `key_no`, `no_of_tyres`, `no_of_battery`, `type_of_drive`, `vehicle_description`, `sl_no`, `stock_ledger_number`, `page_number`, `engine_oil_grade_id`, `bore_stroke`, `no_of_cylinders`, `seating_capacity`, `allottment_order_no`, `allotment_date`, `bhp_rpm`, `opening_km`, `status`, `created_at`, `updated_at`) VALUES
(8, 2, 2, 3, 1, 13131, 'Dolor accusamus iure', 'Aut expedita quo eli', 2, 'Culpa similique cup', 'Itaque tempore nobi', 'Natus non sit ex ver', '2020-04-15', 2, 2, 2, 'Fuga Sunt quo omni', 'Sed deserunt lorem s', 'Et architecto tempor', 'Non proident molest', '41', '737', '2020-04-01', 'Eos blanditiis susc', 'Voluptates sint quib', 'Erin Medina', 'Illum maxime eaque', '981', '2020-04-23', '2020-04-17', '2020-04-09', 94, 'Fugit omnis accusan', 22, 71, 'Labore eaque aliquid', 2, 1, 'Pariatur Ullam mole', 'dasdd', 'Ea nemo sit debitis', '714', '263', 2, NULL, 98, 13, 'Rem et velit hic sun', '2020-04-02', 'Qui ut aliqua Molli', 78, 1, '2020-04-12 02:52:02', '2020-04-12 02:52:02'),
(10, 2, 2, 3, 1, 13131, 'at11at0231', 'at11tr0202', 2, 'adasd', '31a3s1d2a1s', 'ad31a3sd1a', '2020-04-15', 4, 5, 3, '35', '12', '5', 'Non proident molest', '41', '737', '2020-04-01', '31311asd', 'Voluptates sint quib', 'Erin Medina', '13231113', '981', '2020-04-23', '2020-04-17', '2020-04-09', 94, 'gst', 12, 1313131, '3131', 2, 2, 'Pariatur Ullam mole', 'ramanammma', 'Ea nemo sit debitis', '714', '263', 2, NULL, 98, 13, 'Rem et velit hic sun', '2020-04-02', 'Qui ut aliqua Molli', 78, 1, '2020-04-12 02:53:12', '2020-04-13 01:50:44');


INSERT INTO `vehicle_group_masters` (`id`, `vehicle_group_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vh1 group', 1, '2020-04-02 06:28:04', '2020-04-02 06:28:04'),
(2, 'Tata Sumo', 1, '2020-04-02 06:28:12', '2020-04-02 06:28:12'),
(3, 'CONVOY', 1, '2020-04-02 06:28:16', '2020-04-02 06:28:16'),
(4, 'Bolero', 1, '2020-04-02 06:28:20', '2020-04-02 06:28:20');


INSERT INTO `vehicle_make_masters` (`id`, `vehicle_make`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Toyota', 1, '2020-04-02 01:51:54', '2020-04-02 20:14:35'),
(2, 'Mahindra', 1, '2020-04-02 01:51:58', '2020-04-02 06:19:50'),
(3, 'Maruti Suzuki', 1, '2020-04-02 01:52:00', '2020-04-02 06:19:58'),
(4, 'TATA', 1, '2020-04-02 01:52:04', '2020-04-02 06:20:10'),
(5, 'Hyundai', 1, '2020-04-02 06:20:14', '2020-04-02 06:20:14');


INSERT INTO `vehicle_type_masters` (`id`, `vehicle_make_masters_id`, `vehicle_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Innova', 1, '2020-04-02 01:54:50', '2020-04-02 06:23:03'),
(2, 2, 'Scorpio', 1, '2020-04-02 01:54:55', '2020-04-02 20:14:59'),
(3, 3, 'Dzire', 1, '2020-04-02 01:59:49', '2020-04-02 06:20:52'),
(4, 1, 'crysta', 1, '2020-04-02 06:21:01', '2020-04-02 06:21:01'),
(5, 5, 'Verna Fludic', 1, '2020-04-02 06:21:10', '2020-04-02 06:21:10'),
(6, 4, 'Zest', 1, '2020-04-02 06:21:16', '2020-04-02 06:21:16');


INSERT INTO `vehicle_tyres_masters` (`id`, `tyre_make`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Apollo', 1, '2020-04-02 06:17:01', '2020-04-02 06:26:08'),
(2, 'Ceat', 1, '2020-04-02 06:17:05', '2020-04-02 06:26:13'),
(3, 'MRF', 1, '2020-04-02 06:17:09', '2020-04-02 06:26:19'),
(4, 'Bridgestone', 1, '2020-04-02 06:26:25', '2020-04-02 06:26:25');


INSERT INTO `vehicle_tyre_histories` (`id`, `vehicle_detail_id`, `tyre_make`, `tyre_life_span`, `tyre_size`, `tyre_number`, `cost`, `start_km`, `end_km`, `tyre_issue_date`, `tyre_end_date`, `status`, `created_at`, `updated_at`) VALUES
(11, 10, 1, '122', '1251', 90000, NULL, NULL, NULL, '2020-04-01', NULL, 1, '2020-04-13 01:36:38', '2020-04-13 01:36:38'),
(12, 10, 1, '52', '52', 2000, NULL, NULL, NULL, '2020-04-30', NULL, 1, '2020-04-13 01:36:38', '2020-04-13 01:36:38'),
(13, 10, 1, '21', '12', 12, 12121, 12, 12, '2020-04-22', '2020-04-23', 1, NULL, NULL);

INSERT INTO `vehicle_usage_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Tapal', 1, '2020-04-02 20:52:46', '2020-04-02 20:52:46'),
(2, 'PATROL MOBILE', 1, '2020-04-02 20:53:11', '2020-04-02 20:53:11'),
(3, 'Patrol', 1, '2020-04-02 20:53:19', '2020-04-02 20:53:19'),
(4, 'Officer Vehicle', 1, '2020-04-02 20:53:27', '2020-04-02 20:53:27'),
(5, 'Office Purpose', 1, '2020-04-02 20:53:33', '2020-04-02 20:53:33');



INSERT INTO `vehicle_variant_masters` (`id`, `vehicle_make_masters_id`, `vehicle_type_masters_id`, `vehicle_variant`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'GX', 1, '2020-04-02 04:54:58', '2020-04-02 20:22:32'),
(2, 4, 6, 'Zest revotron', 1, '2020-04-02 05:25:33', '2020-04-02 06:25:17'),
(3, 2, 2, 'SX', 1, '2020-04-02 05:25:50', '2020-04-02 06:24:12'),
(4, 3, 3, 'VDI', 1, '2020-04-02 05:40:16', '2020-04-02 06:23:59');

