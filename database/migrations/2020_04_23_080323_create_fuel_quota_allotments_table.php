<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuelQuotaAllotmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_quota_allotments', function (Blueprint $table) {
            $table->id();

            $table->string('allotments_type')->comment('regular,additional,enhance');
            $table->string('vehicle_number');
            $table->integer('quota_liters');
//            $table->date('alloted_date')->nullable();
//            $table->text('indent_number')->nullable();
//            $table->text('remarks')->nullable();
            $table->integer('allotment_user_id')->comment('login user id');
            $table->integer('status')->default(1)->comment('1=active, 2=older');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_quota_allotments');
    }
}
