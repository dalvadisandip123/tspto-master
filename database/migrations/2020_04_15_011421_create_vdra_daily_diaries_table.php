<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVdraDailyDiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vdra_daily_diaries', function (Blueprint $table) {
            $table->id();
            $table->string('vehicle_no');
            $table->date('out_date')->nullable();
            $table->date('date_return')->nullable();
            $table->integer('officer_id');
            $table->integer('driver_id_1');
            $table->integer('driver_id_2')->nullable();
            $table->integer('driver_id_3')->nullable();
            $table->integer('fuel_opening_bal')->nullable();
            $table->integer('fuel_total')->nullable();
            $table->integer('fuel_consumed')->nullable();
            $table->integer('fuel_bal')->nullable();
            $table->integer('journey_opening_km_reading')->nullable();
            $table->integer('journey_close_km_reading')->nullable();
            $table->integer('journey_km_present')->nullable();
            $table->integer('journey_km_present_month')->nullable();
            $table->string('purpose_type')->nullable();
            $table->text('purpose_text')->nullable();
            $table->integer('status')->default(1)->comment('1=open, 2=close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vdra_daily_diaries');
    }
}
