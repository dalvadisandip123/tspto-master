<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_registers', function (Blueprint $table) {
            $table->id();
            $table->string('financial_years');
            $table->date('purchase_date');
            $table->integer('accout_number_id');
            $table->string('invoice_number');
            $table->integer('bill_amount');
            $table->integer('credit_amount_IOCL');
            $table->text('remarks');
            $table->integer('status')->default(1)->comment('1=open, 2=close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_registers');
    }
}
