<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSparePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spare_parts', function (Blueprint $table) {
            $table->id();
            $table->integer('pto_stock_ledger_id')->nullable();
            $table->integer('pto_issue_id')->nullable();
            $table->integer('type')->nullable();
            $table->integer('make_id')->nullable();
            $table->integer('model_id')->nullable();
            $table->integer('part_category_id')->nullable();
            $table->integer('part_name_id')->nullable();
            $table->string('quantity')->nullable();
            $table->string('cost')->nullable();
            $table->string('rock_location')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spare_parts');
    }
}
