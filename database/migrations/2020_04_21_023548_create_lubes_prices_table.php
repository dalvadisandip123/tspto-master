<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLubesPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lubes_prices', function (Blueprint $table) {
            $table->id();
            $table->date('lubes_price_date');
            $table->integer('lubes_type_id');
            $table->integer('lubes_price')->nullable();
            $table->integer('status')->default(1)->comment('1=open, 2=close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lubes_prices');
    }
}
