<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_details', function (Blueprint $table) {
            $table->id();

            $table->integer('vehicle_make_id');
            $table->integer('vehicle_type_id')->nullable();
            $table->integer('vehicle_variant_id')->nullable();
            $table->integer('vehicle_category_id');

            $table->integer('vin_no')->nullable();
            $table->string('vehicle_no');
//            $table->string('vehicle_no')->unique();
            $table->string('tr_no')->nullable();
            $table->integer('fuel_type_id');


            $table->string('model')->nullable();
            $table->string('engine_no')->nullable();
            $table->string('chassis_no')->nullable();
            $table->date('rc_validity_date')->nullable();

            $table->integer('group_id')->nullable();
            $table->integer('usage_id')->nullable();
            $table->integer('body_type_id')->nullable();

            $table->string('tank_capacity')->nullable();
            $table->string('kmpl')->nullable();
            $table->string('reserve_fuel')->nullable();

            $table->string('piston_displacement')->nullable();
            $table->string('current_meter_reading')->nullable();
            $table->string('go_number')->nullable();
            $table->date('go_date')->nullable();

            $table->string('invoice_no')->nullable();
            $table->string('vehicle_purchase_from')->nullable();
            $table->string('dealer_name')->nullable();
            $table->string('dealer_mobile_no')->nullable();

            $table->string('po_number')->nullable();
            $table->date('po_date')->nullable();
            $table->date('delivery_date')->nullable();
            $table->date('date_of_issue')->nullable();

            $table->integer('vehicle_cost')->nullable();
            $table->string('tax_type')->nullable();
            $table->integer('tax_pecentage')->nullable();
            $table->integer('tax_paid_amount')->nullable();


            $table->string('key_no')->nullable();
            $table->integer('no_of_tyres')->nullable();
            $table->integer('no_of_battery')->nullable();
            $table->string('type_of_drive')->nullable();

            $table->text('vehicle_description')->nullable();
            $table->string('sl_no')->nullable();
            $table->string('stock_ledger_number')->nullable();
            $table->string('page_number')->nullable();


            $table->integer('engine_oil_grade_id')->nullable();
            $table->string('bore_stroke')->nullable();
            $table->integer('no_of_cylinders')->nullable();
            $table->integer('seating_capacity')->nullable();

            $table->string('allottment_order_no')->nullable();
            $table->date('allotment_date')->nullable();
            $table->string('bhp_rpm')->nullable();
            $table->integer('opening_km')->default(0);

            $table->integer('unit_id')->nullable();
            $table->integer('officer_id')->nullable();
            $table->integer('status')->default(1)->comment('1=central MT pull, 2=unit mt pull, 3=condemnation, 4=auction, 5=sold');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_details');
    }
}


