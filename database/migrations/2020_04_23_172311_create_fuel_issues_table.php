<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuelIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_issues', function (Blueprint $table) {
            $table->id();
            $table->string('fuel_issues_for')->comment('vehicle, hired, spl');
            $table->string('tx_no_outlet_code');
            $table->string('tx_no_date');
            $table->string('tx_no_day_sno');
            $table->date('issue_date');
            $table->integer('indent_number_by_item');
            $table->integer('generator_spl_id')->nullable();
            $table->string('vehicle_number')->nullable();
            $table->string('fuel_type')->nullable();
            $table->integer('issue_liters')->nullable();
            $table->integer('present_km')->nullable();
            $table->integer('drown')->nullable();
            $table->integer('rec_quota_regular')->nullable();
            $table->integer('rec_quota_addit')->nullable();
            $table->integer('rec_quota_enhan')->nullable();
            $table->integer('rec_quota_total')->nullable();
            $table->integer('rec_quota_balance')->nullable();
            $table->integer('status')->default(1)->comment('1=active, 2=older');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_issues');
    }
}
