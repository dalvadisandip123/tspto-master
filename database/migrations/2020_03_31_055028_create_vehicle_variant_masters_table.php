<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleVariantMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_variant_masters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vehicle_make_masters_id')->nullable();
            $table->foreign('vehicle_make_masters_id')
                ->references('id')->on('vehicle_make_masters')
                ->onDelete('cascade');
            $table->unsignedBigInteger('vehicle_type_masters_id')->nullable();
            $table->foreign('vehicle_type_masters_id')
                ->references('id')->on('vehicle_type_masters')
                ->onDelete('cascade');
            $table->string('vehicle_variant')->nullable();
            $table->integer('status')->default(1)->comment('1=Active,2=Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_variant_masters');
    }
}
