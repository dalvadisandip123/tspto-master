<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleTyreHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_tyre_histories', function (Blueprint $table) {
            $table->id();

            $table->integer('vehicle_detail_id');
            $table->string('tyre_size')->nullable();

            $table->string('tyre_life_span')->nullable();
            $table->integer('tyre_number')->nullable();
            $table->integer('tyre_make');
            $table->integer('cost')->nullable();
            $table->integer('start_km')->nullable();
            $table->integer('end_km')->nullable();
            $table->date('tyre_issue_date')->nullable();
            $table->date('tyre_end_date')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_tyre_histories');
    }
}


