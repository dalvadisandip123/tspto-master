<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePTOIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PTO_issue', function (Blueprint $table) {
            $table->id();
            $table->string('iv_district')->nullable();
            $table->string('unit_name')->nullable();
            $table->date('issue_date')->nullable();
            $table->string('rc_no')->nullable();
            $table->date('rc_date')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PTO_issue');
    }
}
