<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuelPumpsMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_pumps_masters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('outlet_name')->nullable();
            $table->foreign('outlet_name')
                ->references('id')->on('outlets_masters')
                ->onDelete('cascade');

            $table->unsignedBigInteger('fuel_type')->nullable();
            $table->foreign('fuel_type')
                ->references('id')->on('fuel_type_masters')
                ->onDelete('cascade');

            $table->string('pump_number');
            $table->string('pump_make');
            $table->string('machine_number');
            $table->integer('status')->default(1)->comment('1=Active,2=Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_pumps_masters');
    }
}
