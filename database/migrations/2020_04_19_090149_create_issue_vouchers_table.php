<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssueVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_vouchers', function (Blueprint $table) {
            $table->id();
            $table->string('vehicle_no');
            $table->string('vehicle_issued_to')->nullable();
            $table->string('issue_voucher_no');
            $table->string('issue_voucher_type')->default('E2')->nullable();
            $table->year('issue_voucher_year')->useCurrent()->nullable();
            $table->date('issue_voucher_date')->nullable();
            $table->integer('co_allotment_no')->nullable();
            $table->integer('co_allotment_no2')->nullable();
            $table->year('co_allotment_year')->useCurrent()->nullable();
            $table->date('co_allotment_date')->nullable();
            $table->string('vehicle_allotment_purpose')->nullable();
            $table->date('vehicle_charged_off_today')->nullable();
            $table->string('recevier_name')->nullable();
            $table->string('recevier_designation')->nullable();
            $table->integer('unit_id')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_vouchers');
    }
}
