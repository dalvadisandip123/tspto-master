<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutletAllocationMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlet_allocation_masters', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user')->nullable();
            $table->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unsignedBigInteger('outlet')->nullable();
            $table->foreign('outlet')
                ->references('id')->on('outlets_masters')
                ->onDelete('cascade');


            $table->integer('status')->default(1)->comment('1=Active,2=Inactive');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_allocation_masters');
    }
}
