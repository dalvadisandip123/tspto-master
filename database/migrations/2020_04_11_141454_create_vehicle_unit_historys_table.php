<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleUnitHistorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_unit_history', function (Blueprint $table) {
            $table->id();
            $table->integer('vehicle_details_id');
            $table->integer('allotment_order_number');
            $table->integer('unit_id');
            $table->integer('from_km')->nullable();
            $table->integer('to_km')->nullable();
            $table->date('issue_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('receiver_user_id')->nullable();
            $table->integer('usage_pupose_id')->nullable();
            $table->integer('status')->default(1)->comment('1=open, 2=close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_unit_history');
    }
}


