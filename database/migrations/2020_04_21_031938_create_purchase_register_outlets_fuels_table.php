<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRegisterOutletsFuelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_register_outlets_fuels', function (Blueprint $table) {
            $table->id();
            $table->integer('purchase_registers_id');
            $table->integer('outlet_id');
            $table->integer('fuel_type_id');
            $table->integer('fuel_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_register_outlets_fuels');
    }
}
