<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceScheduleOilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_schedule_oils', function (Blueprint $table) {
            $table->id();
            $table->integer('service_schedule_masters_id');
            $table->integer('oil_type_masters_id');
            $table->string('first_servicing')->nullable();
            $table->string('quantity')->nullable();
            $table->string('oil_grade_masters_id');
            $table->string('interval')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_schedule_oils');
    }
}
