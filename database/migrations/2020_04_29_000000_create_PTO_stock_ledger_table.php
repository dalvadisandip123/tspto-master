<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePTOStockLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PTO_stock_ledger', function (Blueprint $table) {
            $table->id();
            $table->string('category')->nullable();
            $table->string('d1_no')->nullable();
            $table->date('stock_date')->nullable();
            $table->string('type')->nullable();
            $table->string('firm_name')->nullable();
            $table->string('po_no')->nullable();
            $table->date('po_date')->nullable();
            $table->string('invoice_no')->nullable();
            $table->date('invoice_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PTO_stock_ledger');
    }
}
