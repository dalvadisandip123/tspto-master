<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneratorSplMachinesMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generator_spl_machines_masters', function (Blueprint $table) {
            $table->id();

            $table->string('description');


            $table->unsignedBigInteger('fuel_type')->nullable();
            $table->foreign('fuel_type')
                ->references('id')->on('fuel_type_masters')
                ->onDelete('cascade');

            $table->unsignedBigInteger('unit_name')->nullable();
            $table->foreign('unit_name')
                ->references('id')->on('units_masters')
                ->onDelete('cascade');

            $table->string('quota_allotted');
            $table->integer('status')->default(1)->comment('1=Active,2=Inactive');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generator_spl_machines_masters');
    }
}
