<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpHolidayDutiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_holiday_duties', function (Blueprint $table) {
            $table->id();
            $table->integer('employees_id');
            $table->date('duty_date');
            $table->text('reason_for_duty')->nullable();
            $table->text('place_of_duty')->nullable();
            $table->text('time')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_holiday_duties');
    }
}
