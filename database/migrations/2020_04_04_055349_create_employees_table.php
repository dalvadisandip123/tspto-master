<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('name');
            $table->string('photo')->nullable();
            $table->integer('employee_role_masters_id')->nullable();
            $table->integer('employee_rank_masters_id')->nullable();
            $table->integer('number')->nullable();
            $table->string('driving_licence_number')->nullable();
            $table->string('licence_type')->nullable();
            $table->date('licence_validity')->nullable();
            $table->integer('units_masters_id')->nullable();
            $table->string('parent_unit')->nullable();
            $table->string('working_section')->nullable();
            $table->string('officer_name_masters_id')->nullable();
            $table->date('date_appointment')->nullable();
            $table->string('duty_type')->nullable();
            $table->date('retirement_date')->nullable();
            $table->string('languages_known')->nullable();
            $table->string('mother_tounge')->nullable();
            $table->string('native_place')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('family_mobile_number')->nullable();
            $table->string('arogya_bhadratha_number')->nullable();
            $table->string('residence')->nullable();
            $table->longText('rewards_or_pathakams')->nullable();
            $table->longText('punishments')->nullable();
            $table->longText('accident_history')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
