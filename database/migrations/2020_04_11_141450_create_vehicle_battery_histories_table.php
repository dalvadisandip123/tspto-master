<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleBatteryHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_battery_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('vehicle_detail_id');
            $table->string('battery_voltage')->nullable();
            $table->string('battery_ampere')->nullable();
            $table->string('battery_size')->nullable();

            $table->integer('battery_make');
            $table->string('battery_life_span')->nullable();
            $table->string('battery_number')->nullable();
            $table->date('battery_issue_date')->nullable();
            $table->date('battery_end_date')->nullable();

            $table->integer('cost')->nullable();
            $table->integer('start_km')->nullable();
            $table->integer('end_km')->nullable();

            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_battery_histories');
    }
}


