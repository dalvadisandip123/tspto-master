<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficerNameMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officer_name_masters', function (Blueprint $table) {
            $table->id();
            $table->string('office_of_master_id');
            $table->string('officer_designation_master_id');
            $table->string('officer_name');
            $table->string('officer_number');
            $table->string('user_id')->nullable();
            $table->integer('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officer_name_masters');
    }
}
