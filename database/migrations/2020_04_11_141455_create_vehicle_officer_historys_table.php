<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleOfficerHistorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_officer_historys', function (Blueprint $table) {
            $table->id();
            $table->integer('vehicle_details_id');
            $table->integer('unit_id');
            $table->integer('officer_id');
            $table->string('attachment_order_number')->nullable();
            $table->text('purpose')->nullable();
            $table->integer('from_km')->nullable();
            $table->integer('to_km')->nullable();
            $table->date('issue_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('status')->default(1)->comment('1=open, 2=close');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_officer_historys');
    }
}


