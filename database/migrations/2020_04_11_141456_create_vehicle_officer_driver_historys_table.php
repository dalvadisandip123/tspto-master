<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleOfficerDriverHistorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_officer_driver_historys', function (Blueprint $table) {
            $table->id();
            $table->integer('vehicle_details_id');
            $table->integer('unit_id');
            $table->integer('officer_id');
            $table->integer('attachment_id');
            $table->integer('diver_id');

            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->integer('start_km')->nullable();
            $table->integer('end_km')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('status')->default(1)->comment('1=open, 2=close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_officer_driver_historys');
    }
}


