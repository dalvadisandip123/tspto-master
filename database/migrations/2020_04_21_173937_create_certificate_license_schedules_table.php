<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificateLicenseSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificate_license_schedules', function (Blueprint $table) {
            $table->id();
            $table->integer('license_certificates_id');
            $table->integer('fuel_outlet_id');
            $table->integer('fuel_type_id');
            $table->text('for_licence');
            $table->date('renewal_date');
            $table->integer('status')->default(1)->comment('1=open, 2=close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate_license_schedules');
    }
}
