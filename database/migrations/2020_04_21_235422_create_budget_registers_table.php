<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_registers', function (Blueprint $table) {
            $table->id();
            $table->string('budget_type');
            $table->string('financial_year');
            $table->string('quarter');
            $table->string('id_number');
            $table->date('id_date');
            $table->integer('budget');
            $table->integer('total_amount');
            $table->integer('budget_handed_over_to_govt');
            $table->integer('budget_returned_from_govt');
            $table->integer('status')->default(1)->comment('1=open, 2=close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_registers');
    }
}
