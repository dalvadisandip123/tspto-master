<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatteriesAndTyresVariantMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batteries_and_tyres_variant_masters', function (Blueprint $table) {
            $table->id();
            $table->integer('vehicle_make_id')->nullable();
            $table->integer('vehicle_type_id')->nullable();
            $table->integer('vehicle_variant_id')->nullable();
            $table->integer('vehicle_category_id')->nullable();

            $table->string('life_span')->nullable();
            $table->string('battery_size')->nullable();
            $table->integer('battery_amp')->nullable();
            $table->integer('battery_volt')->nullable();


            $table->integer('tyre_life_span')->nullable();
            $table->string('tyre_front_size')->nullable();
            $table->string('tyre_rear_size')->nullable();

            $table->string('battery_name')->nullable();
            $table->string('tyre_make')->nullable();
            $table->string('tyre_name')->nullable();
            $table->string('no_tyres')->nullable();
            $table->string('yom')->nullable();

            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batteries_and_tyres_variant_masters');
    }
}
