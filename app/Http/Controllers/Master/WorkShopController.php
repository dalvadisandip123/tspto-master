<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Masters\Workshop\FullItRepairsMaster;
use App\Models\Masters\Workshop\JobCardRepairsMaster;
use App\Models\Masters\Workshop\MechanicsNamesMaster;
use App\Models\Masters\Workshop\PastExpensesVehicleMaster;
use App\Models\Masters\Workshop\SectionNamesMaster;
use App\Models\Masters\Workshop\SparesPartsEntryMaster;
use App\Models\Masters\Workshop\PartCategoryMaster;
use App\Models\Masters\Workshop\PartNameMaster;
use App\Models\Masters\Workshop\FirmNameMaster;
use Illuminate\Http\Request;

class WorkShopController extends Controller
{

    public $pagelimit;

    public function __construct()
    {
        $this->middleware('auth');
        $this->pagelimit = 10;
    }

    public function jobCardRepairs(Request $request)
    {



        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_problems = $request->problems;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'job Card Repairs';
        $data['module_name'] = 'job Card Repairs';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'job_card_repairs';

        $data['lists'] = JobCardRepairsMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('job_card_repairs', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->when($searc_problems, function ($query) use ($filterParam) {
                $query->where('problems', 'like', $filterParam['problems']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    JobCardRepairsMaster::create($insertData);
                    return redirect()->route('site.master.workshop.job_card_repairs')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = JobCardRepairsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.job_card_repairs')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = JobCardRepairsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        JobCardRepairsMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.job_card_repairs')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.job_card_repairs', $data);
    }

    public function PastVehicleExpense(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_problems = $request->problems;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Past Vehicle Expense';
        $data['module_name'] = 'Past Vehicle Expense';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'past_vehicle_expense';
        $data['lists'] = PastExpensesVehicleMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('full_it_repairs', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->when($searc_problems, function ($query) use ($filterParam) {
                $query->where('problems', 'like', $filterParam['problems']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    PastExpensesVehicleMaster::create($insertData);
                    return redirect()->route('site.master.workshop.past_vehicle_expense')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = PastExpensesVehicleMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.past_vehicle_expense')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = PastExpensesVehicleMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        PastExpensesVehicleMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.past_vehicle_expense')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.past_vehicle_expense', $data);
    }



    public function FullITRepairs(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_problems = $request->problems;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'full it repairs';
        $data['module_name'] = 'full it repairs';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'full_it_repairs';
        $data['lists'] = FullItRepairsMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('full_it_repairs', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->when($searc_problems, function ($query) use ($filterParam) {
                $query->where('problems', 'like', $filterParam['problems']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    FullItRepairsMaster::create($insertData);
                    return redirect()->route('site.master.workshop.full_it_repairs')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = FullItRepairsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.full_it_repairs')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = FullItRepairsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        FullItRepairsMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.full_it_repairs')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.full_it_repairs', $data);
    }

    public function MechanicsNames(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Mechanics Names';
        $data['module_name'] = 'Mechanics Names';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'mechanics_names';
        $data['lists'] = MechanicsNamesMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    MechanicsNamesMaster::create($insertData);
                    return redirect()->route('site.master.workshop.mechanics_names')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = MechanicsNamesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.mechanics_names')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = MechanicsNamesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        MechanicsNamesMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.mechanics_names')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.mechanics_names', $data);
    }

    public function SectionNames(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Section Names';
        $data['module_name'] = 'Section Names';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'section_names';
        $data['lists'] = SectionNamesMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    SectionNamesMaster::create($insertData);
                    return redirect()->route('site.master.workshop.section_names')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = SectionNamesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.section_names')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = SectionNamesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        SectionNamesMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.section_names')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.section_names', $data);
    }

    public function sparesPartsEntry(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'spares parts entry';
        $data['module_name'] = 'spares parts entry';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'spares_parts_entry';
        $data['lists'] = SparesPartsEntryMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    SparesPartsEntryMaster::create($insertData);
                    return redirect()->route('site.master.workshop.spares_parts_entry')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = SparesPartsEntryMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.spares_parts_entry')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = SparesPartsEntryMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        SparesPartsEntryMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.spares_parts_entry')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.spares_parts_entry', $data);
    }
    public function partCategory(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Part Category';
        $data['module_name'] = 'Part Category';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'part_category';
        $data['lists'] = PartCategoryMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    PartCategoryMaster::create($insertData);
                    return redirect()->route('site.master.workshop.part_category')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = PartCategoryMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.spares_parts_entry')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = PartCategoryMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        PartCategoryMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.part_category')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.part_category', $data);
    }
    public function partName(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Part Name';
        $data['module_name'] = 'Part Name';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'part_name';
        $data['lists'] = PartNameMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    PartNameMaster::create($insertData);
                    return redirect()->route('site.master.workshop.part_name')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = PartNameMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.part_name')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = PartNameMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        PartNameMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.part_name')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.part_name', $data);
    }
    public function firmName(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Firm Name';
        $data['module_name'] = 'Firm Name';
        $data['module_parent1_name'] = 'master';
        $data['module_parent2_name'] = 'Workshop';
        $data['active'] = 'master';
        $data['sub_active'] = 'workshop';
        $data['sub1_active'] = 'firm_name';
        $data['lists'] = FirmNameMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    FirmNameMaster::create($insertData);
                    return redirect()->route('site.master.workshop.firm_name')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = FirmNameMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.workshop.firm_name')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = FirmNameMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        FirmNameMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.workshop.firm_name')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.workshop.firm_name', $data);
    }



}
