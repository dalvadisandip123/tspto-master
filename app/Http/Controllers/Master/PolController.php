<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Masters\Mt\UnitsMaster;
use App\Models\Masters\Mt\VehicleCategoryMaster;
use App\Models\Masters\Pol\HiredVehicles;
use Illuminate\Http\Request;
use App\Models\Masters\Pol\OilTypeMaster;
use App\Models\Masters\Pol\FiltersMaster;
use App\Models\Masters\Pol\OilGradeMaster;
use App\Models\Masters\Pol\AccountNumbersMaster;
use App\Models\Masters\Pol\OutletsMaster;
use App\Models\Masters\Pol\FuelTypeMaster;
use App\Models\Masters\Pol\FuelPumpsMaster;
use App\Models\Masters\Pol\FuelGroundTankMaster;
use App\Models\Masters\Pol\LicenseCertificatesMaster;
use App\Models\Masters\Pol\GeneratorSplMachinesMaster;
use App\Models\Masters\Pol\OutletAllocationMaster;
use App\Models\Masters\Pol\ServiceScheduleMaster;
use App\Models\Masters\Pol\ServiceScheduleOil;
use App\Models\Masters\Pol\ServiceScheduleFilter;


class PolController extends Controller
{
    public $pagelimit;

    public function __construct()
    {
        $this->middleware('auth');
        $this->pagelimit = 10;
    }

    public function oilType(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Oil type';
        $data['module_name'] = 'Oil type';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'oil_type';
        $data['lists'] = OilTypeMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    OilTypeMaster::create($insertData);
                    return redirect()->route('site.master.pol.oil_type')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = OilTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.oil_type')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = OilTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        OilTypeMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.oil_type')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.oil_type', $data);
    }

    public function filters(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Filters';
        $data['module_name'] = 'Filters';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'filters';
        $data['lists'] = FiltersMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    FiltersMaster::create($insertData);
                    return redirect()->route('site.master.pol.filters')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = FiltersMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.filters')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = FiltersMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        FiltersMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.filters')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.filters', $data);
    }

    public function oilGrade(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Oil Grade';
        $data['module_name'] = 'Oil Grade';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'oil_grade';
        $data['lists'] = OilGradeMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    OilGradeMaster::create($insertData);
                    return redirect()->route('site.master.pol.oil_grade')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = OilGradeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.oil_grade')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = OilGradeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        OilGradeMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.oil_grade')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.oil_grade', $data);
    }

    public function accountNumbers(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Account numbers';
        $data['module_name'] = 'Account numbers';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'accountNumbers';
        $data['lists'] = AccountNumbersMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    AccountNumbersMaster::create($insertData);
                    return redirect()->route('site.master.pol.account_numbers')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = AccountNumbersMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.account_numbers')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = AccountNumbersMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        AccountNumbersMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.account_numbers')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.account_numbers', $data);
    }

    public function outlets(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Outlets';
        $data['module_name'] = 'Outlets';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'outlets';
        $data['lists'] = OutletsMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    OutletsMaster::create($insertData);
                    return redirect()->route('site.master.pol.outlets')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = OutletsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.outlets')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = OutletsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        OutletsMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.outlets')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.outlets', $data);
    }

    public function fuelType(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Fuel Type';
        $data['module_name'] = 'Fuel Type';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'fuel_type';
        $data['lists'] = FuelTypeMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    FuelTypeMaster::create($insertData);
                    return redirect()->route('site.master.pol.fuel_type')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = FuelTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.fuel_type')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = FuelTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        FuelTypeMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.fuel_type')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.fuel_type', $data);
    }

    public function fuelPumps(Request $request, $action = null, $id = null)
    {

        $data = array();
        $data['title'] = 'Fuel pump';
        $data['module_name'] = 'Fuel pump';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'fuel_pumps';


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    FuelPumpsMaster::create($insertData);
                    return redirect()->route('site.master.pol.fuel_pumps')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = FuelPumpsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.fuel_pumps')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = FuelPumpsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        FuelPumpsMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.fuel_pumps')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }

        if ($action) {

            if ($id == '') {
                return view('master.pol.fuel_pumps.add', $data);
            } else {
                $data['list'] = FuelPumpsMaster::where('id', $id)->first();
                return view('master.pol.fuel_pumps.edit', $data);
            }

        } else {

            $filterParam = $request->all();
            $searc_q = $request->q;
            $searc_status = $request->status;

            $data['lists'] = FuelPumpsMaster::when($searc_q, function ($query) use ($filterParam) {
                $query->where('pump_number', 'like', '%' . $filterParam['q'] . '%');
            })
                ->when($searc_status, function ($query) use ($filterParam) {
                    $query->where('status', 'like', $filterParam['status']);
                })
                ->orderBy('id', 'desc')
                ->paginate($this->pagelimit);

            return view('master.pol.fuel_pumps.list', $data);
        }


    }

    public function fuelGroundTank(Request $request, $action = null, $id = null)
    {

        $data = array();
        $data['title'] = 'Fuel Ground Tank';
        $data['module_name'] = 'Fuel Ground Tank';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'fuel_ground_tank';


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    FuelGroundTankMaster::create($insertData);
                    return redirect()->route('site.master.pol.fuel_ground_tank')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = FuelGroundTankMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.fuel_ground_tank')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = FuelGroundTankMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        FuelGroundTankMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.fuel_ground_tank')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }

        if ($action) {

            if ($id == '') {
                return view('master.pol.fuel_ground_tank.add', $data);
            } else {
                $data['list'] = FuelGroundTankMaster::where('id', $id)->first();
                return view('master.pol.fuel_ground_tank.edit', $data);
            }

        } else {

            $filterParam = $request->all();
            $searc_q = $request->q;
            $searc_status = $request->status;

            $data['lists'] = FuelGroundTankMaster::when($searc_q, function ($query) use ($filterParam) {
                $query->where('tank_capacity', 'like', '%' . $filterParam['q'] . '%');
            })
                ->when($searc_status, function ($query) use ($filterParam) {
                    $query->where('status', 'like', $filterParam['status']);
                })
                ->orderBy('id', 'desc')
                ->paginate($this->pagelimit);

            return view('master.pol.fuel_ground_tank.list', $data);
        }


    }

    public function licenseCertificates(Request $request)
    {
        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'License certificates';
        $data['module_name'] = 'License certificates';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'license_certificates';
        $data['lists'] = LicenseCertificatesMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    LicenseCertificatesMaster::create($insertData);
                    return redirect()->route('site.master.pol.license_certificates')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = LicenseCertificatesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.license_certificates')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = LicenseCertificatesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        LicenseCertificatesMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.license_certificates')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.license_certificates', $data);
    }


    public function serviceSchedule(Request $request, $action = null, $id = null)
    {

        $data = array();
        $data['title'] = 'Service schedule';
        $data['module_name'] = 'Service schedule';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'service_schedule';


        if ($request->isMethod('post')) {

            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    $insertedid = ServiceScheduleMaster::create($insertData)->id;

                    $oils = $insertData['oils'];
                    $filters = $insertData['filters'];


//                    dd($oils);

                    if (count($oils) > 0) {
                        foreach ($oils as $oilkey => $oilvalue) {
                            $oilData = array();

                            if (!empty(array_filter($oilvalue))) {


                                $oilData['service_schedule_masters_id'] = $insertedid;
                                $oilData['oil_type_masters_id'] = $oilkey;
                                $oilData['first_servicing'] = $oilvalue['first_service'];
                                $oilData['quantity'] = $oilvalue['quantity'];
                                $oilData['oil_grade_masters_id'] = $oilvalue['grade'];
                                $oilData['interval'] = $oilvalue['interval'];
                                ServiceScheduleOil::create($oilData);
                            }

                        }
                    }

                    if (count($filters) > 0) {
                        foreach ($filters as $filterkey => $filtervalue) {
                            $oilData = array();
                            if (!empty(array_filter($filtervalue))) {
                                $oilData['service_schedule_masters_id'] = $insertedid;
                                $oilData['filters_masters_id'] = $filterkey;
                                $oilData['interval'] = $filtervalue['interval'];
                                ServiceScheduleFilter::create($oilData);
                            }
                        }
                    }

                    return redirect()->route('site.master.pol.service_schedule')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = ServiceScheduleMaster::where('id', $request->id)->first();
                    if (!empty($item)) {

                        $oils = $updateData['oils'];
                        $filters = $updateData['filters'];

                        ServiceScheduleOil::where('service_schedule_masters_id', $request->id)->delete();

                        if (count($oils) > 0) {
                            foreach ($oils as $oilkey => $oilvalue) {
                                if (!empty(array_filter($oilvalue))) {
                                    $oilData = array();
                                    $oilData['service_schedule_masters_id'] = $request->id;
                                    $oilData['oil_type_masters_id'] = $oilkey;
                                    $oilData['first_servicing'] = $oilvalue['first_service'];
                                    $oilData['quantity'] = $oilvalue['quantity'];
                                    $oilData['oil_grade_masters_id'] = $oilvalue['grade'];
                                    $oilData['interval'] = $oilvalue['interval'];
                                    ServiceScheduleOil::create($oilData);
                                }

                            }
                        }

                        ServiceScheduleFilter::where('service_schedule_masters_id', $request->id)->delete();


                        if (count($filters) > 0) {
                            foreach ($filters as $filterkey => $filtervalue) {
                                if (!empty($filtervalue['interval'])) {
                                    $oilData = array();
                                    $oilData['service_schedule_masters_id'] = $request->id;
                                    $oilData['filters_masters_id'] = $filterkey;
                                    $oilData['interval'] = $filtervalue['interval'];
                                    ServiceScheduleFilter::create($oilData);
                                }

                            }
                        }

                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.service_schedule')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = ServiceScheduleMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        ServiceScheduleOil::where('service_schedule_masters_id', $request->id)->delete();
                        ServiceScheduleFilter::where('service_schedule_masters_id', $request->id)->delete();
                        ServiceScheduleMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.service_schedule')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }

        if ($action) {
            $data['oils'] = OilTypeMaster::where('status', 1)->orderBy('id', 'desc')->get();
            $data['filters'] = FiltersMaster::where('status', 1)->orderBy('id', 'desc')->get();
            $data['grades'] = OilGradeMaster::where('status', 1)->orderBy('id', 'desc')->get();
            if ($id == '') {
                return view('master.pol.service_schedule.add', $data);
            } else {
                $data['list'] = ServiceScheduleMaster::where('id', $id)->first();
                $data['existedOils'] = getServiceScheduleOilsBasedOnID($id);
                $data['existedFilters'] = getServiceScheduleFiltersBasedOnID($id);
                return view('master.pol.service_schedule.edit', $data);
            }

        } else {

            $filterParam = $request->all();
            $searc_vehicle_make = $request->vehicle_make;

            $data['lists'] = ServiceScheduleMaster::
            when($searc_vehicle_make, function ($query) use ($filterParam) {
                $query->where('vehicle_make_masters_id', $filterParam['vehicle_make']);
            })
                ->
                orderBy('id', 'desc')
                ->paginate($this->pagelimit);

            return view('master.pol.service_schedule.list', $data);
        }


    }


    public function generatorSplMachines(Request $request, $action = null, $id = null)
    {

        $data = array();
        $data['title'] = 'Generator Spl Machines';
        $data['module_name'] = 'Generator Spl Machines';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'generator_spl_machines';


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    GeneratorSplMachinesMaster::create($insertData);
                    return redirect()->route('site.master.pol.generator_spl_machines')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = GeneratorSplMachinesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.generator_spl_machines')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = GeneratorSplMachinesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        GeneratorSplMachinesMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.generator_spl_machines')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }

        if ($action) {

            if ($id == '') {
                return view('master.pol.generator_spl_machines.add', $data);
            } else {
                $data['list'] = GeneratorSplMachinesMaster::where('id', $id)->first();
                return view('master.pol.generator_spl_machines.edit', $data);
            }

        } else {

            $filterParam = $request->all();
            $searc_q = $request->q;
            $searc_status = $request->status;

            $data['lists'] = GeneratorSplMachinesMaster::when($searc_q, function ($query) use ($filterParam) {
                $query->where('quota_allotted', 'like', '%' . $filterParam['q'] . '%');
            })
                ->when($searc_status, function ($query) use ($filterParam) {
                    $query->where('status', 'like', $filterParam['status']);
                })
                ->orderBy('id', 'desc')
                ->paginate($this->pagelimit);

            return view('master.pol.generator_spl_machines.list', $data);
        }


    }

    public function outletAllocation(Request $request)
    {
        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Outlet allocation';
        $data['module_name'] = 'Outlet allocation';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'outlet_allocation';
        $data['lists'] = OutletAllocationMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('outlet', $filterParam['q']);
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    OutletAllocationMaster::create($insertData);
                    return redirect()->route('site.master.pol.outlet_allocation')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = OutletAllocationMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.pol.outlet_allocation')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = OutletAllocationMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        OutletAllocationMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.outlet_allocation')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.outlet_allocation', $data);
    }

    public function hiredVehicles(Request $request)
    {
        $filterParam = $request->all();
        $search_vehicle_number = $request->search_vehicle_number;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'hired vehicles';
        $data['module_name'] = 'hired-vehicles';
        $data['active'] = 'master';
        $data['sub_active'] = 'pol';
        $data['sub1_active'] = 'hired_vehicles';

        $data['lists'] = HiredVehicles::when($search_vehicle_number, function ($query) use ($filterParam) {
            $query->where('vehicle_number',  'like', "%".$filterParam['search_vehicle_number'].'%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status',$filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);

        $data['vehicleCategories'] = VehicleCategoryMaster::get();
        $data['vehicleUnits'] = UnitsMaster::get();
        $data['fuelTypes'] = FuelTypeMaster::get();

        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':

                    HiredVehicles::add($request);
                    return redirect()->route('site.master.pol.hired_vehicles')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = HiredVehicles::edit($request);

                    return redirect()->route('site.master.pol.hired_vehicles')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = HiredVehicles::where('id', $request->id)->first();
                    if (!empty($item)) {
                        HiredVehicles::destroy($request->id);
                    }
                    return redirect()->route('site.master.pol.hired_vehicles')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.pol.hired_vehicles', $data);
    }

}
