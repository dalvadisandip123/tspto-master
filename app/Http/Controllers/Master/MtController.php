<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Masters\Mt\BatteriesAndTyresVariantMaster;
use App\Models\Masters\Mt\CondemnationMaster;
use App\Models\Masters\Mt\DescriptionByVehicleTypeMaster;
use App\Models\Masters\Mt\EmployeeRankMaster;
use App\Models\Masters\Mt\EmployeeRoleMaster;
use App\Models\Masters\Mt\OfficeOfMaster;
use App\Models\Masters\Mt\OfficerDesignationMaster;
use App\Models\Masters\Mt\OfficerNameMaster;
use App\Models\Masters\Mt\UnitHeadMaster;
use App\Models\Masters\Mt\VehicleBodyTypeMaster;
use App\Models\Masters\Mt\VehicleMakeMaster;
use App\Models\Masters\Mt\VehicleTypeMaster;
use App\Models\Masters\Mt\UnitsMaster;
use App\Models\Masters\Mt\BatteriesMaster;
use App\Models\Masters\Mt\VehicleUsageMaster;
use App\Models\Masters\Mt\VehicleVariantMaster;
use App\Models\Masters\Mt\VehicleTyresMaster;
use App\Models\Masters\Mt\VehicleCategoryMaster;
use App\Models\Masters\Mt\VehicleGroupMaster;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class MtController extends Controller
{

    public $pagelimit;

    public function __construct()
    {
        $this->middleware('auth');
        $this->pagelimit = 10;
    }


    public function vehicleMake(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Vehicle Make';
        $data['module_name'] = 'Vehicle Make';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';

        $data['sub1_active'] = 'vehicleMake';
        $data['lists'] = VehicleMakeMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('vehicle_make', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);

        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
//                    $insertData['image'] = Sponsor::uploadImage($request, 'photo');
//                    dd($insertData);
                    VehicleMakeMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicleMake')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleMakeMaster::where('id', $request->id)->first();
//                    $updateData['alias'] = Str::slug($updateData['name'], '-');;
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicleMake')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $insertData = $request->post();
                    $item = VehicleMakeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleMakeMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicleMake')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
//            dd($insertData);
        }
        return view('master.mt.vehicle_make', $data);
    }

    public function vehicleType(Request $request)
    {
        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Vehicle Type';
        $data['module_name'] = 'Vehicle Type';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'vehicleType';
        $data['lists'] = VehicleTypeMaster::with('getVehicleMake')->when($searc_q, function ($query) use ($filterParam) {
            $query->where('vehicle_type', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    VehicleTypeMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicleType')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicleType')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $insertData = $request->post();
                    $item = VehicleTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleTypeMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicleType')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.vehicle_type', $data);
    }

    public function UnitsMaster(Request $request)
    {

        $filterParam = $request->all();
        $searc_unit_name = $request->unit_name;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'UnitsMaster';
        $data['module_name'] = 'UnitsMaster';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'UnitsMaster';
        $data['lists'] = UnitsMaster::when($searc_unit_name, function ($query) use ($filterParam) {
            $query->where('unit_name', 'like', '%' . $filterParam['unit_name'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    UnitsMaster::create($insertData);
                    return redirect()->route('site.master.mt.units')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = UnitsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.units')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = UnitsMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        UnitsMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.units')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.units', $data);
    }

    public function batteries(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'BatteriesMaster';
        $data['module_name'] = 'BatteriesMaster';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'BatteriesMaster';
        $data['lists'] = BatteriesMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('battery_make', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    BatteriesMaster::create($insertData);
                    return redirect()->route('site.master.mt.batteries')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = BatteriesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.batteries')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = BatteriesMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        BatteriesMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.batteries')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.batteries', $data);
    }

    public function vehicleVariant(Request $request)
    {
        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Vehicle Variant';
        $data['module_name'] = 'Vehicle Variant';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'vehicleVariant';
        $data['lists'] = VehicleVariantMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('vehicle_variant', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    VehicleVariantMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicleVariant')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleVariantMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicleVariant')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleVariantMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleVariantMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicleVariant')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.vehicleVariant', $data);
    }


    public function vehicleTyres(Request $request)
    {
        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Vehicle Tyres';
        $data['module_name'] = 'Vehicle Tyres';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'vehicleTyres';
        $data['lists'] = VehicleTyresMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('tyre_make', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    VehicleTyresMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicleTyres')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleTyresMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicleTyres')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleTyresMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleTyresMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicleTyres')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.vehicletyres', $data);
    }

    public function vehicleCategory(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;

        $data = array();
        $data['title'] = 'Vehicle Category';
        $data['module_name'] = 'Vehicle Category';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'vehicleCategory';
        $data['lists'] = VehicleCategoryMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('vehicle_category', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    VehicleCategoryMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicleCategory')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleCategoryMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicleCategory')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleCategoryMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleCategoryMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicleCategory')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.vehiclecategory', $data);
    }

    public function vehicleGroup(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Vehicle Group';
        $data['module_name'] = 'Vehicle Group';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'vehicleGroup';
        $data['lists'] = VehicleGroupMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('vehicle_group_name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    VehicleGroupMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicleGroup')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleGroupMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicleGroup')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleGroupMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleGroupMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicleGroup')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.vehiclegroup', $data);
    }


    public function Condemnation(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'condemnation';
        $data['module_name'] = 'condemnation';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'condemnation';
        $data['lists'] = CondemnationMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

//                    dd($insertData);

                    CondemnationMaster::create($insertData);
                    return redirect()->route('site.master.mt.condemnation')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = CondemnationMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.condemnation')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = CondemnationMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        CondemnationMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.condemnation')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.condemnation', $data);
    }



    public function EmployeeRole(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Employee Role';
        $data['module_name'] = 'Employee Role';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'employee_role';
        $data['lists'] = EmployeeRoleMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

//                    dd($insertData);

                    EmployeeRoleMaster::create($insertData);
                    return redirect()->route('site.master.mt.employee_role')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = EmployeeRoleMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.employee_role')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = EmployeeRoleMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        EmployeeRoleMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.employee_role')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.employee_role', $data);
    }

    public function EmployeeRank(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Employee Rank';
        $data['module_name'] = 'Employee Rank';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'employee_rank';
        $data['lists'] = EmployeeRankMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

//                    dd($insertData);

                    EmployeeRankMaster::create($insertData);
                    return redirect()->route('site.master.mt.employee_rank')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = EmployeeRankMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.employee_rank')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = EmployeeRankMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        EmployeeRankMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.employee_rank')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.employee_rank', $data);
    }

    public function OfficeDept(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Office Dept';
        $data['module_name'] = 'Office Dept';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'office_dept';
        $data['lists'] = OfficeOfMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

//                    dd($insertData);

                    OfficeOfMaster::create($insertData);
                    return redirect()->route('site.master.mt.office_det')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = OfficeOfMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.office_det')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = OfficeOfMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        OfficeOfMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.office_det')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.office_dept', $data);
    }

    public function OfficeDesignation(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Office Designation';
        $data['module_name'] = 'Office Designation';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'office_designation';
        $data['lists'] = OfficerDesignationMaster::with('getOffice')->when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

//                    dd($insertData);

                    OfficerDesignationMaster::create($insertData);
                    return redirect()->route('site.master.mt.office_designation')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = OfficerDesignationMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.office_designation')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = OfficerDesignationMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        OfficerDesignationMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.office_designation')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.office_designation', $data);
    }


    public function VehicleBodyType(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Vehicle Body Type';
        $data['module_name'] = 'Vehicle Body Type';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'vehicle_body_type';
        $data['lists'] = VehicleBodyTypeMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

//                    dd($insertData);

                    VehicleBodyTypeMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicle_body_type')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleBodyTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicle_body_type')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleBodyTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleBodyTypeMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicle_body_type')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.vehicle_body_type', $data);
    }


    public function VehicleUsage(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Vehicle Usage';
        $data['module_name'] = 'Vehicle Usage';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'vehicle_usage';
        $data['lists'] = VehicleUsageMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

//                    dd($insertData);

                    VehicleUsageMaster::create($insertData);
                    return redirect()->route('site.master.mt.vehicle_usage')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleUsageMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.vehicle_usage')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleUsageMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleUsageMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.vehicle_usage')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.vehicle_usage', $data);
    }

    public function DescriptionByVehicle(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'description vehicle type';
        $data['module_name'] = 'description vehicle type';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'description_vehicle_type';
        $data['lists'] = DescriptionByVehicleTypeMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    DescriptionByVehicleTypeMaster::create($insertData);
                    return redirect()->route('site.master.mt.description_vehicle_type')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = DescriptionByVehicleTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.description_vehicle_type')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = DescriptionByVehicleTypeMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        DescriptionByVehicleTypeMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.description_vehicle_type')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.description_vehicle_type', $data);
    }


    public function OfficerName(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'officer cname';
        $data['module_name'] = 'officer name';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'officer_name';
        $data['lists'] = OfficerNameMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    $insertedid=OfficerNameMaster::create($insertData)->id;

                    $userdata = array();
                    $userdata['name'] = $insertData['officer_name'];
                    $userdata['mobile_no'] = $insertData['mobile_no'];
                    $userdata['login_role'] = $insertData['login_role'];
                    $userdata['user_type'] = 1;
                    $userdata['password'] = Hash::make(getRandomPassword());
                    $userResultData = User::create($userdata);

                    $getData = OfficerNameMaster::where('id', $insertedid)->first();

                    $updateData = array();
                    $updateData['user_id'] = $userResultData->id;

                    $getData->update($updateData);

                    return redirect()->route('site.master.mt.officer_name')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = OfficerNameMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);


                        $userRecord = User::where('id', $item->user_id)->first();


                        $userdata = array();
                        $userdata['name'] = $updateData['officer_name'];
                        $userdata['mobile_no'] = $updateData['mobile_no'];
                        $userdata['login_role'] = $updateData['login_role'];
                        $userRecord->update($userdata);

                    }
                    return redirect()->route('site.master.mt.officer_name')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = OfficerNameMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        OfficerNameMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.officer_name')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.officer_names', $data);
    }


    public function uninHead(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'unit head';
        $data['module_name'] = 'unit head';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'unit_head';
        $data['lists'] = UnitHeadMaster::with('getUnit')->when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);

        $data['units_lists'] = UnitsMaster::get();


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    UnitHeadMaster::create($insertData);
                    return redirect()->route('site.master.mt.uninHead')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = UnitHeadMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.uninHead')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = UnitHeadMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        UnitHeadMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.uninHead')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.unit_head', $data);
    }

    public function BatteriesTyresByVariant(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'batteries tyres by_variant';
        $data['module_name'] = 'batteries tyres by variant';
        $data['active'] = 'master';
        $data['sub_active'] = 'mt';
        $data['sub1_active'] = 'batteries_tyres_by_variant';
        $data['lists'] = BatteriesAndTyresVariantMaster::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    BatteriesAndTyresVariantMaster::create($insertData);
                    return redirect()->route('site.master.mt.batteries_tyres_by_variant')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = BatteriesAndTyresVariantMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.master.mt.batteries_tyres_by_variant')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = BatteriesAndTyresVariantMaster::where('id', $request->id)->first();
                    if (!empty($item)) {
                        BatteriesAndTyresVariantMaster::destroy($request->id);
                    }
                    return redirect()->route('site.master.mt.batteries_tyres_by_variant')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('master.mt.batteries_tyres_by_variant', $data);
    }


}
