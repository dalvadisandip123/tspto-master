<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public $pagelimit;

    public function __construct()
    {
        $this->middleware('auth');
        $this->pagelimit = 10;
    }


    public function users(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'users';
        $data['module_name'] = 'users';
        $data['active'] = 'users';
        $data['sub_active'] = 'users';
        $data['sub1_active'] = 'users';
        $data['lists'] = User::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    if ($insertData['password']) {
                        $insertData['password'] = Hash::make($insertData['password']);
                    } else {
                        $insertData['password'] = Hash::make(getRandomPassword());
                    }

                    if(count($insertData['access_units'])> 0){
                        $insertData['access_units'] = implode(',', $insertData['access_units']);
                    }else{
                        $insertData['access_units'] = null;
                    }

                    User::create($insertData);
                    return redirect()->route('site.users')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = User::where('id', $request->id)->first();


                    if(count($updateData['access_units'])> 0){
                        $updateData['access_units'] = implode(',', $updateData['access_units']);
                    }else{
                        $updateData['access_units'] = null;
                    }

                    if (!empty($item)) {
//                        $updateData['password'] = Hash::make($updateData['password']);
                        $item->update($updateData);
                    }
                    return redirect()->route('site.users')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = User::where('id', $request->id)->first();
                    if (!empty($item)) {
                        User::destroy($request->id);
                    }
                    return redirect()->route('site.users')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('users.users', $data);
    }


}
