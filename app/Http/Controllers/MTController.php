<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Masters\Mt\UnitsMaster;
use App\Models\Masters\Mt\VehicleBodyTypeMaster;
use App\Models\Masters\Mt\VehicleGroupMaster;
use App\Models\Masters\Mt\VehicleUsageMaster;
use App\Models\Masters\Pol\FuelTypeMaster;
use App\Models\Masters\Pol\OilGradeMaster;
use App\Models\MT\EmpAttendance;
use App\Models\MT\EmpDeputationPtos;
use App\Models\MT\EmpHolidayDutie;
use App\Models\MT\Employee;

use App\Models\Masters\Mt\UnitHeadMaster;

use App\Models\MT\EmpReporting;

use App\Models\MT\IssueVoucher;
use App\Models\MT\VdraDailyDiary;
use App\Models\MT\VehicleBatteryHistorie;
use App\Models\MT\VehicleDetail;
use App\Models\MT\VehicleTyreHistorie;
use App\Models\MT\VehicleUnitHistory;
use App\Models\MT\VehicleOfficerHistory;
use App\Models\MT\VehicleOfficerDrivers;

use Carbon\Carbon;
use Illuminate\Http\Request;


class MTController extends Controller
{

    public $pagelimit;

    public function __construct()
    {
        $this->middleware('auth');
        $this->pagelimit = 10;
    }


    public function vehicle(Request $request, $action = null, $id = null)
    {

        $data = array();
        $data['title'] = 'vehicle';
        $data['module_name'] = 'vehicle';
        $data['active'] = 'MT';
        $data['sub_active'] = 'vehicle';
        $data['sub1_active'] = '';


//        dd($request->post());


//        dd($action);

        switch ($action) {
            case 'new':
                if ($request->isMethod('post')) {
                    $VehicleDetail = VehicleDetail::add($request);
                    return redirect()->route('site.mt.vehicle')->with('flash_message', $data['module_name'] . ' Added!');
                }

                $data['FuelTypeMaster'] = FuelTypeMaster::get();
                $data['vehicleGroupLists'] = VehicleGroupMaster::get();
                $data['vehicleUsageLists'] = VehicleUsageMaster::get();
                $data['vehicleBodyTypeLists'] = VehicleBodyTypeMaster::get();
                $data['oilGradeLists'] = OilGradeMaster::get();

                return view('mt.vehicles.add', $data);

                break;
            case 'edit':

                if ($request->isMethod('post')) {
//                    dd($request->post());


                    $empData = VehicleDetail::edit($request);
//                    $empData = Employee::create($insertData);

                    return redirect()->back()->with('flash_message', $data['module_name'] . ' Updated!');

                } else {

                    $data['item'] = VehicleDetail::where('id', $id)->first();

                    $data['FuelTypeMaster'] = FuelTypeMaster::get();
                    $data['vehicleGroupLists'] = VehicleGroupMaster::get();
                    $data['vehicleUsageLists'] = VehicleUsageMaster::get();
                    $data['vehicleBodyTypeLists'] = VehicleBodyTypeMaster::get();
                    $data['oilGradeLists'] = OilGradeMaster::get();


                    return view('mt.vehicles.edit', $data);
                }


                break;

            case 'view':


                $data['item'] = VehicleDetail::with([
                    'getVehicleMake',
                    'getVehicleType',
                    'getVehicleVariant',
                    'getCategory',
                    'getFuelType',
                    'getGroup',
                    'getUsage',
                    'getBodyType',
                    'getOilGrade',
                    'getBatteryHistory',
                    'geteTyreHistory',

                ])->where('id', $id)->first();

                $data['FuelTypeMaster'] = FuelTypeMaster::get();
                $data['vehicleGroupLists'] = VehicleGroupMaster::get();
                $data['vehicleUsageLists'] = VehicleUsageMaster::get();
                $data['vehicleBodyTypeLists'] = VehicleBodyTypeMaster::get();
                $data['oilGradeLists'] = OilGradeMaster::get();


                return view('mt.vehicles.view', $data);


                break;

            case 'remove':

                $item = VehicleDetail::with([
                    'getBatteryHistory',
                    'geteTyreHistory',
                ])
                    ->where('id', $request->id)->first();

//                dd($item);

                if (!empty($item)) {
                    VehicleDetail::destroy($request->id);
                    VehicleTyreHistorie::where('vehicle_detail_id', $request->id)->delete();
                    VehicleBatteryHistorie::where('vehicle_detail_id', $request->id)->delete();
                }
                return redirect()->route('site.mt.vehicle')->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $vehicle_no = $request->vehicle_no;
                $searc_status = $request->status;

                $data['lists'] = VehicleDetail::with([
                    'getVehicleMake',
                    'getVehicleType',
                    'getVehicleVariant',
                    'getCategory',
                    'getFuelType',
                    'getGroup',
                    'getUsage',
                    'getBodyType',
                    'getOilGrade',
                    'getBatteryHistory',
                    'geteTyreHistory',
                ])
                    ->when($vehicle_no, function ($query) use ($filterParam) {
                        $query->where('vehicle_no', 'like', '%' . $filterParam['vehicle_no'] . '%');
                    })
                    ->when($searc_status, function ($query) use ($filterParam) {
                        $query->where('status', 'like', $filterParam['status']);
                    })
                    ->orderBy('id', 'desc')
                    ->paginate($this->pagelimit);;

                return view('mt.vehicles.lists', $data);


                break;
        }


    }


    public function empDeputationPtos(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();


        $data['title'] = 'employee deputation ptos';
        $data['module_name'] = 'employee deputation ptos';
        $data['active'] = 'MT';
        $data['sub_active'] = 'emp_deputation_ptos';
        $data['sub1_active'] = '';


        $data['lists'] = EmpDeputationPtos::with('getEmployee', 'getUnit')->when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

                    if (isset($insertData['atached_date'])) {
                        $insertData['atached_date'] = date('Y-m-d H:i:s', strtotime($insertData['atached_date']));
                    } else {
                        $insertData['atached_date'] = null;
                    }

                    if (isset($insertData['return_date'])) {
                        $insertData['return_date'] = date('Y-m-d H:i:s', strtotime($insertData['return_date']));
                    } else {
                        $insertData['return_date'] = null;
                    }


//                    dd($insertData);

                    EmpDeputationPtos::create($insertData);
                    return redirect()->route('site.mt.emp_deputation_ptos')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();


                    if (isset($updateData['atached_date'])) {
                        $updateData['atached_date'] = date('Y-m-d H:i:s', strtotime($updateData['atached_date']));
                    } else {
                        $updateData['atached_date'] = null;
                    }

                    if (isset($updateData['return_date'])) {
                        $updateData['return_date'] = date('Y-m-d H:i:s', strtotime($updateData['return_date']));
                    } else {
                        $updateData['return_date'] = null;
                    }


                    $item = EmpDeputationPtos::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.mt.emp_deputation_ptos')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = EmpDeputationPtos::where('id', $request->id)->first();
                    if (!empty($item)) {
                        EmpDeputationPtos::destroy($request->id);
                    }
                    return redirect()->route('site.mt.emp_deputation_ptos')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('mt.emp_deputation_ptos', $data);
    }

    public function empHalidyDuties(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();


        $data['title'] = 'employee Halidy Duties';
        $data['module_name'] = 'employee Halidy Duties';
        $data['active'] = 'MT';
        $data['sub_active'] = 'emp_halidy_duties';
        $data['sub1_active'] = '';


        $data['lists'] = EmpHolidayDutie::with('getEmployee')->when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

                    if (isset($insertData['duty_date'])) {
                        $insertData['duty_date'] = date('Y-m-d H:i:s', strtotime($insertData['duty_date']));
                    } else {
                        $insertData['duty_date'] = null;
                    }


                    EmpHolidayDutie::create($insertData);
                    return redirect()->route('site.mt.emp_halidy_duties')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();

                    if (isset($updateData['duty_date'])) {
                        $updateData['duty_date'] = date('Y-m-d H:i:s', strtotime($updateData['duty_date']));
                    } else {
                        $updateData['duty_date'] = null;
                    }


                    $item = EmpHolidayDutie::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.mt.emp_halidy_duties')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = EmpHolidayDutie::where('id', $request->id)->first();
                    if (!empty($item)) {
                        EmpHolidayDutie::destroy($request->id);
                    }
                    return redirect()->route('site.mt.emp_halidy_duties')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('mt.emp_halidy_duties', $data);
    }

    public function empReporting(Request $request)
    {

        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();


        $data['title'] = 'employee Reporting';
        $data['module_name'] = 'employee Reporting';
        $data['active'] = 'MT';
        $data['sub_active'] = 'emp_reporting';
        $data['sub1_active'] = '';


        $data['lists'] = EmpReporting::when($searc_q, function ($query) use ($filterParam) {
            $query->where('name', 'like', '%' . $filterParam['q'] . '%');
        })
            ->when($searc_status, function ($query) use ($filterParam) {
                $query->where('status', 'like', $filterParam['status']);
            })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);;
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();

                    if (isset($insertData['report_date'])) {
                        $insertData['report_date'] = date('Y-m-d H:i:s', strtotime($insertData['report_date']));
                    } else {
                        $insertData['atached_date'] = null;
                    }

                    if (isset($insertData['relieve_date'])) {
                        $insertData['relieve_date'] = date('Y-m-d H:i:s', strtotime($insertData['relieve_date']));
                    } else {
                        $insertData['relieve_date'] = null;
                    }


                    EmpReporting::create($insertData);
                    return redirect()->route('site.mt.emp_reporting')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();


                    if (isset($updateData['report_date'])) {
                        $updateData['report_date'] = date('Y-m-d H:i:s', strtotime($updateData['report_date']));
                    } else {
                        $updateData['atached_date'] = null;
                    }

                    if (isset($updateData['relieve_date'])) {
                        $updateData['relieve_date'] = date('Y-m-d H:i:s', strtotime($updateData['relieve_date']));
                    } else {
                        $updateData['relieve_date'] = null;
                    }


                    $item = EmpReporting::where('id', $request->id)->first();
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('site.mt.emp_reporting')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = EmpReporting::where('id', $request->id)->first();
                    if (!empty($item)) {
                        EmpReporting::destroy($request->id);
                    }
                    return redirect()->route('site.mt.emp_reporting')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('mt.emp_reporting', $data);
    }

    public function employee(Request $request, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'employee';
        $data['module_name'] = 'employee';
        $data['active'] = 'MT';
        $data['sub_active'] = 'employee';
        $data['sub1_active'] = '';


        switch ($action) {
            case 'new':


                if ($request->isMethod('post')) {

                    $empData = Employee::add($request);


                    return redirect()->route('site.mt.employee')->with('flash_message', $data['module_name'] . ' Added!');

                }


//                $data['lists'] = Employee::when($searc_q, function ($query) use ($filterParam) {
//                    $query->where('name', 'like', '%' . $filterParam['q'] . '%');
//                })
//                    ->when($searc_status, function ($query) use ($filterParam) {
//                        $query->where('status', 'like', $filterParam['status']);
//                    })
//                    ->orderBy('id', 'desc')
//                    ->paginate($this->pagelimit);;
////        if ($request->isMethod('post')) {
////
////        }
                return view('mt.employee.add', $data);


//                $insertData = $request->post();
//                Employee::create($insertData);
//                return redirect()->route('site.mt.employee')->with('flash_message', $data['module_name'] . ' Added!');
                break;
            case 'edit':

                if ($request->isMethod('post')) {
//                    dd($request->post());


                    $empData = Employee::edit($request);
//                    $empData = Employee::create($insertData);

                    return redirect()->route('site.mt.employee')->with('flash_message', $data['module_name'] . ' Added!');

                } else {

                    $data['item'] = Employee::with('getUser')->where('id', $id)->first();


                    if (!empty($data['item']->photo)) {
                        $data['item']->photo_url = Employee::uploadDir('url') . '/' . $data['item']->photo;
                    } else {
                        $data['item']->photo_url = imageNotAvalableUrl();
                    }


                    return view('mt.employee.edit', $data);
                }


                break;

            case 'remove':

                $item = Employee::where('id', $request->id)->first();
                if (!empty($item)) {
                    Employee::destroy($request->id);
                }
                return redirect()->route('site.mt.employee')->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $q_mobile_number = $request->q_mobile_number;
                $searc_status = $request->status;

                $data['lists'] = Employee::with('getUser')->when($q_mobile_number, function ($query) use ($filterParam) {
                    $query->where('mobile_number', 'like', '%' . $filterParam['q_mobile_number'] . '%');
                })
                    ->when($searc_status, function ($query) use ($filterParam) {
                        $query->where('status', 'like', $filterParam['status']);
                    })
                    ->orderBy('id', 'desc')
                    ->paginate($this->pagelimit);;

                return view('mt.employee.lists', $data);


                break;
        }


    }

    public function employeeAttendance(Request $request, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'employee Attendance';
        $data['module_name'] = 'employee Attendance';
        $data['active'] = 'MT';
        $data['sub_active'] = 'employee_attendance';
        $data['sub1_active'] = '';


        switch ($action) {
            case 'new':


                if ($request->isMethod('post')) {
//                    dd($request->post());


                    $empData = EmpAttendance::add($request);

                    return redirect()->route('site.mt.employee_attendance')->with('flash_message', $data['module_name'] . ' Added!');

                }

                if ($request->role != '') {

                    $data['emp_lists'] = Employee::with('getRank')->where('employee_role_masters_id', $request->role)->get();
                } else {
                    $data['emp_lists'] = '';
                }

                $current_attendance_date = Carbon::parse($request->attnd_date);

//                dd($current_attendance_date);

                $data['old_attnd'] = EmpAttendance::whereDate('attendance_date', $current_attendance_date)->where('employee_role_masters_id', $request->role)->get();


                return view('mt.employee_attendance.add', $data);

                break;
            case 'edit':

                if ($request->isMethod('post')) {

                    return redirect()->route('site.mt.employee_attendance')->with('flash_message', $data['module_name'] . ' Added!');

                } else {

                    $data['item'] = Employee::where('id', $id)->first();

                    if (!empty($data['item']->photo)) {
                        $data['item']->photo_url = Employee::uploadDir('url') . '/' . $data['item']->photo;
                    } else {
                        $data['item']->photo_url = imageNotAvalableUrl();
                    }


                    return view('mt.employee_attendance.edit', $data);
                }


                break;
            case 'remove':

                $item = Employee::where('id', $request->id)->first();
                if (!empty($item)) {
                    Employee::destroy($request->id);
                }
                return redirect()->route('site.mt.employee_attendance')->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $search_employees_id = $request->search_employees_id;
                $search_role = $request->search_role;
                $from_date_search = $request->from_date_search;
                $to_date_search = $request->to_date_search;


//                $data['old_attnd'] = EmpAttendance::whereDate('attendance_date', $current_attendance_date)->where('employee_role_masters_id', $request->role)->get();


                $data['lists'] = EmpAttendance::with('getEmployee', 'getRole')
                    ->when($search_employees_id, function ($query) use ($search_employees_id) {
                        $query->where('employees_id', $search_employees_id);
                    })
                    ->when($search_role, function ($query) use ($search_role) {
                        $query->where('employee_role_masters_id', $search_role);
                    })
                    ->when($from_date_search, function ($query) use ($from_date_search, $to_date_search) {
                        return $query->whereBetween('attendance_date', [date('Y-m-d', strtotime($from_date_search)), date('Y-m-d', strtotime($to_date_search))]);
                    })
                    ->orderBy('attendance_date', 'desc')
                    ->paginate($this->pagelimit);;

                return view('mt.employee_attendance.lists', $data);


                break;
        }


    }

    public function vdraDailyDiary(Request $request, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'VDRA Daily Diary';
        $data['module_name'] = 'VDRA Daily Diary';
        $data['active'] = 'MT';
        $data['sub_active'] = 'vdra_daily_diary';
        $data['sub1_active'] = '';


        switch ($action) {
            case 'new':


                if ($request->isMethod('post')) {
//                    dd($request->post());


                    $empData = VdraDailyDiary::add($request);

                    return redirect()->route('site.mt.vdra_daily_diary')->with('flash_message', $data['module_name'] . ' Added!');

                }

                if ($request->role != '') {

                    $data['emp_lists'] = Employee::with('getRank')->where('employee_role_masters_id', $request->role)->get();
                } else {
                    $data['emp_lists'] = '';
                }

                $current_attendance_date = Carbon::parse($request->attnd_date);

//                dd($current_attendance_date);

                $data['vehicle_lists'] = VehicleDetail::with([
                    'getVehicleMake',
                    'getVehicleType',
                    'getVehicleVariant',
//                    'getCategory',
//                    'getFuelType',
//                    'getGroup',
//                    'getUsage',
//                    'getBodyType',
//                    'getOilGrade',
//                    'getBatteryHistory',
//                    'geteTyreHistory',
                ])->get();

                $data['drivers_lists'] = Employee::with('getUser')
//                    ->where('employee_role_masters_id', 2)
                    ->get();


                return view('mt.vdra_daily_diary.add', $data);

                break;
            case 'edit':

                if ($request->isMethod('post')) {

                    return redirect()->route('site.mt.vdra_daily_diary')->with('flash_message', $data['module_name'] . ' Added!');

                } else {


                    $data['vehicle_lists'] = VehicleDetail::with([
                        'getVehicleMake',
                        'getVehicleType',
                        'getVehicleVariant',
//                    'getCategory',
//                    'getFuelType',
//                    'getGroup',
//                    'getUsage',
//                    'getBodyType',
//                    'getOilGrade',
//                    'getBatteryHistory',
//                    'geteTyreHistory',
                    ])->get();

                    $data['drivers_lists'] = Employee::with('getUser')
//                    ->where('employee_role_masters_id', 2)
                        ->get();

                    $data['item'] = VdraDailyDiary::where('id', $id)->first();


                    return view('mt.vdra_daily_diary.edit', $data);
                }


                break;
            case 'remove':

                $postData = $request->post();

//                dd();

                $item = VdraDailyDiary::where('id', $postData['id'])->first();
                if (!empty($item)) {
                    VdraDailyDiary::destroy($postData['id']);
                }
                return redirect()->route('site.mt.vdra_daily_diary')->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $search_vehicle_no = $request->search_vehicle_no;
                $search_from_date = $request->search_out_from_date;
                $search_to_return = $request->search_out_to_date;


//                $data['old_attnd'] = EmpAttendance::whereDate('attendance_date', $current_attendance_date)->where('employee_role_masters_id', $request->role)->get();


                $data['lists'] = VdraDailyDiary::with('getDriver1', 'getDriver2', 'getDriver3', 'getOfficer', 'getVehicle')
                    ->when($search_vehicle_no, function ($query) use ($search_vehicle_no) {
                        $query->where('vehicle_no', $search_vehicle_no);
                    })
                    ->when($search_from_date, function ($query) use ($search_from_date, $search_to_return) {
                        return $query->whereBetween('out_date', [date('Y-m-d', strtotime($search_from_date)), date('Y-m-d', strtotime($search_to_return))]);
                    })
//                    ->orderBy('attendance_date', 'desc')
                    ->paginate($this->pagelimit);;


                return view('mt.vdra_daily_diary.lists', $data);


                break;
        }


    }

    public function issueVoucher(Request $request, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'issue voucher';
        $data['module_name'] = 'issue voucher';
        $data['active'] = 'MT';
        $data['sub_active'] = 'issue_voucher';
        $data['sub1_active'] = '';


        switch ($action) {
            case 'new':


                if ($request->isMethod('post')) {
//                    dd($request->post());


                    $empData = IssueVoucher::add($request);

                    return redirect()->route('site.mt.issue_voucher')->with('flash_message', $data['module_name'] . ' Added!');

                }


                $data['vehicle_lists'] = VehicleDetail::with([
                    'getVehicleMake',
                    'getVehicleType',
                    'getVehicleVariant',
                ])->get();

                $data['units_lists'] = UnitsMaster::with('getUnitHead')->get();


                return view('mt.issue_voucher.add', $data);

                break;
            case 'edit':

                if ($request->isMethod('post')) {

                    $empData = IssueVoucher::edit($request);

                    return redirect()->route('site.mt.issue_voucher')->with('flash_message', $data['module_name'] . ' Added!');

                } else {

                    $data['item'] = IssueVoucher::with('getVehicle')->where('id', $id)->first();

                    $data['vehicle_lists'] = VehicleDetail::with([
                        'getVehicleMake',
                        'getVehicleType',
                        'getVehicleVariant',
                    ])
                        ->where('vehicle_no', $data['item']->vehicle_detail_id)->get();

                    $data['units_lists'] = UnitsMaster::with('getUnitHead')->get();


                    return view('mt.issue_voucher.edit', $data);
                }


                break;
            case 'view':

                $data['item'] = IssueVoucher::with('getVehicle')->where('id', $id)->first();

                $data['vehicle_lists'] = VehicleDetail::with([
                    'getVehicleMake',
                    'getVehicleType',
                    'getVehicleVariant',
                ])
                    ->where('vehicle_no', $data['item']->vehicle_detail_id)->get();

                $data['units_lists'] = UnitsMaster::with('getUnitHead')->get();


                return view('mt.issue_voucher.view', $data);

                break;
            case 'remove':

                $postData = $request->post();

//                dd();

                $item = IssueVoucher::where('id', $postData['id'])->first();
                if (!empty($item)) {
                    IssueVoucher::destroy($postData['id']);
                }
                return redirect()->route('site.mt.issue_voucher')->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $search_vehicle_no = $request->search_vehicle_no;
                $search_from_date = $request->search_out_from_date;
                $search_to_return = $request->search_out_to_date;


                $data['lists'] = IssueVoucher::with('getVehicle')
                    ->when($search_from_date, function ($query) use ($search_from_date, $search_to_return) {
                        return $query->whereBetween('issue_voucher_date', [date('Y-m-d', strtotime($search_from_date)), date('Y-m-d', strtotime($search_to_return))]);
                    })
                    ->paginate($this->pagelimit);;


                return view('mt.issue_voucher.lists', $data);


                break;
        }


    }

    public function MTUpdateForm(Request $request, $vehicles_id, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'MTUpdateForm';
        $data['module_name'] = 'MTUpdateForm';
        $data['active'] = 'MT';
        $data['sub_active'] = 'mt_update_form';
        $data['sub1_active'] = '';


        if ($request->isMethod('post')) {
            dd($request->post());


            return redirect()->route('site.mt.mt_update_form')->with('flash_message', $data['module_name'] . ' Added!');

        }


        $filterParam = $request->all();
        $search_vehicle_no = $request->search_vehicle_no;
        $search_from_date = $request->search_out_from_date;
        $search_to_return = $request->search_out_to_date;


        $data['vehicle_lists'] = VehicleDetail::with([
            'getVehicleMake',
            'getVehicleType',
            'getVehicleVariant',
//                    'getCategory',
//                    'getFuelType',
//                    'getGroup',
//                    'getUsage',
//                    'getBodyType',
//                    'getOilGrade',
//                    'getBatteryHistory',
//                    'geteTyreHistory',
        ])->get();


        if ($vehicles_id != '') {
            $data['vehicle_info'] = $data['vehicle_lists']->where('vehicle_no', $vehicles_id)->first();
            $data['getExistingData'] = VehicleBatteryHistorie::where('vehicle_detail_id', $data['vehicle_info']->id)->whereNull('battery_end_date')->get();

            $data['getExistingTyres'] = VehicleTyreHistorie::where('vehicle_detail_id', $data['vehicle_info']->id)->whereNull('tyre_end_date')->get();

        }

        $data['vehicles_number'] = $vehicles_id;

        return view('mt.mt_update_form', $data);


    }


    public function vehicleUnitHistory(Request $request, $action = null, $id = null)
    {

        $data = array();
        $data['title'] = 'Vehicle Unit History';
        $data['module_name'] = 'Vehicle Unit History';
        $data['active'] = 'MT';
        $data['sub_active'] = 'vehicle_unit_history';
        $data['sub1_active'] = 'vehicle_unit_history';


        if ($request->isMethod('post')) {

            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();


                    if (isset($insertData['issue_date'])) {
                        $insertData['issue_date'] = date('Y-m-d H:i:s', strtotime($insertData['issue_date']));
                    } else {
                        $insertData['issue_date'] = null;
                    }


                    $insertedid = VehicleUnitHistory::create($insertData)->id;

                    return redirect()->route('site.mt.vehicle_unit_history')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = VehicleUnitHistory::where('id', $request->id)->first();
                    if (!empty($item)) {

                        if (isset($updateData['issue_date'])) {
                            $updateData['issue_date'] = date('Y-m-d H:i:s', strtotime($updateData['issue_date']));
                        } else {
                            $updateData['issue_date'] = null;
                        }

                        if (isset($updateData['end_date'])) {
                            $updateData['end_date'] = date('Y-m-d H:i:s', strtotime($updateData['end_date']));
                        } else {
                            $updateData['end_date'] = null;
                        }

                        $item->update($updateData);
                    }
                    return redirect()->route('site.mt.vehicle_unit_history')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleUnitHistory::where('id', $request->id)->first();
                    if (!empty($item)) {

                        VehicleUnitHistory::destroy($request->id);
                    }
                    return redirect()->route('site.mt.vehicle_unit_history')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }

        if ($action) {
            if ($id == '') {
                return view('mt.vehicle_unit_history.add', $data);
            } else {
                $data['list'] = VehicleUnitHistory::where('id', $id)->first();
                return view('mt.vehicle_unit_history.edit', $data);
            }

        } else {

            $filterParam = $request->all();
            $allotment_order_number = $request->allotment_order_number;
            $status = $request->status;

            $data['lists'] = VehicleUnitHistory::
            when($allotment_order_number, function ($query) use ($filterParam) {
                $query->where('allotment_order_number', $filterParam['allotment_order_number']);
            })
                ->when($status, function ($query) use ($filterParam) {
                    $query->where('status', $filterParam['status']);
                })
                ->
                orderBy('id', 'desc')
                ->paginate($this->pagelimit);

            return view('mt.vehicle_unit_history.list', $data);
        }


    }


    public function vehicleOfficerHistory(Request $request, $action = null, $id = null)
    {

        $data = array();
        $data['title'] = 'Vehicle Officer History';
        $data['module_name'] = 'Vehicle Officer History';
        $data['active'] = 'MT';
        $data['sub_active'] = 'vehicle_officer_history';
        $data['sub1_active'] = 'vehicle_officer_history';


        if ($request->isMethod('post')) {

            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();


                    if (isset($insertData['issue_date'])) {
                        $insertData['issue_date'] = date('Y-m-d H:i:s', strtotime($insertData['issue_date']));
                    } else {
                        $insertData['issue_date'] = null;
                    }


                    $insertedid = VehicleOfficerHistory::create($insertData)->id;

                    return redirect()->route('site.mt.vehicle_officer_history')->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = VehicleOfficerHistory::where('id', $request->id)->first();
                    if (!empty($item)) {

                        if (isset($updateData['issue_date'])) {
                            $updateData['issue_date'] = date('Y-m-d H:i:s', strtotime($updateData['issue_date']));
                        } else {
                            $updateData['issue_date'] = null;
                        }

                        if (isset($updateData['end_date'])) {
                            $updateData['end_date'] = date('Y-m-d H:i:s', strtotime($updateData['end_date']));
                        } else {
                            $updateData['end_date'] = null;
                        }

                        $item->update($updateData);
                    }
                    return redirect()->route('site.mt.vehicle_officer_history')->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $item = VehicleOfficerHistory::where('id', $request->id)->first();
                    if (!empty($item)) {

                        VehicleOfficerHistory::destroy($request->id);
                    }
                    return redirect()->route('site.mt.vehicle_officer_history')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }

        if ($action) {
            if ($id == '') {
                return view('mt.vehicle_officer_history.add', $data);
            } else {
                $data['list'] = VehicleOfficerHistory::where('id', $id)->first();
                return view('mt.vehicle_officer_history.edit', $data);
            }

        } else {

            $filterParam = $request->all();
            $attachment_order_number = $request->attachment_order_number;
            $status = $request->status;

            $data['lists'] = VehicleOfficerHistory::
            when($attachment_order_number, function ($query) use ($filterParam) {
                $query->where('attachment_order_number', $filterParam['attachment_order_number']);
            })
                ->when($status, function ($query) use ($filterParam) {
                    $query->where('status', $filterParam['status']);
                })
                ->
                orderBy('id', 'desc')
                ->paginate($this->pagelimit);

            return view('mt.vehicle_officer_history.list', $data);
        }


    }


    public function vehicleOfficerDrivers(Request $request,$officerid, $action = null, $id = null)
    {
        $filterParam = $request->all();
        $searc_q = $request->q;
        $searc_status = $request->status;


        $data = array();
        $data['title'] = 'Vehicle Officers Drivers History';
        $data['module_name'] = 'Vehicle Officers Drivers History';
        $data['active'] = 'MT';
        $data['sub_active'] = 'vehicle_officer_history';
        $data['sub1_active'] = 'vehicle_officer_history';

        $data['officerinfo'] = VehicleOfficerHistory::where('id', $officerid)->first();
        $data['lists'] = VehicleOfficerDrivers::
        when($searc_status, function ($query) use ($filterParam) {
            $query->where('status', 'like', $filterParam['status']);
        })
            ->orderBy('id', 'desc')
            ->paginate($this->pagelimit);

        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->post();
                    if (isset($insertData['from_date'])) {
                        $insertData['from_date'] = date('Y-m-d H:i:s', strtotime($insertData['from_date']));
                    } else {
                        $insertData['from_date'] = null;
                    }

                    VehicleOfficerDrivers::create($insertData);

                    return redirect()->route('site.mt.vehicle_officer_drivers',['officerid'=>$officerid])->with('flash_message', $data['module_name'] . ' Added!');
                    break;
                case 'edit':
                    $updateData = $request->post();
                    $item = VehicleOfficerDrivers::where('id', $request->id)->first();
                    if (!empty($item)) {

                        if (isset($updateData['from_date'])) {
                            $updateData['from_date'] = date('Y-m-d H:i:s', strtotime($updateData['from_date']));
                        } else {
                            $updateData['from_date'] = null;
                        }

                        if (isset($updateData['to_date'])) {
                            $updateData['to_date'] = date('Y-m-d H:i:s', strtotime($updateData['to_date']));
                        } else {
                            $updateData['to_date'] = null;
                        }

                        $item->update($updateData);
                    }
                    return redirect()->route('site.mt.vehicle_officer_drivers',['officerid'=>$officerid])->with('flash_message', $data['module_name'] . ' updated !');
                    break;
                case 'remove':
                    $insertData = $request->post();
                    $item = VehicleOfficerDrivers::where('id', $request->id)->first();
                    if (!empty($item)) {
                        VehicleOfficerDrivers::destroy($request->id);
                    }
                    return redirect()->route('site.mt.vehicle_officer_drivers',['officerid'=>$officerid])->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('mt.vehicle_officer_history.vehicle_drivers_history', $data);
    }


}
