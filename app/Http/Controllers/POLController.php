<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;

use App\Models\Masters\Pol\AccountNumbersMaster;
use App\Models\Masters\Pol\FuelTypeMaster;
use App\Models\Masters\Pol\LicenseCertificatesMaster;
use App\Models\Masters\Pol\OilGradeMaster;

use App\Models\Masters\Pol\OutletsMaster;

use App\Models\MT\VehicleDetail;


use App\Models\POL\BudgetRegister;
use App\Models\POL\CertificateLicenseSchedule;
use App\Models\POL\FuelIssue;
use App\Models\POL\FuelPrice;
use App\Models\POL\FuelQuotaAllotment;
use App\Models\POL\lubesPrice;
use App\Models\POL\OutStationFilling;
use App\Models\POL\PurchaseRegister\PurchaseRegister;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class POLController extends Controller
{

    public $pagelimit;

    public function __construct()
    {
        $this->middleware('auth');
        $this->pagelimit = 10;
    }

    public function fuelPrice(Request $request)
    {
        $filterParam = $request->all();
        $search_date = $request->search_date;


        $data = array();
        $data['title'] = 'fuel price';
        $data['module_name'] = 'fuel price';
        $data['active'] = 'pol';
        $data['sub_active'] = 'fuel_price';
        $data['sub1_active'] = '';


        $data['fuelTypes'] = FuelTypeMaster::orderBy('id', 'desc')->get();


//        dd($data['fuelTypes']->count());

        $data['listsItems'] = FuelPrice::when($search_date, function ($query) use ($filterParam) {
            $query->where('fuel_price_date', date('Y-m-d', strtotime($filterParam['search_date'])));
        })
            ->orderBy('fuel_price_date', 'desc')
//            ->get()
            ->paginate($data['fuelTypes']->count() * $this->pagelimit);


        $data['lists'] = $data['listsItems']->groupBy('fuel_price_date');


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':

                    $results = FuelPrice::add($request);

//                    dd($results);

                    if ($results == true) {
                        return redirect()->route('site.pol.fuel_price')->with('flash_message', $data['module_name'] . ' Added!');

                    } else {

                        return redirect()->route('site.pol.fuel_price')->with('flash_message', $data['module_name'] . ' Existed!');
                    }

                    break;

                case 'remove':
                    $item = FuelPrice::where('fuel_price_date', $request->date)->delete();
//                    dd($item);


                    return redirect()->route('site.pol.fuel_price')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('pol.fuel_price', $data);
    }


    public function lubesPrice(Request $request)
    {
        $filterParam = $request->all();
        $search_date = $request->search_date;


        $data = array();
        $data['title'] = 'fuel price';
        $data['module_name'] = 'lubes price';
        $data['active'] = 'pol';
        $data['sub_active'] = 'lubes_price';
        $data['sub1_active'] = '';


        $data['oliGrades'] = OilGradeMaster::orderBy('id', 'desc')->get();


//        dd($data['fuelTypes']->count());

        $data['listsItems'] = lubesPrice::when($search_date, function ($query) use ($filterParam) {
            $query->where('lubes_price_date', date('Y-m-d', strtotime($filterParam['search_date'])));
        })
            ->orderBy('lubes_price_date', 'desc')
//            ->get()
            ->paginate($data['oliGrades']->count() * $this->pagelimit);


//        $data['lists'] = array();
        $data['lists'] = $data['listsItems']->groupBy('lubes_price_date');


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':

                    $results = lubesPrice::add($request);

//                    dd($results);

                    if ($results == true) {
                        return redirect()->route('site.pol.lubes_price')->with('flash_message', $data['module_name'] . ' Added!');

                    } else {

                        return redirect()->route('site.pol.lubes_price')->with('flash_message', $data['module_name'] . ' Existed!');
                    }

                    break;

                case 'remove':
                    $item = lubesPrice::where('lubes_price_date', $request->date)->delete();
//                    dd($item);


                    return redirect()->route('site.pol.lubes_price')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('pol.lubes_price', $data);
    }

    public function OutStationFilling(Request $request)
    {
        $filterParam = $request->all();
        $search_date = $request->search_date;


        $data = array();
        $data['title'] = 'out station filling';
        $data['module_name'] = 'out station filling';
        $data['active'] = 'pol';
        $data['sub_active'] = 'out_station_filling';
        $data['sub1_active'] = '';


        $data['vehicle_lists'] = VehicleDetail::with([
            'getVehicleMake',
            'getVehicleType',
            'getVehicleVariant'
        ])->get();


//        dd($data['fuelTypes']->count());

        $data['lists'] = OutStationFilling::when($search_date, function ($query) use ($filterParam) {
            $query->where('lubes_price_date', date('Y-m-d', strtotime($filterParam['search_date'])));
        })
            ->orderBy('id', 'desc')
//            ->get()
            ->paginate($this->pagelimit);


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':

                    $results = OutStationFilling::add($request);

//                    dd($results);

                    return redirect()->route('site.pol.out_station_filling')->with('flash_message', $data['module_name'] . ' Added!');


                    break;

                case 'remove':
                    $item = OutStationFilling::where('id', $request->id)->delete();
//                    dd($item);


                    return redirect()->route('site.pol.out_station_filling')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('pol.out_station_filling', $data);
    }


    public function CertificateLicenseSchedule(Request $request)
    {
        $filterParam = $request->all();
        $search_date = $request->search_date;


        $data = array();
        $data['title'] = 'Certificate License Schedule';
        $data['module_name'] = 'certificate License Schedule';
        $data['active'] = 'pol';
        $data['sub_active'] = 'certificate_license_schedule';
        $data['sub1_active'] = '';


        $data['licenseCertificates'] = LicenseCertificatesMaster::get();
        $data['fuelTypeLists'] = FuelTypeMaster::get();
        $data['OutletsLists'] = OutletsMaster::get();


//        dd($data['fuelTypes']->count());

        $data['lists'] = CertificateLicenseSchedule::when($search_date, function ($query) use ($filterParam) {
            $query->where('renewal_date', date('Y-m-d', strtotime($filterParam['search_date'])));
        })
            ->orderBy('id', 'desc')
//            ->get()
            ->paginate($this->pagelimit);


        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $results = CertificateLicenseSchedule::add($request);
                    return redirect()->route('site.pol.certificate_license_schedule')->with('flash_message', $data['module_name'] . ' Added!');
                    break;

                case 'edit':
                    $results = CertificateLicenseSchedule::edit($request);
                    return redirect()->route('site.pol.certificate_license_schedule')->with('flash_message', $data['module_name'] . ' Added!');
                    break;

                case 'remove':
                    $item = CertificateLicenseSchedule::where('id', $request->id)->delete();
//                    dd($item);


                    return redirect()->route('site.pol.certificate_license_schedule')->with('flash_message', $data['module_name'] . ' Removed !');
                    break;
            }
        }
        return view('pol.certificate_license_schedule', $data);
    }


    public function fuelLubricantBalancesRegister(Request $request)
    {
        $filterParam = $request->all();
        $search_date = $request->search_date;


        $data = array();
        $data['title'] = 'fuel price';
        $data['module_name'] = 'lubes price';
        $data['active'] = 'pol';
        $data['sub_active'] = 'fuel_lubricant_balances_register';
        $data['sub1_active'] = '';


        $data['fuelTypeMasters'] = FuelTypeMaster::get();
        $data['outletsMasters'] = OutletsMaster::get();

        $data['accountNumbersMasters'] = AccountNumbersMaster::get();
        $data['oilGradeLists'] = OilGradeMaster::get();


        return view('pol.fuel_lubricant_balances_register', $data);
    }

    public function purchaseRegister(Request $request, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'purchase register';
        $data['module_name'] = 'purchase register';
        $data['active'] = 'pol';
        $data['sub_active'] = 'purchase_register';
        $data['sub1_active'] = '';


        $data['fuelTypeMasters'] = FuelTypeMaster::get();
        $data['outletsMasters'] = OutletsMaster::get();

        $data['accountNumbersMasters'] = AccountNumbersMaster::get();
        $data['oilGradeLists'] = OilGradeMaster::get();

//        dd($request->post());


//        dd($action);

        switch ($action) {
            case 'new':
                if ($request->isMethod('post')) {
                    $VehicleDetail = PurchaseRegister::add($request);
                    return redirect()->route('site.pol.purchase_register')->with('flash_message', $data['module_name'] . ' Added!');
                }


                return view('pol.purchase_register.add', $data);

                break;
            case 'edit':

                if ($request->isMethod('post')) {
//                    dd($request->post());


                    $empData = PurchaseRegister::edit($request);
//                    $empData = Employee::create($insertData);

                    return redirect()->back()->with('flash_message', $data['module_name'] . ' Updated!');

                } else {

                    $data['item'] = PurchaseRegister::with([
                        'getFuels',
                        'getLubs',
                        'getAccount',
                    ])->where('id', $id)->first();


                    return view('pol.purchase_register.edit', $data);
                }


                break;

            case 'view':


                $data['item'] = PurchaseRegister::with([
                    'getFuels',
                    'getLubs',
                    'getAccount',
                ])->where('id', $id)->first();


                return view('mt.vehicles.view', $data);


                break;

            case 'remove':


//                dd($request->all());

                $empData = PurchaseRegister::remove($request);


                return redirect()->route('site.pol.purchase_register')->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $vehicle_no = $request->vehicle_no;
                $searc_status = $request->status;

                $data['lists'] = PurchaseRegister::with([
                    'getFuels',
                    'getLubs',
                    'getAccount',

                ])
                    ->when($searc_status, function ($query) use ($filterParam) {
                        $query->where('status', 'like', $filterParam['status']);
                    })
                    ->orderBy('id', 'desc')
                    ->paginate($this->pagelimit);;

                return view('pol.purchase_register.lists', $data);


                break;
        }


    }

    public function budgetRegister(Request $request, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'budget register';
        $data['module_name'] = 'budget register';
        $data['active'] = 'pol';
        $data['sub_active'] = 'budget_register';
        $data['sub1_active'] = '';


//        dd($request->post());


//        dd($action);

        switch ($action) {
            case 'new':
                if ($request->isMethod('post')) {
                    $VehicleDetail = BudgetRegister::add($request);
                    return redirect()->route('site.pol.budget_register')->with('flash_message', $data['module_name'] . ' Added!');
                }


                return view('pol.budget_register.add', $data);

                break;
            case 'edit':

                if ($request->isMethod('post')) {
//                    dd($request->post());


                    $empData = BudgetRegister::edit($request);
//                    $empData = Employee::create($insertData);

                    return redirect()->back()->with('flash_message', $data['module_name'] . ' Updated!');

                } else {

                    $data['item'] = BudgetRegister::where('id', $id)->first();


                    return view('pol.budget_register.edit', $data);
                }


                break;

            case 'view':


                $data['item'] = BudgetRegister::where('id', $id)->first();


                return view('pol.budget_register.view', $data);


                break;

            case 'remove':


//                dd($request->all());

                $empData = BudgetRegister::remove($request);


                return redirect()->route('site.pol.budget_register')->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $vehicle_no = $request->vehicle_no;
                $searc_status = $request->status;

                $data['lists'] = BudgetRegister::when($searc_status, function ($query) use ($filterParam) {
                    $query->where('status', 'like', $filterParam['status']);
                })
                    ->orderBy('id', 'desc')
                    ->paginate($this->pagelimit);;

                return view('pol.budget_register.lists', $data);


                break;
        }


    }

    public function fuelIssue(Request $request, $type = null, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'Fuel Issue';
        $data['module_name'] = 'Fuel Issue';
        $data['active'] = 'pol';
        $data['sub_active'] = 'fuel_issue';
        $data['sub1_active'] = '';


//        dd($request->post());


//        dd($action);


        if ($type == '') {
            return view('pol.fuel_issue.types', $data);
        }


        switch ($type) {
            case 'vehicles':

                switch ($action) {
                    case 'new':
                        if ($request->isMethod('post')) {
                            $VehicleDetail = FuelIssue::add($request);
                            return redirect()->route('site.pol.fuel_issue', ['type'=>$type])->with('flash_message', $data['module_name'] . ' Added!');

                        }

                        $data['vehicle_lists'] = VehicleDetail::with([
                            'getVehicleMake',
                            'getVehicleType',
                            'getVehicleVariant',
                            'getFuelQuotaAllotments',
                            'getFuelType',
                            'getFuelIssuesWithinDay',
                        ])->get();


                        $todayFuelIssueCount = FuelIssue::where('status', 1)
                            ->where('fuel_issues_for', $type)
                            ->whereDay('created_at', Carbon::now()->day)->count();


                        $Userinfo = User::with('getOutlet.getOutlet')->where('id', Auth::id())->first();


                        $tx_info = array();
                        $tx_info['no'] = $todayFuelIssueCount + 1;
                        $tx_info['date'] = date('d-m-Y');
                        $tx_info['outlet'] = (isset($Userinfo->getOutlet->getOutlet)) ? $Userinfo->getOutlet->getOutlet->name : '';

                        $data['tx_info'] = $tx_info;


//                        dd($todayFuelIssueCount);


                        return view('pol.fuel_issue.vehicles.add', $data);
                        break;

                    default:

                        $filterParam = $request->all();
                        $vehicle_no = $request->vehicle_no;
                        $searc_status = $request->status;

                        $data['lists'] = FuelIssue::when($searc_status, function ($query) use ($filterParam) {
                            $query->where('status', 'like', $filterParam['status']);
                        })
                            ->where('fuel_issues_for', $type)
                            ->orderBy('id', 'desc')
                            ->paginate($this->pagelimit);;

                        return view('pol.fuel_issue.vehicles.lists', $data);


                        break;
                }

                break;
            case 'hired_vehicles':

                switch ($action) {
                    case 'new':
                        if ($request->isMethod('post')) {
                            $VehicleDetail = BudgetRegister::add($request);
                            return redirect()->route('site.pol.fuel_issue')->with('flash_message', $data['module_name'] . ' Added!');

                        }
                        return view('pol.fuel_issue.add', $data);
                        break;

                    default:

                        $filterParam = $request->all();
                        $vehicle_no = $request->vehicle_no;
                        $searc_status = $request->status;

                        $data['lists'] = BudgetRegister::when($searc_status, function ($query) use ($filterParam) {
                            $query->where('status', 'like', $filterParam['status']);
                        })
                            ->orderBy('id', 'desc')
                            ->paginate($this->pagelimit);;

                        return view('pol.fuel_issue.lists', $data);


                        break;
                }

                break;
            case 'generator_spl':

                switch ($action) {
                    case 'new':
                        if ($request->isMethod('post')) {
                            $VehicleDetail = BudgetRegister::add($request);
                            return redirect()->route('site.pol.fuel_issue')->with('flash_message', $data['module_name'] . ' Added!');

                        }
                        return view('pol.fuel_issue.add', $data);
                        break;

                    default:

                        $filterParam = $request->all();
                        $vehicle_no = $request->vehicle_no;
                        $searc_status = $request->status;

                        $data['lists'] = BudgetRegister::when($searc_status, function ($query) use ($filterParam) {
                            $query->where('status', 'like', $filterParam['status']);
                        })
                            ->orderBy('id', 'desc')
                            ->paginate($this->pagelimit);;

                        return view('pol.fuel_issue.lists', $data);


                        break;
                }

                break;
        }


    }


    public function fuelQuotaAllotment(Request $request, $type, $action = null, $id = null)
    {


        $data = array();
        $data['title'] = 'fuel quota allotments ' . $type;
        $data['module_name'] = 'fuel quota allotments ' . $type;
        $data['active'] = 'pol';
        $data['sub_active'] = 'fuel_quota_allotments_' . $type;
        $data['sub1_active'] = '';


//        dd($request->post());


//        dd($action);

        switch ($action) {
            case 'add':
                if ($request->isMethod('post')) {

//                    dump($request->all());

                    $results = FuelQuotaAllotment::add($request);

                    return redirect()->route('site.pol.fuel_quota_allotments', ['type' => $type])->with('flash_message', $data['module_name'] . ' Added!');
                }

                $data['vehicle_lists'] = VehicleDetail::with([
                    'getVehicleMake',
                    'getVehicleType',
                    'getVehicleVariant'
                ])->get();


                return view('pol.fuel_quota_allotments.add', $data);

                break;


            case 'view':


                $data['item'] = BudgetRegister::where('id', $id)->first();


                return view('pol.fuel_quota_allotments.view', $data);


                break;

            case 'remove':


//                dd($request->all());

                $empData = BudgetRegister::remove($request);


                return redirect()->route('site.pol.fuel_quota_allotments', ['type' => $type])->with('flash_message', $data['module_name'] . ' Removed !');
                break;
            default:

                $filterParam = $request->all();
                $vehicle_no = $request->vehicle_no;
                $searc_status = $request->status;

                $data['lists'] = FuelQuotaAllotment::when($vehicle_no, function ($query) use ($filterParam) {
                    $query->where('vehicle_number', 'like', '%' . $filterParam['vehicle_no'] . '%');
                })
                    ->where('allotments_type', $type)
                    ->where('status', 1)
                    ->orderBy('id', 'desc')
                    ->paginate($this->pagelimit);


                $data['lists']->map(function ($item) use ($type) {
//                    dd($item);

                    $item->logs = FuelQuotaAllotment::where('vehicle_number', $item->vehicle_number)
                        ->where('allotments_type', $type)
                        ->where('status', 2)
                        ->get();
                });

//                dd( $data['lists']);


                return view('pol.fuel_quota_allotments.lists', $data);


                break;
        }


    }

}
