<?php

namespace App\Http\Controllers;

use App\Models\Masters\Mt\OfficerNameMaster;
use App\Models\Masters\Mt\VehicleMakeMaster;
use App\Models\Masters\Mt\VehicleTypeMaster;
use App\Models\Masters\Mt\VehicleVariantMaster;
use App\Models\Masters\Pol\FiltersMaster;
use App\Models\Masters\Pol\OilGradeMaster;
use App\Models\Masters\Pol\OilTypeMaster;
use App\Models\MT\Employee;
use App\Models\MT\VehicleBatteryHistorie;
use App\Models\MT\VehicleDetail;
use App\Models\MT\VehicleTyreHistorie;
use Illuminate\Http\Request;

class AjaxController extends Controller
{

    public function getVehicletypes(Request $request)
    {


        $vehiclemake = $request->vehiclemake;

//        dd($vehiclemake);

        $return = array();
        $return['vehicletypes'] = VehicleTypeMaster::where('vehicle_make_masters_id', $vehiclemake)->where('status', 1)->pluck('vehicle_type', 'id');

        return $return;

//        $return['length'] = VehicleTypeMaster::where('vehicle_make', $vehiclemake)->where('status',1)->count();
//        echo json_encode($return);
//        exit();
    }


    public function getVehicleVariant(Request $request)
    {


        $vehicle_type = $request->vehicle_type;

//        dd($vehicle_type);

        $return = array();
        $return['vehicle_variant'] = VehicleVariantMaster::where('vehicle_type_masters_id', $vehicle_type)->where('status', 1)->pluck('vehicle_variant', 'id');

        return $return;

//        $return['length'] = VehicleTypeMaster::where('vehicle_make', $vehiclemake)->where('status',1)->count();
//        echo json_encode($return);
//        exit();
    }


    public function getVehicleVariantData(Request $request)
    {


        $return_data = array();


//        return VehicleTypeMaster::with('getVehicleMake', 'getVehicleVariant')->get();


        $return_data['oils']['types'] = OilTypeMaster::where('status', 1)->get();
        $return_data['oils']['grade'] = OilGradeMaster::where('status', 1)->get();


        $filters = FiltersMaster::where('status', 1)->get();


        $return_data['filters'] = '';


        if (count($filters) > 0) {
            foreach ($filters AS $filter) {

                $return_data['filters'] .= '<tr>';
                $return_data['filters'] .= '<td>';
                $return_data['filters'] .= '<div class="input-field">' . $filter->name . '</div>';

                $return_data['filters'] .= '</td>';
                $return_data['filters'] .= '<td>';
                $return_data['filters'] .= '<div
                                class="input-field">
                                <input
                                    id="intervel_' . $filter->id . '"
                                    type="text"
    
                                    name="intervel"
                                    class="">
                                <label
                                    for="intervel_' . $filter->id . '">Intervel</label>
                            </div>';
                $return_data['filters'] .= '</td>';
                $return_data['filters'] .= '</tr>';


            }
        }

        return $return_data;


    }

    public function getOfficer(Request $request)
    {


        $officer_id = $request->officer_id;

//        dd($vehicle_type);

        $return = array();
        $return['officer'] = OfficerNameMaster::with('getOffice', 'getOfficeDesignation')->where('id', $officer_id)->where('status', 1)->first();

        return $return;

//        $return['length'] = VehicleTypeMaster::where('vehicle_make', $vehiclemake)->where('status',1)->count();
//        echo json_encode($return);
//        exit();
    }

    public function getEmp(Request $request)
    {


        $emp_id = $request->emp_id;

//        dd($vehicle_type);

        $return = array();
        $return['emp'] = Employee::with('getRank', 'getRole')->where('id', $emp_id)->where('status', 1)->first();

        return $return;

//        $return['length'] = VehicleTypeMaster::where('vehicle_make', $vehiclemake)->where('status',1)->count();
//        echo json_encode($return);
//        exit();
    }
    public function getVehicleByUnit(Request $request)
    {


        $units_id = $request->units_id;
        $vehicle_category = $request->vehicle_category;


//        dd($vehicle_type);

        $return = array();
        $return['list'] = VehicleDetail::when($vehicle_category, function ($query) use ($vehicle_category) {
            $query->where('vehicle_category_id', $vehicle_category);
        })
            ->where('status', 1)->get();

        return $return;

//        $return['length'] = VehicleTypeMaster::where('vehicle_make', $vehiclemake)->where('status',1)->count();
//        echo json_encode($return);
//        exit();
    }


    public function getVehicleBatary(Request $request)
    {

        $return = '';

        $no_bataries = $request->no_bataries;


        $getExistingData = VehicleBatteryHistorie::where('vehicle_detail_id', $request->item_id)->get();


        if ($request->item_id != '' && count($getExistingData) <= $no_bataries) {


            if ($no_bataries > 0) {
                for ($i = 0; $i < $no_bataries; $i++) {

                    $itemarray = isset($getExistingData[$i]) ? $getExistingData[$i] : '';

                    $Value_battery_voltage = ($itemarray != "" && $itemarray->battery_voltage != '') ? $itemarray->battery_voltage : '';
                    $Value_battery_size = ($itemarray != "" && $itemarray->battery_size != '') ? $itemarray->battery_size : '';
                    $Value_battery_ampere = ($itemarray != "" && $itemarray->battery_ampere != '') ? $itemarray->battery_ampere : '';
                    $Value_battery_life_span = ($itemarray != "" && $itemarray->battery_life_span != '') ? $itemarray->battery_life_span : '';
                    $Value_battery_number = ($itemarray != "" && $itemarray->battery_number != '') ? $itemarray->battery_number : '';
                    $Value_battery_issue_date = ($itemarray != "" && $itemarray->battery_issue_date != '') ?date('d-m-Y', strtotime($itemarray->battery_issue_date)) : '';

                    $return .= '<div class="row bataries_list">


    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">


                <select name="battery[' . $i . '][battery_make]">';


                    $getBatteriesmake = getBatteriesmake();


                    if (count($getBatteriesmake) > 0) {

                    }
                    foreach ($getBatteriesmake AS $getBatteriesmak) {
                        $selectedVal = ($itemarray != "" && $getBatteriesmak->id == $itemarray->battery_make) ? "selected" : "";
                        $return .= ' <option ' . $selectedVal . ' value = "' . $getBatteriesmak->id . '" > ' . $getBatteriesmak->battery_make . ' </option >';
                    }

                    $return .= '
                </select>


                <label for="battery_make_' . $i . '">
                    Make
                </label>
            </div>
        </div>
    </div>


    <div class="col m1">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_voltage_' . $i . '"
                       type="text"
                       value="'.$Value_battery_voltage.'"
                       name="battery[' . $i . '][battery_voltage]"
                       class="validate ">
                <label for="battery_voltage_' . $i . '" class="active">
                    Voltage
                </label>
            </div>
        </div>
    </div>


    <div class="col m1">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_ampere_' . $i . '"
                       type="text"
                        value="'.$Value_battery_ampere.'"
                       name="battery[' . $i . '][battery_ampere]"
                       class="validate battery_ampere">
                <label for="battery_ampere_' . $i . '" class="active">
                    Ampere
                </label>
            </div>
        </div>
    </div>

    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_size_' . $i . '"
                       type="text"
                         value="'.$Value_battery_size.'"
                       
                       name="battery[' . $i . '][battery_size]"
                       class="validate battery_size">
                <label for="battery_size_' . $i . '" class="active">
                    Size
                </label>
            </div>
        </div>
    </div>
    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_life_span_' . $i . '"
                       type="text"
                         value="'.$Value_battery_life_span.'"
                       name="battery[' . $i . '][battery_life_span]"
                       class="validate battery_life_span">
                <label
                    for="battery_life_span_' . $i . '" class="active">
                    Life Span
                </label>
            </div>
        </div>
    </div>

    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_number_' . $i . '"
                       type="text"
                         value="'.$Value_battery_number.'"
                       name="battery[' . $i . '][battery_number]"
                       class="validate battery_number">
                <label for="battery_number_' . $i . '" class="active">
                    Number
                </label>
            </div>
        </div>
    </div>

    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_issue_date_' . $i . '"
                       type="text"
                        value="'.$Value_battery_issue_date.'"
                       name="battery[' . $i . '][battery_issue_date]"
                       class="validate datepicker_ui battery_issue_date">
                <label
                    for="battery_issue_date_' . $i . '" class="active">
                    Issue Date
                </label>
            </div>
        </div>
    </div>


</div>';
                }
            }

        } else {
            if ($no_bataries > 0) {
                for ($i = 0; $i < $no_bataries; $i++) {
                    $return .= '<div class="row bataries_list">


    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">


                <select name="battery[' . $i . '][battery_make]">';


                    $getBatteriesmake = getBatteriesmake();


                    if (count($getBatteriesmake) > 0) {

                    }
                    foreach ($getBatteriesmake AS $getBatteriesmak) {
                        $return .= ' <option value = "' . $getBatteriesmak->id . '" > ' . $getBatteriesmak->battery_make . ' </option >';
                    }

                    $return .= '
                </select>


                <label for="battery_make_' . $i . '">
                    Make
                </label>
            </div>
        </div>
    </div>


    <div class="col m1">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_voltage_' . $i . '"
                       type="text"
                       name="battery[' . $i . '][battery_voltage]"
                       class="validate ">
                <label for="battery_voltage_' . $i . '">
                    Voltage
                </label>
            </div>
        </div>
    </div>


    <div class="col m1">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_ampere_' . $i . '"
                       type="text"
                       name="battery[' . $i . '][battery_ampere]"
                       class="validate battery_ampere">
                <label for="battery_ampere_' . $i . '">
                    Ampere
                </label>
            </div>
        </div>
    </div>

    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_size_' . $i . '"
                       type="text"
                       name="battery[' . $i . '][battery_size]"
                       class="validate battery_size">
                <label for="battery_size_' . $i . '">
                    Size
                </label>
            </div>
        </div>
    </div>
    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_life_span_' . $i . '"
                       type="text"
                       name="battery[' . $i . '][battery_life_span]"
                       class="validate battery_life_span">
                <label
                    for="battery_life_span_' . $i . '">
                    Life Span
                </label>
            </div>
        </div>
    </div>

    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_number_' . $i . '"
                       type="text"
                       name="battery[' . $i . '][battery_number]"
                       class="validate battery_number">
                <label for="battery_number_' . $i . '">
                    Number
                </label>
            </div>
        </div>
    </div>

    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">
                <input id="battery_issue_date_' . $i . '"
                       type="text"
                       name="battery[' . $i . '][battery_issue_date]"
                       class="validate datepicker_ui battery_issue_date">
                <label
                    for="battery_issue_date_' . $i . '">
                    Issue Date
                </label>
            </div>
        </div>
    </div>


</div>';
                }
            }
        }
        return $return;

//        $return['length'] = VehicleTypeMaster::where('vehicle_make', $vehiclemake)->where('status',1)->count();
//        echo json_encode($return);
//        exit();
    }

    public function getVehicleTyre(Request $request)
    {

        $return = '';

        $getExistingTyres = VehicleTyreHistorie::where('vehicle_detail_id', $request->item_id)->get();


        $no_tyers = $request->no_tyers;

        if ($request->item_id != '' && count($getExistingTyres) <= $no_tyers) {
            if ($no_tyers > 0) {
                for ($i = 0; $i < $no_tyers; $i++) {
                    $itemarray = isset($getExistingTyres[$i]) ? $getExistingTyres[$i] : '';

//                    dump($itemarray);

                    $return .= '<div class="row tyresLists">
    <div class="col m3">
        <div class="input-group">
            <div class="input-field ">
               <select class="tyre_make"
                     name="tyre[' . $i . '][tyre_make]">';
                    $getTyremake = getTyremake();
                    if (count($getTyremake) > 0) {
                        foreach ($getTyremake AS $getTyremak) {
                            $selectedVal = ($itemarray != "" && $getTyremak->id == $itemarray->tyre_make) ? "selected" : "";
                            $return .= ' <option ' . $selectedVal . '  value="' . $getTyremak->id . '">' . $getTyremak->tyre_make . '</option>';

                        }
                    }

                    $return .= '
             </select>
                <label for="tyre_make_1" >
                     Make
                </label>
            </div>
        </div>
    </div>


    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">';
                    $Value_tyre_life_span = ($itemarray != "" && $itemarray->tyre_life_span != '') ? $itemarray->tyre_life_span : '';
                    $return .= '  <input id="tyre_life_span_' . $i . '"
                       type="text"
                       value="' . $Value_tyre_life_span . '"
                       name="tyre[' . $i . '][tyre_life_span]"
                       class="validate tyre_life_span">
                <label for="tyre_life_span_' . $i . '" class="active">
                     Life Span
                </label>
            </div>
        </div>
    </div>


    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">';
                    $Value_tyre_size = ($itemarray != "" && $itemarray->tyre_size != '') ? $itemarray->tyre_size : '';
                    $return .= '
                <input id="tyre_size_' . $i . '"
                       type="text"
                        value="' . $Value_tyre_size . '"
                       name="tyre[' . $i . '][tyre_size]"
                       class="validate tyre_size">
                <label for="tyre_size_' . $i . '" class="active">
                     Size
                </label>
            </div>
        </div>
    </div>

    <div class="col m3">
        <div class="input-group">
            <div class="input-field ">';
                    $Value_tyre_issue_date = ($itemarray != "" && $itemarray->tyre_issue_date != '') ? date('d-m-Y', strtotime($itemarray->tyre_issue_date)) : '';
                    $return .= '
                <input id="tyre_issue_date_' . $i . '"
                       type="text"
                        value="' . $Value_tyre_issue_date . '"
                       name="tyre[' . $i . '][tyre_issue_date]"
                       class="validate datepicker_ui tyre_issue_date ">
                <label for="tyre_issue_date_' . $i . '" class="active">
                     issue date
                </label>
            </div>
        </div>
    </div>
    <div class="col m2">
        <div class="input-group">
            <div class="input-field ">';
                    $Value_tyre_number = ($itemarray != "" && $itemarray->tyre_number != '') ? $itemarray->tyre_number : '';
                    $return .= '
                    <input id = "tyre_number_' . $i . '"
                       type = "text"
                      value="' . $Value_tyre_number . '"
                       name = "tyre[' . $i . '][tyre_number]"
                       class="validate tyre_number" >
                <label for="tyre_number_' . $i . '" class="active">
                    Number
                </label >
                
            </div >
        </div >
    </div >


</div > ';
                }
            }
        } else {
            if ($no_tyers > 0) {
                for ($i = 0; $i < $no_tyers; $i++) {
                    $return .= '<div class="row tyresLists" >
    <div class="col m3" >
        <div class="input-group" >
            <div class="input-field " >
               <select class="tyre_make"
                     name = "tyre[' . $i . '][tyre_make]" > ';
                    $getTyremake = getTyremake();
                    if (count($getTyremake) > 0) {
                        foreach ($getTyremake AS $getTyremak) {
                            $return .= ' <option value = "' . $getTyremak->id . '" > ' . $getTyremak->tyre_make . '</option > ';
                        }
                    }

                    $return .= '
             </select >
                <label for="tyre_make_1" >
                    Make
                </label >
            </div >
        </div >
    </div >


    <div class="col m2" >
        <div class="input-group" >
            <div class="input-field " >
                <input id = "tyre_life_span_' . $i . '"
                       type = "text"
                       value = ""
                       name = "tyre[' . $i . '][tyre_life_span]"
                       class="validate tyre_life_span" >
                <label for="tyre_life_span_' . $i . '" >
                    Life Span
                    </label >
            </div >
        </div >
    </div >


    <div class="col m2" >
        <div class="input-group" >
            <div class="input-field " >
                <input id = "tyre_size_' . $i . '"
                       type = "text"
                       value = ""
                       name = "tyre[' . $i . '][tyre_size]"
                       class="validate tyre_size" >
                <label for="tyre_size_' . $i . '" >
                    Size
                </label >
            </div >
        </div >
    </div >

    <div class="col m3" >
        <div class="input-group" >
            <div class="input-field " >
                <input id = "tyre_issue_date_' . $i . '"
                       type = "text"
                       value = ""
                       name = "tyre[' . $i . '][tyre_issue_date]"
                       class="validate datepicker_ui tyre_issue_date " >
                <label for="tyre_issue_date_' . $i . '" >
                    issue date
                    </label >
            </div >
        </div >
    </div >
    <div class="col m2" >
        <div class="input-group" >
            <div class="input-field " >
                <input id = "tyre_number_' . $i . '"
                       type = "text"
                       value = ""
                       name = "tyre[' . $i . '][tyre_number]"
                       class="validate tyre_number" >
                <label for="tyre_number_' . $i . '" >
                    Number
                </label >
                
            </div >
        </div >
    </div >


</div > ';
                }
            }
        }


        return $return;

//        $return['length'] = VehicleTypeMaster::where('vehicle_make', $vehiclemake)->where('status',1)->count();
//        echo json_encode($return);
//        exit();
    }

}
