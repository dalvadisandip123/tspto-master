<?php

namespace App\Http\Controllers\Workshop;

use App\Http\Controllers\Controller;

use App\Models\Masters\Mt\VehicleMakeMaster;
use App\Models\Masters\Mt\VehicleTypeMaster;
use App\Models\Masters\Workshop\PartNameMaster;
use App\Models\Masters\Workshop\PartCategoryMaster;
use App\Models\Masters\Workshop\FirmNameMaster;
use App\Models\Workshop\PTOStockLedger;
use App\Models\Workshop\SpareParts;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class PTOStockLedgerController extends Controller
{

    

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    { 
        return view('workshop.pto_stock_ledger.index');
    }

    public function list(Request $request)
    {

        $columns = array( 
            0 => 'id',
            1 => 'category',
            2 => 'd1_no',
            3 => 'date',
            4 => 'type',
            5 => 'firm_name',
            6 => 'po_no',
            7 => 'po_date',
            8 => 'invoice_no',
            9 => 'invoice_date'
        );
  
        $totalData = PTOStockLedger::count();
        $totalFiltered = $totalData; 
        $limit = $request->request->get('length');
        $start = $request->request->get('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(!empty($request->input('search.value')))
        {            
            $search = $request->input('search.value'); 

            $posts =  PTOStockLedger::Where('id', 'LIKE',"%{$search}%")
                    ->orWhere('d1_no', 'LIKE',"%{$search}%")
                    ->orWhere('stock_date', 'LIKE',"%{$search}%")
                    ->orWhere('firm_name', 'LIKE',"%{$search}%")
                    ->orWhere('po_no', 'LIKE',"%{$search}%")
                    ->orWhere('po_date', 'LIKE',"%{$search}%")
                    ->orWhere('invoice_no', 'LIKE',"%{$search}%")
                    ->orWhere('invoice_date', 'LIKE',"%{$search}%")
                    ->orwhereHas('hasOneFirm', function($q) use($search) {
                        $q->Where('name', 'LIKE',"%{$search}%"); 
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

            $totalFiltered = PTOStockLedger::Where('id', 'LIKE',"%{$search}%")
                    ->orWhere('d1_no', 'LIKE',"%{$search}%")
                    ->orWhere('stock_date', 'LIKE',"%{$search}%")
                    ->orWhere('firm_name', 'LIKE',"%{$search}%")
                    ->orWhere('po_no', 'LIKE',"%{$search}%")
                    ->orWhere('po_date', 'LIKE',"%{$search}%")
                    ->orWhere('invoice_date', 'LIKE',"%{$search}%")
                    ->orwhereHas('hasOneFirm', function($q) use($search) {
                        $q->Where('name', 'LIKE',"%{$search}%"); 
                    })
                    ->count();
        }   
        else
        {            
            $posts = PTOStockLedger::with('hasOneFirm')->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        $data = array();
        $data1=array();

                // $post = new PTOStockLedger;
        if($posts)
        {
            foreach ($posts as $post) 
            { 
                        // echo "<pre>";print_r($post);exit;
                $firmObj = $post->hasOneFirm ? $post->hasOneFirm : '';
                $data['id'] = $post->id;
                $data['category'] = $post->category == 1 ? 'General Ledger' : 'Reserve Ledger';
                $data['d1_no'] = $post->d1_no ? $post->d1_no : '';
                $data['date'] = $post->stock_date;
                $data['type'] = $post->type == 1 ? 'Date' : 'R V No.' ;
                $data['firm_name'] = $firmObj ? $firmObj->name : '';
                $data['po_no'] = $post->po_no ? $post->po_no : '';
                $data['po_date'] = $post->po_date ? $post->po_date : '';
                $data['invoice_no'] = $post->invoice_no ? $post->invoice_no : '';
                $data['invoice_date'] = $post->invoice_date ? $post->invoice_date : '';

                $data['action'] = "<div style='display: flex;'><a style='float:left;' href=".route('workshop.pto_stock_ledger.form',['id'=>$post->id])." title='EDIT' class='btn btn-primary' >Edit</a>
                <form style='float:left;margin-left:6px;' method='POST' action=".route('workshop.pto_stock_ledger.delete',['id'=>$post->id]).">";
               
                $data['action'] .=  csrf_field();
                $data['action'] .= method_field("DELETE");
                $data['action'] .=  "<button class='btn btn-danger'>Delete</button></form></div>";

                $data1[]=$data;
            }
        }
        $json_data = array(
            "draw"            => intval($request->request->get('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data1   
        );
        echo json_encode($json_data); 
    }
    
    public function form(Request $request)
    {   
        $PTOStockLedger = $request->id ? PTOStockLedger::find($request->id) : new PTOStockLedger ;  
        $makes = VehicleMakeMaster::where('status','1')->get();
        // $models = VehicleTypeMaster::where('status','1')->get();
        $partCategorys = PartCategoryMaster::where('status','1')->get();
        $partNames = PartNameMaster::where('status','1')->get();
        $firms = FirmNameMaster::where('status','1')->get();
        $spareParts = $request->id ? SpareParts::where('pto_stock_ledger_id',$request->id)->where('type',1)->get() : [];
        $spareCommons = $request->id ? SpareParts::where('pto_stock_ledger_id',$request->id)->where('type',2)->get() : [];
        $sparePartModel = $spareCommonModel = [];
        if(count($spareParts)){
            foreach($spareParts as $sparePart){
                $sparePartModel[] = VehicleTypeMaster::where('vehicle_make_masters_id',$sparePart->model_id)->get();
            }
        }
        if(count($spareCommons)){
            foreach($spareCommons as $spareCommon){
                $spareCommonModel[] = VehicleTypeMaster::where('vehicle_make_masters_id',$spareCommon->model_id)->get();
            }
        }
        // echo "<pre>"; print_r($spareCommonModel);exit;

        return view('workshop.pto_stock_ledger.create',['PTOStockLedger'=>$PTOStockLedger,'makes'=>$makes,'partCategorys'=>$partCategorys,'partNames'=>$partNames,'firms'=>$firms,'spareParts'=>$spareParts,'spareCommons'=>$spareCommons,'spareCommonModel'=>$spareCommonModel,'sparePartModel'=>$sparePartModel]);
    }


    public function store(Request $request)
    {
        // echo "<pre>";print_r($request->all());
        $validator = Validator::make($request->all(), [
            'category' => 'required|max:255',
            'd1_no' => 'required|max:255',
            'firm_name' => 'required|max:255',
            'po_name' => 'required|max:255',
            'invoice_no' => 'required|max:255'
            
        ]);

        if($validator->fails())
        {
            return redirect()->route('workshop.pto_stock_ledger.form',['id'=>$request->id])->withErrors($validator)->withInput();
        } 
        $PTOStockLedger = $request->id ? PTOStockLedger::findOrFail($request->id) : new PTOStockLedger;
        // $PTOStockLedger->name = $request->name;
    
        $PTOStockLedger->category = $request->category;
        $PTOStockLedger->d1_no = $request->d1_no;
        $PTOStockLedger->stock_date = $request->stock_date;
        $PTOStockLedger->type = $request->type;
        $PTOStockLedger->firm_name = $request->firm_name;
        $PTOStockLedger->po_no = $request->po_name;
        $PTOStockLedger->po_date = $request->ponamedate;
        $PTOStockLedger->invoice_no = $request->invoice_no;
        $PTOStockLedger->invoice_date = $request->invoice_date;

        $PTOStockLedger->save();
        $makeCommons = $request->make_common;
        $modelCommon = $request->model_common;
        $part_categoryCommon = $request->part_category_common;
        $part_nameCommon = $request->part_name_common;
        $quantityCommon = $request->quantity_common;
        $costCommon = $request->cost_common;
        $locationCommon = $request->location_common;
        $commonId = $request->commonId;
        foreach($makeCommons as $key=>$makeCommon){
            $sparePart = isset($commonId[$key]) ?  SpareParts::find($commonId[$key]) : new SpareParts;
            //  ? $modelCommon[$key] : '';
            
            $sparePart->pto_stock_ledger_id = $PTOStockLedger->id;
            $sparePart->pto_issue_id = 0;
            $sparePart->type = 2;
            $sparePart->make_id = $makeCommon;
            $sparePart->model_id = isset($modelCommon[$key]) ? $modelCommon[$key] : '' ;
            $sparePart->part_category_id = isset($part_categoryCommon[$key]) ? $part_categoryCommon[$key] : '' ;
            $sparePart->part_name_id = isset($part_nameCommon[$key]) ? $part_nameCommon[$key] : '' ;
            $sparePart->quantity = isset($quantityCommon[$key]) ? $quantityCommon[$key] : '' ;
            $sparePart->cost = isset($costCommon[$key]) ? $costCommon[$key] : '' ;
            $sparePart->rock_location = isset($locationCommon[$key]) ? $locationCommon[$key] : '' ;
            $sparePart->save();
        }

        $makeSpares = $request->make_spare;
        $modelSpare = $request->model_spare;
        $part_categorySpare = $request->part_category_spare;
        $part_nameSpare = $request->part_name_spare;
        $quantitySpare = $request->quantity_spare;
        $costSpare = $request->cost_spare;
        $locationSpare = $request->location_spare;
        $spareId = $request->spareId;
        foreach($makeSpares as $key=>$makeSpare){
            // new SpareParts;
            $sparePart = isset($spareId[$key]) ?  SpareParts::find($spareId[$key]) : new SpareParts;
            $sparePart->pto_stock_ledger_id = $PTOStockLedger->id;
            $sparePart->pto_issue_id = 0;
            $sparePart->type = 1;
            $sparePart->make_id = $makeSpare;
            $sparePart->model_id = isset($modelSpare[$key]) ? $modelSpare[$key] : '' ;
            $sparePart->part_category_id = isset($part_categorySpare[$key]) ? $part_categorySpare[$key] : '' ;
            $sparePart->part_name_id = isset($part_nameSpare[$key]) ? $part_nameSpare[$key] : '' ;
            $sparePart->quantity = isset($quantitySpare[$key]) ? $quantitySpare[$key] : '' ;
            $sparePart->cost = isset($costSpare[$key]) ? $costSpare[$key] : '' ;
            $sparePart->rock_location = isset($locationSpare[$key]) ? $locationSpare[$key] : '' ;
            $sparePart->save();
        }
        $message = $request->id ? "PTO Stock Ledger Updated Successfully" :"New PTO Stock Ledger Created Successfully";
        return redirect()->route('workshop.pto_stock_ledger.index')->with('message', $message );
    }

    public function destroy($id)
    {   
        $PTOStockLedger = PTOStockLedger::findOrFail($id);
        $spareParts = SpareParts::where('pto_stock_ledger_id',$id)->get();
        foreach($spareParts as $sparePart){
            $sparePart->delete();
        }
        $PTOStockLedger->delete();
        return redirect()->route('workshop.pto_stock_ledger.index')->with('message',"PTO Stock Ledger Deleted Successfully");
      
    }
    public function deleteSpareParts(Request $request){ 
        $spareParts = SpareParts::find($request->id);
        $spareParts->delete();
    }

    public function loadModelMaster(Request $request){ 
        $vehicleMasters = VehicleTypeMaster::where('vehicle_make_masters_id',$request->id)->get();
        $vehicleHtml = '<option value="">Select Model</option>';
        foreach($vehicleMasters as $vehicleMaster){
            $vehicleHtml .= "<option value='".$vehicleMaster->id."'>".$vehicleMaster->vehicle_type."</option>";
        }
        return $vehicleHtml;
    }
}