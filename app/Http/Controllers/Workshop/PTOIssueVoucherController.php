<?php

namespace App\Http\Controllers\Workshop;

use App\Http\Controllers\Controller;

use App\Models\Masters\Mt\VehicleMakeMaster;
use App\Models\Masters\Mt\VehicleTypeMaster;
use App\Models\Masters\Workshop\PartNameMaster;
use App\Models\Masters\Workshop\PartCategoryMaster;
use App\Models\Masters\Workshop\FirmNameMaster;
use App\Models\Workshop\PTOIssue;

use App\Models\Workshop\SpareParts;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use URL;

class PTOIssueVoucherController extends Controller
{

    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    { 
        return view('workshop.pto_issue_voucher.index');
    }
    public function list(Request $request)
    {
        
        $columns = array( 
            'id',
            'iv_district',
            'unit_name',
            'issue_date',
            'rc_no',
            'rc_date',
            'image'
        );
  
        $totalData = PTOIssue::count();
        $totalFiltered = $totalData; 
        $limit = $request->request->get('length');
        $start = $request->request->get('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $urlnew = url(''); 
        $newUrl = str_replace('index.php', '', $urlnew);    
        if(!empty($request->input('search.value')))
        {            
            $search = $request->input('search.value'); 

            $posts =  PTOIssue::Where('id', 'LIKE',"%{$search}%")
                    ->orWhere('iv_district', 'LIKE',"%{$search}%")
                    ->orWhere('unit_name', 'LIKE',"%{$search}%")
                    ->orWhere('issue_date', 'LIKE',"%{$search}%")
                    ->orWhere('rc_no', 'LIKE',"%{$search}%")
                    ->orWhere('rc_date', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

            $totalFiltered = PTOIssue::Where('id', 'LIKE',"%{$search}%")
                    ->orWhere('iv_district', 'LIKE',"%{$search}%")
                    ->orWhere('unit_name', 'LIKE',"%{$search}%")
                    ->orWhere('issue_date', 'LIKE',"%{$search}%")
                    ->orWhere('rc_no', 'LIKE',"%{$search}%")
                    ->orWhere('rc_date', 'LIKE',"%{$search}%")
                    ->count();
        }   
        else
        {            
            $posts = PTOIssue::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        $data = array();
        $data1=array();

                // $post = new PTOStockLedger;
                // echo "<pre>";print_r($post);exit;
        if($posts)
        {
            foreach ($posts as $post) 
            { 

                
                $data['id'] = $post->id;
                $data['iv_district'] = $post->iv_district ? $post->iv_district : '';
                $data['unit_name'] = $post->unit_name ? $post->unit_name : '';
                $data['issue_date'] = $post->issue_date ? $post->issue_date : '';
                $data['rc_no'] = $post->rc_no ? $post->rc_no : '' ;
                $data['image'] = '';
                if($post->image != ""){
                    $data['image'] = '<img src="'.$newUrl.'upload/pto_issue/'.$post->image.'" height="100px" width="100px">';
                    
                }
                $data['rc_date'] = $post->rc_date ? $post->rc_date : '';

                $data['action'] = "<div style='display: flex;'><a style='float:left;' href=".route('workshop.pto_issue_voucher.form',['id'=>$post->id])." title='EDIT' class='btn btn-primary' >Edit</a>
                <form style='float:left;margin-left:6px;' method='POST' action=".route('workshop.pto_issue_voucher.delete',['id'=>$post->id]).">";
               
                $data['action'] .=  csrf_field();
                $data['action'] .= method_field("DELETE");
                $data['action'] .=  "<button class='btn btn-danger'>Delete</button></form><div>";

                $data1[]=$data;
            }
        }
        $json_data = array(
            "draw"            => intval($request->request->get('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data1   
        );
        echo json_encode($json_data); 
    }

    public function form(Request $request)
    {   
        $PTOIssue = $request->id ? PTOIssue::find($request->id) : new PTOIssue ;  
        $makes = VehicleMakeMaster::where('status','1')->get();
        // $models = VehicleTypeMaster::where('status','1')->get();
        $partCategorys = PartCategoryMaster::where('status','1')->get();
        $partNames = PartNameMaster::where('status','1')->get();
        $firms = FirmNameMaster::where('status','1')->get();
        $spareParts = $request->id ? SpareParts::where('pto_stock_ledger_id',$request->id)->where('type',1)->get() : [];
        $spareCommons = $request->id ? SpareParts::where('pto_stock_ledger_id',$request->id)->where('type',2)->get() : [];
        $sparePartModel = $spareCommonModel = [];
        if(count($spareParts)){
            foreach($spareParts as $sparePart){
                $sparePartModel[] = VehicleTypeMaster::where('vehicle_make_masters_id',$sparePart->model_id)->get();
            }
        }
        if(count($spareCommons)){
            foreach($spareCommons as $spareCommon){
                $spareCommonModel[] = VehicleTypeMaster::where('vehicle_make_masters_id',$spareCommon->model_id)->get();
            }
        }
        // vehicle_make  vehicle_type
        return view('workshop.pto_issue_voucher.create',['PTOIssue'=>$PTOIssue,'makes'=>$makes,'models'=>$models,'partCategorys'=>$partCategorys,'partNames'=>$partNames,'firms'=>$firms,'spareParts'=>$spareParts,'spareCommons'=>$spareCommons,'spareCommonModel'=>$spareCommonModel,'sparePartModel'=>$sparePartModel]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'name' => 'required|max:255'
        ]);

        if($validator->fails())
        {
            return redirect()->route('workshop.pto_issue_voucher.form',['id'=>$request->id])->withErrors($validator)->withInput();
        } 
        $PTOIssue = $request->id ? PTOIssue::findOrFail($request->id) : new PTOIssue;
        $PTOIssue->iv_district =$request->ivno; 
        $PTOIssue->unit_name =$request->unitname; 
        $PTOIssue->issue_date =$request->issuedate; 
        $PTOIssue->rc_no =$request->rcno; 
        $PTOIssue->rc_date =$request->rcdate; 
        if ($files = $request->file('image')) 
        {
            $destinationPath = public_path('upload/pto_issue/'); // upload path
            $PTOImage = time() . "." . $files->getClientOriginalName();
            $files->move($destinationPath, $PTOImage);
            $oldFile = $PTOIssue->image ? public_path('upload/pto_issue/'.$PTOIssue->image) : '';
            $oldFile != '' ? unlink($oldFile) : '';
            $PTOIssue->image = $PTOImage;    
        }
        $PTOIssue->save();
        $makeCommons = $request->make_common;
        $modelCommon = $request->model_common;
        $part_categoryCommon = $request->part_category_common;
        $part_nameCommon = $request->part_name_common;
        $quantityCommon = $request->quantity_common;
        $costCommon = $request->cost_common;
        $locationCommon = $request->location_common;
        $commonId = $request->commonId;
        foreach($makeCommons as $key=>$makeCommon){
            $sparePart = isset($commonId[$key]) ?  SpareParts::find($commonId[$key]) : new SpareParts;
            //  ? $modelCommon[$key] : '';
            
            $sparePart->pto_stock_ledger_id = 0;
            $sparePart->pto_issue_id = $PTOIssue->id;
            $sparePart->type = 2;
            $sparePart->make_id = $makeCommon;
            $sparePart->model_id = isset($modelCommon[$key]) ? $modelCommon[$key] : '' ;
            $sparePart->part_category_id = isset($part_categoryCommon[$key]) ? $part_categoryCommon[$key] : '' ;
            $sparePart->part_name_id = isset($part_nameCommon[$key]) ? $part_nameCommon[$key] : '' ;
            $sparePart->quantity = isset($quantityCommon[$key]) ? $quantityCommon[$key] : '' ;
            $sparePart->cost = isset($costCommon[$key]) ? $costCommon[$key] : '' ;
            $sparePart->rock_location = isset($locationCommon[$key]) ? $locationCommon[$key] : '' ;
            $sparePart->save();
        }

        $makeSpares = $request->make_spare;
        $modelSpare = $request->model_spare;
        $part_categorySpare = $request->part_category_spare;
        $part_nameSpare = $request->part_name_spare;
        $quantitySpare = $request->quantity_spare;
        $costSpare = $request->cost_spare;
        $locationSpare = $request->location_spare;
        $spareId = $request->spareId;
        foreach($makeSpares as $key=>$makeSpare){
            // new SpareParts;
            $sparePart = isset($spareId[$key]) ?  SpareParts::find($spareId[$key]) : new SpareParts;
            $sparePart->pto_stock_ledger_id = 0;
            $sparePart->pto_issue_id = $PTOIssue->id;
            $sparePart->type = 1;
            $sparePart->make_id = $makeSpare;
            $sparePart->model_id = isset($modelSpare[$key]) ? $modelSpare[$key] : '' ;
            $sparePart->part_category_id = isset($part_categorySpare[$key]) ? $part_categorySpare[$key] : '' ;
            $sparePart->part_name_id = isset($part_nameSpare[$key]) ? $part_nameSpare[$key] : '' ;
            $sparePart->quantity = isset($quantitySpare[$key]) ? $quantitySpare[$key] : '' ;
            $sparePart->cost = isset($costSpare[$key]) ? $costSpare[$key] : '' ;
            $sparePart->rock_location = isset($locationSpare[$key]) ? $locationSpare[$key] : '' ;
            $sparePart->save();
        }
        $message = $request->id ? "PTO Issue Voucher Updated Successfully" :"New PTO Issue Voucher Created Successfully";
        return redirect()->route('workshop.pto_issue_voucher.index')->with('message', $message );
    }
    
    
    public function destroy($id)
    {   
        $PTOIssue = PTOIssue::findOrFail($id);
        $oldFile = $PTOIssue->image ? public_path('upload/pto_issue/'.$PTOIssue->image) : '';
        $oldFile != '' ? unlink($oldFile) : '';
        $spareParts = SpareParts::where('pto_issue_id',$id)->get();
        foreach($spareParts as $sparePart){
            $sparePart->delete();
        }
        $PTOIssue->delete();
        return redirect()->route('workshop.pto_issue_voucher.index')->with('message',"PTO Issue Voucher Deleted Successfully");
    }
}