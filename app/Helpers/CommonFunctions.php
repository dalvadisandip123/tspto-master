<?php

use App\Models\Categories;
use App\Models\Page;
use App\Models\PageViews;
use App\Models\Posts;

use App\User;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;


define('ADMIN_EMAIL', 'info@idconnect.com');


function form_flash_message($name = null)
{


    if ($name == null) {
        $name = "flash_message";
    }

    if (\Illuminate\Support\Facades\Session::has($name)) {
        ?>

        <div class="card-alert card green lighten-5">
            <div class="card-content green-text">
                <p>
                    <?= Session::get($name) ?>
                </p>
                <!--                <p>SUCCESS : The page has been added.</p>-->
            </div>
            <button type="button" class="close green-text" data-dismiss="alert"
                    aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>


        <!--        <br/>-->
        <!--        <div class="alert alert-success alert-dismissable">-->
        <!--            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
        <!--            <strong>--><?//= Session::get($name) ?><!--</strong>-->
        <!--        </div>-->
        <?php
    }

}

function jobCardRepairsProblams($status = '')
{
    $array = array(
        '1' => 'Mechanical Problems',
        '2' => 'Electrical Problems ',
        '3' => 'Wheels & Tyres Problems',
        '4' => 'Chassis & Body Problems',
        '5' => 'Accessories',
        '6' => 'Lubricants Problems',
        '7' => 'Miscellenous',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}

function attendanceTypes($status = '')
{
    $array = array(
        'P' => 'Present',
        'A' => 'Absent',
        'D' => 'D',
        'S' => 'SICK',
        'C' => 'C/L',
        'E' => 'E/L',
        'DO' => 'D/OFF',
        'ND' => 'N/DUTY',
        'H' => 'H/DUTY',
        'O' => 'OTHER',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}

function fullITRepairsProblams($status = '')
{
    $array = array(
        '1' => 'Engine',
        '2' => 'Transmission',
        '3' => 'Brakes / Pedal',
        '4' => 'Steering',
        '5' => 'Chassis',
        '6' => 'Electricals',
        '7' => 'BODY(Interior)',
        '8' => 'BODY(Esterior)',
        '9' => 'Tyres',
        '10' => 'Batteries',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}

function budgetType($status = '')
{


    $array = array(
        '1' => 'Engine',
        '2' => 'Transmission',
        '3' => 'Brakes / Pedal',
        '4' => 'Steering',
        '5' => 'Chassis',
        '6' => 'Electricals',
        '7' => 'BODY(Interior)',
        '8' => 'BODY(Esterior)',
        '9' => 'Tyres',
        '10' => 'Batteries',
    );

    return $array;
}

function financialYears($status = '')
{

    $yearsLists = range(2007, date('Y'));

    $array = array();

    foreach ($yearsLists AS $yearsList) {

        $year = $yearsList . '-' . ($yearsList + 1);

        $array[] = $year;
    }
    $array = array_reverse($array);

    return $array;

//    if ($status === '') {
//        return $array;
//    } else {
//        if (isset($array[$status])) {
//            return $array[$status];
//        } else {
//            return '';
//        }
//    }

}


function gerUserById($id)
{
    return User::where('id', $id)->first();
}

function status_list($status = '')
{
    $array = array(
        '1' => 'Active',
        '2' => 'Block',
        '3' => 'Draft',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}
function vehicle_status_list($status = '')
{
    $array = array(
        '1' => 'Open',
        '2' => 'Close',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}

function usertypes($type = '')
{
    $array = array(
        '1' => 'Officer',
        '2' => 'Non Officer'
    );

    if ($type === '') {
        return $array;
    } else {
        if (isset($array[$type])) {
            return $array[$type];
        } else {
            return '';
        }
    }
}


function bloodGroups($status = '')
{
    $array = array(
        'A+',
        'A-',
        'A1+',
        'A1-',
        'A1B+',
        'A1B',
        'A2+',
        'A2',
        'A2B+',
        'A2B',
        'AB+',
        'AB-',
        'B+',
        'B-',
        'O+',
        'O-',
        'Bombay Blood',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}

function condemnation_master_condition($status = '')
{
    $array = array(
        'AND' => 'AND',
        'OR' => 'OR',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}

function activity_list($status = '')
{
    $array = array(
        '1' => 'Open',
        '2' => 'Close',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}


function userRoles($status = '')
{
    $array = array(
//        '404' => 'admin',
//        '0' => 'Others',
        '1' => 'Old & Current Students',
        '2' => 'School/college Teachers & Management',
        '3' => 'Volunteers',
        '4' => 'Sponsors',
        '5' => 'Employees',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }


}


function imageNotAvalableUrl()
{
    return 'https://dummyimage.com/600x400/FFF/999999&text=image+not+available';
}


function form_validation_error($errors, $name)
{

    if ($errors->has($name)) {
        ?>

        <div id="name-error" class="error">
            <?= $errors->first($name) ?>
        </div>
        <?php
    }


}


function imputOldValue($name, $oldValue = null)
{
    return ($oldValue == null) ? old($name) : $oldValue;
}

function getRandomPassword()
{
    return "password";
}

function getLoginRoles($role = '')
{
    $array = array(
        '1' => 'Super Admin',
        '2' => 'Admin',
        '3' => 'Ibn Incharge',
        '4' => 'Unit head',
        '5' => 'Pto',
    );

    if ($role === '') {
        return $array;
    } else {
        if (isset($array[$role])) {
            return $array[$role];
        } else {
            return '';
        }
    }
}


