<?php

use App\Models\Masters\Mt\BatteriesMaster;
use App\Models\Masters\Mt\VehicleTyresMaster;

function getVehicleMakes()
{
    $records = \App\Models\Masters\Mt\VehicleMakeMaster::where('status', 1)->orderBy('id', 'asc')->pluck('vehicle_make', 'id');
    return $records;
}

function getVehicleType()
{
    $records = \App\Models\Masters\Mt\VehicleTypeMaster::where('status', 1)->orderBy('id', 'asc')->pluck('vehicle_make', 'id');
    return $records;
}

function office_dept()
{
    $records = \App\Models\Masters\Mt\OfficeOfMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function vehicleCategory()
{
    $records = \App\Models\Masters\Mt\VehicleCategoryMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function office_designation()
{
    $records = \App\Models\Masters\Mt\OfficerDesignationMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getEmpRoles()
{
    $records = \App\Models\Masters\Mt\EmployeeRoleMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getOutlets()
{
    $records = \App\Models\Masters\Pol\OutletsMaster::where('status', 1)->orderBy('id', 'asc')->pluck('name', 'id');
    return $records;
}

function getEmpRank()
{
    $records = \App\Models\Masters\Mt\EmployeeRankMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getUnits()
{
    $records = \App\Models\Masters\Mt\UnitsMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getOfficers()
{
    $records = \App\Models\Masters\Mt\OfficerNameMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getVehicleUseages()
{
    $records = \App\Models\Masters\Mt\VehicleUsageMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getEmployees()
{
    $records = \App\Models\MT\Employee::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getUsers($type = '')
{
    if ($type) {
//1=office 2=non officer
        $records = \App\User::where('user_type', $type)->orderBy('id', 'asc')->pluck('name', 'id');
    } else {

        $records = \App\User::orderBy('id', 'asc')->pluck('name', 'id');
    }

    return $records;
}

function getDrivers()
{

    $records = \App\Models\MT\Employee::where('employee_role_masters_id', 6)->orderBy('id', 'asc')->pluck('name', 'id');

    return $records;
}

function getFuelTypes()
{
    $records = \App\Models\Masters\Pol\FuelTypeMaster::where('status', 1)->orderBy('id', 'asc')->pluck('name', 'id');
    return $records;
}

function getVehicleDetails()
{
    $records = \App\Models\MT\VehicleDetail::where('status', 1)->orderBy('id', 'asc')->pluck('vehicle_no', 'id');
    return $records;
}

function getTyremake()
{
    $records = VehicleTyresMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getBatteriesmake()
{
    $records = BatteriesMaster::where('status', 1)->orderBy('id', 'asc')->get();
    return $records;
}

function getServiceScheduleOilsBasedOnID($id)
{
    $records = \App\Models\Masters\Pol\ServiceScheduleOil::with('getOilType', 'getGrade')->where('service_schedule_masters_id', $id)->get()->toArray();
//    echo "<pre>";
//    print_r($records);
//    exit();


    return $records;
}

function getServiceScheduleFiltersBasedOnID($id)
{
    $records = \App\Models\Masters\Pol\ServiceScheduleFilter::with('getFilter')->where('service_schedule_masters_id', $id)->get()->toArray();

//        echo "<pre>";
//    print_r($records);
//    exit();
    return $records;
}

function getArrayFromKeys($key, $array, $key_name)
{
    foreach ($array as $arraykey => $value) {
        if ($value[$key_name] == $key)
            return $value;

    }
}

