<?php

namespace App\Models\Workshop;

use App\Models\Masters\Mt\BatteriesMaster;
use Illuminate\Database\Eloquent\Model;

class PTOStockLedger extends Model
{
    protected $table = 'PTO_stock_ledger';
    
    protected $fillable = [
        'category',
        'd1_no',
        'stock_date',
        'type',
        'firm_name',
        'po_no',
        'po_date',
        'invoice_no',
        'invoice_date'
    ];
   protected $primaryKey = 'id';
    public function hasOneFirm()
    {
          return $this->hasOne('App\Models\Masters\Workshop\FirmNameMaster', 'id', 'firm_name');
    }
}
