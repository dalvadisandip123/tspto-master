<?php

namespace App\Models\Workshop;

use App\Models\Masters\Mt\BatteriesMaster;
use Illuminate\Database\Eloquent\Model;

class SpareParts extends Model
{
    protected $table = 'spare_parts';
    
    protected $fillable = [
        'pto_stock_ledger_id',
        'pto_issue_id',
        'type',
        'make_id',
        'model_id',
        'part_category_id',
        'part_name_id',
        'quantity',
        'cost',
        'rock_location',
        'created_at',
        'updated_at'
    ];
   protected $primaryKey = 'id';
}
