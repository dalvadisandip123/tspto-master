<?php

namespace App\Models\Workshop;

use App\Models\Masters\Mt\BatteriesMaster;
use Illuminate\Database\Eloquent\Model;

class PTOIssue extends Model
{
    protected $table = 'PTO_issue';
    
    protected $fillable = [
        'iv_district',
        'unit_name',
        'issue_date',
        'rc_no',
        'rc_date',
        'image',
        'created_at',
        'updated_at'
    ];
   protected $primaryKey = 'id';
}
