<?php

namespace App\Models\MT;

use Illuminate\Database\Eloquent\Model;

class EmpReporting extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'employees_id',
        'report_date',
        'relieve_date',
        'purpose',
        'status'
    ];

    public function getEmployee()
    {
        return $this->belongsTo(Employee::Class, 'employees_id');
    }
}
