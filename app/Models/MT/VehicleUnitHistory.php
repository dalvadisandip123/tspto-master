<?php

namespace App\Models\MT;

use Illuminate\Database\Eloquent\Model;

class VehicleUnitHistory extends Model
{

    protected $table = 'vehicle_unit_history';


    protected $fillable = [
        'vehicle_details_id',
        'allotment_order_number',
        'unit_id',
        'from_km',
        'to_km',
        'issue_date',
        'end_date',
        'remarks',
        'receiver_user_id',
        'usage_pupose_id',
        'status',
    ];

    public function getVehicleDetails()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_details_id');
    }

    public function getUnitInfo()
    {
        return $this->belongsTo('App\Models\Masters\Mt\UnitsMaster', 'unit_id');
    }
}
