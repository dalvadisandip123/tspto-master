<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\UnitsMaster;
use Illuminate\Database\Eloquent\Model;

class IssueVoucher extends Model
{
    protected $fillable = [
        'vehicle_no',
        'vehicle_issued_to',
        'issue_voucher_no',
        'issue_voucher_type',
        'issue_voucher_year',
        'issue_voucher_date',
        'co_allotment_no',
        'co_allotment_no2',
        'co_allotment_year',
        'co_allotment_date',
        'vehicle_allotment_purpose',
        'vehicle_charged_off_today',
        'recevier_name',
        'recevier_designation',
        'unit_id',
        'status'
    ];
    public function getVehicle()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_no', 'vehicle_no');
    }

    public function getUnit()
    {
        return $this->belongsTo(UnitsMaster::Class, 'unit_id');
    }

    public static function add($request)
    {


//        $request->validate(
//            [
//                'name' => 'required|unique:employees',
//                'employee_id' => 'required|unique:employees',
//            ],
//            [
//                'name.required' => 'Enter name',
//                'name.unique' => 'This name  Exist',
//                'employee_id.required' => 'Enter employee id',
//                'employee_id.unique' => 'This employee id  Exist',
//
//            ]
//        );

        $requestData = $request->post();


        if (isset($requestData['issue_voucher_date'])) {
            $requestData['issue_voucher_date'] = date('Y-m-d H:i:s', strtotime($requestData['issue_voucher_date']));
        }

        if (isset($requestData['co_allotment_date'])) {
            $requestData['co_allotment_date'] = date('Y-m-d H:i:s', strtotime($requestData['co_allotment_date']));
        }
        if (isset($requestData['vehicle_charged_off_today'])) {
            $requestData['vehicle_charged_off_today'] = date('Y-m-d H:i:s', strtotime($requestData['vehicle_charged_off_today']));
        }



        if (count($requestData['vehicle_ids']) > 0) {
            foreach ($requestData['vehicle_ids'] AS $vehicle_id) {

                $requestData['vehicle_no'] = $vehicle_id;

                $createData = self::create($requestData);

            }
        }

//        dd($requestData);




        return $createData;

    }

    public static function edit($request)
    {

        $requestData = $request->post();

//        $request->validate(
//            [
//                'name' => 'required|unique:employees,name,' . $requestData['id'] . ',id',
//                'employee_id' => 'required|unique:employees,employee_id,' . $requestData['id'] . ',id',
//            ],
//            [
//                'name.required' => 'Enter name',
//                'name.unique' => 'This name  Exist',
//                'employee_id.required' => 'Enter employee id',
//                'employee_id.unique' => 'This employee id  Exist',
//
//            ]
//        );


//        dd($requestData);

        $getData = self::where('id', $requestData['id'])->first();


        if (isset($requestData['issue_voucher_date'])) {
            $requestData['issue_voucher_date'] = date('Y-m-d H:i:s', strtotime($requestData['issue_voucher_date']));
        }

        if (isset($requestData['co_allotment_date'])) {
            $requestData['co_allotment_date'] = date('Y-m-d H:i:s', strtotime($requestData['co_allotment_date']));
        }
        if (isset($requestData['vehicle_charged_off_today'])) {
            $requestData['vehicle_charged_off_today'] = date('Y-m-d H:i:s', strtotime($requestData['vehicle_charged_off_today']));
        }


        $createData = $getData->update($requestData);


        return $createData;

    }
}
