<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\EmployeeRankMaster;
use App\Models\Masters\Mt\EmployeeRoleMaster;
use App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\User;

class Employee extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'employee_id',
        'user_id',
        'name',
        'employee_role_masters_id',
        'employee_rank_masters_id',
        'number',
        'photo',
        'driving_licence_number',
        'licence_type',
        'licence_validity',
        'units_masters_id',
        'parent_unit',
        'working_section',
        'officer_name_masters_id',
        'date_appointment',
        'duty_type',
        'retirement_date',
        'languages_known',
        'mother_tounge',
        'native_place',
        'blood_group',
        'family_mobile_number',
        'arogya_bhadratha_number',
        'residence',
        'rewards_or_pathakams',
        'punishments',
        'accident_history',
        'status'
    ];

    public function getRole()
    {
        return $this->belongsTo(EmployeeRoleMaster::Class, 'employee_role_masters_id');
    }

    public function getUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getRank()
    {
        return $this->belongsTo(EmployeeRankMaster::Class, 'employee_rank_masters_id');
    }


    public function getAttendance()
    {
        return $this->hasMany(EmpAttendance::Class, 'employees_id');
    }

    public function getDeputationPtos()
    {
        return $this->hasMany(EmpDeputationPtos::Class, 'employees_id');
    }

    public function getHolidayDutie()
    {
        return $this->hasMany(EmpHolidayDutie::Class, 'employees_id');
    }

    public function getReporting()
    {
        return $this->hasMany(EmpReporting::Class, 'employees_id');
    }


    public static function uploadDir($url = null)
    {
        $folder = '/uploads/employee';
        if ($url == 'url') {
            return url($folder . '/');
        } else {
            return public_path($folder);
        }
    }

    public static function uploadImage($request, $input, $oldImage = null)
    {
        if ($request->hasFile($input)) {

            $file = $request->file($input);
            $uploadPath = self::uploadDir();
            $extension = $file->getClientOriginalExtension();
            $fileName = 'press_' . time() . '.' . $extension;
            $file->move($uploadPath, $fileName);

            if ($oldImage != null) {
                if ($oldImage != null && $fileName != '') {
                    File::delete($uploadPath . '/' . $oldImage);
                }
            }
            return $fileName;
        }
    }


    public static function add($request)
    {


        $request->validate(
            [
                'name' => 'required|unique:employees',
                'employee_id' => 'required|unique:employees',
            ],
            [
                'name.required' => 'Enter name',
                'name.unique' => 'This name  Exist',
                'employee_id.required' => 'Enter employee id',
                'employee_id.unique' => 'This employee id  Exist',

            ]
        );


        $requestData = $request->post();

//        dd( $requestData['date_appointment']);

        if (isset($requestData['date_appointment'])) {
            $requestData['date_appointment'] = date('Y-m-d H:i:s', strtotime($requestData['date_appointment']));
        }

        if (isset($requestData['retirement_date'])) {
            $requestData['retirement_date'] = date('Y-m-d H:i:s', strtotime($requestData['retirement_date']));
        }
        if (isset($requestData['licence_validity'])) {
            $requestData['licence_validity'] = date('Y-m-d H:i:s', strtotime($requestData['licence_validity']));
        }

//        dd($request);

        if ($request->hasFile('photo')) {
            $requestData['photo'] = self::uploadImage($request, 'photo', $request->photo);
        }

//        dd($requestData);

        $createData = self::create($requestData);

        $userdata = array();
        $userdata['name'] = $requestData['name'];
        $userdata['mobile_no'] = $requestData['mobile_number'];
        $userdata['login_role'] = $requestData['login_role'];
        $userdata['user_type'] = 2;
        $userdata['password'] = Hash::make(getRandomPassword());
        $userResultData = User::create($userdata);

        $getData = Employee::where('id', $createData->id)->first();

        $updateData = array();
        $updateData['user_id'] = $userResultData->id;

        $getData->update($updateData);


        return $createData;

    }

    public static function edit($request)
    {

        $requestData = $request->post();

        $request->validate(
            [
                'name' => 'required|unique:employees,name,' . $requestData['id'] . ',id',
                'employee_id' => 'required|unique:employees,employee_id,' . $requestData['id'] . ',id',
            ],
            [
                'name.required' => 'Enter name',
                'name.unique' => 'This name  Exist',
                'employee_id.required' => 'Enter employee id',
                'employee_id.unique' => 'This employee id  Exist',

            ]
        );


        $getData = Employee::where('id', $requestData['id'])->first();


        if (isset($requestData['date_appointment'])) {
            $requestData['date_appointment'] = date('Y-m-d H:i:s', strtotime($requestData['date_appointment']));
        } else {
            $requestData['date_appointment'] = null;
        }

        if (isset($requestData['retirement_date'])) {
            $requestData['retirement_date'] = date('Y-m-d H:i:s', strtotime($requestData['retirement_date']));
        } else {
            $requestData['retirement_date'] = null;
        }
        if (isset($requestData['licence_validity'])) {
            $requestData['licence_validity'] = date('Y-m-d H:i:s', strtotime($requestData['licence_validity']));
        } else {
            $requestData['licence_validity'] = null;
        }

        if ($request->hasFile('photo')) {
            $requestData['photo'] = self::uploadImage($request, 'photo', $request->photo);
        }

        $createData = $getData->update($requestData);


        $userRecord = User::where('id', $getData->user_id)->first();


        $userdata = array();
        $userdata['name'] = $requestData['name'];
        $userdata['mobile_no'] = $requestData['mobile_number'];
        $userdata['login_role'] = $requestData['login_role'];
        $userRecord->update($userdata);

        return $createData;

    }

}
