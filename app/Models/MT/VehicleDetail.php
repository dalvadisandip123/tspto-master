<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\VehicleBodyTypeMaster;
use App\Models\Masters\Mt\VehicleCategoryMaster;
use App\Models\Masters\Mt\VehicleGroupMaster;
use App\Models\Masters\Mt\VehicleMakeMaster;
use App\Models\Masters\Mt\VehicleTypeMaster;
use App\Models\Masters\Mt\VehicleUsageMaster;
use App\Models\Masters\Mt\VehicleVariantMaster;
use App\Models\Masters\Pol\FuelTypeMaster;
use App\Models\Masters\Pol\OilGradeMaster;
use App\Models\POL\FuelIssue;
use App\Models\POL\FuelQuotaAllotment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VehicleDetail extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'vehicle_make_id',
        'vehicle_type_id',
        'vehicle_variant_id',
        'vehicle_category_id',
        'vin_no',
        'vehicle_no',
        'tr_no',
        'fuel_type_id',
        'model',
        'engine_no',
        'chassis_no',
        'rc_validity_date',
        'group_id',
        'usage_id',
        'body_type_id',
        'tank_capacity',
        'kmpl',
        'reserve_fuel',
        'engine_oil_grade_id',
        'no_of_cylinders',
        'seating_capacity',
        'allottment_order_no',
        'allotment_date',
        'bhp_rpm',
        'opening_km',
        'piston_displacement',
        'current_meter_reading',
        'go_number',
        'go_date',
        'invoice_no',
        'vehicle_purchase_from',
        'dealer_name',
        'dealer_mobile_no',
        'po_number',
        'po_date',
        'delivery_date',
        'date_of_issue',
        'vehicle_cost',
        'tax_type',
        'tax_pecentage',
        'tax_paid_amount',
        'key_no',
        'no_of_tyres',
        'no_of_battery',
        'type_of_drive',
        'stock_ledger_number',
        'page_number',
        'sl_no',
        'vehicle_description',
        'status'
    ];


    public function getVehicleMake()
    {
        return $this->belongsTo(VehicleMakeMaster::Class, 'vehicle_make_id');
    }

    public function getVehicleType()
    {
        return $this->belongsTo(VehicleTypeMaster::Class, 'vehicle_type_id');
    }

    public function getVehicleVariant()
    {
        return $this->belongsTo(VehicleVariantMaster::Class, 'vehicle_variant_id');
    }

    public function getCategory()
    {
        return $this->belongsTo(VehicleCategoryMaster::Class, 'vehicle_category_id');
    }


    public function getFuelType()
    {
        return $this->belongsTo(FuelTypeMaster::Class, 'fuel_type_id');
    }

    public function getGroup()
    {
        return $this->belongsTo(VehicleGroupMaster::Class, 'group_id');
    }

    public function getUsage()
    {
        return $this->belongsTo(VehicleUsageMaster::Class, 'usage_id');
    }

    public function getBodyType()
    {
        return $this->belongsTo(VehicleBodyTypeMaster::Class, 'body_type_id');
    }

    public function getOilGrade()
    {
        return $this->belongsTo(OilGradeMaster::Class, 'engine_oil_grade_id');
    }


    public function getBatteryHistory()
    {
        return $this->hasMany(VehicleBatteryHistorie::Class, 'vehicle_detail_id');
    }

    public function geteTyreHistory()
    {
        return $this->hasMany(VehicleTyreHistorie::Class, 'vehicle_detail_id');
    }


    public function getFuelIssues()
    {
        return $this->hasMany(FuelIssue::Class, 'vehicle_number', 'vehicle_no');
    }

    public function getFuelIssuesWithinMonth()
    {
        return $this->hasMany(FuelIssue::Class, 'vehicle_number', 'vehicle_no')
            ->whereMonth('created_at', Carbon::now()->month);
    }

    public function getFuelIssuesWithinDay()
    {
        return $this->hasMany(FuelIssue::Class, 'vehicle_number', 'vehicle_no')
            ->whereDay('created_at', Carbon::now()->day);
    }

    public function getFuelQuotaAllotments($allotments_type = null)
    {

        if ($allotments_type == 'additional') {
            return $this->hasMany(FuelQuotaAllotment::Class, 'vehicle_number', 'vehicle_no')
                ->when($allotments_type, function ($query) use ($allotments_type) {
                    $query->where('allotments_type', $allotments_type);
                })
                ->whereMonth('created_at', Carbon::now()->month);
        } else {
            return $this->hasMany(FuelQuotaAllotment::Class, 'vehicle_number', 'vehicle_no')
                ->when($allotments_type, function ($query) use ($allotments_type) {
                    $query->where('allotments_type', $allotments_type);
                });
        }
    }


    public static function add($request)
    {

        $insertData = $request->post();

        $request->validate(
            [
                'vehicle_no' => 'required|unique:vehicle_details',
                'tr_no' => 'required|unique:vehicle_details',
            ],
            [
                'vehicle_no.required' => 'Enter vehicle_no',
                'vehicle_no.unique' => 'This vehicle_no  Exist',
                'tr_no.required' => 'Enter tr_no',
                'tr_no.unique' => 'This tr_no  Exist',

            ]
        );


        if (isset($insertData['rc_validity_date'])) {
            $insertData['rc_validity_date'] = date('Y-m-d H:i:s', strtotime($insertData['rc_validity_date']));
        } else {
            $insertData['rc_validity_date'] = null;
        }

        if (isset($insertData['allotment_date'])) {
            $insertData['allotment_date'] = date('Y-m-d H:i:s', strtotime($insertData['allotment_date']));
        } else {
            $insertData['allotment_date'] = null;
        }

        if (isset($insertData['go_date'])) {
            $insertData['go_date'] = date('Y-m-d H:i:s', strtotime($insertData['go_date']));
        } else {
            $insertData['go_date'] = null;
        }
        if (isset($insertData['po_date'])) {
            $insertData['po_date'] = date('Y-m-d H:i:s', strtotime($insertData['po_date']));
        } else {
            $insertData['po_date'] = null;
        }

        if (isset($insertData['delivery_date'])) {
            $insertData['delivery_date'] = date('Y-m-d H:i:s', strtotime($insertData['delivery_date']));
        } else {
            $insertData['delivery_date'] = null;
        }
        if (isset($insertData['date_of_issue'])) {
            $insertData['date_of_issue'] = date('Y-m-d H:i:s', strtotime($insertData['date_of_issue']));
        } else {
            $insertData['date_of_issue'] = null;
        }
//        dd($insertData);
        $vehicle = self::create($insertData);

        if (count($insertData['tyre']) > 0) {
            foreach ($insertData['tyre'] AS $tyre) {
//                dump($vehicle);
                VehicleTyreHistorie::add($tyre, $vehicle->id);
            }

        }

        if (count($insertData['battery']) > 0) {
            foreach ($insertData['battery'] AS $battery) {
                VehicleBatteryHistorie::add($battery, $vehicle->id);
            }

        }

//        dd($vehicle);
        return $vehicle;

    }

    public static function edit($request)
    {

        $updateData = $request->post();


        $request->validate(

            [
                'vehicle_no' => 'required|unique:vehicle_details,vehicle_no,' . $updateData['id'] . ',id',
                'tr_no' => 'required|unique:vehicle_details,tr_no,' . $updateData['id'] . ',id',
            ],


            [
                'vehicle_no.required' => 'Enter vehicle_no',
                'vehicle_no.unique' => 'This vehicle_no  Exist',
                'tr_no.required' => 'Enter tr_no',
                'tr_no.unique' => 'This tr_no  Exist',

            ]
        );


//        dd($updateData);

        if (isset($updateData['rc_validity_date'])) {
            $updateData['rc_validity_date'] = date('Y-m-d H:i:s', strtotime($updateData['rc_validity_date']));
        } else {
            $updateData['rc_validity_date'] = null;
        }

        if (isset($updateData['allotment_date'])) {
            $updateData['allotment_date'] = date('Y-m-d H:i:s', strtotime($updateData['allotment_date']));
        } else {
            $updateData['allotment_date'] = null;
        }

        if (isset($updateData['go_date'])) {
            $updateData['go_date'] = date('Y-m-d H:i:s', strtotime($updateData['go_date']));
        } else {
            $updateData['go_date'] = null;
        }
        if (isset($updateData['po_date'])) {
            $updateData['po_date'] = date('Y-m-d H:i:s', strtotime($updateData['po_date']));
        } else {
            $updateData['po_date'] = null;
        }

        if (isset($updateData['delivery_date'])) {
            $updateData['delivery_date'] = date('Y-m-d H:i:s', strtotime($updateData['delivery_date']));
        } else {
            $updateData['delivery_date'] = null;
        }
        if (isset($updateData['date_of_issue'])) {
            $updateData['date_of_issue'] = date('Y-m-d H:i:s', strtotime($updateData['date_of_issue']));
        } else {
            $updateData['date_of_issue'] = null;
        }
//        dd($insertData);

        $vehicle = self::where('id', $updateData['id'])->first();

//        dd($vehicle);
//        dd($updateData);

        $createData = $vehicle->update($updateData);

        if (isset($updateData['tyre']) && count($updateData['tyre']) > 0) {

            VehicleTyreHistorie::where('vehicle_detail_id', $vehicle->id)->delete();

            foreach ($updateData['tyre'] AS $tyre) {
                VehicleTyreHistorie::add($tyre, $vehicle->id);
            }
        }

        if (isset($updateData['battery']) && count($updateData['battery']) > 0) {
            VehicleBatteryHistorie::where('vehicle_detail_id', $vehicle->id)->delete();
            foreach ($updateData['battery'] AS $battery) {
                VehicleBatteryHistorie::add($battery, $vehicle->id);
            }

        }

//        dd($vehicle);
        return $vehicle;

    }


}
