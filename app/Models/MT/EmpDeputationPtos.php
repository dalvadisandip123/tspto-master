<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\UnitsMaster;
use Illuminate\Database\Eloquent\Model;

class EmpDeputationPtos extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'employees_id',
        'units_masters_id',
        'atached_date',
        'return_date',
        'status'
    ];

    public function getEmployee()
    {
        return $this->belongsTo(Employee::Class, 'employees_id');
    }


    public function getUnit()
    {
        return $this->belongsTo(UnitsMaster::Class, 'units_masters_id');
    }


}
