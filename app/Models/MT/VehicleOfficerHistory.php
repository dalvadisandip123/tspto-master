<?php

namespace App\Models\MT;

use Illuminate\Database\Eloquent\Model;

class VehicleOfficerHistory extends Model
{
    protected $table = 'vehicle_officer_historys';


    protected $fillable = [
        'vehicle_details_id',
        'unit_id',
        'officer_id',
        'attachment_order_number',
        'purpose',
        'from_km',
        'to_km',
        'issue_date',
        'end_date',
        'remarks',
        'status',
    ];

    public function getVehicleDetails()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_details_id');
    }

    public function getUnitInfo()
    {
        return $this->belongsTo('App\Models\Masters\Mt\UnitsMaster', 'unit_id');
    }
   public function getOfficerInfo()
    {
        return $this->belongsTo('App\User', 'officer_id');
    }
}
