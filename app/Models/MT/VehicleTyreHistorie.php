<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\VehicleTyresMaster;
use Illuminate\Database\Eloquent\Model;

class VehicleTyreHistorie extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'vehicle_detail_id',
        'tyre_make',
        'tyre_life_span',
        'tyre_size',
        'tyre_number',
        'cost',
        'start_km',
        'end_km',
        'tyre_issue_date',
        'tyre_end_date',
        'status',
    ];


    public static function add($tyreData, $vehicle_detail_id)
    {

//        $insertData = $request->post();


        if ($vehicle_detail_id != '') {

            if (isset($tyreData['tyre_issue_date'])) {
                $tyreData['tyre_issue_date'] = date('Y-m-d H:i:s', strtotime($tyreData['tyre_issue_date']));
            } else {
                $tyreData['tyre_issue_date'] = null;
            }

            if (isset($tyreData['tyre_end_date'])) {
                $tyreData['tyre_end_date'] = date('Y-m-d H:i:s', strtotime($tyreData['tyre_end_date']));
            } else {
                $tyreData['tyre_end_date'] = null;
            }

            $tyreData['vehicle_detail_id'] = $vehicle_detail_id;

//            $tyreData['tyre_number'] = $tyreData['tyre_number'];

//            dump($tyreData);


            return self::create($tyreData);
        }


//        dd($request);

    }


    public function getVehicleDetail()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_detail_id');
    }

    public function getTyreMake()
    {
        return $this->belongsTo(VehicleTyresMaster::Class, 'tyre_make' );
    }

}
