<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\OfficerNameMaster;
use App\Models\Masters\Mt\VehicleMakeMaster;
use App\Models\Masters\Mt\VehicleTypeMaster;
use Illuminate\Database\Eloquent\Model;

class VdraDailyDiary extends Model
{
    //
    protected $fillable = [
        'vehicle_no',
        'out_date',
        'date_return',
        'officer_id',
        'driver_id_1',
        'driver_id_2',
        'driver_id_3',
        'fuel_opening_bal',
        'fuel_total',
        'fuel_consumed',
        'fuel_bal',
        'journey_opening_km_reading',
        'journey_close_km_reading',
        'journey_km_present',
        'journey_km_present_month',
        'purpose_type',
        'purpose_text',
        'status'
    ];


    public function getOfficer()
    {
        return $this->belongsTo(OfficerNameMaster::Class, 'officer_id');
    }

    public function getVehicle()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_number', 'vehicle_no');
    }

    public function getDriver1()
    {
        return $this->belongsTo(Employee::Class, 'driver_id_1');
    }

    public function getDriver2()
    {
        return $this->belongsTo(Employee::Class, 'driver_id_2');
    }

    public function getDriver3()
    {
        return $this->belongsTo(Employee::Class, 'driver_id_3');
    }


    public static function add($request)
    {


//        $request->validate(
//            [
//                'name' => 'required|unique:employees',
//                'employee_id' => 'required|unique:employees',
//            ],
//            [
//                'name.required' => 'Enter name',
//                'name.unique' => 'This name  Exist',
//                'employee_id.required' => 'Enter employee id',
//                'employee_id.unique' => 'This employee id  Exist',
//
//            ]
//        );

        $requestData = $request->post();


//        dd($requestData);

//        dd( $requestData['date_appointment']);

        if (isset($requestData['out_date'])) {
            $requestData['out_date'] = date('Y-m-d H:i:s', strtotime($requestData['out_date']));
        }

        if (isset($requestData['date_return'])) {
            $requestData['date_return'] = date('Y-m-d H:i:s', strtotime($requestData['date_return']));
        }


        $createData = self::create($requestData);


        return $createData;

    }

    public static function edit($request)
    {

        $requestData = $request->post();

//        $request->validate(
//            [
//                'name' => 'required|unique:employees,name,' . $requestData['id'] . ',id',
//                'employee_id' => 'required|unique:employees,employee_id,' . $requestData['id'] . ',id',
//            ],
//            [
//                'name.required' => 'Enter name',
//                'name.unique' => 'This name  Exist',
//                'employee_id.required' => 'Enter employee id',
//                'employee_id.unique' => 'This employee id  Exist',
//
//            ]
//        );


        $getData = self::where('id', $requestData['id'])->first();


        if (isset($requestData['out_date'])) {
            $requestData['out_date'] = date('Y-m-d H:i:s', strtotime($requestData['out_date']));
        } else {
            $requestData['out_date'] = null;
        }

        if (isset($requestData['date_return'])) {
            $requestData['date_return'] = date('Y-m-d H:i:s', strtotime($requestData['date_return']));
        } else {
            $requestData['date_return'] = null;
        }


        $createData = $getData->update($requestData);


        return $createData;

    }
}
