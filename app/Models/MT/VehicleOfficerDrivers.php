<?php

namespace App\Models\MT;

use Illuminate\Database\Eloquent\Model;

class VehicleOfficerDrivers extends Model
{
    protected $table = 'vehicle_officer_driver_historys';


    protected $fillable = [
        'vehicle_details_id',
        'unit_id',
        'officer_id',
        'attachment_id',
        'diver_id',
        'from_date',
        'to_date',
        'start_km',
        'end_km',
        'remarks',
        'status',
    ];

    public function getVehicleDetails()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_details_id');
    }

    public function getUnitInfo()
    {
        return $this->belongsTo('App\Models\Masters\Mt\UnitsMaster', 'unit_id');
    }
}
