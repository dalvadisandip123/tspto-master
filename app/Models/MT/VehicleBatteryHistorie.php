<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\BatteriesMaster;
use Illuminate\Database\Eloquent\Model;

class VehicleBatteryHistorie extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'vehicle_detail_id',
        'battery_make',
        'battery_voltage',
        'battery_ampere',
        'battery_size',
        'battery_life_span',
        'battery_number',
        'battery_issue_date',
        'battery_end_date',
            'cost',
        'start_km',
        'end_km',
        'status'
    ];

//    public function getEmployee()
//    {
//        return $this->belongsTo(Employee::Class, 'employees_id');
//    }


    public static function add($bataryData, $vehicle_detail_id)
    {

//        $insertData = $request->post();


        if ($vehicle_detail_id != '') {
            if (isset($bataryData['battery_issue_date'])) {
                $bataryData['battery_issue_date'] = date('Y-m-d H:i:s', strtotime($bataryData['battery_issue_date']));
            } else {
                $bataryData['battery_issue_date'] = null;
            }

            if (isset($bataryData['battery_end_date'])) {
                $bataryData['battery_end_date'] = date('Y-m-d H:i:s', strtotime($bataryData['battery_end_date']));
            } else {
                $bataryData['battery_end_date'] = null;
            }


            $bataryData['vehicle_detail_id'] = $vehicle_detail_id;

//            dd($bataryData);
            return self::create($bataryData);
        }


    }

    public function getVehicleDetail()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_detail_id' );
    }

    public function getBatteryMake()
    {
        return $this->belongsTo(BatteriesMaster::Class, 'battery_make' );
    }




}
