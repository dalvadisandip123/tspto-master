<?php

namespace App\Models\MT;

use App\Models\Masters\Mt\EmployeeRoleMaster;
use Illuminate\Database\Eloquent\Model;

class EmpAttendance extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'employees_id',
        'employee_role_masters_id',
        'attendance',
        'attendance_date',
        'duty_type',
        'other_remarks',
        'status'
    ];

    public function getEmployee()
    {
        return $this->belongsTo(Employee::Class, 'employees_id');
    }

    public function getRole()
    {
        return $this->belongsTo(EmployeeRoleMaster::Class, 'employee_role_masters_id');
    }


    public static function add($request)
    {


//        $request->validate(
//            [
//                'name' => 'required|unique:employees',
//                'employee_id' => 'required|unique:employees',
//            ],
//            [
//                'name.required' => 'Enter name',
//                'name.unique' => 'This name  Exist',
//                'employee_id.required' => 'Enter employee id',
//                'employee_id.unique' => 'This employee id  Exist',
//
//            ]
//        );


        $requestData = $request->post();

//        dd( $requestData['date_appointment']);

        if (isset($requestData['attendance_date'])) {
            $requestData['attendance_date'] = date('Y-m-d H:i:s', strtotime($requestData['attendance_date']));
        }


        if (count($requestData['emp']) > 0) {
            foreach ($requestData['emp'] AS $emp) {

                $newattnd = array();
                $newattnd['employees_id'] = $emp['employees_id'];
                $newattnd['employee_role_masters_id'] = $requestData['role'];
                $newattnd['attendance'] = $emp['attendance'];
                $newattnd['attendance_date'] = $requestData['attendance_date'];


                $createData = self::create($newattnd);

            }
        }


        return true;

    }

    public static function edit($request)
    {

        $requestData = $request->post();

        $request->validate(
            [
                'name' => 'required|unique:employees,name,' . $requestData['id'] . ',id',
                'employee_id' => 'required|unique:employees,employee_id,' . $requestData['id'] . ',id',
            ],
            [
                'name.required' => 'Enter name',
                'name.unique' => 'This name  Exist',
                'employee_id.required' => 'Enter employee id',
                'employee_id.unique' => 'This employee id  Exist',

            ]
        );


        $getData = Employee::where('id', $requestData['id'])->first();


        if (isset($requestData['date_appointment'])) {
            $requestData['date_appointment'] = date('Y-m-d H:i:s', strtotime($requestData['date_appointment']));
        } else {
            $requestData['date_appointment'] = null;
        }

        if (isset($requestData['retirement_date'])) {
            $requestData['retirement_date'] = date('Y-m-d H:i:s', strtotime($requestData['retirement_date']));
        } else {
            $requestData['retirement_date'] = null;
        }
        if (isset($requestData['licence_validity'])) {
            $requestData['licence_validity'] = date('Y-m-d H:i:s', strtotime($requestData['licence_validity']));
        } else {
            $requestData['licence_validity'] = null;
        }

        if ($request->hasFile('photo')) {
            $requestData['photo'] = self::uploadImage($request, 'photo', $request->photo);
        }

        $createData = $getData->update($requestData);

        return $createData;

    }

}
