<?php

namespace App\Models\MT;

use Illuminate\Database\Eloquent\Model;

class EmpHolidayDutie extends Model
{
//    protected $primaryKey = 'id';
    protected $fillable = [
        'employees_id',
        'duty_date',
        'reason_for_duty',
        'place_of_duty',
        'time',
        'status'
    ];

    public function getEmployee()
    {
        return $this->belongsTo(Employee::Class, 'employees_id');
    }
}
