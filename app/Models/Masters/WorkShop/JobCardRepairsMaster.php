<?php

namespace App\Models\Masters\Workshop;

use Illuminate\Database\Eloquent\Model;

class JobCardRepairsMaster extends Model
{
//    protected $table = 'vehicle_usage_master';
//    protected $primaryKey = 'id';

    protected $fillable = [
        'problems',
        'job_card_repairs',
        'status'
    ];
}
