<?php

namespace App\Models\Masters\Workshop;

use Illuminate\Database\Eloquent\Model;

class FirmNameMaster extends Model
{

   protected $table = 'firm_name_masters';

   protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'status'
    ];
}
