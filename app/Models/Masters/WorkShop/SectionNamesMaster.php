<?php

namespace App\Models\Masters\Workshop;

use Illuminate\Database\Eloquent\Model;

class SectionNamesMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'status'
    ];
}
