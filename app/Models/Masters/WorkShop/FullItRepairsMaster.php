<?php

namespace App\Models\Masters\Workshop;

use Illuminate\Database\Eloquent\Model;

class FullItRepairsMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'problems',
        'full_it_repairs',
        'status'
    ];
}
