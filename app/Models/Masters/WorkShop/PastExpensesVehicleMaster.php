<?php

namespace App\Models\Masters\Workshop;

use Illuminate\Database\Eloquent\Model;

class PastExpensesVehicleMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'vehicle_number',
        'amount',
        'financial_year',
        'status'
    ];
}
