<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class OfficerDesignationMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'office_of_master_id',
        'status'
    ];



    public function getOffice()
    {
        return $this->belongsTo(OfficeOfMaster::Class, 'office_of_master_id');
    }


}
