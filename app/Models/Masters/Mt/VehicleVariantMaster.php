<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class VehicleVariantMaster extends Model
{

    protected $fillable = ['vehicle_make_masters_id','vehicle_type_masters_id','vehicle_variant','status'];

    public function getVehicleMake()
    {
        return $this->belongsTo(VehicleMakeMaster::Class, 'vehicle_make_masters_id');
    }
    public function getVehicleType()
    {
        return $this->belongsTo(VehicleTypeMaster::Class, 'vehicle_make_masters_id');
    }
}
