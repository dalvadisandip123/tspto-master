<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class EmployeeRoleMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'name',

        'status'
    ];
}
