<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class DescriptionByVehicleTypeMaster extends Model
{

//    protected $table = 'description_by_vehicle_master';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'status'
    ];
}
