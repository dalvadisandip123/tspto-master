<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class BatteriesAndTyresVariantMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'vehicle_make_id',
        'vehicle_type_id',
        'vehicle_variant_id',
        'vehicle_category_id',
        'life_span',
        'battery_size',
        'battery_amp',
        'battery_volt',
        'battery_name',
        'tyre_life_span',
        'tyre_front_size',
        'tyre_rear_size',

        'tyre_make',
        'tyre_name',
        'no_tyres',
        'yom',
        'status'
    ];

    public function getVehicleMake()
    {
        return $this->belongsTo(VehicleMakeMaster::Class, 'vehicle_make_id');
    }
    public function getVehicleType()
    {
        return $this->belongsTo(VehicleTypeMaster::Class, 'vehicle_type_id');
    }

    public function getVehicleVariant()
    {
        return $this->belongsTo(VehicleVariantMaster::Class, 'vehicle_variant_id');
    }

    public function getVehicleCategory()
    {
        return $this->belongsTo(VehicleCategoryMaster::Class, 'vehicle_category_id');
    }
}
