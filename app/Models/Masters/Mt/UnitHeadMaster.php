<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class UnitHeadMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'unit_id',
        'status'
    ];


    public function getUnit()
    {
        return $this->belongsTo(UnitsMaster::Class, 'unit_id');
    }

}
