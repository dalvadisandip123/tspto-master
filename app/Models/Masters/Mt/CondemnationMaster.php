<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class CondemnationMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

    protected $primaryKey = 'id';

    protected $fillable = [
        'years',
        'kms_reading',
        'mcondition',
        'status'
    ];
}
