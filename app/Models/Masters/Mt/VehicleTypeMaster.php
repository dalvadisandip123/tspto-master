<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class VehicleTypeMaster extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
//    protected $table = 'vehicletypes';

    /**
     * The database primary key value.
     *
     * @var string
     */
//    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['vehicle_type','vehicle_make_masters_id','status'];

    public function getVehicleMake()
    {
        return $this->belongsTo(VehicleMakeMaster::Class, 'vehicle_make_masters_id');
    }

    public function getVehicleVariant()
    {
        return $this->hasMany(VehicleVariantMaster::Class, 'vehicle_type_masters_id' );
    }

}
