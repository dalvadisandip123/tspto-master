<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class VehicleTyresMaster extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
//    protected $table = 'vehicletyres';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tyre_make','status'];
}
