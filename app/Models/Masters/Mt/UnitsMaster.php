<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class UnitsMaster extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
//    protected $table = 'units';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['unit_name', 'unit_code','status'];



    public function getUnitHead()
    {
        return $this->belongsTo(UnitHeadMaster::Class, 'id','unit_id');
    }
}
