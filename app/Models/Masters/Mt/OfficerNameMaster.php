<?php

namespace App\Models\Masters\Mt;

use Illuminate\Database\Eloquent\Model;

class OfficerNameMaster extends Model
{

//    protected $table = 'vehicle_usage_master';

//    protected $primaryKey = 'id';

    protected $fillable = [
        'office_of_master_id',
        'officer_designation_master_id',
        'officer_name',
        'officer_number',
        'user_id',
        'status'
    ];


    public function getOffice()
    {
        return $this->belongsTo(OfficeOfMaster::Class, 'office_of_master_id');
    }

    public function getUser()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getOfficeDesignation()
    {
        return $this->belongsTo(OfficerDesignationMaster::Class, 'officer_designation_master_id');
    }
}
