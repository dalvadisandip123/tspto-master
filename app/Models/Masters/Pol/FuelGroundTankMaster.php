<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class FuelGroundTankMaster extends Model
{
    protected $fillable = [
        'outlet_name',
        'fuel_type',
        'tank_capacity',
        'evaporation',
        'status'
    ];

    public function getOutletname()
    {
        return $this->belongsTo(OutletsMaster::Class, 'outlet_name');
    }
    public function getFuelType()
    {
        return $this->belongsTo(FuelTypeMaster::Class, 'fuel_type');
    }
}
