<?php

namespace App\Models\Masters\Pol;

use App\Models\Masters\Mt\UnitsMaster;
use App\Models\Masters\Mt\VehicleCategoryMaster;
use Illuminate\Database\Eloquent\Model;

class HiredVehicles extends Model
{
    protected $fillable = [
        'vehicle_number',
        'vehicle_make_type',
        'category_id',
        'unit_id',
        'fuel_type_id',
        'purpose',
        'quota_allotment',
        'status'
    ];

    public function getUnit()
    {
        return $this->belongsTo(UnitsMaster::Class, 'unit_id');
    }

    public function getCategory()
    {
        return $this->belongsTo(VehicleCategoryMaster::Class, 'category_id');
    }


    public function getFuelType()
    {
        return $this->belongsTo(FuelTypeMaster::Class, 'fuel_type_id');
    }


    public static function add($request)
    {

        $insertData = $request->post();

//        $request->validate(
//            [
//                'vehicle_no' => 'required|unique:vehicle_details',
//                'tr_no' => 'required|unique:vehicle_details',
//            ],
//            [
//                'vehicle_no.required' => 'Enter vehicle_no',
//                'vehicle_no.unique' => 'This vehicle_no  Exist',
//                'tr_no.required' => 'Enter tr_no',
//                'tr_no.unique' => 'This tr_no  Exist',
//
//            ]
//        );

//dd($insertData);

        $vehicle = self::create($insertData);

        return $vehicle;

    }

    public static function edit($request)
    {

        $updateData = $request->post();


//        $request->validate(
//
//            [
//                'vehicle_no' => 'required|unique:vehicle_details,vehicle_no,' . $updateData['id'] . ',id',
//                'tr_no' => 'required|unique:vehicle_details,tr_no,' . $updateData['id'] . ',id',
//            ],
//
//
//            [
//                'vehicle_no.required' => 'Enter vehicle_no',
//                'vehicle_no.unique' => 'This vehicle_no  Exist',
//                'tr_no.required' => 'Enter tr_no',
//                'tr_no.unique' => 'This tr_no  Exist',
//
//            ]
//        );


//        dd($updateData);


        $UpdateItem = self::where('id', $updateData['id'])->first();

//        dd($UpdateItem);
//        dd($updateData);

        $updateData = $UpdateItem->update($updateData);


        return $updateData;

    }

}
