<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class OilTypeMaster extends Model
{
    protected $fillable = [
        'name',
        'status'
    ];
}
