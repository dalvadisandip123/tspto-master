<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class LicenseCertificatesMaster extends Model
{
    protected $fillable = [
        'name',
        'intervel',
        'status'
    ];
}
