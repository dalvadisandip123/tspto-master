<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class OilGradeMaster extends Model
{
    protected $fillable = [
        'name',
        'status'
    ];
}
