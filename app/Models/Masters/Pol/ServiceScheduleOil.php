<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class ServiceScheduleOil extends Model
{
    protected $fillable = [

        'service_schedule_masters_id',
        'oil_type_masters_id',
        'first_servicing',
        'quantity',
        'oil_grade_masters_id',
        'interval'


    ];


    public function getServiceSchedule()
    {
        return $this->belongsTo(ServiceScheduleMaster::Class, 'service_schedule_masters_id');
    }

    public function getOilType()
    {
        return $this->belongsTo(OilTypeMaster::Class, 'oil_type_masters_id');
    }

    public function getGrade()
    {
        return $this->belongsTo(OilGradeMaster::Class, 'oil_grade_masters_id');
    }


}
