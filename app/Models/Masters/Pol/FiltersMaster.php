<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class FiltersMaster extends Model
{
    protected $fillable = [
        'name',
        'status'
    ];
}
