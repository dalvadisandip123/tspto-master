<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class OutletsMaster extends Model
{
    protected $fillable = [
        'name',
        'status'
    ];
}
