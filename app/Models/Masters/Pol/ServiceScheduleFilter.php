<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class ServiceScheduleFilter extends Model
{
    protected $fillable = [


        'service_schedule_masters_id',
        'filters_masters_id',
        'interval'


    ];


    public function getServiceSchedule()
    {
        return $this->belongsTo(ServiceScheduleMaster::Class, 'service_schedule_masters_id');
    }

    public function getFilter()
    {
        return $this->belongsTo(FiltersMaster::Class, 'filters_masters_id');
    }


}
