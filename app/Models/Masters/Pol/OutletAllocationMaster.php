<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class OutletAllocationMaster extends Model
{

    protected $fillable = [
        'user',
        'outlet',
        'status'
    ];

    public function getOutlet()
    {
        return $this->belongsTo(OutletsMaster::Class, 'outlet');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User', 'user');
    }
}
