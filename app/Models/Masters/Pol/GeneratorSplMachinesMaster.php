<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class GeneratorSplMachinesMaster extends Model
{
    protected $fillable = [
        'description',
        'fuel_type',
        'unit_name',
        'quota_allotted',
        'status'
    ];

    public function getFuelType()
    {
        return $this->belongsTo(FuelTypeMaster::Class, 'fuel_type');
    }
    public function getUnitInfo()
    {
        return $this->belongsTo('App\Models\Masters\Mt\UnitsMaster', 'unit_name');
    }
}
