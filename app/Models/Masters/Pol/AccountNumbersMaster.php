<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class AccountNumbersMaster extends Model
{
    protected $fillable = [
        'name',
        'status'
    ];
}
