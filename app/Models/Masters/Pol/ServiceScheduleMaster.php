<?php

namespace App\Models\Masters\Pol;

use Illuminate\Database\Eloquent\Model;

class ServiceScheduleMaster extends Model
{
    protected $fillable = [
        'vehicle_make_masters_id',
        'vehicle_type_masters_id',
        'vehicle_variant_masters_id',

    ];


    public function getVehicleMake()
    {
        return $this->belongsTo('App\Models\Masters\Mt\VehicleMakeMaster', 'vehicle_make_masters_id');
    }

    public function getVehicleType()
    {
        return $this->belongsTo('App\Models\Masters\Mt\VehicleTypeMaster', 'vehicle_type_masters_id');
    }

    public function getVehicleVariant()
    {
        return $this->belongsTo('App\Models\Masters\Mt\VehicleVariantMaster', 'vehicle_variant_masters_id');
    }


}
