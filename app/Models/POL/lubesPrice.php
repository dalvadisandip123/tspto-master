<?php

namespace App\Models\POL;

use App\Models\Masters\Pol\OilGradeMaster;
use Illuminate\Database\Eloquent\Model;

class lubesPrice extends Model
{
    protected $fillable = [
        'lubes_price_date',
        'lubes_type_id',
        'lubes_price',
        'status'
    ];

    public function getLubes()
    {
        return $this->belongsTo(OilGradeMaster::Class, 'lubes_type_id');
    }


    public static function add($request)
    {

        $insertData = $request->post();

//        dd($insertData);


        $OldItem = self::where('lubes_price_date', date('Y-m-d H:i:s', strtotime($insertData['lubes_price_date'])))->first();

//        dd($OldItem);


        if ($OldItem == '') {
            if (count($insertData['lubes_type']) > 0) {
                foreach ($insertData['lubes_type'] AS $fuel_type) {
                    $insertRecord = array();

                    if (isset($insertData['lubes_price_date'])) {
                        $insertRecord['lubes_price_date'] = date('Y-m-d H:i:s', strtotime($insertData['lubes_price_date']));
                    } else {
                        $insertRecord['lubes_price_date'] = null;
                    }

                    $insertRecord['lubes_type_id'] = $fuel_type['lubes_type_id'];
                    $insertRecord['lubes_price'] = $fuel_type['lubes_price'];

                    $vehicle = self::create($insertRecord);
                }

            }

            return true;

        }else{
            return false;
        }




    }
}
