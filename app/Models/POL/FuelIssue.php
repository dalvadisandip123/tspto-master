<?php

namespace App\Models\POL;

use App\Models\Masters\Pol\GeneratorSplMachinesMaster;
use App\Models\MT\VehicleDetail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FuelIssue extends Model
{
    protected $fillable = [
        'fuel_issues_for',
        'tx_no_outlet_code',
        'tx_no_date',
        'tx_no_day_sno',
        'issue_date',
        'indent_number_by_item',
        'generator_spl_id',
        'vehicle_number',
        'issue_liters',
        'fuel_type',
        'drown',
        'present_km',
        'rec_quota_regular',
        'rec_quota_addit',
        'rec_quota_enhan',
        'rec_quota_total',
        'rec_quota_balance',
        'status'
    ];

//    public function getMonthlyDrown()
//    {
//        return $this->hasMany(self::Class, 'vehicle_number', 'vehicle_number')
//            ->whereMonth('created_at', Carbon::now()->month);
//    }


    public function getGeneratorSpl()
    {
        return $this->belongsTo(GeneratorSplMachinesMaster::Class, 'generator_spl_id', 'vehicle_no');
    }

    public function getVehicle()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_number', 'vehicle_no');
    }


    static public function add($reqest){





        $insertData = $reqest->post();

        if (isset($insertData['issue_date'])) {
            $insertData['issue_date'] = date('Y-m-d H:i:s', strtotime($insertData['issue_date']));
        } else {
            $insertData['issue_date'] = null;
        }

        if (isset($insertData['tx_no_date'])) {
            $insertData['tx_no_date'] = date('Y-m-d H:i:s', strtotime($insertData['tx_no_date']));
        } else {
            $insertData['tx_no_date'] = null;
        }

//        dd($insertData);

      return  self::create($insertData);

    }


}
