<?php

namespace App\Models\POL;

use App\Models\MT\VehicleDetail;
use Illuminate\Database\Eloquent\Model;

class OutStationFilling extends Model
{
    protected $fillable = [
        'vehicle_number',
        'filling_date',
        'quantity',
        'office_incharge',
        'status'
    ];

    public function getVehicle()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_number', 'vehicle_no');
    }


    public static function add($request)
    {

        $requestData = $request->post();

//        dd($requestData);

//        $insertData =array();

        if (isset($requestData['filling_date'])) {
            $requestData['filling_date'] = date('Y-m-d H:i:s', strtotime($requestData['filling_date']));
        } else {
            $requestData['filling_date'] = null;
        }

        $requestData['vehicle_number']= $requestData['vehicle_no'];


        $vehicle = self::create($requestData);

        return $vehicle;



    }


//    public static function edit($request)
//    {
//
//        $insertData = $request->post();
//
////        dd($insertData);
//
//
//        $OldItem = self::where('lubes_price_date', date('Y-m-d H:i:s', strtotime($insertData['lubes_price_date'])))->first();
//
////        dd($OldItem);
//
//
//        if ($OldItem == '') {
//            if (count($insertData['lubes_type']) > 0) {
//                foreach ($insertData['lubes_type'] AS $fuel_type) {
//                    $insertRecord = array();
//
//                    if (isset($insertData['lubes_price_date'])) {
//                        $insertRecord['lubes_price_date'] = date('Y-m-d H:i:s', strtotime($insertData['lubes_price_date']));
//                    } else {
//                        $insertRecord['lubes_price_date'] = null;
//                    }
//
//                    $insertRecord['lubes_type_id'] = $fuel_type['lubes_type_id'];
//                    $insertRecord['lubes_price'] = $fuel_type['lubes_price'];
//
//                    $vehicle = self::create($insertRecord);
//                }
//
//            }
//
//            return true;
//
//        }else{
//            return false;
//        }
//
//
//
//
//    }

}
