<?php

namespace App\Models\POL;

use App\Models\Masters\Pol\FuelTypeMaster;
use App\Models\Masters\Pol\LicenseCertificatesMaster;
use App\Models\Masters\Pol\OutletsMaster;
use Illuminate\Database\Eloquent\Model;

class CertificateLicenseSchedule extends Model
{
    protected $fillable = [
        'license_certificates_id',
        'fuel_outlet_id',
        'fuel_type_id',
        'for_licence',
        'renewal_date',
        'status'
    ];

    public function getLicenseCertificates()
    {
        return $this->belongsTo(LicenseCertificatesMaster::Class, 'license_certificates_id');
    }


    public function getFuelType()
    {
        return $this->belongsTo(FuelTypeMaster::Class, 'fuel_type_id');
    }
    public function getOutlet()
    {
        return $this->belongsTo(OutletsMaster::Class, 'fuel_outlet_id');
    }


    public static function add($request)
    {

        $requestData = $request->post();

//        dd($requestData);

//        $insertData =array();

        if (isset($requestData['renewal_date'])) {
            $requestData['renewal_date'] = date('Y-m-d H:i:s', strtotime($requestData['renewal_date']));
        } else {
            $requestData['renewal_date'] = null;
        }

//        $requestData['vehicle_number']= $requestData['vehicle_no'];


        $vehicle = self::create($requestData);

        return $vehicle;

    }

    public static function edit($request)
    {

        $requestData = $request->post();


        $updateItem = self::where('id', $requestData['id'])->first();

//        dump($updateItem);
//        dd($requestData);

//        $insertData =array();

        if (isset($requestData['renewal_date'])) {
            $requestData['renewal_date'] = date('Y-m-d H:i:s', strtotime($requestData['renewal_date']));
        } else {
            $requestData['renewal_date'] = null;
        }

        $updateItem->update($requestData);


        return $updateItem;

    }

}
