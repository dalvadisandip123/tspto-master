<?php

namespace App\Models\POL;

use Illuminate\Database\Eloquent\Model;

class BudgetRegister extends Model
{
    protected $fillable = [
        'budget_type',
        'financial_year',
        'quarter',
        'id_number',
        'id_date',
        'budget',
        'total_amount',
        'budget_handed_over_to_govt',
        'budget_returned_from_govt',
        'status'
    ];


    public static function add($request)
    {

        $insertData = $request->post();

        $registerInsert = array();


        if (isset($insertData['purchase_date'])) {
            $registerInsert['purchase_date'] = date('Y-m-d H:i:s', strtotime($insertData['purchase_date']));
        } else {
            $registerInsert['purchase_date'] = null;
        }

        $registerInsert['financial_years'] = $insertData['financial_years'];
        $registerInsert['accout_number_id'] = $insertData['accout_number_id'];
        $registerInsert['invoice_number'] = $insertData['invoice_number'];
        $registerInsert['bill_amount'] = $insertData['bill_amount'];
        $registerInsert['credit_amount_IOCL'] = $insertData['credit_amount_IOCL'];
        $registerInsert['remarks'] = $insertData['remarks'];

//        dd($registerInsert);

        $purchaseOrder = self::create($registerInsert);


        return $purchaseOrder;

//        dump($purchaseOrder);
//        dd($registerInsert);
//        dd($insertData);


    }

    public static function edit($request)
    {

        $requestData = $request->post();

        $updateItem = self::where('id', $request->id)->first();

        if (!empty($updateItem)) {

            if (isset($requestData['purchase_date'])) {
                $registerUpdate['purchase_date'] = date('Y-m-d H:i:s', strtotime($requestData['purchase_date']));
            } else {
                $registerUpdate['purchase_date'] = null;
            }
            $registerUpdate['financial_years'] = $requestData['financial_years'];
            $registerUpdate['accout_number_id'] = $requestData['accout_number_id'];
            $registerUpdate['invoice_number'] = $requestData['invoice_number'];
            $registerUpdate['bill_amount'] = $requestData['bill_amount'];
            $registerUpdate['credit_amount_IOCL'] = $requestData['credit_amount_IOCL'];
            $registerUpdate['remarks'] = $requestData['remarks'];

            $updateItem->update($registerUpdate);

        }


        return $updateItem;

//        dump($purchaseOrder);
//        dd($registerInsert);
//        dd($insertData);


    }

    public static function remove($request)
    {

        $requestData = $request->post();

//        dd($requestData);

        $updateItem = self::where('id', $request['id'])->first();

        if (!empty($updateItem)) {

            self::where('id', $request->id)->delete();
        }


        return true;

    }

}
