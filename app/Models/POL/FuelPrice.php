<?php

namespace App\Models\POL;

use App\Models\Masters\Pol\FuelTypeMaster;
use Illuminate\Database\Eloquent\Model;

class FuelPrice extends Model
{
    protected $fillable = [
        'fuel_price_date',
        'fuel_type_id',
        'fuel_price',
        'status'
    ];

    public function getFuelType()
    {
        return $this->belongsTo(FuelTypeMaster::Class, 'fuel_type_id');
    }


    public static function add($request)
    {

        $insertData = $request->post();

//        dd($insertData);


        $OldItem = self::where('fuel_price_date', date('Y-m-d H:i:s', strtotime($insertData['fuel_price_date'])))->first();

//        dd($OldItem);


        if ($OldItem == '') {
            if (count($insertData['fuel_type']) > 0) {
                foreach ($insertData['fuel_type'] AS $fuel_type) {
                    $insertRecord = array();

                    if (isset($insertData['fuel_price_date'])) {
                        $insertRecord['fuel_price_date'] = date('Y-m-d H:i:s', strtotime($insertData['fuel_price_date']));
                    } else {
                        $insertRecord['fuel_price_date'] = null;
                    }

                    $insertRecord['fuel_type_id'] = $fuel_type['fuel_type_id'];
                    $insertRecord['fuel_price'] = $fuel_type['fuel_price'];

                    $vehicle = self::create($insertRecord);
                }

            }

            return true;

        }else{
            return false;
        }




    }

    public static function edit($request)
    {

        $updateData = $request->post();


        $UpdateItem = self::where('id', $updateData['id'])->first();

        $updateData = $UpdateItem->update($updateData);


        return $updateData;

    }
}
