<?php

namespace App\Models\POL;

use App\Models\MT\VehicleDetail;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FuelQuotaAllotment extends Model
{
    protected $fillable = [
        'allotments_type',
        'vehicle_number',
        'quota_liters',
        'allotment_user_id',
        'status'
    ];

    public function getVehicle()
    {
        return $this->belongsTo(VehicleDetail::Class, 'vehicle_number', 'vehicle_no');
    }






    public static function add($request)
    {


        $requestData = $request->post();


        if (count(array_filter($requestData['vehicle_nos']))) {
            $vehicle_nos = array_filter($requestData['vehicle_nos']);
            if (count($vehicle_nos) > 0) {
                foreach ($vehicle_nos AS $vehicle_no) {

                    $oldvehicle_number = self::where('vehicle_number', $vehicle_no)
                        ->where('allotments_type', $requestData['allotments_type'])
                        ->where('status', 1)
                        ->first();

                    if (!empty($oldvehicle_number)) {
                        $oldvehicle_numberUpdate = array();
                        $oldvehicle_numberUpdate['status'] = 2;

                        $oldvehicle_number->update($oldvehicle_numberUpdate);
                    }


                    $insertData = array();
                    $insertData['allotments_type'] = $requestData['allotments_type'];
                    $insertData['vehicle_number'] = $vehicle_no;
                    $insertData['quota_liters'] = $requestData['quota_liters'];
                    $insertData['allotment_user_id'] = Auth::id();
                    self::create($insertData);
//                    dd($insertData);
                }
            }

        }


//        dd($insertData);
    }

}
