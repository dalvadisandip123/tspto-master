<?php

namespace App\Models\POL\PurchaseRegister;

use App\Models\Masters\Pol\AccountNumbersMaster;
use Illuminate\Database\Eloquent\Model;

class PurchaseRegister extends Model
{
    protected $fillable = [
        'financial_years',
        'purchase_date',
        'accout_number_id',
        'invoice_number',
        'bill_amount',
        'credit_amount_IOCL',
        'remarks',
        'status'
    ];

    public function getAccount()
    {
        return $this->belongsTo(AccountNumbersMaster::Class, 'accout_number_id');
    }

    public function getFuels()
    {
        return $this->hasMany(PurchaseRegisterOutletsFuels::Class, 'purchase_registers_id');
    }

    public function getLubs()
    {
        return $this->hasMany(PurchaseRegisterOliLubes::Class, 'purchase_registers_id');
    }


    public static function add($request)
    {

        $insertData = $request->post();

        $registerInsert = array();


        if (isset($insertData['purchase_date'])) {
            $registerInsert['purchase_date'] = date('Y-m-d H:i:s', strtotime($insertData['purchase_date']));
        } else {
            $registerInsert['purchase_date'] = null;
        }

        $registerInsert['financial_years'] = $insertData['financial_years'];
        $registerInsert['accout_number_id'] = $insertData['accout_number_id'];
        $registerInsert['invoice_number'] = $insertData['invoice_number'];
        $registerInsert['bill_amount'] = $insertData['bill_amount'];
        $registerInsert['credit_amount_IOCL'] = $insertData['credit_amount_IOCL'];
        $registerInsert['remarks'] = $insertData['remarks'];

//        dd($registerInsert);

        $purchaseOrder = self::create($registerInsert);


        $outlets = $insertData['outlet'];
        $lubes = $insertData['lubes'];

        if (count($outlets) > 0) {
            foreach ($outlets AS $outlet_id => $outlet) {


                if (count($outlet) > 0) {
                    foreach ($outlet AS $fuel_id => $fuel) {

                        $fuelsInsert = array();
                        $fuelsInsert['purchase_registers_id'] = $purchaseOrder->id;
                        $fuelsInsert['outlet_id'] = $outlet_id;
                        $fuelsInsert['fuel_type_id'] = $fuel_id;
                        $fuelsInsert['fuel_price'] = $fuel['price'];

//                        dd($fuelsInsert);

                        PurchaseRegisterOutletsFuels::create($fuelsInsert);
                    }
                }


            }
        }


        if (count($lubes) > 0) {
            foreach ($lubes AS $lube_id => $lube) {

                $LubesInsert = array();
                $LubesInsert['purchase_registers_id'] = $purchaseOrder->id;
                $LubesInsert['lube_id'] = $lube_id;
                $LubesInsert['lube_price'] = $lube['price'];

                PurchaseRegisterOliLubes::create($LubesInsert);
            }
        }

        return $purchaseOrder;

//        dump($purchaseOrder);
//        dd($registerInsert);
//        dd($insertData);


    }

    public static function edit($request)
    {

        $requestData = $request->post();

        $updateItem = self::where('id', $request->id)->first();

        if (!empty($updateItem)) {

            if (isset($requestData['purchase_date'])) {
                $registerUpdate['purchase_date'] = date('Y-m-d H:i:s', strtotime($requestData['purchase_date']));
            } else {
                $registerUpdate['purchase_date'] = null;
            }
            $registerUpdate['financial_years'] = $requestData['financial_years'];
            $registerUpdate['accout_number_id'] = $requestData['accout_number_id'];
            $registerUpdate['invoice_number'] = $requestData['invoice_number'];
            $registerUpdate['bill_amount'] = $requestData['bill_amount'];
            $registerUpdate['credit_amount_IOCL'] = $requestData['credit_amount_IOCL'];
            $registerUpdate['remarks'] = $requestData['remarks'];

            $updateItem->update($registerUpdate);

        }

        $outlets = $requestData['outlet'];
        $lubes = $requestData['lubes'];

        if (count($outlets) > 0) {

            PurchaseRegisterOutletsFuels::where('purchase_registers_id', $updateItem->id)->delete();

            foreach ($outlets AS $outlet_id => $outlet) {


                if (count($outlet) > 0) {
                    foreach ($outlet AS $fuel_id => $fuel) {

                        $fuelsInsert = array();
                        $fuelsInsert['purchase_registers_id'] = $updateItem->id;
                        $fuelsInsert['outlet_id'] = $outlet_id;
                        $fuelsInsert['fuel_type_id'] = $fuel_id;
                        $fuelsInsert['fuel_price'] = $fuel['price'];

//                        dd($fuelsInsert);

                        PurchaseRegisterOutletsFuels::create($fuelsInsert);
                    }
                }


            }
        }


        if (count($lubes) > 0) {
            PurchaseRegisterOliLubes::where('purchase_registers_id', $updateItem->id)->delete();
            foreach ($lubes AS $lube_id => $lube) {

                $LubesInsert = array();
                $LubesInsert['purchase_registers_id'] = $updateItem->id;
                $LubesInsert['lube_id'] = $lube_id;
                $LubesInsert['lube_price'] = $lube['price'];

                PurchaseRegisterOliLubes::create($LubesInsert);
            }
        }

        return $updateItem;

//        dump($purchaseOrder);
//        dd($registerInsert);
//        dd($insertData);


    }

    public static function remove($request)
    {

        $requestData = $request->post();

//        dd($requestData);

        $updateItem = self::where('id', $request['id'])->first();

        PurchaseRegisterOliLubes::where('purchase_registers_id', $request['id'])->delete();
        PurchaseRegisterOutletsFuels::where('purchase_registers_id', $request['id'])->delete();

        self::where('id', $request->id)->delete();


        return $updateItem;

//        dump($purchaseOrder);
//        dd($registerInsert);
//        dd($insertData);


    }
}
