<?php

namespace App\Models\POL\PurchaseRegister;

use App\Models\Masters\Pol\OutletsMaster;
use Illuminate\Database\Eloquent\Model;

class PurchaseRegisterOutletsFuels extends Model
{

    protected $fillable = [
        'purchase_registers_id',
        'outlet_id',
        'fuel_type_id',
        'fuel_price'
    ];

    public function getOutlet()
    {
        return $this->belongsTo(OutletsMaster::Class, 'outlet_id');
    }
}
