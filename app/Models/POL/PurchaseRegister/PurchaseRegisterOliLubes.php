<?php

namespace App\Models\POL\PurchaseRegister;

use App\Models\Masters\Pol\OilGradeMaster;
use Illuminate\Database\Eloquent\Model;

class PurchaseRegisterOliLubes extends Model
{
    protected $fillable = [
        'purchase_registers_id',
        'lube_id',
        'lube_price',
    ];

    public function getLube()
    {
        return $this->belongsTo(OilGradeMaster::Class, 'lube_id');
    }
}
